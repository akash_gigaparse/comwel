<div class="sidebar-menu toggle-others fixed">
			
			<div class="sidebar-menu-inner">	
				
				<header class="logo-env">
					
					<!-- logo -->
					<div class="logo">
						<a href="ReportingDashboard.html" class="logo-expanded">
							<img src="assets/images/logo@2x.png" width="80" alt="" />
						</a>
						
						<a href="ReportingDashboard.html" class="logo-collapsed">
							<img src="assets/images/logo-collapsed@2x.png" width="40" alt="" />
						</a>
					</div>
					
					<!-- This will toggle the mobile menu and will be visible only on mobile devices -->
					<div class="mobile-menu-toggle visible-xs">
						<a href="#" data-toggle="user-info-menu">
							<i class="fa-bell-o"></i>
							<span class="badge badge-success">7</span>
						</a>
						
						<a href="#" data-toggle="mobile-menu">
							<i class="fa-bars"></i>
						</a>
					</div>
					
					<!-- This will open the popup with user profile settings, you can use for any purpose, just be creative -->
					<div class="settings-icon">
						<a href="#" data-toggle="settings-pane" data-animate="true">
							<i class="linecons-cog"></i>
						</a>
					</div>
					
								
				</header>
						
				
						
				<ul id="main-menu" class="main-menu">
					<!-- add class "multiple-expanded" to allow multiple submenus to open -->
					<!-- class "auto-inherit-active-class" will automatically add "active" class for parent elements who are marked already with class "active" -->
					<li>
						<a href="<?php echo base_url();?>admin">
							<i class="linecons-cog"></i>
							<span class="title">Dashboard</span>
						</a>
						
					</li>
					<li>
						<a href="<?php echo base_url();?>admin/register">
							<i class="linecons-desktop"></i>
							<span class="title">Register</span>
						</a>
						
					</li>
					<li>
						<a href="<?php echo base_url();?>admin/showList">
							<i class="linecons-note"></i>
							<span class="title">Employee List</span>
						</a>
						
					</li>
					<li class="has-sub">
						<a href="tables-basic.html">
							<i class="linecons-database"></i>
							<span class="title">Roles</span>
						</a>
						<ul style="display: none;">
							<li class="">
								<a href="<?php echo base_url();?>admin/createroles">
									<span class="title">Create</span>
								</a>
							</li>
							<li class="">
								<a href="<?php echo base_url();?>admin/assignroles">
									<span class="title">Roles Access</span>
								</a>
							</li>
							
						</ul>
					</li>
					<li class="has-sub">
						<a href="tables-basic.html">
							<i class="linecons-database"></i>
							<span class="title">Shop</span>
						</a>
						<ul style="display: none;">
							<li class="">
								<a href="<?php echo base_url();?>admin/createShop">
									<span class="title">Create</span>
								</a>
							</li>
							<li class="">
								<a href="<?php echo base_url();?>admin/ListShop">
									<span class="title">Shop List</span>
								</a>
							</li>
							
						</ul>
					</li>.
					<li>
						<a href="<?php echo base_url();?>admin/suspendList">
							<i class="linecons-cog"></i>
							<span class="title">Suspend User </span>
						</a>
						
					</li>
							
						</ul>
					</li>
					
				</ul>
						
			</div>
			
		</div>