<?php //echo '<pre>';print_r($postData);  
?>

		<div class="main-content">
		<?php $this->load->view('page_header');?>
			<div class="page-title">
				
				<div class="title-env">
					<h1 class="title">Tabs &amp; Accordions</h1>
					<p class="description">Tabs and accordions and their variants</p>
				</div>
				
					<div class="breadcrumb-env">
					
								<ol class="breadcrumb bc-1" >
									<li>
							<a href="ReportingDashboard.html"><i class="fa-home"></i>Home</a>
						</li>
								<li>
						
										<a href="ui-panels.html">UI Elements</a>
								</li>
							<li class="active">
						
										<strong>Tabs &amp; Accordions</strong>
								</li>
								</ol>
								
				</div>
					
			</div>
			<h3>Justified Tabs</h3>
			<br />
			
			<div class="row">
			
				<div class="col-md-12">
					
					<ul class="nav nav-tabs nav-tabs-justified">
						<li <?php if(!sset($_POST['tillid']) && !empty($_POST['tillid'])) echo "class=''";else echo "class='active'";?>>
							<a href="#edit-shop" data-toggle="tab">
								<span class="visible-xs"><i class="fa-home"></i></span>
								<span class="hidden-xs"> Edit Shop </span>
							</a>
						</li>
						<li <?php if(isset($_POST['tillid']) && !empty($_POST['tillid'])) echo "class=''"; ?>>
							<a href="#list-till" data-toggle="tab">
								<span class="visible-xs"><i class="fa-home"></i></span>
								<span class="hidden-xs"> Till List </span>
							</a>
						</li>
						<li <?php if(isset($_POST['tillid']) && !empty($_POST['tillid'])) echo "class='active'"; else echo "class=''";?> 
							<a href="#add-till" data-toggle="tab">
								<span class="visible-xs"><i class="fa-user"></i></span>
								<span class="hidden-xs">Add till</span>
							</a>
						</li>
					
						
					</ul>
					
					<div class="tab-content">
					<div <?php  if(isset($_POST['tillid']) && !empty($_POST['tillid'])) echo "class='tab-pane'"; else echo "class='tab-pane active'";?> id="edit-shop">
					
					</div>
						<div <?php  if(isset($_POST['tillid']) && !empty($_POST['tillid'])) echo "class='tab-pane'"; else echo "class='tab-pane active'";?> id="list-till">
							<div class="panel-body">
				<table class="table table-model-2 table-hover">
					<thead>
						<tr>
							<th>#</th>
							<th>Till ID </th>
							<th>Till Username</th>
							<th>Till Password</th>
							<th>Port Number</th>
							<th>IP Address</th>
							<th>Edit Till</th>
							<th>Delete Till</th>
						</tr>
					</thead>
					<tbody>
					<?php
//					echo '<pre>';print_r($this->session->userdata());die;
					foreach($value as $row){
			 		?>
						<tr>
							<td><?php echo $row->id;?></td>
							<td><?php //echo $row->fname ."&nbsp;".$row->lname;?></td>
							<td><?php //echo $row->email;?></td>
							<td><?php //echo $row->address;?></td>
							<td><a class="btn btn-success" role="button" href="<?php //echo base_url()?>Admin/editUser?id=<?php //echo $row->id;?>">Edit</a></td>
							<td><a id="<?php //echo $row->id;?>" class="btn btn-danger remove" values="<?php echo $row->id;?>" data-toggle="modal" role="button" href="#">Delete</a></td>
							<td><a id="<?php //echo $row->id;?>" class="btn btn-danger suspend" values="<?php echo $row->id;?>" data-toggle="modal" role="button" href="#">Suspend</a></td>
						
						</tr>
					<?php  }?>						
					</tbody>
				</table>
				
			</div>
							
						</div>

						<div <?php if(isset($_POST['tillid']) && !empty($_POST['tillid'])) echo "class='tab-pane active'"; else echo "class='tab-pane'";?> id="add-till">
						<!--start html for register-->
							<div class="panel-body">
								<form  action="<?php echo base_url();?>till" method="post" class="form-horizontal" role="form">
							
									<div class="form-group">
										<label for="field-1" class="col-sm-2 control-label">Till ID  *</label>
							
										<div class="col-sm-10">

											<input type="text" placeholder="Till ID" name="tillid" id="tillid" class="form-control" value="">

										</div>
							
										<?php echo form_error('email'); ?>
										<span id="email_error" class="error" style="display:none">Username Already Exists *</span>
									</div>
									
									<div class="form-group-separator"></div>
									
									<div class="form-group">
										<label for="field-2" class="col-sm-2 control-label">Till User Name *</label>
										<div class="col-sm-10">
											<input type="text" placeholder="Till User Name" name="tillUser" id="tillUser" class="form-control">
										</div>
										<?php echo form_error('password'); ?>
									</div>

									<div class="form-group-separator"></div>
									
									<div class="form-group">
										<label for="field-2" class="col-sm-2 control-label">Till Password *</label>
										<div class="col-sm-10">
											<input type="password" placeholder="Till Password" name="password" id="field-2" class="form-control">
										</div>
										<?php echo form_error('fname'); ?>
									</div>

									<div class="form-group-separator"></div>
									
									<div class="form-group">
										<label for="field-2" class="col-sm-2 control-label">Port Number</label>
										<div class="col-sm-10">
											<input type="text" placeholder="Port Number " name="port" id="port" class="form-control">
										</div>
											<?php echo form_error('lname'); ?>
									</div>

									<div class="form-group-separator"></div>
									
									<div class="form-group">
										<label for="field-2" class="col-sm-2 control-label">IP Address *</label>
										<div class="col-sm-10">
											<input type="text" placeholder="IP address" name="ipadd" id="ipadd" class="form-control">
										</div>
										<?php echo form_error('phone'); ?>
									</div>

									
									<div class="btn-group">					
										<input type="submit" name="submit" class="btn btn-success" value="Register">
									</div>
									
								</form>
								
								</div>
						</div>

					</div>
					
					
				</div>
			</div>
			
			
			<!-- Main Footer -->
			<!-- Choose between footer styles: "footer-type-1" or "footer-type-2" -->
			<!-- Add class "sticky" to  always stick the footer to the end of page (if page contents is small) -->
			<!-- Or class "fixed" to  always fix the footer to the end of page -->

		</div>
		
			
		<div id="chat" class="fixed"><!-- start: Chat Section -->
			
			<div class="chat-inner">
			
				
				<h2 class="chat-header">
					<a  href="#" class="chat-close" data-toggle="chat">
						<i class="fa-plus-circle rotate-45deg"></i>
					</a>
					
					Chat
					<span class="badge badge-success is-hidden">0</span>
				</h2>
				
				<script type="text/javascript">
				// Here is just a sample how to open chat conversation box
				jQuery(document).ready(function($)
				{
					var $chat_conversation = $(".chat-conversation");
					
					$(".chat-group a").on('click', function(ev)
					{
						ev.preventDefault();
						
						$chat_conversation.toggleClass('is-open');
						
						$(".chat-conversation textarea").trigger('autosize.resize').focus();
					});
					
					$(".conversation-close").on('click', function(ev)
					{
						ev.preventDefault();
						$chat_conversation.removeClass('is-open');
					});
				});
				</script>
				
				
				<div class="chat-group">
					<strong>Favorites</strong>
					
					<a href="#"><span class="user-status is-online"></span> <em>Catherine J. Watkins</em></a>
					<a href="#"><span class="user-status is-online"></span> <em>Nicholas R. Walker</em></a>
					<a href="#"><span class="user-status is-busy"></span> <em>Susan J. Best</em></a>
					<a href="#"><span class="user-status is-idle"></span> <em>Fernando G. Olson</em></a>
					<a href="#"><span class="user-status is-offline"></span> <em>Brandon S. Young</em></a>
				</div>
				
				
				<div class="chat-group">
					<strong>Work</strong>
					
					<a href="#"><span class="user-status is-busy"></span> <em>Rodrigo E. Lozano</em></a>
					<a href="#"><span class="user-status is-offline"></span> <em>Robert J. Garcia</em></a>
					<a href="#"><span class="user-status is-offline"></span> <em>Daniel A. Pena</em></a>
				</div>
				
				
				<div class="chat-group">
					<strong>Other</strong>
					
					<a href="#"><span class="user-status is-online"></span> <em>Dennis E. Johnson</em></a>
					<a href="#"><span class="user-status is-online"></span> <em>Stuart A. Shire</em></a>
					<a href="#"><span class="user-status is-online"></span> <em>Janet I. Matas</em></a>
					<a href="#"><span class="user-status is-online"></span> <em>Mindy A. Smith</em></a>
					<a href="#"><span class="user-status is-busy"></span> <em>Herman S. Foltz</em></a>
					<a href="#"><span class="user-status is-busy"></span> <em>Gregory E. Robie</em></a>
					<a href="#"><span class="user-status is-busy"></span> <em>Nellie T. Foreman</em></a>
					<a href="#"><span class="user-status is-busy"></span> <em>William R. Miller</em></a>
					<a href="#"><span class="user-status is-idle"></span> <em>Vivian J. Hall</em></a>
					<a href="#"><span class="user-status is-offline"></span> <em>Melinda A. Anderson</em></a>
					<a href="#"><span class="user-status is-offline"></span> <em>Gary M. Mooneyham</em></a>
					<a href="#"><span class="user-status is-offline"></span> <em>Robert C. Medina</em></a>
					<a href="#"><span class="user-status is-offline"></span> <em>Dylan C. Bernal</em></a>
					<a href="#"><span class="user-status is-offline"></span> <em>Marc P. Sanborn</em></a>
					<a href="#"><span class="user-status is-offline"></span> <em>Kenneth M. Rochester</em></a>
					<a href="#"><span class="user-status is-offline"></span> <em>Rachael D. Carpenter</em></a>
				</div>
			
			</div>
			
			<!-- conversation template -->
			<div class="chat-conversation">
				
				<div class="conversation-header">
					<a href="#" class="conversation-close">
						&times;
					</a>
					
					<span class="user-status is-online"></span>
					<span class="display-name">Arlind Nushi</span> 
					<small>Online</small>
				</div>
				
				<ul class="conversation-body">	
					<li>
						<span class="user">Arlind Nushi</span>
						<span class="time">09:00</span>
						<p>Are you here?</p>
					</li>
					<li class="odd">
						<span class="user">Brandon S. Young</span>
						<span class="time">09:25</span>
						<p>This message is pre-queued.</p>
					</li>
					<li>
						<span class="user">Brandon S. Young</span>
						<span class="time">09:26</span>
						<p>Whohoo!</p>
					</li>
					<li class="odd">
						<span class="user">Arlind Nushi</span>
						<span class="time">09:27</span>
						<p>Do you like it?</p>
					</li>
				</ul>
				
				<div class="chat-textarea">
					<textarea class="form-control autogrow" placeholder="Type your message"></textarea>
				</div>
				
			</div>
			
		<!-- end: Chat Section -->
		</div>
		
	</div>
	<!-- Bottom Scripts -->
	<script src="assets/js/bootstrap.min.js"></script>
	<script src="assets/js/TweenMax.min.js"></script>
	<script src="assets/js/resizeable.js"></script>
	<script src="assets/js/joinable.js"></script>
	<script src="assets/js/xenon-api.js"></script>
	<script src="assets/js/xenon-toggles.js"></script>


	<!-- JavaScripts initializations and stuff -->
	<script src="assets/js/xenon-custom.js"></script>
	<script>
//account parmanently remove functionality
jQuery(document).ready(function()
    {
	var remove_id;
	$(".remove").click(function(e)
	{
		if (confirm("Are you sure you want to Delete")) {
			remove_id=this.id;
			 var info = 'remove=' + remove_id;
			 $.ajax({
			     type: "POST",
			     url: "<?php echo base_url()?>/Admin/deleteUser",
			     data: info,
			     success: function(data)
				 {
				 	//alert("Recorde Delete successfully");
				     $("#"+remove_id+"").parent().parent().remove();
				 }
			});
		}
	});
	var suspend_id;
	$(".suspend").click(function(e)
	{
		if (confirm("Are you sure you want to Suspend")) {
			suspend_id=this.id;
			 var info = 'suspend=' + suspend_id;
			 $.ajax({
			     type: "POST",
			     url: "<?php echo base_url()?>/Admin/suspendUser",
			     data: info,
			     success: function(data)
				 {
				 	//alert("Recorde Delete successfully");
				     $("#"+suspend_id+"").parent().parent().remove();
				 }
			});
		}
	});
	var rejoin_id;
	$(".rejoin").click(function(e)
	{
		if (confirm("Are you sure you want to Rejoin Employee")) {
			rejoin_id=this.id;
			 var info = 'rejoin=' + rejoin_id;
			 $.ajax({
			     type: "POST",
			     url: "<?php echo base_url()?>/Admin/rejoinUser",
			     data: info,
			     success: function(data)
				 {
				     $("#"+rejoin_id+"").parent().parent().remove();
				 }
			});
		}
	});
});
</script>
<script type="text/javascript">
function myFunction(role,id)
{
	
	
    $.post( "<?php echo base_url()?>admin/assignroles", { id: id,role:role } );
}
</script>


</body>
</html>