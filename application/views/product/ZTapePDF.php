<?php 
if (!isset($_SESSION)) session_start();		
include("./class/Auth.php");	
include("./class/Terminal.php");
require_once './dompdf/dompdf_config.inc.php';
$Terminal = Terminal::getInstance();
$startDate = "05/01/2015";//date("m/d/Y");
$endDate = date("m/d/Y");

if(isset($_GET) && !empty($_GET) && isset($_GET['startDate']) && !empty($_GET['startDate']) && isset($_GET['endDate']) && !empty($_GET['endDate']) ){
	$startDate = isset($_GET['startDate'])?$_GET['startDate']:$startDate;
	$endDate = isset($_GET['endDate'])?$_GET['endDate']:$endDate;	
}
$post = array("ClientID" => Auth::getUsers()->clientid,'StartDate' => $startDate , 'EndDate' => $endDate , 'DepartMentID'=>Auth::getUsers()->deptid);

$Reports = $Terminal->get_ztape_reports($post);
$dompdf = new DOMPDF();

// Prepair HTML for PDF.

//Start Main Div
$html = '<div class="panel panel-default">';

//Display Title of PDF page
$html .= '<div class="row"><div class="col-md-12"><h3> Z Tape Report </h3></div></div>';

// Display Vendor Name
$html .= '<div class="row"><div class="col-md-12"><strong> Vendor:  </strong>';
if(isset(Auth::getUsers()->deptname) && !empty(Auth::getUsers()->deptname)){
	$html .= Auth::getUsers()->deptname;
} 
$html .= '</div></div>';

// Display Total Sales.
$html .= '<div class="row"><div class="col-md-12"><strong> Total Sales:  </strong>';
if(isset($Reports->TotalSales) && !empty($Reports->TotalSales)){
	$html .= $Reports->TotalSales; 
} 
$html .= '</div></div>';

//Display Start and End Date.
$html .= '<div class="row"><div class="col-md-12"><strong> Dates:  </strong>';
$html .= $startDate . '-' . $endDate; 
$html .= '</div></div>';

// Main div for inner data
$html .= '<div class="row"><div class="col-md-12">';
if(isset($Reports->Results) && !empty($Reports->Results)) {  
	foreach($Reports->Results as $terminalID => $Report) { 
		//Display Empty Row. 
		$html .= '<div class="row"><div class="col-md-12">&nbsp;</div></div>';

		//Display Terminal ID
		$html .= '<div class="row"><div class="col-md-12"><strong> Terminal ID:  </strong>';
		$html .= $terminalID . '</div></div>';

		$totalSale = 0.00;
		foreach($Report as $row){ 
			$totalSale +=  ((double)(str_replace("$","",$row->Total))); 
			$xml="<?xml version='1.0' encoding='utf-8'?><document>".$row->xmldetails."</document>";
			$array = json_decode(json_encode((array) simplexml_load_string($xml)),1);

			// Display Empty Row
			$html .= '<div class="row"><div class="col-md-12">&nbsp;</div></div>';

			//Display all inner data.
			$html .= '<div class="row"><div class="col-md-12"><span> Sequence:  </span>';
			$html .= $row->OrderID; 

			$html .= '<span> Trans ID:  </span>';
			$html .= $row->TransID;

			$html .= '<span> Dept: </span>';
			$html .= $row->DeptName; 

			$html .= '<span> User: </span>';
			$html .= $row->User; 

			$html .= '<span> Transaction Total: </span>';
			$html .= $row->Total; 

			$html .= '<span> TIPS: </span>';
			$html .= $row->Tips; 

			$html .= '<span> Date-Time: </span>';
			$html .= $row->Date; 
							
			$html .= '<div>	<span> Item: </span>';						
								
			$str = "";
			foreach($array as $itemsData){
				if(is_array($itemsData)){ 											
					$str .= $itemsData['qty']."-".$itemsData['itemname']." ("."$".number_format($itemsData['itemprice'],2)."), ";
				}
			} 
			$html .= "<span> ".rtrim(rtrim($str," "),",")."</span>";
			$html .= '</div></div></div>';
		} 
		$html .= '<br/>';

		//Display Total Sales for TerminalID.
		$html .= '<div class="row"><div class="col-md-12"><span> Total Sales for Terminal ID: </span>';
		$html .= "$".number_format($totalSale,2); 
		$html .= '</div></div>';
	} 
} 
$html .= '</div></div></div>';

//print_r($html);die;

  $dompdf->load_html($html);
  //$dompdf->set_paper($_POST["paper"], $_POST["orientation"]);
  $dompdf->render();
  $output = $dompdf->output();
  //file_put_contents('test.pdf', $output);
  $dompdf->stream("ztapepdf.pdf");
  exit(0);
 // header('Location: ZTape.php?download_pdf=yes');
?>