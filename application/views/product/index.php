<?php //echo '<pre>'; print_r($value); die;
$empShopIds=explode(',',$empshop->shop_ids);?>
<div class="main-content">
	<?php $this->load->view('admin/page_header');?>
	<div class="page-title">
		
		<div class="title-env">
			<h1 class="title">Product</h1>
		</div>
		<div class="breadcrumb-env">
			<ol class="breadcrumb bc-1" >
				<li>
					<a href="<?php echo base_url();?>product"><i class="fa-home"></i>Product</a>
				</li>
			</ol>
		</div>
			
	</div>
	<div>
		<ul class="nav nav-tabs nav-tabs-justified">
						
			<li class="active">
				<a href="#home-3" data-toggle="tab">
					<span class="visible-xs"><i class="fa-home"></i></span>
					<span class="hidden-xs"> Edit Product </span>
				</a>
			</li>
			<li>
				<a href="#profile-3" data-toggle="tab">
					<span class="visible-xs"><i class="fa-user"></i></span>
					<span class="hidden-xs">Add Wastage</span>
				</a>
			</li>
		
						
						
		</ul>
					
		<div class="tab-content">
					
			<div <?php if( (!empty($postData) && isset($postData) ) OR ( !empty($postRoll) && isset($postRoll) ) ) { echo 'class="tab-pane"'; } else{ echo 'class="tab-pane active"';} ?>class="tab-pane active" id="home-3">
				<div class="panel-body">
			<?php if($this->session->userdata('user_roll')==1){
				if(count($empShopIds)!=1){?>
						<form  action="" method="post"  role="form">
							<div class="form-group">
								<label for="field-1" class="col-sm-2 control-label">Shop Name </label>
								<div class="col-sm-10">

									<select class="form-control" name="shop">
											
										<?php foreach($shops as $shop){
												if(in_array($shop->id,$empShopIds))
												echo "<option value='$shop->id'>$shop->name</option>";
												
												}?>
									</select>
								</div>
									<br><br>
									<br><br>
									<label for="field-1" class="col-sm-2 control-label"></label>
									<div class="col-sm-9">
										<div class="input-group">
											<input type="submit" name="search" class="btn btn-success" value="Search">
										</div>
									</div>
								</div>
							</form>
										
									</br></br></br></br>	
					<?php }	}?>
						
							<?php
							if((!empty($shopRecord) && isset($shopRecord)))
							{ ?>
						<table class="table table-model-2 table-hover" id="rejointable">
							<thead>
								<tr>
									<th>#</th>
									<th>Name</th>
									<th>Class</th>
									<th>Cost</th>
									<th>Wholesale</th>
									<th>Retail</th>
									<th>Edit</th>
									
								</tr>
							</thead>
							<tbody>
						<?php 
		//					
								foreach($shopRecord as $row){
									//echo '<pre>';print_r($row);die;
									?>
									<tr>
										<td><?php echo $row->id;?></td>
										<td><?php echo $row->name;?></td>
										<td><?php echo $row->class;?></td>
										<td><?php echo $row->cost;?></td>
										<td><?php echo $row->wholesale;?></td>
										<td><?php echo $row->retail;?></td>
										<td><a class="btn btn-success" role="button" href="<?php echo base_url()?>product/editProduct?id=<?php echo $row->id;?>">Edit</a></td>


									</tr>
								<?php } 
							}
							if((!empty($value) && isset($value)))
							{ ?>
							<table class="table table-model-2 table-hover" id="rejointable">
							<thead>
								<tr>
									<th>#</th>
									<th>Name</th>
									<th>Class</th>
									<th>Cost</th>
									<th>Wholesale</th>
									<th>Retail</th>
									<th>Edit</th>
									
								</tr>
							</thead>
								<?php foreach($value as $row){
									//echo '<pre>';print_r($row);die;
									?>
									<tr>
										<td><?php echo $row->id;?></td>
										<td><?php echo $row->name;?></td>
										<td><?php echo $row->class;?></td>
										<td><?php echo $row->cost;?></td>
										<td><?php echo $row->wholesale;?></td>
										<td><?php echo $row->retail;?></td>
										<td><a class="btn btn-success" role="button" href="<?php echo base_url()?>product/editProduct?id=<?php echo $row->id;?>">Edit</a></td>


									</tr>
								<?php } 	
							}

							?>						
							</tbody>
						</table>
						</div>
					</div>
					
				
		<div class="tab-pane" id="profile-3">
			<div class="panel-body">

							<form  action="" method="post" class="form-horizontal" role="form">
							
								
									
									<?php 
										if($this->session->userdata('user_roll')==1){?>
									<div class="form-group">
										<label for="field-1" class="col-sm-2 control-label">Select Shop </label>
							
										<div class="col-sm-10">

											<select class="form-control" id="shop" name="shop_id">
										<option></option>
										<?php 
										
											foreach($shops as $shop){
												if(in_array($shop->id,$empShopIds))
													echo "<option value='$shop->id'>$shop->name</option>";
											
											}
										?>
									</select>
										</div>
							
									
									</div>
									<div class="form-group-separator"></div>
								<?php }
								else{
									  $shopID=$this->session->userdata('shop_id');
									echo "<input type='hidden' id='shop' name='shop_id' value='$shopID' /> ";
								}
								?>
										
									
									
									<div class="form-group">
										<div class="panel-body">
			

					
					<table  class="table table-striped table-bordered example-1" id="productTable" cellspacing="0" width="100%">
						<thead>
						<tr>
					<th width="2%"><input id="check_all" class="formcontrol" type="checkbox"/></th>
							<th>Product Name</th>
							<th>Quantity</th>
							<th>Notes for wastage</th>
							
						</tr>
					</thead>
					<tbody>
						<tr>
							<td><input class="case" type="checkbox"/></td>
							<input type="hidden"  name="pid[]" id="pid_1" value=""  autocomplete="off">
							<td><input type="text" data-type="productName" name="itemName[]" id="itemName_1" class="form-control autocomplete_txt2" autocomplete="off"></td>
							<td><input type="text" name="qty[]"  id="qty_1" class="form-control changesNo" autocomplete="off" ></td>
							<td><input type="text" name="notes[]"  id="notes_1" class="form-control changesNo" autocomplete="off" ></td>
						</tr>	
					</tbody>
					
					</table>
				
			</div>
									</div>
<div class="form-group-separator">
				</div>
									
									<div class="form-group">
									 	<div class='row'>
      		<div class='col-xs-12 col-sm-3 col-md-3 col-lg-3'>
      			<button class="btn btn-danger delete" type="button">- Delete</button>
      			<button class="btn btn-success addmore2" type="button">+ Add More</button>
      		</div>
      		
      	</div>
      	</div>

									<div class="form-group-separator"></div>
									
									
				
									<div class="btn-group">					
										<input type="submit" name="submit" id="submit" class="btn btn-success" value="Add wastage">
									</div>

									
								</form>


				

							</div>
		</div>
	</div>

	<!-- Main Footer -->
	<!-- Choose between footer styles: "footer-type-1" or "footer-type-2" -->
	<!-- Add class "sticky" to  always stick the footer to the end of page (if page contents is small) -->
	<!-- Or class "fixed" to  always fix the footer to the end of page -->

</div>

	<!-- Bottom Scripts -->
	
	
	</body>
</html>