<?php if (!isset($_SESSION)) session_start();		
include("./class/Auth.php");	
include("./class/Terminal.php");
$Terminal = Terminal::getInstance();
$startDate = date("m/d/Y");
$endDate = date("m/d/Y");
//echo "<pre>"; print_r(Auth::getUsers()->deptname);die;
if(isset($_POST) && !empty($_POST)){
	$startDate = isset($_POST['startDate'])?$_POST['startDate']:$startDate;
	$endDate = isset($_POST['endDate'])?$_POST['endDate']:$endDate;	
}
$post = array("ClientID" => Auth::getUsers()->clientid,'StartDate' => $startDate , 'EndDate' => $endDate , 'DepartMentID'=>Auth::getUsers()->deptid);

$Reports = $Terminal->get_ztape_reports($post);
//echo "<pre>"; print_r($Reports);die;
?>
<!DOCTYPE html>
<html lang="en">
<head>
	<?php include_once("head.php"); ?>	
	<script type="text/javascript">
	jQuery(document).ready(function($)
	{
		//alert(' yess');
		$('#pleaseWaitDialog').modal('hide');
	});
	function pleaseWaitDivShow(){
		$('#pleaseWaitDialog').modal('show');	
	}
	function pleaseWaitDivHide(){
		$('#pleaseWaitDialog').modal('hide');	
	}
</script>
</head>
<?php
if(isset($_REQUEST['download_pdf']) && $_REQUEST['download_pdf'] = 'yes'){
	header("Refresh:0");
	header('Location: ZTape2.php');
}
?>
<div class="page-loading-overlay">
	<div class="loader-2"></div>
</div>
<body class="page-body">

	
	
	<div class="page-container"><!-- add class "sidebar-collapsed" to close sidebar by default, "chat-visible" to make chat appear always -->
			
		<!-- Add "fixed" class to make the sidebar fixed always to the browser viewport. -->
		<!-- Adding class "toggle-others" will keep only one menu item open at a time. -->
		<!-- Adding class "collapsed" collapse sidebar root elements and show only icons. -->
		<div class="sidebar-menu toggle-others fixed">
			
			<div class="sidebar-menu-inner">	
				
				<header class="logo-env">
					
					<!-- logo -->
					<div class="logo">
						<a href="Dashboard.php" class="logo-expanded">
							<img src="assets/images/logo@2x.png" width="200" alt="" />
						</a>
						
						<a href="Dashboard.php" class="logo-collapsed">
							<img src="assets/images/logo-collapsed@2x.png" width="40" alt="" />
						</a>
					</div>
					
					<!-- This will toggle the mobile menu and will be visible only on mobile devices -->
					<div class="mobile-menu-toggle visible-xs">
						<a href="#" data-toggle="user-info-menu">
							<i class="fa-bell-o"></i>
							<span class="badge badge-success">7</span>
						</a>
						
						<a href="#" data-toggle="mobile-menu">
							<i class="fa-bars"></i>
						</a>
					</div>
					
					
					
								
				</header>					
				<?php include_once("left_menu.php"); ?>
    </div>
  </div>
		
		<div class="main-content">
					
			<?php include_once("nav.php"); ?>
            
 <!-- INSERT MAIN CONTENT HERE -->
			<div class="page-title">
      <div class="title-env col-sm-6">
        <h1 class="title">Generate Detailed Report</h1>
      </div>
          <div class="title-env col-sm-6">
    <h1 class="title text-info">Today's Date: <SCRIPT LANGUAGE="Javascript">
<!-- 

// Array of day names
var dayNames = new Array("Sunday","Monday","Tuesday","Wednesday",
				"Thursday","Friday","Saturday");

// Array of month Names
var monthNames = new Array(
"January","February","March","April","May","June","July",
"August","September","October","November","December");

var now = new Date();
document.write(dayNames[now.getDay()] + ", " + 
monthNames[now.getMonth()] + " " + 
now.getDate() + ", " + now.getFullYear());

// -->
</SCRIPT></h1>
    </div>
    </div>
			
			<div class="panel panel-default">
			
			<!-- Xenon Counter Widget -->
			<div class="row">
				<form method="post" id="dateRangeForm">
				<div class="col-md-3">
					
					<div class="form-group">				
			<input type="text" id="startDate" name="startDate" class="form-control datepicker" value="<?php echo $startDate; ?>" data-format="mm/dd/yyyy">
					</div>				
				</div>	
				<div class="col-md-3">
					
					<div class="form-group">				
			<input type="text" id="endDate" name="endDate" value="<?php echo $endDate; ?>" class="form-control datepicker" data-format="mm/dd/yyyy">
					</div>				
				</div>			
				<div class="col-md-6">
					
					<div class="form-group">				
						<button class="btn btn-info btn-lg">Submit</button>
						<button class="btn btn-info btn-lg">Download PDF</button>
					</div>				
				</div>
				</form>
			</div>

			

			<div class="row">
				<div class="col-md-12">					
					<h3> Detailed Report </h3>				
				</div>					
			</div>
			<div class="row">
				<div class="col-md-12">	
					<strong> Vendor:  </strong>
					<?php if(isset(Auth::getUsers()->deptname) && !empty(Auth::getUsers()->deptname)) echo Auth::getUsers()->deptname; ?>
				</div>

			</div>
			<div class="row">
				<div class="col-md-12">	
					<strong> Total Sales:  </strong>
					<?php if(isset($Reports->TotalSales) && !empty($Reports->TotalSales)) echo $Reports->TotalSales; ?>
				</div>
			
			</div>
			<div class="row">
				<div class="col-md-12">	
					<strong> Dates:  </strong>
					<?php echo $startDate; ?>
					-
					<?php echo $endDate; ?>
				</div>
			
			</div>
           		<div class="row">
				<div class="col-md-12">	
				<?php if(isset($Reports->Results) && !empty($Reports->Results)) {  foreach($Reports->Results as $terminalID => $Report) {  ?>
					<div class="row">
						<div class="col-md-12">	
							&nbsp;							
						</div>			
					</div>
					<div class="row">
						<div class="col-md-12">	
							<strong> Terminal ID:  </strong>
							<?php echo $terminalID; ?>
						</div>
					
					</div>

					<?php
					$totalSale = 0.00;
					foreach($Report as $row){ 
						$totalSale +=  ((double)(str_replace("$","",$row->Total))); 
						$xml="<?xml version='1.0' encoding='utf-8'?><document>".$row->xmldetails."</document>";
						$array = json_decode(json_encode((array) simplexml_load_string($xml)),1);

					?>
					<div class="row">
						<div class="col-md-12">	
							&nbsp;							
						</div>			
					</div>
					<div class="row">
						<div class="col-md-12">	
							<span> Sequence:  </span>
							<?php echo $row->OrderID; ?>

							<span> Trans ID:  </span>
							<?php echo $row->TransID; ?>

							<span> Dept: </span>
							<?php echo $row->DeptName; ?>

							<span> User: </span>
							<?php echo $row->User; ?>

							<span> Transaction Total: </span>
							<?php echo $row->Total; ?>

							<span> TIPS: </span>
							<?php echo $row->Tips; ?>
	
							<span> Date-Time: </span>
							<?php echo $row->Date; ?> 
							
							<div>
								<span> Item: </span>						
								<?php 
									$str = "";
									foreach($array as $itemsData){
										if(is_array($itemsData)){ //echo "<pre>"; print_r($itemsData);
											//foreach($itemsData as $itemRow){
						$str .= $itemsData['qty']."-".$itemsData['itemname']." ("."$".number_format($itemsData['itemprice'],2)."), ";
											//}
										}
									} 
									echo "<span> ".rtrim(rtrim($str," "),",")."</span>";?>
							</div>
						</div>
					
					</div>
					<?php } ?>
					<br/>
					<div class="row">
						<div class="col-md-12">
							<span> Total Sales for Terminal ID: </span>
							<?php echo "$".number_format($totalSale,2); ?>
						</div>
					</div>
					
				<?php } } ?>
				</div>
			</div>			
			</div>
			
<!-- END MAIN CONTENT HERE -->
			<!-- Main Footer -->
			<!-- Choose between footer styles: "footer-type-1" or "footer-type-2" -->
			<!-- Add class "sticky" to  always stick the footer to the end of page (if page contents is small) -->
			<!-- Or class "fixed" to  always fix the footer to the end of page -->
			<footer class="main-footer sticky footer-type-1">
				
				<div class="footer-inner">
				
					<!-- Add your copyright text here -->
					<div class="footer-text">
						
					</div>
					
					
					<!-- Go to Top Link, just add rel="go-top" to any link to add this functionality -->
					<div class="go-up">
					
						<a href="#" rel="go-top">
							<i class="fa-angle-up"></i>
						</a>
						
					</div>
					
				</div>
				
			</footer>
		</div>
		
			
		<div id="chat" class="fixed"><!-- start: Chat Section -->
			
			<div class="chat-inner">
			
				
				<h2 class="chat-header">
					<a  href="#" class="chat-close" data-toggle="chat">
						<i class="fa-plus-circle rotate-45deg"></i>
					</a>
					
					Chat
					<span class="badge badge-success is-hidden">0</span>
				</h2>
				
				<script type="text/javascript">
				// Here is just a sample how to open chat conversation box
				jQuery(document).ready(function($)
				{
					var $chat_conversation = $(".chat-conversation");
					
					$(".chat-group a").on('click', function(ev)
					{
						ev.preventDefault();
						
						$chat_conversation.toggleClass('is-open');
						
						$(".chat-conversation textarea").trigger('autosize.resize').focus();
					});
					
					$(".conversation-close").on('click', function(ev)
					{
						ev.preventDefault();
						$chat_conversation.removeClass('is-open');
					});
					$("button").click(function(){
						console.log($(this).text());
						if($(this).text()=="Submit"){ console.log("submit");
							$("#dateRangeForm").submit();
						}
						else if($(this).text()=="Download PDF"){
							pleaseWaitDivShow();
							//console.log(myApp);
							//myApp.showPleaseWait();
							var baseurl = window.location.origin+window.location.pathname;
							var newBaseUrl = baseurl.replace('ZTape.php','ZTapePDF.php');
							newBaseUrl += "?startDate="+$("#startDate").val()+"&endDate="+$("#endDate").val();
							window.location.href = newBaseUrl;
							//$(window).unload();
							return false;
						}
					});
				});
				</script>
				
				
				<div class="chat-group">
					<strong>Favorites</strong>
					
					<a href="#"><span class="user-status is-online"></span> <em>Catherine J. Watkins</em></a>
					<a href="#"><span class="user-status is-online"></span> <em>Nicholas R. Walker</em></a>
					<a href="#"><span class="user-status is-busy"></span> <em>Susan J. Best</em></a>
					<a href="#"><span class="user-status is-idle"></span> <em>Fernando G. Olson</em></a>
					<a href="#"><span class="user-status is-offline"></span> <em>Brandon S. Young</em></a>
				</div>
				
				
				<div class="chat-group">
					<strong>Work</strong>
					
					<a href="#"><span class="user-status is-busy"></span> <em>Rodrigo E. Lozano</em></a>
					<a href="#"><span class="user-status is-offline"></span> <em>Robert J. Garcia</em></a>
					<a href="#"><span class="user-status is-offline"></span> <em>Daniel A. Pena</em></a>
				</div>
				
				
				<div class="chat-group">
					<strong>Other</strong>
					
					<a href="#"><span class="user-status is-online"></span> <em>Dennis E. Johnson</em></a>
					<a href="#"><span class="user-status is-online"></span> <em>Stuart A. Shire</em></a>
					<a href="#"><span class="user-status is-online"></span> <em>Janet I. Matas</em></a>
					<a href="#"><span class="user-status is-online"></span> <em>Mindy A. Smith</em></a>
					<a href="#"><span class="user-status is-busy"></span> <em>Herman S. Foltz</em></a>
					<a href="#"><span class="user-status is-busy"></span> <em>Gregory E. Robie</em></a>
					<a href="#"><span class="user-status is-busy"></span> <em>Nellie T. Foreman</em></a>
					<a href="#"><span class="user-status is-busy"></span> <em>William R. Miller</em></a>
					<a href="#"><span class="user-status is-idle"></span> <em>Vivian J. Hall</em></a>
					<a href="#"><span class="user-status is-offline"></span> <em>Melinda A. Anderson</em></a>
					<a href="#"><span class="user-status is-offline"></span> <em>Gary M. Mooneyham</em></a>
					<a href="#"><span class="user-status is-offline"></span> <em>Robert C. Medina</em></a>
					<a href="#"><span class="user-status is-offline"></span> <em>Dylan C. Bernal</em></a>
					<a href="#"><span class="user-status is-offline"></span> <em>Marc P. Sanborn</em></a>
					<a href="#"><span class="user-status is-offline"></span> <em>Kenneth M. Rochester</em></a>
					<a href="#"><span class="user-status is-offline"></span> <em>Rachael D. Carpenter</em></a>
				</div>
			
			</div>
			
			<!-- conversation template -->
			<div class="chat-conversation">
				
				<div class="conversation-header">
					<a href="#" class="conversation-close">
						&times;
					</a>
					
					<span class="user-status is-online"></span>
					<span class="display-name">Arlind Nushi</span> 
					<small>Online</small>
				</div>
				
				<ul class="conversation-body">	
					<li>
						<span class="user">Arlind Nushi</span>
						<span class="time">09:00</span>
						<p>Are you here?</p>
					</li>
					<li class="odd">
						<span class="user">Brandon S. Young</span>
						<span class="time">09:25</span>
						<p>This message is pre-queued.</p>
					</li>
					<li>
						<span class="user">Brandon S. Young</span>
						<span class="time">09:26</span>
						<p>Whohoo!</p>
					</li>
					<li class="odd">
						<span class="user">Arlind Nushi</span>
						<span class="time">09:27</span>
						<p>Do you like it?</p>
					</li>
				</ul>
				
				<div class="chat-textarea">
					<textarea class="form-control autogrow" placeholder="Type your message"></textarea>
				</div>
				
			</div>
			
		<!-- end: Chat Section -->
		</div>
		
	</div>

<!-- Show pdf download processing-->
 <!-- Region Edit & Create Modal -->
    <div class="modal fade custom-width" id="pleaseWaitDialog">
        <div class="modal-dialog">
            <div class="modal-content">
 				<div class="modal-header">
            		<h1>Generating.. Please Wait...</h1>
        		</div>

                <div class="modal-body">
	                <div class="progress progress-striped active">
                		<div class="bar" style="width: 100%;"></div>
            		</div>
                </div>
            </div>
        </div>
    </div>

<link rel="stylesheet" href="assets/js/daterangepicker/daterangepicker-bs3.css">

	<!-- Bottom Scripts -->
	<script src="assets/js/bootstrap.min.js"></script>
	<script src="assets/js/TweenMax.min.js"></script>
	<script src="assets/js/resizeable.js"></script>
	<script src="assets/js/joinable.js"></script>
	<script src="assets/js/xenon-api.js"></script>
	<script src="assets/js/xenon-toggles.js"></script>
    
    <!-- Imported scripts on this page -->
	<script src="assets/js/xenon-widgets.js"></script>
	<script src="assets/js/jvectormap/jquery-jvectormap-1.2.2.min.js"></script>
	<script src="assets/js/jvectormap/regions/jquery-jvectormap-world-mill-en.js"></script>
<script src="assets/js/daterangepicker/daterangepicker.js"></script> 
<script src="assets/js/datepicker/bootstrap-datepicker.js"></script> 


	<!-- JavaScripts initializations and stuff -->
	<script src="assets/js/xenon-custom.js"></script>

</body>
</html>
