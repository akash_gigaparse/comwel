<div class="main-content">
<?php $this->load->view('admin/page_header');?>
	<div class="page-title">
		<div class="title-env">
			<h1 class="title">Edit Product</h1>
			<p class="description"></p>
		</div>
		<div class="breadcrumb-env">
			<ol class="breadcrumb bc-1" >
				<li>
					<a href="<?php echo base_url();?>product"><i class="fa-home"></i>Product</a>
				</li>
			</ol>
		</div>
	</div>
	<div class="row">
		<div class="col-sm-12">
			<div class="panel panel-default">
				<div class="panel-heading">
					<h3 class="panel-title"></h3>
					<div class="panel-options">
						<a data-toggle="panel" href="#">
							<span class="collapse-icon">&ndash;</span>
							<span class="expand-icon">+</span>
						</a>
						<a data-toggle="remove" href="#">
							×
						</a>
					</div>
				</div>
				<div class="panel-body">
					<form  action="<?php echo base_url();?>product/updatePorduct" method="post" class="form-horizontal" role="form">
							
						<div class="form-group">
							<label for="field-1" class="col-sm-2 control-label">Name *</label>
				
							<div class="col-sm-10">
								<input type="hidden" placeholder="" name="id" value="<?php if(!empty($postProductData)) { echo $postProductData['id']; } else {  echo $value[0]->id; } ?>" >

								<input type="text" placeholder="Name" name="name" id="name" class="form-control" value="<?php if(!empty($postProductData)) { echo $postProductData['name']; } else {  echo $value[0]->name; } ?>">

							</div>
				
							<?php echo form_error('name'); ?>
						</div>
						
						<div class="form-group-separator"></div>
						
						<div class="form-group">
							<label for="field-2" class="col-sm-2 control-label">Class *</label>
							<div class="col-sm-10">
								<input type="text" placeholder="Class" name="class" id="field-2" class="form-control" value="<?php if(!empty($postProductData)) { echo $postProductData['class']; } else {   echo $value[0]->class; } ?>">
							</div>
							<?php echo form_error('class'); ?>
						</div>

						<div class="form-group-separator"></div>
						
						<div class="form-group">
							<label for="field-2" class="col-sm-2 control-label">Cost *</label>
							<div class="col-sm-10">
								<input type="text" placeholder="Cost" name="cost" id="field-2" class="form-control" value="<?php if(!empty($postProductData)) {  echo $postProductData['cost']; } else {   echo $value[0]->cost; } ?>">
							</div>
							<?php echo form_error('cost'); ?>
						</div>
						

						<div class="form-group-separator"></div>

						
						<div class="form-group">
							<label for="field-2" class="col-sm-2 control-label">Wholesale *</label>
							<div class="col-sm-10">
								
								<input type="text" placeholder="Wholesale" name="wholesale" id="field-2" class="form-control" value="<?php if(!empty($postProductData)) { echo $postProductData['wholesale']; } else {   echo $value[0]->wholesale; } ?>">
							</div>
							<?php echo form_error('wholesale'); ?>
						</div>



						<div class="form-group-separator"></div>
						
						<div class="form-group">
							<label for="field-2" class="col-sm-2 control-label">Retail </label>
							<div class="col-sm-10">
								<input type="text" placeholder="Retail" name="retail" readonly id="field-2" class="form-control" value="<?php if(!empty($postProductData)) { echo $postProductData['retail']; } else {   echo $value[0]->retail; } ?>" >
							</div>
								<?php echo form_error('retail'); ?>
						</div>
												<div class="form-group-separator"></div>
						
						<div class="form-group">
							<label for="field-2" class="col-sm-2 control-label">Enable For search</label>
							<div class="col-sm-10">
							<p>
											<label class="radio-inline">
												<input type="radio" name="filter"  value="0" <?php if(!empty($postProductData)) { if($postProductData['filter']==0) echo "checked='checked'"; } else {   if($value[0]->filter==0) echo "checked='checked'";  } ?>>
												Enabled
											</label>
											<label class="radio-inline">
												<input type="radio" name="filter" value="1" <?php if(!empty($postProductData)) { if($postProductData['filter']==1) echo "checked='checked'"; } else {   if($value[0]->filter==1) echo "checked='checked'";  } ?>>
												
												Disabled
											</label>
								
										</p>
									</div>
								<?php // echo form_error('retail'); ?>
						</div>

						<div class="btn-group">					
							<input type="submit" name="submit" class="btn btn-success" value="Update Product">
						</div>
									
					</form>
				</div>

			</div>
		</div>  
	</div>
