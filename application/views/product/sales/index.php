<?php
//echo '<pre>';print_r($charts);die;
//$array = json_decode($chart[0]->item);
//echo '<pre>';print_r($array);
//die;
$empShopIds=explode(',',$empshop->shop_ids);?>

<div class="main-content">
<?php $this->load->view('employee/page_header');?>

	<div class="page-title">

	<div class="title-env">
		<h1 class="title">Sales Report</h1>
		<p class="description"></p>
	</div>
	<div class="breadcrumb-env">
		<ol class="breadcrumb bc-1" >
			<li>
				<a href="ReportingDashboard.html"><i class="fa-home"></i>Home</a>
			</li>
			<li>
				<a href="tables-basic.html">Tables</a>
			</li>
			<li class="active">
				<strong>Basic Tables</strong>
			</li>
		</ol>
					
	</div>
	
</div>
<div class="row">
	<div class="col-sm-12">
		<form  action="" method="post" class="form-horizontal" role="form">
		<div class="form-group">
			<label for="field-1" class="col-sm-2 control-label">User Name *</label>
			<div class="col-sm-10">
				<select class="form-control" name="shop_id">
				<option value="">Select Shop</option>
					
					<?php foreach($shops as $shop){ 
						if(in_array($shop->id,$empShopIds)){
						 echo "<option value='$shop->id'>$shop->name</option>";
						}
						} ?>
				</select>
			</div>
			<br><br>
			<label for="field-1" class="col-sm-2 control-label">To Date *</label>
			<div class="col-sm-9">
				<div class="input-group">
					<input type="text" class="form-control datepicker" data-format="yyyy-mm-dd" name="date1" required value="">
					
					<div class="input-group-addon">
						<a href="#"><i class="linecons-calendar"></i></a>
					</div>
				</div>
			</div>
			<br><br>
			<label for="field-1" class="col-sm-2 control-label">From Date *</label>
			<div class="col-sm-9">
				<div class="input-group">
					<input type="text" class="form-control datepicker" data-format="yyyy-mm-dd" name="date2" required value="">
					
					<div class="input-group-addon">
						<a href="#"><i class="linecons-calendar"></i></a>
					</div>
				</div>
			</div>
			<br><br>
			<label for="field-1" class="col-sm-2 control-label"></label>
			<div class="col-sm-9">
				<div class="input-group">
				<input type="submit" name="submit" class="btn btn-success" value="Search">
				</div>
			</div>
			</form>
		</div>
	</div>
</div>
<br><br><br>

<?php if((!empty($charts) && isset($charts)) || (!empty($getTemp) && isset($getTemp)))
{
	//echo $date['user'];
	//echo '<pre>';print_r($charts);die('asf');
?>
<div class="row">
	<div class="col-sm-12">
	
		<div class="panel panel-default">
			<div class="panel-heading">
				<h3 class="panel-title">Standard Bar</h3>
				<div class="panel-options">
					<a href="#" data-toggle="panel">
						<span class="collapse-icon">&ndash;</span>
						<span class="expand-icon">+</span>
					</a>
					<a href="#" data-toggle="remove">
						&times;
					</a>
				</div>
			</div>
			<div>
			
			</div>
			<?php //echo "<pre>";print_r($charts);die;?>
				<div class="panel-body">
				<script type="text/javascript">
								jQuery(document).ready(function($)
								{
									if( ! $.isFunction($.fn.dxChart))
										return;
										
									var dataSource = [
										<?php  foreach ($getTemp as $temp) {
										?>
									
										{ date: "<?php  if($datedif==0) echo $temp->hour; else echo $temp->date; ?>", avgT:<?php echo $temp->avgtem; ?>, minT: <?php echo $temp->mintem; ?>, maxT:<?php echo $temp->maxtem; ?>},
										<?php }?>
									];
									
									$("#bar-9").dxChart({
										dataSource: dataSource,
										commonSeriesSettings:{
											argumentField: "date"
										},
										panes: [{
												name: "topPane"
											}],
										defaultPane: "topPane",
										series: [{ 
												pane: "topPane",
												color: "#87CEEB",
												type: "rangeArea",
												rangeValue1Field: "minT",
												rangeValue2Field: "maxT",
												name: "Monthly Temperature Ranges, °C"
											}, {
												pane: "topPane", 
												valueField: "maxT",
												name: "Average Temperature, °C",
												label: {
													visible: true,
													customizeText: function (){
														return this.valueText + " °C";
													}
												},
												color: "#00b19d"
											}
										],	
										valueAxis: [{
											pane: "topPane",
											min: 0,
											max: 30,
											grid: {
												visible: true
											},
											title: {
												text: "Temperature, °C"
											}
										}],
										legend: {
											verticalAlignment: "bottom",
											horizontalAlignment: "center"
										},
										title: {
											text: "Weather in Glendale, CA"
										}
									});
								});
							</script>
							</script>
							<div id="bar-9" style="height: 450px; width: 100%;"></div>	
							<script type="text/javascript">
								jQuery(document).ready(function($)
								{
									if( ! $.isFunction($.fn.dxChart))
										return;
										
									var dataSource = [
								<?php foreach($charts as $chart){ ?>
										{ date: "<?php if($datedif==0) echo $chart->hour; else echo $chart->transaction_date;?>", gst:<?php echo  $total= $chart->totalSale-$chart->notGst ;?>, nongst:<?php echo $chart->notGst ;?> },
										<?php }?>
										
									];
									
									$("#bar-5").dxChart({
										rotated: true,
										pointSelectionMode: "multiple",
										dataSource: dataSource,
										commonSeriesSettings: {
											argumentField: "date",
											type: "stackedbar",
											selectionStyle: {
												hatching: {
													direction: "left"
												}
											}
										},
										series: [
											{ valueField: "gst", name: "Gst included", color: "#ffd700" },
											{ valueField: "nongst", name: "Gst not included", color: "#c0c0c0" }
										],
										title: {
											text: "Sales report"
										},
										legend: {
											verticalAlignment: "bottom",
											horizontalAlignment: "center"
										},
										pointClick: function(point) {
											point.isSelected() ? point.clearSelection() : point.select();
										}
									});
								});
							</script>
							<div id="bar-5" style="height: 450px; width: 100%;"></div>
							
						</div>
		</div>
			
	</div>
	<?php } ?>
</div>
<script type="text/javascript">
	jQuery(document).ready(function($)
	{
		if( ! $.isFunction($.fn.dxChart))
			$(".dx-warning").removeClass('hidden');
	});
</script>

<script>
	var xenonPalette = ['#68b828','#7c38bc','#0e62c7','#fcd036','#4fcdfc','#00b19d','#ff6264','#f7aa47'];
</script>
