<div class="main-content">
<?php $this->load->view('page_header');?>
	<div class="page-title">
		
		<div class="title-env">
			<h1 class="title">Product</h1>
		</div>
		
		<div class="breadcrumb-env">

			<ol class="breadcrumb bc-1" >
				<li>
					<a href="ReportingDashboard.html"><i class="fa-home"></i>Home</a>
				</li>
				<li>
					<a href="ui-panels.html">UI Elements</a>
				</li>
				<li class="active">
					<strong>Tabs &amp; Accordions</strong>
				</li>
			</ol>
					
		</div>
			
	</div>
		<div>
			<form name="form" method="post" action="<?php echo base_url();?>product/query">
			<input name="submit" value="Query Add" type="submit" class="btn btn-success" role="button">
			</form>
		</div>
	<div class="row">

		<div class="col-md-12">
			
			<ul class="nav nav-tabs nav-tabs-justified">
				<li  <?php if((!empty($postData) && isset($postData) ) OR (!empty($postRoll) && isset($postRoll)) ){ echo 'class=""'; } else{ echo 'class="active"';} ?>>
					<a href="#home-3" data-toggle="tab">
						<span class="visible-xs"><i class="fa-home"></i></span>
						<span class="hidden-xs"> Product List </span>
					</a>
				</li>
				<li  <?php if(!empty($postData) && isset($postData)){ echo 'class="active"'; } else{ echo 'class=""';} ?>>
					<a href="#profile-3" data-toggle="tab">
						<span class="visible-xs"><i class="fa-user"></i></span>
						<span class="hidden-xs">Add Product</span>
					</a>
				</li>
			<!--
				<li>
					<a href="#messages-3" data-toggle="tab">
						<span class="visible-xs"><i class="fa-envelope-o"></i></span>
						<span class="hidden-xs">Suspend User </span>
					</a>
				</li>
				<li  <?php if((!empty($postRoll) && isset($postRoll))) { echo 'class="active"'; } else{ echo 'class=""';} ?>>

					<a href="#settings-3" data-toggle="tab">
						<span class="visible-xs"><i class="fa-cog"></i></span>
						<span class="hidden-xs">Create Roles</span>
					</a>
				</li>
				<li>
					<a href="#inbox-3" data-toggle="tab">
						<span class="visible-xs"><i class="fa-bell-o"></i></span>
						<span class="hidden-xs">Roles Access</span>
					</a>
				</li>
				-->
			</ul>
			<div class="tab-content">
				<div <?php if( (!empty($postData) && isset($postData) ) OR ( !empty($postRoll) && isset($postRoll) ) ) { echo 'class="tab-pane"'; } else{ echo 'class="tab-pane active"';} ?>class="tab-pane active" id="home-3">
					<div class="panel-body">
						<table class="table table-model-2 table-hover" id="rejointable">
							<thead>
								<tr>
									<th>#</th>
									<th>Name</th>
									<th>Class</th>
									<th>Cost</th>
									<th>Wholesale</th>
									<th>Retail</th>
									<th>Edit</th>
									<th>Delete</th>
								</tr>
							</thead>
							<tbody>
							<?php

		//					echo '<pre>';print_r($this->session->userdata());die;
							foreach($value as $row){
								?>
								<tr>
									<td><?php echo $row->id;?></td>
									<td><?php echo $row->name;?></td>
									<td><?php echo $row->class;?></td>
									<td><?php echo $row->cost;?></td>
									<td><?php echo $row->wholesale;?></td>
									<td><?php echo $row->retail;?></td>
									<td><a class="btn btn-success" role="button" href="<?php echo base_url()?>product/editProduct?id=<?php echo $row->id;?>">Edit</a></td>
									<td><a id="<?php echo $row->id;?>" class="btn btn-danger removeProduct" values="<?php echo $row->id;?>" data-toggle="modal" role="button" href="#">Delete</a></td>

								</tr>
							<?php } ?>						
							</tbody>
						</table>
						
					</div>
					
				</div>

				<div 
				<?php if(!empty($postData) && isset($postData) ) {  echo 'class="tab-pane active"'; } else{ echo 'class="tab-pane"';} ?>
				id="profile-3">
				<!--start html for register-->
					<div class="panel-body">
								<form  action="<?php echo base_url();?>product/newPoduct" method="post" class="form-horizontal" role="form">
							
									<div class="form-group">
										<label for="field-1" class="col-sm-2 control-label">Name *</label>
							
										<div class="col-sm-10">

											<input type="text" placeholder="Name" name="name" id="name" class="form-control" value="<?php if(!empty($postData)) echo $postData['name'];?>">

										</div>
							
										<?php echo form_error('name'); ?>
										<span id="email_error" class="error" style="display:none"> </span>
									</div>
									
									<div class="form-group-separator"></div>
									
									<div class="form-group">
										<label for="field-2" class="col-sm-2 control-label">Class *</label>
										<div class="col-sm-10">
											<input type="text" placeholder="Class" name="class" id="field-2" class="form-control" value="<?php if(!empty($postData)) echo $postData['class'];?>">
										</div>
										<?php echo form_error('class'); ?>
									</div>

									<div class="form-group-separator"></div>
									
									<div class="form-group">
										<label for="field-2" class="col-sm-2 control-label">Cost *</label>
										<div class="col-sm-10">
											<input type="text" placeholder="Cost" name="cost" id="field-2" class="form-control" value="<?php if(!empty($postData)) echo $postData['cost'];?>">
										</div>
										<?php echo form_error('cost'); ?>
									</div>
									

									<div class="form-group-separator"></div>

									
									<div class="form-group">
										<label for="field-2" class="col-sm-2 control-label">Wholesale *</label>
										<div class="col-sm-10">
											
											<input type="text" placeholder="Wholesale" name="wholesale" id="field-2" class="form-control" value="<?php if(!empty($postData)) echo $postData['wholesale'];?>">
										</div>
										<?php echo form_error('wholesale'); ?>
									</div>



									<div class="form-group-separator"></div>
									
									<div class="form-group">
										<label for="field-2" class="col-sm-2 control-label">Retail *</label>
										<div class="col-sm-10">
											<input type="text" placeholder="Retail" name="retail" id="field-2" class="form-control" >
										</div>
											<?php echo form_error('retail'); ?>
									</div>
									<div class="btn-group">					
										<input type="submit" name="submit" class="btn btn-success" value="Add Product">
									</div>
									
								</form>
						
					</div>
				</div>

				<div  class="tab-pane" id="messages-3">
					<div class="panel-body">
					</div>
					
				</div>
				
				
			
				<div class="tab-pane" id="inbox-3">
					<div class="panel-body">
						
					
					</div>	
				
				</div>
			</div>
		</div>
	</div>
	<!-- Main Footer -->
	<!-- Choose between footer styles: "footer-type-1" or "footer-type-2" -->
	<!-- Add class "sticky" to  always stick the footer to the end of page (if page contents is small) -->
	<!-- Or class "fixed" to  always fix the footer to the end of page -->

</div>
	<div id="chat" class="fixed"><!-- start: Chat Section -->
			
		<div class="chat-inner">
		
			
			<h2 class="chat-header">
				<a  href="#" class="chat-close" data-toggle="chat">
					<i class="fa-plus-circle rotate-45deg"></i>
				</a>
				
				Chat
				<span class="badge badge-success is-hidden">0</span>
			</h2>
			
			<script type="text/javascript">
			// Here is just a sample how to open chat conversation box
			jQuery(document).ready(function($)
			{
				var $chat_conversation = $(".chat-conversation");
				
				$(".chat-group a").on('click', function(ev)
				{
					ev.preventDefault();
					
					$chat_conversation.toggleClass('is-open');
					
					$(".chat-conversation textarea").trigger('autosize.resize').focus();
				});
				
				$(".conversation-close").on('click', function(ev)
				{
					ev.preventDefault();
					$chat_conversation.removeClass('is-open');
				});
			});
			</script>
			
			
			<div class="chat-group">
				<strong>Favorites</strong>
				
				<a href="#"><span class="user-status is-online"></span> <em>Catherine J. Watkins</em></a>
				<a href="#"><span class="user-status is-online"></span> <em>Nicholas R. Walker</em></a>
				<a href="#"><span class="user-status is-busy"></span> <em>Susan J. Best</em></a>
				<a href="#"><span class="user-status is-idle"></span> <em>Fernando G. Olson</em></a>
				<a href="#"><span class="user-status is-offline"></span> <em>Brandon S. Young</em></a>
			</div>
			
			
			<div class="chat-group">
				<strong>Work</strong>
				
				<a href="#"><span class="user-status is-busy"></span> <em>Rodrigo E. Lozano</em></a>
				<a href="#"><span class="user-status is-offline"></span> <em>Robert J. Garcia</em></a>
				<a href="#"><span class="user-status is-offline"></span> <em>Daniel A. Pena</em></a>
			</div>
			
			
			<div class="chat-group">
				<strong>Other</strong>
				
				<a href="#"><span class="user-status is-online"></span> <em>Dennis E. Johnson</em></a>
				<a href="#"><span class="user-status is-online"></span> <em>Stuart A. Shire</em></a>
				<a href="#"><span class="user-status is-online"></span> <em>Janet I. Matas</em></a>
				<a href="#"><span class="user-status is-online"></span> <em>Mindy A. Smith</em></a>
				<a href="#"><span class="user-status is-busy"></span> <em>Herman S. Foltz</em></a>
				<a href="#"><span class="user-status is-busy"></span> <em>Gregory E. Robie</em></a>
				<a href="#"><span class="user-status is-busy"></span> <em>Nellie T. Foreman</em></a>
				<a href="#"><span class="user-status is-busy"></span> <em>William R. Miller</em></a>
				<a href="#"><span class="user-status is-idle"></span> <em>Vivian J. Hall</em></a>
				<a href="#"><span class="user-status is-offline"></span> <em>Melinda A. Anderson</em></a>
				<a href="#"><span class="user-status is-offline"></span> <em>Gary M. Mooneyham</em></a>
				<a href="#"><span class="user-status is-offline"></span> <em>Robert C. Medina</em></a>
				<a href="#"><span class="user-status is-offline"></span> <em>Dylan C. Bernal</em></a>
				<a href="#"><span class="user-status is-offline"></span> <em>Marc P. Sanborn</em></a>
				<a href="#"><span class="user-status is-offline"></span> <em>Kenneth M. Rochester</em></a>
				<a href="#"><span class="user-status is-offline"></span> <em>Rachael D. Carpenter</em></a>
			</div>
		
		</div>
	
		<!-- conversation template -->
		<div class="chat-conversation">
			
			<div class="conversation-header">
				<a href="#" class="conversation-close">
					&times;
				</a>
				
				<span class="user-status is-online"></span>
				<span class="display-name">Arlind Nushi</span> 
				<small>Online</small>
			</div>
			
			<ul class="conversation-body">	
				<li>
					<span class="user">Arlind Nushi</span>
					<span class="time">09:00</span>
					<p>Are you here?</p>
				</li>
				<li class="odd">
					<span class="user">Brandon S. Young</span>
					<span class="time">09:25</span>
					<p>This message is pre-queued.</p>
				</li>
				<li>
					<span class="user">Brandon S. Young</span>
					<span class="time">09:26</span>
					<p>Whohoo!</p>
				</li>
				<li class="odd">
					<span class="user">Arlind Nushi</span>
					<span class="time">09:27</span>
					<p>Do you like it?</p>
				</li>
			</ul>
			
			<div class="chat-textarea">
				<textarea class="form-control autogrow" placeholder="Type your message"></textarea>
			</div>
			
		</div>
	<!-- end: Chat Section -->
	</div>

</div>
	<!-- Bottom Scripts -->
	<script src="assets/js/bootstrap.min.js"></script>
	<script src="assets/js/TweenMax.min.js"></script>
	<script src="assets/js/resizeable.js"></script>
	<script src="assets/js/joinable.js"></script>
	<script src="assets/js/xenon-api.js"></script>
	<script src="assets/js/xenon-toggles.js"></script>


	<!-- JavaScripts initializations and stuff -->
	<script src="assets/js/xenon-custom.js"></script>
	
	</body>
</html>