<?php include("./class/Auth.php");
	include("./class/Terminal.php");
	include("./class/Employee.php");
	$employee = Employee::getInstance();
	$employees = array();
	if(isset($_POST) && !empty($_POST)){
		if(isset($_POST['controller']) && $_POST['controller'] == 'employee' && isset($_POST['action']) && $_POST['action'] == 'add'){
			$post = array('FirstName' => $_POST['firstname'],
			'LastName' => $_POST['lastname'],
			'PIN' => $_POST['pin'],
			'Wage' => $_POST['wage'],
			'PIN' => $_POST['pin'],
			'Address1' => $_POST['address1'],
			'City' => $_POST['city'],
			'State' => $_POST['state'],
			'Zip' => $_POST['zip'],
			'PhoneNumber' => $_POST['phonenumber'],
			'DeptID' => Auth::getUsers()->deptid,
			'ClientID' => Auth::getUsers()->clientid);
			$Employees = $employee->add($post);					
			if($response->Message=="Success"){
				$_SESSION['msg'] = "The Employee has been added successfully";
			}
			header("Location:Employees.php");
		}

		if(isset($_POST['controller']) && $_POST['controller'] == 'employee' && isset($_POST['action']) && $_POST['action'] == 'edit'){
			$post = array('ID' => $_POST['id'] ,
					'FirstName' => $_POST['firstname'] , 
					'LastName' => $_POST['lastname'] , 
					'Pin' => $_POST['pin'] , 
					'State' => $_POST['state'], 
					'City' => $_POST['city'] , 
					'Zip' => $_POST['zip'] , 
					'Wage' => $_POST['wage'] , 
					'Address1' => $_POST['address'], 
					'Phonenumber'=>$_POST['phonenumber'],
					'DeptID' => Auth::getUsers()->deptid,
					'ClientID' => Auth::getUsers()->clientid);
			$Employees = $employee->updat($post);					
			if($response->Message=="Success"){
				$_SESSION['msg'] = "The Employee has been updated successfully";
			}
			header("Location:Employees.php");
		}
		if(isset($_POST['controller']) && $_POST['controller'] == 'employee' && isset($_POST['action']) && $_POST['action'] == 'delete'){
			$post = array('EmployeeID' => $_POST['DeleteEmployeeID']);
			$response = $employee->delete($post);					
			header("Location:Employees.php");
		}
	}
	$Employees=$employee->fetch_all(array('DeptID' => Auth::getUsers()->deptid,'ClientID' => Auth::getUsers()->clientid));
//echo "<pre>"; print_r($Employees);die;
	
?>
<!DOCTYPE html>
<html lang="en">
<head>
	<?php include_once("head.php"); ?>	
</head>
<body class="page-body">

	<div class="settings-pane">
			
		<a href="#" data-toggle="settings-pane" data-animate="true">
			&times;
		</a>
		
		<div class="settings-pane-inner">
			
			<div class="row">
				
				<div class="col-md-4">
					
					<div class="user-info">
						
						<div class="user-image">
							<a href="extra-profile.html">
								<img src="assets/images/user-2.png" class="img-responsive img-circle" />
							</a>
						</div>
						
						<div class="user-details">
							
							<h3>
								<a href="extra-profile.html">John Smith</a>
								
								<!-- Available statuses: is-online, is-idle, is-busy and is-offline -->
								<span class="user-status is-online"></span>
							</h3>
							
							<p class="user-title">Web Developer</p>
							
							<div class="user-links">
								<a href="extra-profile.html" class="btn btn-primary">Edit Profile</a>
								<a href="extra-profile.html" class="btn btn-success">Upgrade</a>
							</div>
							
						</div>
						
					</div>
					
				</div>
				
				<div class="col-md-8 link-blocks-env">
					
					<div class="links-block left-sep">
						<h4>
							<span>Notifications</span>
						</h4>
						
						<ul class="list-unstyled">
							<li>
								<input type="checkbox" class="cbr cbr-primary" checked="checked" id="sp-chk1" />
								<label for="sp-chk1">Messages</label>
							</li>
							<li>
								<input type="checkbox" class="cbr cbr-primary" checked="checked" id="sp-chk2" />
								<label for="sp-chk2">Events</label>
							</li>
							<li>
								<input type="checkbox" class="cbr cbr-primary" checked="checked" id="sp-chk3" />
								<label for="sp-chk3">Updates</label>
							</li>
							<li>
								<input type="checkbox" class="cbr cbr-primary" checked="checked" id="sp-chk4" />
								<label for="sp-chk4">Server Uptime</label>
							</li>
						</ul>
					</div>
					
					<div class="links-block left-sep">
						<h4>
							<a href="#">
								<span>Help Desk</span>
							</a>
						</h4>
						
						<ul class="list-unstyled">
							<li>
								<a href="#">
									<i class="fa-angle-right"></i>
									Support Center
								</a>
							</li>
							<li>
								<a href="#">
									<i class="fa-angle-right"></i>
									Submit a Ticket
								</a>
							</li>
							<li>
								<a href="#">
									<i class="fa-angle-right"></i>
									Domains Protocol
								</a>
							</li>
							<li>
								<a href="#">
									<i class="fa-angle-right"></i>
									Terms of Service
								</a>
							</li>
						</ul>
					</div>
					
				</div>
				
			</div>
		
		</div>
		
	</div>
	
	<div class="page-container"><!-- add class "sidebar-collapsed" to close sidebar by default, "chat-visible" to make chat appear always -->
			
		<!-- Add "fixed" class to make the sidebar fixed always to the browser viewport. -->
		<!-- Adding class "toggle-others" will keep only one menu item open at a time. -->
		<!-- Adding class "collapsed" collapse sidebar root elements and show only icons. -->
		<div class="sidebar-menu toggle-others fixed">
			
			<div class="sidebar-menu-inner">	
				
				<header class="logo-env">
					
					<!-- logo -->
					<div class="logo">
						<a href="ReportingDashboard.php" class="logo-expanded">
							<img src="assets/images/logo@2x.png" width="80" alt="" />
						</a>
						
						<a href="ReportingDashboard.php" class="logo-collapsed">
							<img src="assets/images/logo-collapsed@2x.png" width="40" alt="" />
						</a>
					</div>
					
					<!-- This will toggle the mobile menu and will be visible only on mobile devices -->
					<div class="mobile-menu-toggle visible-xs">
						<a href="#" data-toggle="user-info-menu">
							<i class="fa-bell-o"></i>
							<span class="badge badge-success">7</span>
						</a>
						
						<a href="#" data-toggle="mobile-menu">
							<i class="fa-bars"></i>
						</a>
					</div>
								
				</header>
				<?php include_once("left_menu.php"); ?>				
			</div>
			
		</div>
		
		<div class="main-content">
					
			<nav class="navbar user-info-navbar" role="navigation"><!-- User Info, Notifications and Menu Bar -->
				
				<!-- Left links for user info navbar -->
				<ul class="user-info-menu left-links list-inline list-unstyled">
					
					<li class="hidden-sm hidden-xs">
						<a href="#" data-toggle="sidebar">
							<i class="fa-bars"></i>
						</a>
					</li>
					
					<li>
						<h3 style="margin-top:26px;">
							Manage Employees
						</h3>
					</li>
					
				</ul>


                <!-- Right links for user info navbar -->
                <ul class="user-info-menu right-links list-inline list-unstyled">

                    <li>
                        <a href="logout.php">
                            <i class="fa-lock"></i>
                            Logout
                        </a>
                    </li>

                </ul>
				
				
			</nav>
			<?php if(isset($_SESSION['msg']) && !empty($_SESSION['msg'])){ ?>
			<div class="dx-warning">
				<div>
					<p><?php echo $_SESSION['msg'];?></p>					
				</div>
			</div>
			<?php } 
			unset($_SESSION['msg']);
			?>

            <ul class="nav nav-tabs right-aligned"><!-- available classes "right-aligned" -->
                <li><a href="javascript:;" onclick="jQuery('#region-modal').modal('show');">
                    <span class="hidden-xs">Add Employee</span>
                </a>
                </li>
            </ul>

            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title">Employees</h3>

                    <div class="panel-options">
                        <a href="#" data-toggle="panel">
                            <span class="collapse-icon">&ndash;</span>
                            <span class="expand-icon">+</span>
                        </a>
                    </div>
                </div>
        
                <div class="panel-body">

                    <script type="text/javascript">
                        jQuery(document).ready(function($)
                        {

                        });
                    </script>

                    <table class="table table-bordered table-striped table-responsive" id="userTable">
                        <thead>
                        <tr>
                            <th>#</th>
                            <th>First Name</th>
                            <th>Last Name</th>
                           <!-- <th>PIN</th>
                            <th>Wage</th>
                            <th>Address</th>
                            <th>City</th>
                            <th>State</th>
                            <th>Zip</th>-->
                            <th>Percentage</th>
                            <th>Phone Number</th>
 							<th>&nbsp;</th>
                        </tr>
                        </thead>

                        <tbody class="middle-align">
                	 	<?php 
                	 	$employee=array();
                	 	if(isset($Employees->Employee) && !empty($Employees->Employee)){
                        	foreach ($Employees->Employee as $keys => $Employee) {

                        	//echo '<pre>';print_r($Employee);die;
                        	//foreach ($employees as $employee) { 
                        	?>	
                        	<tr>
                       		<?php //$i=1;foreach ($employee as $key=>$value) { 
                       			//if($i>11)
                        		//continue; ?> 
                    		<td><?php echo $Employee->EmpID;?></td>
			 	<td><?php echo $Employee->FirstName;?></td>
				<td><?php echo $Employee->LastName;?></td>
		
				<td><?php if(isset($Employee->Percentage) && !empty($Employee->Percentage)) { echo number_format($Employee->Percentage,2)."%"; } else { echo "0%"; }?></td>
				<td><?php echo $Employee->PhoneNumber;?></td>
				<?php  //$i++;} ?>                             
                            <td>
				<input type="hidden" id="PIN<?php echo $Employee->EmpID; ?>" value="<?php echo $Employee->PIN; ?>"/>
				<input type="hidden" id="Wage<?php echo $Employee->EmpID; ?>" value="<?php echo $Employee->Wage; ?>"/>
				<input type="hidden" id="Address1<?php echo $Employee->EmpID; ?>" value="<?php echo $Employee->Address1; ?>"/>
				<input type="hidden" id="City<?php echo $Employee->EmpID; ?>" value="<?php echo $Employee->City; ?>"/>
				<input type="hidden" id="State<?php echo $Employee->EmpID; ?>" value="<?php echo $Employee->State; ?>"/>
				<input type="hidden" id="Zip<?php echo $Employee->EmpID; ?>" value="<?php echo $Employee->Zip; ?>"/>
				<input type="hidden" id="Percentage<?php echo $Employee->EmpID; ?>" value="<?php echo $Employee->Percentage; ?>"/>
                                <a href="javascript:;" onclick="jQuery('#employee-modal-edit').modal('show');" class="btn btn-secondary btn-sm btn-icon icon-left">
                                    Edit
                                </a>
				<a href="javascript:;" onclick="jQuery('#employee-modal-delete').modal('show');" class="btn btn-danger btn-icon"><i class="icon-white icon-heart"></i> Delete</a>
                                <!--<a href="#" class="btn btn-danger btn-sm btn-icon icon-left">
                                    Delete
                                </a>-->
                            </td>
                        </tr>
                        <?php    } }?>

                      <!--  <tr>
                            <td>2</td>
                            <td>Latin America</td>
                            <td>
                                <a href="javascript:;" onclick="jQuery('#region-modal-edit').modal('show');" class="btn btn-secondary btn-sm btn-icon icon-left">
                                    Edit
                                </a>
                                <a href="#" class="btn btn-danger btn-sm btn-icon icon-left">
                                    Delete
                                </a>
                            </td>
                        </tr>

                        <tr>
                            <td>3</td>
                            <td>Asia Pacific</td>
                            <td>
                                <a href="javascript:;" onclick="jQuery('#region-modal-edit').modal('show');" class="btn btn-secondary btn-sm btn-icon icon-left">
                                    Edit
                                </a>
                                <a href="#" class="btn btn-danger btn-sm btn-icon icon-left">
                                    Delete
                                </a>
                            </td>
                        </tr> -->

                        </tbody>
                    </table>

                </div>
                 
            </div>

			<!-- Main Footer -->
			<!-- Choose between footer styles: "footer-type-1" or "footer-type-2" -->
			<!-- Add class "sticky" to  always stick the footer to the end of page (if page contents is small) -->
			<!-- Or class "fixed" to  always fix the footer to the end of page -->
			<footer class="main-footer sticky footer-type-1">

				<div class="footer-inner">

					<!-- Add your copyright text here -->
					<div class="footer-text">
						&copy; 2015
						$<strong>CASHLESS inc.</strong>
					</div>


					<!-- Go to Top Link, just add rel="go-top" to any link to add this functionality -->
					<div class="go-up">

						<a href="#" rel="go-top">
							<i class="fa-angle-up"></i>
						</a>

					</div>

				</div>

			</footer>
		</div>

			
		<div id="chat" class="fixed"><!-- start: Chat Section -->
			
			<div class="chat-inner">
			
				
				<h2 class="chat-header">
					<a  href="#" class="chat-close" data-toggle="chat">
						<i class="fa-plus-circle rotate-45deg"></i>
					</a>
					
					Chat
					<span class="badge badge-success is-hidden">0</span>
				</h2>
				
				<script type="text/javascript">
				// Here is just a sample how to open chat conversation box
				jQuery(document).ready(function($)
				{
					var $chat_conversation = $(".chat-conversation");
					
					$(".chat-group a").on('click', function(ev)
					{
						ev.preventDefault();
						
						$chat_conversation.toggleClass('is-open');
						
						$(".chat-conversation textarea").trigger('autosize.resize').focus();
					});
					
					$(".conversation-close").on('click', function(ev)
					{
						ev.preventDefault();
						$chat_conversation.removeClass('is-open');
					});
				});
				</script>
				
				
				<div class="chat-group">
					<strong>Favorites</strong>
					
					<a href="#"><span class="user-status is-online"></span> <em>Catherine J. Watkins</em></a>
					<a href="#"><span class="user-status is-online"></span> <em>Nicholas R. Walker</em></a>
					<a href="#"><span class="user-status is-busy"></span> <em>Susan J. Best</em></a>
					<a href="#"><span class="user-status is-idle"></span> <em>Fernando G. Olson</em></a>
					<a href="#"><span class="user-status is-offline"></span> <em>Brandon S. Young</em></a>
				</div>
				
				
				<div class="chat-group">
					<strong>Work</strong>
					
					<a href="#"><span class="user-status is-busy"></span> <em>Rodrigo E. Lozano</em></a>
					<a href="#"><span class="user-status is-offline"></span> <em>Robert J. Garcia</em></a>
					<a href="#"><span class="user-status is-offline"></span> <em>Daniel A. Pena</em></a>
				</div>
				
				
				<div class="chat-group">
					<strong>Other</strong>
					
					<a href="#"><span class="user-status is-online"></span> <em>Dennis E. Johnson</em></a>
					<a href="#"><span class="user-status is-online"></span> <em>Stuart A. Shire</em></a>
					<a href="#"><span class="user-status is-online"></span> <em>Janet I. Matas</em></a>
					<a href="#"><span class="user-status is-online"></span> <em>Mindy A. Smith</em></a>
					<a href="#"><span class="user-status is-busy"></span> <em>Herman S. Foltz</em></a>
					<a href="#"><span class="user-status is-busy"></span> <em>Gregory E. Robie</em></a>
					<a href="#"><span class="user-status is-busy"></span> <em>Nellie T. Foreman</em></a>
					<a href="#"><span class="user-status is-busy"></span> <em>William R. Miller</em></a>
					<a href="#"><span class="user-status is-idle"></span> <em>Vivian J. Hall</em></a>
					<a href="#"><span class="user-status is-offline"></span> <em>Melinda A. Anderson</em></a>
					<a href="#"><span class="user-status is-offline"></span> <em>Gary M. Mooneyham</em></a>
					<a href="#"><span class="user-status is-offline"></span> <em>Robert C. Medina</em></a>
					<a href="#"><span class="user-status is-offline"></span> <em>Dylan C. Bernal</em></a>
					<a href="#"><span class="user-status is-offline"></span> <em>Marc P. Sanborn</em></a>
					<a href="#"><span class="user-status is-offline"></span> <em>Kenneth M. Rochester</em></a>
					<a href="#"><span class="user-status is-offline"></span> <em>Rachael D. Carpenter</em></a>
				</div>
			
			</div>
			
			<!-- conversation template -->
			<div class="chat-conversation">
				
				<div class="conversation-header">
					<a href="#" class="conversation-close">
						&times;
					</a>
					
					<span class="user-status is-online"></span>
					<span class="display-name">Arlind Nushi</span> 
					<small>Online</small>
				</div>
				
				<ul class="conversation-body">	
					<li>
						<span class="user">Arlind Nushi</span>
						<span class="time">09:00</span>
						<p>Are you here?</p>
					</li>
					<li class="odd">
						<span class="user">Brandon S. Young</span>
						<span class="time">09:25</span>
						<p>This message is pre-queued.</p>
					</li>
					<li>
						<span class="user">Brandon S. Young</span>
						<span class="time">09:26</span>
						<p>Whohoo!</p>
					</li>
					<li class="odd">
						<span class="user">Arlind Nushi</span>
						<span class="time">09:27</span>
						<p>Do you like it?</p>
					</li>
				</ul>
				
				<div class="chat-textarea">
					<textarea class="form-control autogrow" placeholder="Type your message"></textarea>
				</div>
				
			</div>
			
		<!-- end: Chat Section -->
		</div>
		
	</div>
	
	
	<div class="page-loading-overlay">
		<div class="loader-2"></div>
	</div>

	<div class="modal fade custom-width" id="employee-modal-delete">
        <div class="modal-dialog">
            <div class="modal-content">

                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title">Are you sure? </h4>
                </div>
                 <form method="post" action="" id="EmployeeDelete">  
                 	<input type="hidden" name="DeleteEmployeeID" id="DeleteEmployeeID" value="">
                 	<input type="hidden" name="action" value="delete">
                	<input type="hidden" name="controller" value="employee">              	
                   <div class="modal-footer">
	                    <button type="button" class="btn btn-white" data-dismiss="modal">Close</button>
	                    <button type="button" class="btn btn-info" data-dismiss="modal">Delete</button>
                	</div>
                	</form>
            	</div>
        	</div>
    	</div>

    <!-- Region Edit & Create Modal -->
    <div class="modal fade custom-width" id="region-modal">
        <div class="modal-dialog">
            <div class="modal-content">

                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title">Add Employee</h4>
                </div>

                <div class="modal-body">
	                <form id="Region" method="post">
	                    <div class="row">
	                        <div class="col-md-12">

	                            <div class="form-group">
	                            	<input type="hidden" name="controller" value="employee">
	                            	<input type="hidden" name="action" value="add">

	                                <label for="field-1" class="control-label">Firstname</label>
	                                <input type="text" class="form-control" id="field-1" name="firstname" placeholder="">
	                            </div>
	                            <div class="form-group">
	                             	<label for="field-1" class="control-label">Lastname</label>
	                                <input type="text" class="form-control" id="field-1" name="lastname" placeholder="">
	                            </div>
	                            <div class="form-group">
	                            	<label for="field-1" class="control-label">PIN</label>
	                                <input type="text" class="form-control" id="field-1" name="pin" placeholder="">
	                            </div>
	                            <div class="form-group">
	                            	<label for="field-1" class="control-label">WAGE</label>
	                                <input type="text" class="form-control" id="field-1" name="wage" placeholder="">
	                            </div>
	                            <div class="form-group">
	                            	<label for="field-1" class="control-label">Address</label>
	                                <input type="text" class="form-control" id="field-1" name="address1" placeholder="">
	                            </div>
	                            <div class="form-group">
	                            	<label for="field-1" class="control-label">City</label>
	                                <input type="text" class="form-control" id="field-1" name="city" placeholder="">
	                            </div>
	                            <div class="form-group">
	                            	<label for="field-1" class="control-label">State</label>
	                                <input type="text" class="form-control" id="field-1" name="state" placeholder="">
	                            </div>
	                            <div class="form-group">
	                            	<label for="field-1" class="control-label">Zip</label>
	                                <input type="text" class="form-control" id="field-1" name="zip" placeholder="">
	                            </div>
	                            <div class="form-group">
	                            	<label for="field-1" class="control-label">Phonenumber</label>
	                                <input type="text" class="form-control" id="field-1" name="phonenumber" placeholder="">
	                            </div>
	                        </div>

	                    </div>
	                   
	                </form>
                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-white" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-info" data-dismiss="modal">Create</button>
                </div>
            </div>
        </div>
    </div>
<script>
$( document ).ready(function() {
	console.log( "ready!" );
	$("button.btn-info").click(function(){
		console.log($(this).text());
		if($(this).text()=="Create"){
			$( "#Region" ).submit();
		}
		else if($(this).text()=="Save Changes"){
			$( "#RegionEdit" ).submit();
		}
		else if($(this).text()=="Delete"){			
			$("#EmployeeDelete").submit();
		}
		//$( "#Region" ).submit();
	});

	$("a.btn-danger").click(function(){	
		$("#DeleteEmployeeID").val($(this).parent().prev().prev().prev().prev().prev().text());
	});

	$("a.btn-secondary").click(function(){
		//console.log();
		
		$("#FirstNameEdit").val($(this).parent().prev().prev().prev().prev().text());
		$("#LastNameEdit").val($(this).parent().prev().prev().prev().text());
		var EmpID = $(this).parent().prev().prev().prev().prev().prev().text();

			
		$("#PinEdit").val($("#PIN"+EmpID).val());
		$("#WageEdit").val($("#Wage"+EmpID).val());
		$("#AddressEdit").val($("#Address1"+EmpID).val());
		$("#CityEdit").val($("#City"+EmpID).val());
		$("#StateEdit").val($("#State"+EmpID).val());
		$("#ZipEdit").val($("#Zip"+EmpID).val());
		$("#PercentageEdit").val($("#Percentage"+EmpID).val());
		$("#PhonenumberEdit").val($(this).parent().prev().text());
		//$("#FirstNameEdit").val($(this).parent().prev().prev().text());
		$("#EditId").val(EmpID);
		});
});
</script>
    <div class="modal fade custom-width" id="employee-modal-edit">
        <div class="modal-dialog">
            <div class="modal-content">

                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title">Edit Employee</h4>
                </div>

                <div class="modal-body">

                    <div class="row">
                        <div class="col-md-12">
                        <form id="RegionEdit" method="post" >
                            <div class="form-group">
                                <label for="field-1" class="control-label">First Name</label>

                                <input type="hidden" name="action" value="edit">
                                <input type="hidden" name="controller" value="employee">
                                <input type="hidden" id="EditId" name="id" value="">
                                <input type="text" class="form-control" id="FirstNameEdit" name="firstname" placeholder="">
                            </div>
                            <div class="form-group">
                                <label for="field-1" class="control-label">Last Name</label>
                                <input type="text" class="form-control" id="LastNameEdit" name="lastname" placeholder="">
                            </div>
                            <div class="form-group">
                                <label for="field-1" class="control-label">PIN</label>
                                <input type="text" class="form-control" id="PinEdit" name="pin" placeholder="">
                            </div>
                            <div class="form-group">
                                <label for="field-1" class="control-label">Wage</label>
                                <input type="text" class="form-control" id="WageEdit" name="wage" placeholder="">
                            </div>
                            <div class="form-group">
                                <label for="field-1" class="control-label">Address</label>
                                <input type="text" class="form-control" id="AddressEdit" name="address" placeholder="">
                            </div>
                            <div class="form-group">
                                <label for="field-1" class="control-label">City</label>
                                <input type="text" class="form-control" id="CityEdit" name="city" placeholder="">
                            </div>
                            <div class="form-group">
                                <label for="field-1" class="control-label">State</label>
                                <input type="text" class="form-control" id="StateEdit" name="state" placeholder="">
                            </div>
                            <div class="form-group">
                                <label for="field-1" class="control-label">Zip</label>
                                <input type="text" class="form-control" id="ZipEdit" name="zip" placeholder="">
                            </div>
                            <div class="form-group">
                                <label for="field-1" class="control-label">Phonenumber</label>
                                <input type="text" class="form-control" id="PhonenumberEdit" name="phonenumber" placeholder="">
                            </div>


                        </form>
                        </div>
                    </div>

                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-white" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-info" data-dismiss="modal">Save Changes</button>
                </div>
            </div>
        </div>
    </div>


    <!-- Imported styles on this page -->
    <link rel="stylesheet" href="assets/js/datatables/dataTables.bootstrap.css">
    <link rel="stylesheet" href="assets/js/daterangepicker/daterangepicker-bs3.css">
    <link rel="stylesheet" href="assets/js/select2/select2.css">
    <link rel="stylesheet" href="assets/js/select2/select2-bootstrap.css">
    <link rel="stylesheet" href="assets/js/multiselect/css/multi-select.css">
    <link rel="stylesheet" href="assets/js/wysihtml5/src/bootstrap-wysihtml5.css">
    <link rel="stylesheet" href="assets/js/uikit/vendor/codemirror/codemirror.css">
    <link rel="stylesheet" href="assets/js/uikit/uikit.css">
    <link rel="stylesheet" href="assets/js/uikit/css/addons/uikit.almost-flat.addons.min.css">

	<!-- Bottom Scripts -->
	<script src="assets/js/bootstrap.min.js"></script>
	<script src="assets/js/TweenMax.min.js"></script>
	<script src="assets/js/resizeable.js"></script>
	<script src="assets/js/joinable.js"></script>
	<script src="assets/js/xenon-api.js"></script>
	<script src="assets/js/xenon-toggles.js"></script>
    <script src="assets/js/datatables/js/jquery.dataTables.min.js"></script>
    <script src="assets/js/moment.min.js"></script>
    <script src="assets/js/wysihtml5/lib/js/wysihtml5-0.3.0.js"></script>

	<!-- Imported scripts on this page -->
	<script src="assets/js/xenon-widgets.js"></script>
	<script src="assets/js/devexpress-web-14.1/js/globalize.min.js"></script>
	<script src="assets/js/devexpress-web-14.1/js/dx.chartjs.js"></script>
	<script src="assets/js/toastr/toastr.min.js"></script>
    <script src="assets/js/datatables/dataTables.bootstrap.js"></script>
    <script src="assets/js/datatables/yadcf/jquery.dataTables.yadcf.js"></script>
    <script src="assets/js/datatables/tabletools/dataTables.tableTools.min.js"></script>
    <script src="assets/js/select2/select2.min.js"></script>
    <script src="assets/js/jquery-ui/jquery-ui.min.js"></script>
    <script src="assets/js/selectboxit/jquery.selectBoxIt.min.js"></script>
    <script src="assets/js/multiselect/js/jquery.multi-select.js"></script>
    <script src="assets/js/daterangepicker/daterangepicker.js"></script>
    <script src="assets/js/datepicker/bootstrap-datepicker.js"></script>
    <script src="assets/js/inputmask/jquery.inputmask.bundle.js"></script>
    <script src="assets/js/formwizard/jquery.bootstrap.wizard.min.js"></script>
    <script src="assets/js/jquery-validate/jquery.validate.min.js"></script>
    <script src="assets/js/ckeditor/ckeditor.js"></script>
    <script src="assets/js/ckeditor/adapters/jquery.js"></script>
    <!--<script src="assets/js/rwd-table/js/rwd-table.min.js"></script> -->


	<!-- JavaScripts initializations and stuff -->
	<script src="assets/js/xenon-custom.js"></script>

</body>
</html>
