<?php //echo '<pre>';print_r($date);die;
//$array = json_decode($chart[0]->item);
//echo '<pre>';print_r($array);
//die;
$empShopIds=explode(',',$empshop->shop_ids);?>

<div class="main-content">
<?php $this->load->view('admin/page_header');?>

	<div class="page-title">

	<div class="title-env">
		<h1 class="title">Sales Report</h1>
		<p class="description"></p>
	</div>
	<div class="breadcrumb-env">
		<ol class="breadcrumb bc-1" >
			<li>
				<a href="#"><i class="fa-home"></i>Sales Report</a>
			</li>
			<!--<li>
				<a href="tables-basic.html">Tables</a>
			</li>
			<li class="active">
				<!--<strong>Basic Tables</strong>
			</li>-->
		</ol>
					
	</div>
	
</div>
<div class="row">
	<div class="col-sm-12">
		<form  action="" method="post" class="form-horizontal" role="form">
		<div class="form-group">
			<label for="field-1" class="col-sm-2 control-label">Shop Name *</label>
			<div class="col-sm-10">
				<select class="form-control" name="shop_id" id="shop_id">
				<option value="">Select Shop</option>
					<?php //if($this->session->userdata('user_roll')=='superadmin')
					 foreach($shops as $shop){ 
						if($this->session->userdata('user_roll')==5)
        				{ 
        					 echo "<option value='$shop->id'>$shop->name</option>";
						}else{
							if(in_array($shop->id,$empShopIds)){
							 echo "<option value='$shop->id'";
							 if(!empty($date) && $shop->id==$date['shop_id'])
							 echo "selected='selected'";
							 echo">$shop->name</option>";
							}
						} 
					} ?>
				</select>
			</div>
			<br><br>
			<label for="field-1" class="col-sm-2 control-label">To Date *</label>
			<div class="col-sm-9">
				<div class="input-group">
					<input type="text" class="form-control datepicker" data-format="yyyy-mm-dd" name="date1" required value="<?php if((!empty($date) && isset($date))) { echo $date['date1']; }?>">
					
					<div class="input-group-addon">
						<a href="#"><i class="linecons-calendar"></i></a>
					</div>
				</div>
			</div>
			<br><br>
			<label for="field-1" class="col-sm-2 control-label">From Date *</label>
			<div class="col-sm-9">
				<div class="input-group">
					<input type="text" class="form-control datepicker" data-format="yyyy-mm-dd" name="date2" required value="<?php if((!empty($date) && isset($date))) { echo $date['date2']; }?>">
					
					<div class="input-group-addon">
						<a href="#"><i class="linecons-calendar"></i></a>
					</div>
				</div>
			</div>
			<br><br>
			<label for="field-1" class="col-sm-2 control-label"></label>
			<div class="col-sm-9">
				<div class="input-group">
				<input type="submit" name="submit" class="btn btn-success" value="Search">
				</div>
			</div>
			</form>
		</div>
	</div>
</div>
<br><br><br>

<?php if((!empty($charts) && isset($charts)) || (!empty($getTemp) && isset($getTemp)))
{
	//echo $date['user'];
	//echo '<pre>';print_r($charts);die('asf');
?>
<div class="row">
	<div class="col-sm-12">
	
		<div class="panel panel-default">
			<div class="panel-heading">
				<h3 class="panel-title"></h3>
				<div class="panel-options">
					<a href="#" data-toggle="panel">
						<span class="collapse-icon">&ndash;</span>
						<span class="expand-icon">+</span>
					</a>
					<a href="#" data-toggle="remove">
						&times;
					</a>
				</div>
			</div>
			<div>
			
			</div>
			<form name="" method="post" action="<?php echo base_url();?>Sales/salesReportCsv">
				<input name='shop_id' type='hidden' value="<?php if((!empty($date) && isset($date))) { echo $date['shop_id']; }?>">
					<input name='date1' type='hidden' value="<?php if((!empty($date) && isset($date))) { echo $date['date1']; }?>">
					<input name='date2' type='hidden' value="<?php if((!empty($date) && isset($date))) { echo $date['date2']; }?>">
					<input name="csv" type="submit" value="Export CSV" class="btn btn-primary btn-small">
				</form>

			</div>
			<?php //echo "<pre>";print_r($charts);die;?>
				<div class="panel-body">
				<script type="text/javascript">
								jQuery(document).ready(function($)
								{
									var shopname=$("#shop_id option:selected").text();
									if( ! $.isFunction($.fn.dxChart))
										return;
										
									var dataSource = [
										<?php  $gst=0; 
										$notGst=0;
										foreach ($getTemp as $temp) {
											foreach($charts as $chart){
												 if($datedif==0) {
												 	if($temp->hour==$chart->hour){
																							$gst=$chart->totalSale-$chart->notGst ;
																							$notGst=$chart->notGst;
																		}

												 }
												 else{
																		if($temp->date==$chart->transaction_date){
																							$gst=$chart->totalSale-$chart->notGst ;
																							$notGst=$chart->notGst;
																		}
													}
											}
										?>
									
										{ date: "<?php  if($datedif==0) echo $temp->hour; else echo $temp->date; ?>", avgT:<?php echo $temp->avgtem; ?>, minT: <?php echo $temp->mintem; ?>, maxT:<?php echo $temp->maxtem; ?>,nongst:<?php echo $notGst?>,gst:<?php echo $gst;?>},
										<?php }?>
									];
									
									$("#bar-9").dxChart({
										dataSource: dataSource,
										commonSeriesSettings:{
											argumentField: "date"
										},
										panes: [{
												name: "topPane"
											}, {
												name: "bottomPane"
											}],
										defaultPane: "bottomPane",
										series: [ {
												pane: "topPane", 
												valueField: "maxT",
												name: "Maximum Temperature, 째C",
												label: {
													visible: true,
													customizeText: function (){
														return this.valueText + " 째C";
													}
												},
												color: "#00b19d"
											}, 
											{ pane:"bottomPane",
												type: "stackedbar",valueField: "gst", name: "Gst not included", color: "#ffd700" },{
												pane:"bottomPane",
												type: "stackedbar",
												valueField: "nongst", name: "Gst not included", color: "#c0c0c0"
											}
											
										],	
										valueAxis: [{
											pane: "bottomPane",
											
											grid: {
												visible: true
											},
											title: {
												text: "Total Sales"
											}
										}, {
											pane: "topPane",
											min: 0,
											max: 30,
											grid: {
												visible: true
											},
											title: {
												text: "Temperature, 째C"
											}
										}],
										legend: {
											verticalAlignment: "bottom",
											horizontalAlignment: "center"
										},
										title: {
											text: shopname+""+"<?php echo '<br><br>Date Range:'.$date['date1']." ,".$date['date2'];?>"
										}
									});
								});
							</script>
							<div id="bar-9" style="height: 600px; width: 100%;"></div>	
							
							
						</div>
		</div>
			
	</div>
	<?php } ?>
</div>
<script type="text/javascript">
	jQuery(document).ready(function($)
	{
		if( ! $.isFunction($.fn.dxChart))
			$(".dx-warning").removeClass('hidden');
	});
</script>

<script>
	var xenonPalette = ['#68b828','#7c38bc','#0e62c7','#fcd036','#4fcdfc','#00b19d','#ff6264','#f7aa47'];
</script>
