
<?php 

/*cho "<pre>";
print_r($shopEmpPage);
echo $shopEmpPage[0]->pageId;
$empPage=explode(',', $shopEmpPage[0]->pageId);
print_r($empPage);
die(); */
if(!empty($shopEmpPage[0]))
$empPage=explode(',', $shopEmpPage[0]->pageId);
else
$empPage=array();
echo $str = substr($_SERVER['PATH_INFO'], 1);?>

<div class="sidebar-menu toggle-others fixed">
			
			<div class="sidebar-menu-inner">	
				
				<header class="logo-env">
					
					<!-- logo -->
					<div class="logo">
						<a href="ReportingDashboard.html" class="logo-expanded">
							<img src="assets/images/logo@2x.png" width="80" alt="" />
						</a>
						
						<a href="ReportingDashboard.html" class="logo-collapsed">
							<img src="assets/images/logo-collapsed@2x.png" width="40" alt="" />
						</a>
					</div>
					
					<!-- This will toggle the mobile menu and will be visible only on mobile devices -->
					<div class="mobile-menu-toggle visible-xs">
						<a href="#" data-toggle="user-info-menu">
							<i class="fa-bell-o"></i>
							<span class="badge badge-success">7</span>
						</a>
						
						<a href="#" data-toggle="mobile-menu">
							<i class="fa-bars"></i>
						</a>
					</div>
					
					
					
								
				</header>
						
				
						
				<ul id="main-menu" class="main-menu">
					<!-- add class "multiple-expanded" to allow multiple submenus to open -->
					<!-- class "auto-inherit-active-class" will automatically add "active" class for parent elements who are marked already with class "active" -->
					<li>
						<a href="<?php echo base_url();?>employee/empdashboard">
							<i class="linecons-cog"></i>
							<span class="title">Dashboard</span>
						</a>
						
					</li>
					<?php if(!empty($pages)){
					 foreach($pages as $page){
						if(in_array($page['id'],$empPage)){?>
					<li>
						<a href="<?php echo base_url().$page['url'];?>">
							<i class="linecons-desktop"></i>
							<span class="title"><?php echo $page['page']?></span>
						</a>
						
					</li>
					<?php }
					}
					}?>
					
					
				</ul>
						
			</div>
			
		</div>