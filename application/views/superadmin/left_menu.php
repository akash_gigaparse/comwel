<div class="sidebar-menu toggle-others fixed">
			
			<div class="sidebar-menu-inner">	
				
				<header class="logo-env">
					
				
					<!-- This will toggle the mobile menu and will be visible only on mobile devices -->
					<div class="mobile-menu-toggle visible-xs">
						<a href="#" data-toggle="user-info-menu">
							<i class="fa-bell-o"></i>
							<span class="badge badge-success">7</span>
						</a>
						
						<a href="#" data-toggle="mobile-menu">
							<i class="fa-bars"></i>
						</a>
					</div>
					
					
								
				</header>
						
				
						
				<ul id="main-menu" class="main-menu">
					<!-- add class "multiple-expanded" to allow multiple submenus to open -->
					<!-- class "auto-inherit-active-class" will automatically add "active" class for parent elements who are marked already with class "active" -->
					<li>
						<a href="<?php echo base_url();?>SuperAdmin">
							<i class="linecons-cog"></i>
							<span class="title">Dashboard</span>
						</a>
						
					</li>
					<li>
						<a href="<?php echo base_url();?>employee">
							<i class="linecons-note"></i>
							<span class="title">Users</span>
						</a>
						
					</li>
						<li class="">
							<a href="<?php echo base_url();?>customer">
								<i class="linecons-note"></i>
								<span class="title">Create Customer</span>
							</a>
						</li>
					<li class="">

						<a href="<?php echo base_url();?>SuperAdmin/ListShop">
						<i class="linecons-database"></i>
							<span class="title">Shop List</span>
						</a>
					</li>
					<li class="">

						<a href="<?php echo base_url();?>sales">
						<i class="linecons-database"></i>
							<span class="title">Employee Sales Report </span>
						</a>
					</li>
					<li class="">

						<a href="<?php echo base_url();?>sales/sales">
						<i class="linecons-database"></i>
							<span class="title">Wrost and Best sales report </span>
						</a>
					</li>
					<li class="">

						<a href="<?php echo base_url();?>sales/salesReport">
						<i class="linecons-database"></i>
							<span class="title">Sales Reports</span>
						</a>
					</li>
					<li class="">

						<a href="<?php echo base_url();?>ProfitMargin">
						<i class="linecons-database"></i>
							<span class="title">Profit Margin Report</span>
						</a>
					</li>
					<li class="">
						<a href="<?php echo base_url();?>Wastage/index">
						<i class="linecons-database"></i>
							<span class="title">Wastage Report</span>
						</a>
					</li>
				
				</ul>
						
			</div>
			
		</div>
