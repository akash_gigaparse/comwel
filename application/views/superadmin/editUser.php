<?php //echo '<pre>';print_r($value);die("as");
 //$shop_id=(explode(",",$value->shop_ids));
 //echo '<pre>'; print_r($shop_id);die;
//die();
?>
<div class="main-content">
<?php $this->load->view('admin/page_header');?>
	<div class="page-title">
		<div class="title-env">
			<h1 class="title">Edit Page</h1>
			<p class="description">Users can register from this page</p>
		</div>
		<!--<div class="breadcrumb-env">
			<ol class="breadcrumb bc-1" >
				<li>
					<a href="ReportingDashboard.html"><i class="fa-home"></i>Home</a>
				</li>
				<li>
					<a href="tables-basic.html">Tables</a>
				</li>
				<li class="active">
						<strong>Basic Tables</strong>
				</li>
			</ol>
		</div>-->
	</div>
	<div class="row">
		<div class="col-sm-12">
			<div class="panel panel-default">
				<div class="panel-heading">
					<h3 class="panel-title">Default form inputs</h3>
					<div class="panel-options">
						<a data-toggle="panel" href="#">
							<span class="collapse-icon">&ndash;</span>
							<span class="expand-icon">+</span>
						</a>
						<a data-toggle="remove" href="#">
							×
						</a>
					</div>
				</div>
				<div class="panel-body">
					<form  action="<?php echo base_url();?>SuperAdmin/userUpdate" method="post" class="form-horizontal" role="form">
						<div class="form-group">
							<label for="field-1" class="col-sm-2 control-label">Username(Email)</label>
							<div class="col-sm-10">
							<input type="hidden" placeholder="" name="id" id="field-2" class="form-control" value="<?php if(!empty($postUserData)) { echo $postUserData['id']; } else {   echo $value->users_id; } ?> ">
								<input type="text" placeholder="Email" name="email" id="email" class="form-control" value="<?php if(!empty($postUserData)) { echo $postUserData['email']; } else {   echo $value->email; } ?>">
							</div>
							<?php echo form_error('email'); ?>
							<span id="email_error" class="error" style="display:none">Username Already Exists</span>
						</div>
						
						<div class="form-group-separator"></div>
						<div class="form-group">
							<label for="field-2" class="col-sm-2 control-label">First Name*</label>
							<div class="col-sm-10">
								<input type="text" placeholder="First Name" name="fname" id="field-2" class="form-control" value="<?php if(!empty($postUserData)) { echo $postUserData['fname']; } else {   echo $value->fname; }?>">
							</div>
							<?php echo form_error('fname'); ?>
						</div>
						<div class="form-group-separator"></div>
						<div class="form-group">
							<label for="field-2" class="col-sm-2 control-label">Last Name </label>
							<div class="col-sm-10">
								<input type="text" placeholder="Last Name" name="lname" id="field-2" class="form-control" value="<?php if(!empty($postUserData)) { echo $postUserData['lname']; } else {   echo $value->lname; }?>">
							</div>
								<?php echo form_error('lname'); ?>
						</div>
						<div class="form-group-separator"></div>
						
						<div class="form-group">
							<label for="field-2" class="col-sm-2 control-label">Phone *</label>
							<div class="col-sm-10">
								<input type="text" placeholder="Phone" name="mobile" id="field-2" class="form-control" value="<?php if(!empty($postUserData)) { echo $postUserData['mobile']; } else {   echo $value->mobile; }?>">
							</div>
							<?php echo form_error('mobile'); ?>
						</div>
						<div class="form-group-separator"></div>
						<div class="form-group">
							<label for="field-2" class="col-sm-2 control-label">Address *</label>
							<div class="col-sm-10">
								<input type="text" placeholder="Address" name="address" id="field-2" class="form-control" value="<?php if(!empty($postUserData)) { echo $postUserData['address']; } else {   echo $value->address; }?>">
							</div>
							<?php echo form_error('address'); ?>
						</div>
						<div class="form-group-separator"></div>
						<div class="form-group">
							<label for="field-2" class="col-sm-2 control-label">Employement Date *</label>
							<div class="col-sm-10">
								<input type="text" placeholder="Employement Date" name="employement" id="field-2" class="form-control" value="<?php if(!empty($postUserData)) { echo $postUserData['employement']; } else {   echo $value->employement; }?>">
							</div>
							<?php echo form_error('employement'); ?>
						</div>
						<div class="form-group-separator"></div>
						<div class="form-group">
							<label for="field-2" class="col-sm-2 control-label">Termination Date</label>
							<div class="col-sm-10">
								<input type="text" placeholder="Termination Date" name="termination" id="field-2" class="form-control" value="<?php if(!empty($postUserData)) { echo $postUserData['termination']; } else {   echo $value->termination; }?>">
							</div>
							<?php echo form_error('termination'); ?>
						</div>
						<div class="form-group-separator"></div>
						<div class="form-group">
							<label for="field-2" class="col-sm-2 control-label">Position *</label>
							<div class="col-sm-10">
								<input type="text" placeholder="Position" name="position" id="field-2" class="form-control" value="<?php if(!empty($postUserData)) { echo $postUserData['position']; } else {   echo $value->position; }?>">
							</div>
							<?php echo form_error('position'); ?>
						</div>
						<div class="form-group-separator"></div>
						<div class="form-group">
							<label for="field-2" class="col-sm-2 control-label">Roll</label>
							<div class="col-sm-10">
							<select name="roll" class="RollChange">
							<?php foreach($roll as $key=>$val){ 
								if($val->id==5){ continue; } 
								if(!empty($postUserData['roll'])) { ?>	
									<option value='<?php echo $val->id;?>' <?php if($postUserData['roll']==$val->id) { echo "selected='selected'"; } else { } ?> ><?php echo $val->name;?> </option>
									<?php  } else { ?>
									<option value='<?php echo $val->id;?>' <?php if($value->roles_id==$val->id) { echo "selected='selected'"; } else { } ?> ><?php echo $val->name;?> </option>
								<?php  } } ?>
							</select>
							</div>
						</div>
						<?php //if($value->roles_id)?>
						<div class="form-group-separator"></div>
							<div class="form-group shopSelect" <?php if(!empty($postUserData['roll'] ) && ($postUserData['roll']==2 || $postUserData['roll']==6 || $postUserData['roll']==1 ) ){  }else if($value->roles_id==1 || $value->roles_id==6 || $value->roles_id==2) {  } else{ echo 'style="display:none"';} ?> >
									<label class="control-label col-sm-2">Shops  *</label>
									
										
									<script type="text/javascript">
										jQuery(document).ready(function($)
										{
											$("#s2example-2").select2({
												placeholder: 'Select Shop',
												allowClear: true
											}).on('select2-open', function()
											{
												// Adding Custom Scrollbar
												$(this).data('select2').results.addClass('overflow-hidden').perfectScrollbar();
											});
											
										});
									</script>
									<?php //echo '<pre>';print_r($postUserData);die; ?>
									<div class="col-sm-10">
									<select class="form-control" id="s2example-2" name="shop[]" multiple>
										<option></option>
										
										<?php foreach($shops as $shop){
										  ?>
									<option value='<?php echo $shop->id;?>' <?php if(!empty($postUserData['shop'])){  if(in_array($shop->id,$postUserData['shop'])) echo "selected='selected'";}else{ $shop_ids=explode(',',$value->shop_ids); if(!empty($shop_ids) && in_array($shop->id,$shop_ids))echo "selected='selected'";}?> ><?php echo $shop->name;?></option>

									<?php 	 }?>
									</select>
									</div>
										<?php echo form_error('shop[]'); ?>
								</div>
						<div class="btn-group">					
							<input type="submit" name="submit" class="btn btn-success" value="Update">
						</div>
					</form>
				</div>

			</div>
		</div>  
	</div>
<script type="text/javascript">
$( ".RollChange" ).change(function () {
		var roll=$( this ).val();
		if(roll==6 || roll==2 || roll==1)
		{
			$(".shopSelect").show();
		}else{
			$(".shopSelect").hide();
		}	
	})
</script>