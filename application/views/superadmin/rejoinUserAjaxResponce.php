<?php
foreach($value as $row){
	if($row->suspend==1)
	{ ?>
	<tr>
		<td><?php echo $row->id;?></td>
		<td><?php echo $row->fname ."&nbsp;".$row->lname;?></td>
		<td><?php echo $row->email;?></td>
		<td><?php echo $row->address;?></td>
		<td><a class="btn btn-success" role="button" href="<?php echo base_url()?>SuperAdmin/editUser?id=<?php echo $row->id;?>">Edit</a></td>
		<td><a id="<?php echo $row->id;?>" class="btn btn-danger suspendUser" values="<?php echo $row->id;?>" data-toggle="modal" role="button" href="#">Suspend user</a></td>									
	</tr>
<?php }
 } ?>						
 <script>
//sespend user by super admin
var suspend_id;
$(".suspendUser").click(function(e)
{
	if (confirm("Are you sure you want to Suspend")) {
		suspend_id=this.id;
		 var info = "suspendUser=" + suspend_id;
		$.ajax({
		    type: "POST",
		    url: "<?php echo base_url()?>/SuperAdmin/suspendUser",
		    data: info,
		    success: function(data)
			{
			 	$("#"+suspend_id+"").parent().parent().remove();
			 	$("#Rejointable tbody").html(data);
			 }
		});
	}
});
 </script>