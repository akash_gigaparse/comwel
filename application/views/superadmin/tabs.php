<?php
	
 ?>

<div class="main-content">
<?php $this->load->view('admin/page_header');?>
	<div class="page-title">
		<div class="title-env">
			<h1 class="title">Employee</h1>
		</div>
		
		<div class="breadcrumb-env">
			<ol class="breadcrumb bc-1" >
				<!--<li>
					<a href="ReportingDashboard.html"><i class="fa-home"></i>Home</a>
				</li>
				<li>
					<a href="ui-panels.html">UI Elements</a>
				</li>
				<li class="active">
					<strong>Tabs &amp; Accordions</strong>
				</li> -->
			</ol>
		</div>
	</div>
	
	<div class="row">
		<div class="col-md-12">
			<ul class="nav nav-tabs nav-tabs-justified">
				<li  <?php if((!empty($postData) && isset($postData) ) OR (!empty($postRoll) && isset($postRoll))  OR ( !empty($rolesPostData) && isset($rolesPostData) )){ echo 'class=""'; } else{ echo 'class="active"';} ?>>
					<a href="#home-3" data-toggle="tab">
						<span class="visible-xs"><i class="fa-home"></i></span>
						<span class="hidden-xs"> Employee List </span>
					</a>
				</li>
				<li  <?php if(!empty($postData) && isset($postData)){ echo 'class="active"'; } else{ echo 'class=""';} ?>>
					<a href="#profile-3" data-toggle="tab">
						<span class="visible-xs"><i class="fa-user"></i></span>
						<span class="hidden-xs">Register</span>
					</a>
				</li>
			
				<li>
					<a href="#messages-3" data-toggle="tab">
						<span class="visible-xs"><i class="fa-envelope-o"></i></span>
						<span class="hidden-xs">Suspend User </span>
					</a>
				</li>
				<li  <?php if((!empty($postRoll) && isset($postRoll))) { echo 'class="active"'; } else{ echo 'class=""';} ?>>

					<a href="#settings-3" data-toggle="tab">
						<span class="visible-xs"><i class="fa-cog"></i></span>
						<span class="hidden-xs">Create Roles</span>
					</a>
				</li>
				<li v <?php if((!empty($rolesPostData) && isset($rolesPostData))) { echo 'class="active"'; } else{ echo 'class=""';} ?>>
					<a href="#inbox-3" data-toggle="tab">
						<span class="visible-xs"><i class="fa-bell-o"></i></span>
						<span class="hidden-xs">Roles Access</span>
					</a>
				</li>
			</ul>
			
			<div class="tab-content">
				<div <?php if( (!empty($postData) && isset($postData) ) OR ( !empty($postRoll) && isset($postRoll) ) OR ( !empty($rolesPostData) && isset($rolesPostData) )) { echo 'class="tab-pane"'; } else{ echo 'class="tab-pane active"';} ?>class="tab-pane active" id="home-3">
					<div class="panel-body">
						<table class="table table-model-2 table-hover" id="rejointable">
							<thead>
								<tr>
									<th>#</th>
									<th>Full Name</th>
									<th>Email</th>
									<th>Address</th>
									<th>Edit</th>
									<th>Suspend User</th>
								</tr>
							</thead>
							<tbody>
							<?php

		//					echo '<pre>';print_r($this->session->userdata());die;
							foreach($users as $row){
					 		if($this->session->userdata('user_id')==$row->id)
							{

							}else{ 
									if($row->suspend==1)
									{
								?>
								<tr>
									<td><?php echo $row->id;?></td>
									<td><?php echo $row->fname ."&nbsp;".$row->lname;?></td>
									<td><?php echo $row->email;?></td>
									<td><?php echo $row->address;?></td>
									<td><a class="btn btn-success" role="button" href="<?php echo base_url()?>superAdmin/editEmployee?id=<?php echo $row->id;?>">Edit</a></td>
									<td><a id="<?php echo $row->id;?>" class="btn btn-danger suspend" values="<?php echo $row->id;?>" data-toggle="modal" role="button" href="#">Suspend</a></td>
								
								</tr>
							<?php } } }?>						
							</tbody>
						</table>
						
					</div>
					
				</div>

				<div 
				<?php if(!empty($postData) && isset($postData) ) {  echo 'class="tab-pane active"'; } else{ echo 'class="tab-pane"';} ?>
				id="profile-3">
				<!--start html for register-->
					<div class="panel-body">
						<form  action="" method="post" class="form-horizontal" role="form">
					
							<div class="form-group">
								<label for="field-1" class="col-sm-2 control-label">Username(Email) *</label>
					
								<div class="col-sm-10">

									<input type="text" placeholder="Email" name="email" id="email" class="form-control" value="<?php if(!empty($postData)) echo $postData['email'];?>">

								</div>
					
								<?php echo form_error('email'); ?>
								<span id="email_error" class="error" style="display:none">Username Already Exists *</span>
							</div>
							
							<div class="form-group-separator"></div>
							
							<div class="form-group">
								<label for="field-2" class="col-sm-2 control-label">Password *</label>
								<div class="col-sm-10">
									<input type="password" placeholder="Password" name="password" id="field-2" class="form-control" value="<?php if(!empty($postData)) echo $postData['password'];?>">
								</div>
								<?php echo form_error('password'); ?>
							</div>

							<div class="form-group-separator"></div>
							
							<div class="form-group">
								<label for="field-2" class="col-sm-2 control-label">First Name *</label>
								<div class="col-sm-10">
									<input type="text" placeholder="First Name" name="fname" id="field-2" class="form-control" value="<?php if(!empty($postData)) echo $postData['fname'];?>">
								</div>
								<?php echo form_error('fname'); ?>
							</div>

							<div class="form-group-separator"></div>
							
							<div class="form-group">
								<label for="field-2" class="col-sm-2 control-label">Last Name</label>
								<div class="col-sm-10">
									<input type="text" placeholder="Last Name" name="lname" id="field-2" class="form-control" >
								</div>
									<?php echo form_error('lname'); ?>
							</div>

							<div class="form-group-separator"></div>
							
							<div class="form-group">
								<label for="field-2" class="col-sm-2 control-label">Phone *</label>
								<div class="col-sm-10">
									<input type="text" placeholder="Phone" name="mobile" id="field-2" class="form-control" value="<?php if(!empty($postData)) echo $postData['mobile'];?>">
								</div>
								<?php echo form_error('mobile'); ?>
							</div>

							<div class="form-group-separator"></div>
							
							<div class="form-group">
								<label for="field-2" class="col-sm-2 control-label">Address *</label>
								<div class="col-sm-10">
									<input type="text" placeholder="Address" name="address" id="field-2" class="form-control" value="<?php if(!empty($postData)) echo $postData['address'];?>">
								</div>
								<?php echo form_error('address'); ?>
							</div>

							<div class="form-group-separator"></div>
							
							<div class="form-group">
								<label for="field-2" class="col-sm-2 control-label">Employement Date*</label>
								<div class="col-sm-10">
									<input type="text" placeholder="Employement Date" name="employement" id="field-2" class="form-control datepicker" data-format="dd-mm-yyyy"  value="<?php if(!empty($postData)) echo $postData['employement'];?>">
								</div>
								<?php echo form_error('employement'); ?>
							</div>


							<div class="form-group-separator"></div>
							
							<div class="form-group">
								<label for="field-2" class="col-sm-2 control-label">Termination Date</label>
								<div class="col-sm-10">
									<input type="text" placeholder="Termination Date" name="termination" id="field-2" class="form-control datepicker" data-format="dd-mm-yyyy" value="<?php if(!empty($postData)) echo $postData['termination'];?>">
								</div>
								<?php echo form_error('termination'); ?>
							</div>

							<div class="form-group-separator"></div>
							
							<div class="form-group">
								<label for="field-2" class="col-sm-2 control-label">Position *</label>
								<div class="col-sm-10">
									<input type="text" placeholder="Position" name="position" id="field-2" class="form-control" value="<?php if(!empty($postData)) echo $postData['position'];?>">
								</div>
								<?php echo form_error('position'); ?>
							</div>
							<div class="form-group">
								<label class="control-label">Roll</label>
								
								<script type="text/javascript">
									jQuery(document).ready(function($)
									{
										$("#s2example-12").select2({
											placeholder: 'Select roll',
											allowClear: true
										}).on('select2-open', function()
										{
											// Adding Custom Scrollbar
											$(this).data('select2').results.addClass('overflow-hidden').perfectScrollbar();
										});
										
									});
								</script>
								<select class="form-control RollChange" id="s2example-12" name="roll">
								<optgroup label="Select">
								<option value="none">select</option>

									<!-- if roll is exsist-->
								<?php if(!empty($postData)) { ?>
								
									<?php foreach($roll as $key=>$val){
										if($postData['roll']== $val->id) { ?>
											<option value='<?php echo $val->id;?>' selected="selected"><?php echo $val->name;?></option>
											<?php }else{
											if( $val->id==5) { continue; }  ?>
											<option value='<?php echo $val->id;?>' ><?php echo $val->name;?></option>
									<?php  } } }else{ ?>
									<!-- if roll is Not select-->
									
									<?php foreach($roll as $key=>$val){ 
										if( $val->id==5)
										{
											continue;
										}

										?>
									<option value='<?php echo $val->id;?>' ><?php echo $val->name;?></option>
									<?php  } }?>
									
									</optgroup >
								</select>
									<?php echo form_error('roll'); ?>
							</div>
			
							<div class="form-group shopSelect" <?php if(!empty($postData['roll'] ) && ($postData['roll']==2 || $postData['roll']==6 || $postData['roll']==1 ) ){  }else{ echo 'style="display:none"';} ?> >
								<label class="control-label">Shop List</label>
									
								<script type="text/javascript">
									jQuery(document).ready(function($)
									{
										$("#s2example-2").select2({
											placeholder: 'Select shops',
											allowClear: true
										}).on('select2-open', function()
										{
											// Adding Custom Scrollbar
											$(this).data('select2').results.addClass('overflow-hidden').perfectScrollbar();
										});
										
									});
								</script>
								
								<select class="form-control" id="s2example-2" multiple name="shop[]">
									<optgroup label="Select">
									<?php foreach($shops as $value){

										 	foreach($value as $key=>$val){ ?>
											<option value='<?php echo $val->id;?>' <?php if(!empty($postData['shop'])){ if(in_array($val->id,$postData['shop']))  { echo "selected='selected'"; } } ?> ><?php echo $val->name;?></option>
										<?php  } }?>
									</optgroup>
								</select>
									<?php echo form_error('shop[]'); ?>
							</div>

							<div class="btn-group">					
								<input type="submit" name="submit" class="btn btn-success" value="Register">
							</div>
							
						</form>
						<script type="text/javascript">
						$( ".RollChange" ).change(function () {
								var roll=$( this ).val();
								if(roll==6 || roll==2 || roll==1)
								{
									$(".shopSelect").show();
								}else{
									$(".shopSelect").hide();
								}	
							})
						</script>
						
					</div>
				</div>

				<div  class="tab-pane" id="messages-3">
					<div class="panel-body">
						<table id="suspendtable" class="table table-model-2 table-hover">
							<thead>
								<tr>
									<th>#</th>
									<th>Full Name</th>
									<th>Email</th>
									<th>Address</th>
									<th>Suspend Date</th>
									<th>Delete</th>
									<th>Suspend User</th>
								</tr>
							</thead>
							<tbody>
							<?php
							//echo '<pre>';print_r($value);die;
							foreach($users as $row){
					 		if($this->session->userdata('user_id')==$row->id)
							{

							}else{ 
									if($row->suspend==0)
									{
								?>
								<tr>
									<td><?php echo $row->id;?></td>
									<td><?php echo $row->fname ."&nbsp;".$row->lname;?></td>
									<td><?php echo $row->email;?></td>
									<td><?php echo $row->address;?></td>
									<td><?php echo $row->suspend_date;?></td>
									<td><a id="<?php echo $row->id;?>" class="btn btn-danger remove" values="<?php echo $row->id;?>" data-toggle="modal" role="button" href="#">Delete</a></td>
									<td><a id="<?php echo $row->id;?>" class="btn btn-success rejoin" values="<?php echo $row->id;?>" data-toggle="modal" role="button" href="#">Rejoin</a></td>
								
								</tr>
							<?php } } }?>						
							</tbody>
						</table>
					</div>
					
				</div>
				
				<div <?php if((!empty($postRoll) && isset($postRoll))) { echo 'class="active tab-pane"'; } else{ echo 'class="tab-pane"';} ?>

				id="settings-3">
						
					<div class="panel-body">
						<form  action="" method="post" class="form-horizontal" role="form">
							<div class="form-group">
								<label for="field-2" class="col-sm-2 control-label">Role Name</label>
								<div class="col-sm-10">
									<input type="text" placeholder="Enter Role Name" name="name" id="field-2" class="form-control" value="<?php if(!empty($postRoll)) echo $postRoll['name'];?>">
								</div>
								<?php echo form_error('name'); ?>
							</div>

							<div class="btn-group">					
								<input type="submit" name="creeatrole" class="btn btn-success" value="Submit">
							</div>
							
						</form>
					
					</div>
				</div>
			
				<div <?php if((!empty($rolesPostData) && isset($rolesPostData) )  ){ echo 'class="active tab-pane"'; } else{ echo 'class="tab-pane"';} ?> id="inbox-3" >
					<div class="panel-body">
						<form id="frmSaveData"  action="" method="post" class="form-horizontal" role="form">
							


							<div class="form-group">
								<label for="field-1" class="col-sm-2 control-label">Shop Name </label>
								<div class="col-sm-10">
									<select class="form-control shop_select" name="shopId">
										<option>Select</option>
									<?php foreach($shops as $shop){
											foreach ($shop as $val) { ?>
												
											<!--if(in_array($val->id,$empShopIds))
											echo "<option value='$val->id'>$val->name</option>";-->
												<option value="<?php echo $val->id; ?>"><?php echo $val->name;?></option>
											
											<?php }}?>
									</select>
									<?php echo form_error('shop'); ?>
								</div>
							</div>	

							<div class="form-group">
								<label for="field-1" class="col-sm-2 control-label">employee Name </label>
								<div class="col-sm-10">
									<select class="form-control empId" id="empId" name="empId">
										<option>Select</option>
									
									</select>
									<?php echo form_error('employee'); ?>
								</div>
							</div>	
							<script type="text/javascript">
										
											jQuery(document).ready(function($){
												
												//this ajax calling for selecting shop from shop table
													$(".shop_select").change(function(){
													var shopId=$(this).val();
													//alert(shopId);

														jQuery.ajax({
															type: "POST",
															url: "<?php echo base_url(); ?>" + "/admin/selectEmpById",
															dataType: 'text',
															data: {shopId: shopId},
															success: function(response){
																
															$(".empId").html(response);
															} 

														});

												});
													//this ajax is calling for selecting all pages from Page table from database
													
													//this is calling ajax for submiting form data Like 'shopId , empId , pageId'  
													/*$("#empShopPagePost").click(function(){

														$("#frmSaveData").submit();
													    var formdata = $("#frmSaveData").serialize();
													    //alert(formdata);

													    jQuery.ajax({
															type: "POST",
															url: "<?php echo base_url(); ?>" + "/admin/shopEmpPageData",
															dataType: 'text',
															data: formdata,
															
															success: function(response){
																alert(response);	
															//$(".page_select").html(response);
															} 

														});                       

														

													})*/
											
											});
							</script> 
										
							<div class="form-group"> 
								<!--this is select tag is selecting multiple page for access authority-->
								<label for="field-1" class="col-sm-2 control-label">Page Name </label>
								<div class="col-sm-10">
								<script type="text/javascript">
									jQuery(document).ready(function($)
									{
										$("#page_select").select2({
											placeholder: 'Select shops',
											allowClear: true
										}).on('select2-open', function()
										{
											// Adding Custom Scrollbar
											$(this).data('select2').results.addClass('overflow-hidden').perfectScrollbar();
										});
										
									});
								</script>
									<select class="form-control page_select" name="pageId[]" id="page_select" multiple name="page[]">
										<optgroup label="Select">
											 <?php  foreach ($pages as $page) {

            //echo '<pre>';print_r($page);die("user table data");
            
                echo "<option value='$page->id'>$page->page</option>";
            
        }?>

										</optgroup>
									
									</select>
									<?php echo form_error('pages'); ?>
								</div>
							</div>
							<div class="form-group"> 
								
								<div class="col-sm-10">
									<input type="submit" name="empShopPagePost" id="empShopPagePost" class="btn btn-success empShopPagePost" value="save">
								</div>
							</div>

							<?php 

							/*<table class="table responsive">
									<thead>
										
									</thead>
									
									<tbody>

										<!--this <tr> for select on shop from dropdown list-->
										<tr>
											<th>
												Select Shop
											</th>
											<td>
												<!--fetching all shop from SHOPE TABLE-->
												<select name="shop_select" id="shop_select" onChange="selectCustomerById();" class="shop_select">
													<option value="none" selected="selected">select</option>
													<?php foreach ($shops as $shop) { ?>
												<?php foreach ($shop as $key => $value) { ?>
													<option value="<?php echo $value->id;?>" selected="selected"><?php echo $value->name; ?></option>
												<?php } }?>
												</select>
											</td>

										</tr>
										<!--this js code for alert when shop is selected from dropdown-->
										
										<script type="text/javascript">
										
											jQuery(document).ready(function($){
												
												$(".shop_select").change(function(){
													var shop=$(this).val();
													alert(shop);
												});
											
											});
										</script>  										
										<!--this <tr> for select Employee which belongs to selected shop-->

										<tr>
											<th>
												Select Employee
											</th>
											<td>
												<select name="employee_select" id="employee_select" class="employee_select">

													<option value="none" selected="selected">select</option>
											        
											         <?php foreach($allCustomer as $key=>$val){ ?>
													<option value="<?php echo $val->sales_person_name; ?>"><?php echo $val->sales_person_name; ?></option>
													<?php } ?>
												</select>
											</td>

										</tr>
										<!--this <tr> for selecting multiple pages from -->
										<tr>
											<th> 
												Select Pages
											</th>
											<td>
												<select name="page_select" id="page_select" class="page_select">
													<option value="none" selected="selected">select</option>
													<?php foreach ($pages as $page) { ?>
													<option value="<?php  echo $page->id;?>" selected="selected"><?php echo $page->name;?></option>
													<?php  } ?>
												</select>
											</td>

										</tr>
										<tr>
											<td>
												<input class="btn btn-success" type="submit" name="save" value="save"/>
											</td>
										</tr>	

								</table>  */?>

							<!--
							<table class="table responsive">

									<thead>
										<tr>
											<th>Serial Number</th>
											<th>Page Name</th>
											<th>Assigned Role</th>
										</tr>
									</thead>
									
									<tbody>
									<?php //$i=1;
									//foreach($value2 as $pages){?>
										<tr>
											<td><?php //echo $i;echo "<input type='hidden' name='ids[]' value='$pages[id]'/> ";?></td>
											<td><?php //echo $pages['page'];?></td>
											<td><?php //$selectedrole=explode(',',$pages['role_id']);
											//print_r($selectedrole);
											//foreach($value3 as $droles){
											//	$rolevalue='R_'.$droles['id'];
									//echo "<input type='checkbox' name='roles[]' onclick='myFunction(this.value,$pages[id])' value='$rolevalue'";
									//if(in_array($rolevalue,$selectedrole))
									//echo "checked='checked'";
									//echo "/> $droles[name]";}?></td>
										</tr>
										
									<?php //$i++;}?>
								</table> -->
						</form>
					
					</div>	
				
				</div>
			</div>
		</div>
	</div>
	<!-- Main Footer -->
	<!-- Choose between footer styles: "footer-type-1" or "footer-type-2" -->
	<!-- Add class "sticky" to  always stick the footer to the end of page (if page contents is small) -->
	<!-- Or class "fixed" to  always fix the footer to the end of page -->

</div>
	<div id="chat" class="fixed"><!-- start: Chat Section -->
			
		<div class="chat-inner">
		
			
			<h2 class="chat-header">
				<a  href="#" class="chat-close" data-toggle="chat">
					<i class="fa-plus-circle rotate-45deg"></i>
				</a>
				
				Chat
				<span class="badge badge-success is-hidden">0</span>
			</h2>
			
			<script type="text/javascript">
			// Here is just a sample how to open chat conversation box
			jQuery(document).ready(function($)
			{
				var $chat_conversation = $(".chat-conversation");
				
				$(".chat-group a").on('click', function(ev)
				{
					ev.preventDefault();
					
					$chat_conversation.toggleClass('is-open');
					
					$(".chat-conversation textarea").trigger('autosize.resize').focus();
				});
				
				$(".conversation-close").on('click', function(ev)
				{
					ev.preventDefault();
					$chat_conversation.removeClass('is-open');
				});
			});
			</script>
			
			
			<div class="chat-group">
				<strong>Favorites</strong>
				
				<a href="#"><span class="user-status is-online"></span> <em>Catherine J. Watkins</em></a>
				<a href="#"><span class="user-status is-online"></span> <em>Nicholas R. Walker</em></a>
				<a href="#"><span class="user-status is-busy"></span> <em>Susan J. Best</em></a>
				<a href="#"><span class="user-status is-idle"></span> <em>Fernando G. Olson</em></a>
				<a href="#"><span class="user-status is-offline"></span> <em>Brandon S. Young</em></a>
			</div>
			
			
			<div class="chat-group">
				<strong>Work</strong>
				
				<a href="#"><span class="user-status is-busy"></span> <em>Rodrigo E. Lozano</em></a>
				<a href="#"><span class="user-status is-offline"></span> <em>Robert J. Garcia</em></a>
				<a href="#"><span class="user-status is-offline"></span> <em>Daniel A. Pena</em></a>
			</div>
			
			
			<div class="chat-group">
				<strong>Other</strong>
				
				<a href="#"><span class="user-status is-online"></span> <em>Dennis E. Johnson</em></a>
				<a href="#"><span class="user-status is-online"></span> <em>Stuart A. Shire</em></a>
				<a href="#"><span class="user-status is-online"></span> <em>Janet I. Matas</em></a>
				<a href="#"><span class="user-status is-online"></span> <em>Mindy A. Smith</em></a>
				<a href="#"><span class="user-status is-busy"></span> <em>Herman S. Foltz</em></a>
				<a href="#"><span class="user-status is-busy"></span> <em>Gregory E. Robie</em></a>
				<a href="#"><span class="user-status is-busy"></span> <em>Nellie T. Foreman</em></a>
				<a href="#"><span class="user-status is-busy"></span> <em>William R. Miller</em></a>
				<a href="#"><span class="user-status is-idle"></span> <em>Vivian J. Hall</em></a>
				<a href="#"><span class="user-status is-offline"></span> <em>Melinda A. Anderson</em></a>
				<a href="#"><span class="user-status is-offline"></span> <em>Gary M. Mooneyham</em></a>
				<a href="#"><span class="user-status is-offline"></span> <em>Robert C. Medina</em></a>
				<a href="#"><span class="user-status is-offline"></span> <em>Dylan C. Bernal</em></a>
				<a href="#"><span class="user-status is-offline"></span> <em>Marc P. Sanborn</em></a>
				<a href="#"><span class="user-status is-offline"></span> <em>Kenneth M. Rochester</em></a>
				<a href="#"><span class="user-status is-offline"></span> <em>Rachael D. Carpenter</em></a>
			</div>
		
		</div>
	
		<!-- conversation template -->
		<div class="chat-conversation">
			
			<div class="conversation-header">
				<a href="#" class="conversation-close">
					&times;
				</a>
				
				<span class="user-status is-online"></span>
				<span class="display-name">Arlind Nushi</span> 
				<small>Online</small>
			</div>
			
			<ul class="conversation-body">	
				<li>
					<span class="user">Arlind Nushi</span>
					<span class="time">09:00</span>
					<p>Are you here?</p>
				</li>
				<li class="odd">
					<span class="user">Brandon S. Young</span>
					<span class="time">09:25</span>
					<p>This message is pre-queued.</p>
				</li>
				<li>
					<span class="user">Brandon S. Young</span>
					<span class="time">09:26</span>
					<p>Whohoo!</p>
				</li>
				<li class="odd">
					<span class="user">Arlind Nushi</span>
					<span class="time">09:27</span>
					<p>Do you like it?</p>
				</li>
			</ul>
			
			<div class="chat-textarea">
				<textarea class="form-control autogrow" placeholder="Type your message"></textarea>
			</div>
			
		</div>
	<!-- end: Chat Section -->
	</div>

</div>
	<!-- Bottom Scripts -->
	<script src="assets/js/bootstrap.min.js"></script>
	<script src="assets/js/TweenMax.min.js"></script>
	<script src="assets/js/resizeable.js"></script>
	<script src="assets/js/joinable.js"></script>
	<script src="assets/js/xenon-api.js"></script>
	<script src="assets/js/xenon-toggles.js"></script>


	<!-- JavaScripts initializations and stuff -->
	<script src="assets/js/xenon-custom.js"></script>
	
<script type="text/javascript">
/*function myFunction(role,id)
{
	
	
    $.post( "<?php echo base_url()?>admin/assignroles", { id: id,role:role } );
}*/
</script>


</body>
</html>