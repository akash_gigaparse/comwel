<?php //echo '<pre>';print_r($value);die; ?>

<div class="main-content">
<?php $this->load->view('page_header');?>
	<div class="page-title">
		<div class="title-env">
			<h1 class="title">Shop Owner</h1>
		</div>
		
		<div class="breadcrumb-env">
			<ol class="breadcrumb bc-1" >
				<li>
					<a href="ReportingDashboard.html"><i class="fa-home"></i>Home</a>
				</li>
				<li>
					<a href="ui-panels.html">UI Elements</a>
				</li>
				<li class="active">
					<strong>Tabs &amp; Accordions</strong>
				</li>
			</ol>
		</div>
	</div>
	
	<div class="row">
		<div class="col-md-12">
			<ul class="nav nav-tabs nav-tabs-justified">
				<li  <?php if((!empty($postData) && isset($postData) ) OR (!empty($postRoll) && isset($postRoll)) ){ echo 'class=""'; } else{ echo 'class="active"';} ?>>
					<a href="#home-3" data-toggle="tab">
						<span class="visible-xs"><i class="fa-home"></i></span>
						<span class="hidden-xs"> Shop Owner List </span>
					</a>
				</li>
				<li  <?php if(!empty($postData) && isset($postData)){ echo 'class="active"'; } else{ echo 'class=""';} ?>>
					<a href="#profile-3" data-toggle="tab">
						<span class="visible-xs"><i class="fa-user"></i></span>
						<span class="hidden-xs">Register</span>
					</a>
				</li>
			
				<li>
					<a href="#messages-3" data-toggle="tab">
						<span class="visible-xs"><i class="fa-envelope-o"></i></span>
						<span class="hidden-xs">Shop List </span>
					</a>
				</li>
					<li>

					<a href="#settings-3" data-toggle="tab">
						<span class="visible-xs"><i class="fa-cog"></i></span>
						<span class="hidden-xs">Suspend shop</span>
					</a>
				</li>
			</ul>
			
			<div class="tab-content">
				<div <?php if( (!empty($postData) && isset($postData) ) OR ( !empty($postRoll) && isset($postRoll) ) ) { echo 'class="tab-pane"'; } else{ echo 'class="tab-pane active"';} ?>class="tab-pane active" id="home-3">
					<div class="panel-body">
						<table class="table table-model-2 table-hover" id="rejointable">
							<thead>
								<tr>
									<th>#</th>
									<th>Full Name</th>
									<th>Email</th>
									<th>Address</th>
									<th>Edit</th>
								</tr>
							</thead>
							<tbody>
							<?php

							foreach($value as $row){
					 			if($row->roles_id==5)
									{
								?>
								<tr>
									<td><?php echo $row->users_id;?></td>
									<td><?php echo $row->fname ."&nbsp;".$row->lname;?></td>
									<td><?php echo $row->email;?></td>
									<td><?php echo $row->address;?></td>
									<td><a class="btn btn-success" role="button" href="<?php echo base_url()?>SuperAdmin/editUser?id=<?php echo $row->users_id;?>">Edit</a></td>
									
								</tr>
							<?php } }?>						
							</tbody>
						</table>
						
					</div>
					
				</div>

				<div 
				<?php if(!empty($postData) && isset($postData) ) {  echo 'class="tab-pane active"'; } else{ echo 'class="tab-pane"';} ?>
				id="profile-3">
				<!--start html for register-->
					<div class="panel-body">
						<form  action="<?php echo base_url();?>SuperAdmin/register" method="post" class="form-horizontal" role="form">
					
							<div class="form-group">
								<label for="field-1" class="col-sm-2 control-label">Username(Email) *</label>
					
								<div class="col-sm-10">

									<input type="text" placeholder="Email" name="email" id="email" class="form-control" value="<?php if(!empty($postData)) echo $postData['email'];?>">

								</div>
					
								<?php echo form_error('email'); ?>
								<span id="email_error" class="error" style="display:none">Username Already Exists *</span>
							</div>
							
							<div class="form-group-separator"></div>
							
							<div class="form-group">
								<label for="field-2" class="col-sm-2 control-label">Password *</label>
								<div class="col-sm-10">
									<input type="password" placeholder="Password" name="password" id="field-2" class="form-control" value="<?php if(!empty($postData)) echo $postData['password'];?>">
								</div>
								<?php echo form_error('password'); ?>
							</div>

							<div class="form-group-separator"></div>
							
							<div class="form-group">
								<label for="field-2" class="col-sm-2 control-label">First Name *</label>
								<div class="col-sm-10">
									<input type="text" placeholder="First Name" name="fname" id="field-2" class="form-control" value="<?php if(!empty($postData)) echo $postData['fname'];?>">
								</div>
								<?php echo form_error('fname'); ?>
							</div>

							<div class="form-group-separator"></div>
							
							<div class="form-group">
								<label for="field-2" class="col-sm-2 control-label">Last Name</label>
								<div class="col-sm-10">
									<input type="text" placeholder="Last Name" name="lname" id="field-2" class="form-control" >
								</div>
									<?php echo form_error('lname'); ?>
							</div>

							<div class="form-group-separator"></div>
							
							<div class="form-group">
								<label for="field-2" class="col-sm-2 control-label">Phone *</label>
								<div class="col-sm-10">
									<input type="text" placeholder="Phone" name="mobile" id="field-2" class="form-control" value="<?php if(!empty($postData)) echo $postData['mobile'];?>">
								</div>
								<?php echo form_error('mobile'); ?>
							</div>

							<div class="form-group-separator"></div>
							
							<div class="form-group">
								<label for="field-2" class="col-sm-2 control-label">Address *</label>
								<div class="col-sm-10">
									<input type="text" placeholder="Address" name="address" id="field-2" class="form-control" value="<?php if(!empty($postData)) echo $postData['address'];?>">
								</div>
								<?php echo form_error('address'); ?>
							</div>

							<div class="form-group-separator"></div>
							
							<div class="form-group">
								<label for="field-2" class="col-sm-2 control-label">Employement Date*</label>
								<div class="col-sm-10">
									<input type="text" placeholder="Employement Date" name="employement" id="field-2" class="form-control" value="<?php if(!empty($postData)) echo $postData['employement'];?>">
								</div>
								<?php echo form_error('employement'); ?>
							</div>


							<div class="form-group-separator"></div>
							
							<div class="form-group">
								<label for="field-2" class="col-sm-2 control-label">Termination Date*</label>
								<div class="col-sm-10">
									<input type="text" placeholder="Termination Date" name="termination" id="field-2" class="form-control" value="<?php if(!empty($postData)) echo $postData['termination'];?>">
								</div>
								<?php echo form_error('termination'); ?>
							</div>

							<div class="form-group-separator"></div>
							
							<div class="form-group">
								<label for="field-2" class="col-sm-2 control-label">Position *</label>
								<div class="col-sm-10">
									<input type="text" placeholder="Position" name="position" id="field-2" class="form-control" value="<?php if(!empty($postData)) echo $postData['position'];?>">
								</div>
								<?php echo form_error('position'); ?>
							</div>
							<div class="form-group">
								<label for="field-2" class="col-sm-2 control-label">Roll</label>
								<div class="col-sm-10">
								<select name="roll">
									<?php foreach($roll as $key=>$val){ 
									if($val->name=='shop owner') { ?>

									<option value='<?php echo $val->id;?>' ><?php echo $val->name;?></option>
									<?php } } ?>
								</select>
								</div>
								<?php echo form_error('position'); ?>
							</div>

							<div class="btn-group">					
								<input type="submit" name="submit" class="btn btn-success" value="Register">
							</div>
							
						</form>
						
					</div>
				</div>

				<div  class="tab-pane" id="messages-3">
				<?php //echo '<pre>';print_r($shops);die; ?>
					<div class="panel-body">
						<table id="suspendtable" class="table table-model-2 table-hover">
							<thead>
								<tr>
									<th>#</th>
									<th>Name</th>
									<th>Address</th>
									<th>Phone</th>
									<th>Logo</th>
									<th>Deposite</th>
									<th>Suspend Shop</th>
								</tr>
							</thead>
							<tbody>
							<?php
							foreach($shops as $shop){
									if($shop->suspendShop==1)
									{
								?>
								<tr>
									<td><?php echo $shop->id;?></td>
									<td><?php echo $shop->name;?></td>
									<td><?php echo $shop->address;?></td>
									<td><?php echo $shop->phone;?></td>
									<td><img src="<?php echo base_url('uploads/'. $shop->logo);?>" hight="70px" width="70px"/></td>
									<td><?php echo $shop->deposite;?></td>
									<td><a id="<?php echo $shop->id;?>" class="btn btn-success suspend_shop" values="<?php echo $shop->id;?>" data-toggle="modal" role="button" href="#">Suspend</a></td>
								
								</tr>
							<?php } } 
							?>						
							</tbody>
						</table>
					</div>
					
				</div>
			<div  class="tab-pane" id="messages-3">
				<div class="panel-body">
					<table id="suspendtable" class="table table-model-2 table-hover">
						<thead>
						<tr>
							<th>#</th>
							<th>Name</th>
							<th>Address</th>
							<th>Phone</th>
							<th>Logo</th>
							<th>Deposite</th>
							<th>Suspend Shop</th>
						</tr>
					</thead>
					<tbody>
					<?php
					foreach($shops as $shop){
							if($shop->suspendShop==0)
							{
						?>
						<tr>
							<td><?php echo $shop->id;?></td>
							<td><?php echo $shop->name;?></td>
							<td><?php echo $shop->address;?></td>
							<td><?php echo $shop->phone;?></td>
							<td><img src="<?php echo base_url('uploads/'. $shop->logo);?>" hight="70px" width="70px"/></td>
							<td><?php echo $shop->deposite;?></td>
							<td><a id="<?php echo $shop->id;?>" class="btn btn-success suspend_shop" values="<?php echo $shop->id;?>" data-toggle="modal" role="button" href="#">Suspend</a></td>
						
						</tr>
					<?php } } 
					?>						
					</tbody>
					</table>
				</div>
				
				</div>




				
			</div>
		</div>
	</div>
	<!-- Main Footer -->
	<!-- Choose between footer styles: "footer-type-1" or "footer-type-2" -->
	<!-- Add class "sticky" to  always stick the footer to the end of page (if page contents is small) -->
	<!-- Or class "fixed" to  always fix the footer to the end of page -->

</div>
	<div id="chat" class="fixed"><!-- start: Chat Section -->
			
		<div class="chat-inner">
		
			
			<h2 class="chat-header">
				<a  href="#" class="chat-close" data-toggle="chat">
					<i class="fa-plus-circle rotate-45deg"></i>
				</a>
				
				Chat
				<span class="badge badge-success is-hidden">0</span>
			</h2>
			
			<script type="text/javascript">
			// Here is just a sample how to open chat conversation box
			jQuery(document).ready(function($)
			{
				var $chat_conversation = $(".chat-conversation");
				
				$(".chat-group a").on('click', function(ev)
				{
					ev.preventDefault();
					
					$chat_conversation.toggleClass('is-open');
					
					$(".chat-conversation textarea").trigger('autosize.resize').focus();
				});
				
				$(".conversation-close").on('click', function(ev)
				{
					ev.preventDefault();
					$chat_conversation.removeClass('is-open');
				});
			});
			</script>
			
			
			<div class="chat-group">
				<strong>Favorites</strong>
				
				<a href="#"><span class="user-status is-online"></span> <em>Catherine J. Watkins</em></a>
				<a href="#"><span class="user-status is-online"></span> <em>Nicholas R. Walker</em></a>
				<a href="#"><span class="user-status is-busy"></span> <em>Susan J. Best</em></a>
				<a href="#"><span class="user-status is-idle"></span> <em>Fernando G. Olson</em></a>
				<a href="#"><span class="user-status is-offline"></span> <em>Brandon S. Young</em></a>
			</div>
			
			
			<div class="chat-group">
				<strong>Work</strong>
				
				<a href="#"><span class="user-status is-busy"></span> <em>Rodrigo E. Lozano</em></a>
				<a href="#"><span class="user-status is-offline"></span> <em>Robert J. Garcia</em></a>
				<a href="#"><span class="user-status is-offline"></span> <em>Daniel A. Pena</em></a>
			</div>
			
			
			<div class="chat-group">
				<strong>Other</strong>
				
				<a href="#"><span class="user-status is-online"></span> <em>Dennis E. Johnson</em></a>
				<a href="#"><span class="user-status is-online"></span> <em>Stuart A. Shire</em></a>
				<a href="#"><span class="user-status is-online"></span> <em>Janet I. Matas</em></a>
				<a href="#"><span class="user-status is-online"></span> <em>Mindy A. Smith</em></a>
				<a href="#"><span class="user-status is-busy"></span> <em>Herman S. Foltz</em></a>
				<a href="#"><span class="user-status is-busy"></span> <em>Gregory E. Robie</em></a>
				<a href="#"><span class="user-status is-busy"></span> <em>Nellie T. Foreman</em></a>
				<a href="#"><span class="user-status is-busy"></span> <em>William R. Miller</em></a>
				<a href="#"><span class="user-status is-idle"></span> <em>Vivian J. Hall</em></a>
				<a href="#"><span class="user-status is-offline"></span> <em>Melinda A. Anderson</em></a>
				<a href="#"><span class="user-status is-offline"></span> <em>Gary M. Mooneyham</em></a>
				<a href="#"><span class="user-status is-offline"></span> <em>Robert C. Medina</em></a>
				<a href="#"><span class="user-status is-offline"></span> <em>Dylan C. Bernal</em></a>
				<a href="#"><span class="user-status is-offline"></span> <em>Marc P. Sanborn</em></a>
				<a href="#"><span class="user-status is-offline"></span> <em>Kenneth M. Rochester</em></a>
				<a href="#"><span class="user-status is-offline"></span> <em>Rachael D. Carpenter</em></a>
			</div>
		
		</div>
	
		<!-- conversation template -->
		<div class="chat-conversation">
			
			<div class="conversation-header">
				<a href="#" class="conversation-close">
					&times;
				</a>
				
				<span class="user-status is-online"></span>
				<span class="display-name">Arlind Nushi</span> 
				<small>Online</small>
			</div>
			
			<ul class="conversation-body">	
				<li>
					<span class="user">Arlind Nushi</span>
					<span class="time">09:00</span>
					<p>Are you here?</p>
				</li>
				<li class="odd">
					<span class="user">Brandon S. Young</span>
					<span class="time">09:25</span>
					<p>This message is pre-queued.</p>
				</li>
				<li>
					<span class="user">Brandon S. Young</span>
					<span class="time">09:26</span>
					<p>Whohoo!</p>
				</li>
				<li class="odd">
					<span class="user">Arlind Nushi</span>
					<span class="time">09:27</span>
					<p>Do you like it?</p>
				</li>
			</ul>
			
			<div class="chat-textarea">
				<textarea class="form-control autogrow" placeholder="Type your message"></textarea>
			</div>
			
		</div>
	<!-- end: Chat Section -->
	</div>

</div>
	<!-- Bottom Scripts -->
	<script src="assets/js/bootstrap.min.js"></script>
	<script src="assets/js/TweenMax.min.js"></script>
	<script src="assets/js/resizeable.js"></script>
	<script src="assets/js/joinable.js"></script>
	<script src="assets/js/xenon-api.js"></script>
	<script src="assets/js/xenon-toggles.js"></script>


	<!-- JavaScripts initializations and stuff -->
	<script src="assets/js/xenon-custom.js"></script>
	
<script type="text/javascript">
function myFunction(role,id)
{
	
	
    $.post( "<?php echo base_url()?>admin/assignroles", { id: id,role:role } );
}
</script>


</body>
</html>