<?php //echo '<pre>'; print_r($orders); die;?>
<div class="main-content">
	<?php $this->load->view('page_header');?>
		<div class="page-title">
			
			<div class="title-env">
				<h1 class="title">Special Order</h1>
			</div>
			
			<div class="breadcrumb-env">
				<ol class="breadcrumb bc-1" >
					<li>
						<a href="ReportingDashboard.html"><i class="fa-home"></i>Home</a>
					</li>
					<li>
						<a href="ui-panels.html">UI Elements</a>
					</li>
					<li class="active">
						<strong>Tabs &amp; Accordions</strong>
					</li>
				</ol>
			</div>
					
		</div>
			<h3></h3>
			<br />
			
			<div class="row">
			
				<div class="col-md-12">
					
					<ul class="nav nav-tabs nav-tabs-justified">
						<li class="active">
							<a href="#home-3" data-toggle="tab">
								<span class="visible-xs"><i class="fa-home"></i></span>
								<span class="hidden-xs"> Place Order </span>
							</a>
						</li>
						<li>
							<a href="#profile-3" data-toggle="tab">
								<span class="visible-xs"><i class="fa-user"></i></span>
								<span class="hidden-xs">Invoice</span>
							</a>
						</li>
					
						
						
					</ul>
					
					<div class="tab-content">
						<div class="tab-pane active" id="home-3">
							<div class="panel-body">

							<form  action="" method="post" class="form-horizontal" role="form">
							
									<div class="form-group">
										<label for="field-1" class="col-sm-2 control-label">Name *</label>
							
										<div class="col-sm-10">

											<select class="form-control" id="customer" name="customer">
										<option></option>
										<?php foreach($customers as $customer){
											echo "<option value='$customer->id'>$customer->name</option>";
											
											}?>
									</select>
										</div>
							
									
									</div>
									
									<div class="form-group-separator"></div>
									<div class="form-group">
										<label for="field-1" class="col-sm-2 control-label">Date Required</label>
							
										<div class="col-sm-10">

										<input type="text" name="orderdate" data-end-date="+1w" data-start-date="-2d" class="form-control datepicker">
								
								</div>
										</div>
										<div class="form-group-separator"></div>
									<div class="form-group">
										<label for="field-1" class="col-sm-2 control-label">Paid</label>
							
										<div class="col-sm-10">

									<input type="radio" name="paid"   value="1" /><label>Yes</label>
									<input type="radio" name="paid"   value="2" checked/><label>No</label>
								</div>
										</div>

							<div class="form-group-separator"></div>
									
									<div class="form-group">
										<label for="field-2" class="col-sm-2 control-label">Order Number</label>
										
										
										<div class="col-sm-10">
											<input type="text" placeholder="Order number" name="ordnum" id="" class="form-control" value="<?php print_r($orders);?>" readonly>
										</div>
										
									</div>
										
									<div class="form-group-separator"></div>
									
									<div class="form-group">
										<div class="panel-body">
				<table id="productTable" class="table table-model-2 table-hover">
					<thead>
						<tr>
						<th>#</th>
							<th>Name</th>
							<th>Price</th>
							<th>Qty</th>
							<th>Total</th>
						</tr>
					</thead>
					<tbody>
					<?php
//					echo '<pre>';print_r($this->session->userdata());die;
					foreach($products as $product){
			 		?>
						<tr>
						<td><input type="checkbox" name="select[]"  onchange="myFunction22(<?php echo $product->id?>);"id="select<?php echo $product->id?>" value="1" /></td>
							<td>
									<p id="name<?php echo $product->id?>"><?php echo $product->name ;?></p>
									<input name="name[]" type="hidden"  class="name" id="postname<?php echo $product->id?>">
							</td>
							
							<td>
									<p  id="price<?php echo $product->id?>"><?php echo $product->retail ;?></p>
									<input name="price[]" type="hidden"  class="price" id="postprice<?php echo $product->id?>">
							</td>
							<td id="custom_qty">
											<p id="qty<?php echo $product->id?>"></p>
											<input type="hidden" class="qty" id="postqty<?php echo $product->id?>" placeholder="Quantity" name="qty[]"  class="form-control qty1" value="">
							</td>
							<td>
											<p class="total" id="total<?php echo $product->id;?>"></p>
											<input name="total[]" type="hidden" class="total2" id="posttotal<?php echo $product->id?>">
							</td>

						</tr>
						
					<?php  }?>	
					<tr>
						<td colspan="3"</td>
						<td>
								Total:
						</td>
						<td >
								<p class="grandtotal"></p>
								<input name="grandtotal" type="hidden"  id="grandtotal">
						</td>

						</tr>					
					</tbody>
				</table>
				
			</div>
									</div>

									<div class="form-group-separator"></div>
									
									
				
									<div class="btn-group">					
										<input type="submit" name="submit" id="submit" class="btn btn-success" value="Order">
									</div>
									
								</form>


				

							</div>
							
						</div>

						<div class="tab-pane" id="profile-3">
						<!--start html for register-->
							<div class="panel-body">
								
								
								</div>
						</div>

						<div class="tab-pane" id="messages-3">
							<div class="panel-body">
								
							</div>
							
						</div>
						
						<div class="tab-pane" id="settings-3">
								
							<div class="panel-body">
							
							
						</div>
						</div>
					
						<div class="tab-pane" id="inbox-3">
							<div class="panel-body">
							
							
						</div>	
						
						</div>
					</div>
					
					
				</div>
			</div>
			
			
			<!-- Main Footer -->
			<!-- Choose between footer styles: "footer-type-1" or "footer-type-2" -->
			<!-- Add class "sticky" to  always stick the footer to the end of page (if page contents is small) -->
			<!-- Or class "fixed" to  always fix the footer to the end of page -->

		</div>
		
			
		<div id="chat" class="fixed"><!-- start: Chat Section -->
			
			<div class="chat-inner">
			
				
				<h2 class="chat-header">
					<a  href="#" class="chat-close" data-toggle="chat">
						<i class="fa-plus-circle rotate-45deg"></i>
					</a>
					
					Chat
					<span class="badge badge-success is-hidden">0</span>
				</h2>
				
				<script type="text/javascript">
				 var sum=0;
				 $(".price").hide();
				 $(".name").hide();
				 $(".total2").hide();
				 $(".qty").hide();
				function myFunction22(id) {
					
							var sel='#select'+id;

							var nameid='#name'+id;
							var postname='#postname'+id;

							var qty='#qty'+id;
							var postqty='#postqty'+id;
							
							var pid='#price'+id;
							var postprice='#postprice'+id;

							var totalid='#total'+id;
							var posttotal='#posttotal'+id;

							var price=$(pid).text();
							if($(sel).is( ":checked" )) {
		    				var getqty = prompt("Please enter quantity", "");
		    			}

    					
						if($(sel).is( ":checked" ) && getqty != null) {
							var total=price*getqty;
							var getName=$(nameid).text();
							
							$(totalid).text(total);
 							sum +=total;  
					 		$(".grandtotal").text(sum);
					 		$("#grandtotal").val(sum);

							$(qty).text(getqty);
							
							$(postqty).show();
							$(postprice).show();
							$(postname).show();
							$(posttotal).show();

							$(postqty).val(getqty);
							$(postprice).val(price); 
							$(postname).val(getName); 
							$(posttotal).val(total); 
							
							
					 	}
					 	else
					 	{
					 		var total=price*0;
					 		var prevTotal=$(totalid).text();

					 		$(postqty).hide();
							$(postprice).hide();
							$(postname).hide();
							$(posttotal).hide();
					 		
					 		$(qty).text("");
					 		$(postqty).val("");
					 		$(sel).val("");
							$(postprice).val(""); 
							$(postname).val(""); 
							$(posttotal).val(""); 
					 		
					 		$(totalid).text(total);
 							sum -=prevTotal;  
					 		$(".grandtotal").text(sum);
					 		$("#grandtotal").val(sum);
					 	}
					
    			}

				
				
				// Here is just a sample how to open chat conversation box
				jQuery(document).ready(function($)
				{
					var $chat_conversation = $(".chat-conversation");
					
					$(".chat-group a").on('click', function(ev)
					{
						ev.preventDefault();
						
						$chat_conversation.toggleClass('is-open');
						
						$(".chat-conversation textarea").trigger('autosize.resize').focus();
					});
					
					$(".conversation-close").on('click', function(ev)
					{
						ev.preventDefault();
						$chat_conversation.removeClass('is-open');
					});
				});
				</script>
				
				
				<div class="chat-group">
					<strong>Favorites</strong>
					
					<a href="#"><span class="user-status is-online"></span> <em>Catherine J. Watkins</em></a>
					<a href="#"><span class="user-status is-online"></span> <em>Nicholas R. Walker</em></a>
					<a href="#"><span class="user-status is-busy"></span> <em>Susan J. Best</em></a>
					<a href="#"><span class="user-status is-idle"></span> <em>Fernando G. Olson</em></a>
					<a href="#"><span class="user-status is-offline"></span> <em>Brandon S. Young</em></a>
				</div>
				
				
				<div class="chat-group">
					<strong>Work</strong>
					
					<a href="#"><span class="user-status is-busy"></span> <em>Rodrigo E. Lozano</em></a>
					<a href="#"><span class="user-status is-offline"></span> <em>Robert J. Garcia</em></a>
					<a href="#"><span class="user-status is-offline"></span> <em>Daniel A. Pena</em></a>
				</div>
				
				
				<div class="chat-group">
					<strong>Other</strong>
					
					<a href="#"><span class="user-status is-online"></span> <em>Dennis E. Johnson</em></a>
					<a href="#"><span class="user-status is-online"></span> <em>Stuart A. Shire</em></a>
					<a href="#"><span class="user-status is-online"></span> <em>Janet I. Matas</em></a>
					<a href="#"><span class="user-status is-online"></span> <em>Mindy A. Smith</em></a>
					<a href="#"><span class="user-status is-busy"></span> <em>Herman S. Foltz</em></a>
					<a href="#"><span class="user-status is-busy"></span> <em>Gregory E. Robie</em></a>
					<a href="#"><span class="user-status is-busy"></span> <em>Nellie T. Foreman</em></a>
					<a href="#"><span class="user-status is-busy"></span> <em>William R. Miller</em></a>
					<a href="#"><span class="user-status is-idle"></span> <em>Vivian J. Hall</em></a>
					<a href="#"><span class="user-status is-offline"></span> <em>Melinda A. Anderson</em></a>
					<a href="#"><span class="user-status is-offline"></span> <em>Gary M. Mooneyham</em></a>
					<a href="#"><span class="user-status is-offline"></span> <em>Robert C. Medina</em></a>
					<a href="#"><span class="user-status is-offline"></span> <em>Dylan C. Bernal</em></a>
					<a href="#"><span class="user-status is-offline"></span> <em>Marc P. Sanborn</em></a>
					<a href="#"><span class="user-status is-offline"></span> <em>Kenneth M. Rochester</em></a>
					<a href="#"><span class="user-status is-offline"></span> <em>Rachael D. Carpenter</em></a>
				</div>
			
			</div>
			
			<!-- conversation template -->
			<div class="chat-conversation">
				
				<div class="conversation-header">
					<a href="#" class="conversation-close">
						&times;
					</a>
					
					<span class="user-status is-online"></span>
					<span class="display-name">Arlind Nushi</span> 
					<small>Online</small>
				</div>
				
				<ul class="conversation-body">	
					<li>
						<span class="user">Arlind Nushi</span>
						<span class="time">09:00</span>
						<p>Are you here?</p>
					</li>
					<li class="odd">
						<span class="user">Brandon S. Young</span>
						<span class="time">09:25</span>
						<p>This message is pre-queued.</p>
					</li>
					<li>
						<span class="user">Brandon S. Young</span>
						<span class="time">09:26</span>
						<p>Whohoo!</p>
					</li>
					<li class="odd">
						<span class="user">Arlind Nushi</span>
						<span class="time">09:27</span>
						<p>Do you like it?</p>
					</li>
				</ul>
				
				<div class="chat-textarea">
					<textarea class="form-control autogrow" placeholder="Type your message"></textarea>
				</div>
				
			</div>
			
		<!-- end: Chat Section -->
		</div>
		
	</div>
	<!-- Bottom Scripts -->
	<script src="assets/js/bootstrap.min.js"></script>
	<script src="assets/js/TweenMax.min.js"></script>
	<script src="assets/js/resizeable.js"></script>
	<script src="assets/js/joinable.js"></script>
	<script src="assets/js/xenon-api.js"></script>
	<script src="assets/js/xenon-toggles.js"></script>


	<!-- JavaScripts initializations and stuff -->
	<script src="assets/js/xenon-custom.js"></script>
	<script>
	 

//account parmanently remove functionality
jQuery(document).ready(function()
    {
	var remove_id;
	$("#customer").change(function(e)
	{
		
			var cutomerId=this.value;
			var info='getCustomerId='+cutomerId;
			//alert(info);
			 $.ajax({
			     type: "POST",
			     url: "<?php echo base_url()?>order/wholesale",
			     data: info,
			     success: function(data)
				 {
				 	$('#productTable tbody').html(data);
				 	//alert("Recorde Delete successfully");
				    
				 }
			});
		
	});
	$("#submit").click(function(e)
	{
		var price = [];
			 price=$(".price").toArray();
			  price=$(".price").toArray();
			 var priceArr = [];
  for ( var i = 0; i < price.length; i++ ) {
    a.push( price[ i ].innerHTML );
  }
			var arr = jQuery.makeArray(price);
			var info='price='+a
			alert(arr);

			//alert(info);
			 $.ajax({
			     type: "POST",
			     url: "<?php echo base_url()?>order/wholesale",
			     data: info,
			     success: function(data)
				 {
				 	//$('#productTable tbody').html(data);
				 	//alert("Recorde Delete successfully");
				    
				 }
			});
		
	});
	var suspend_id;
	$(".suspend").click(function(e)
	{
		if (confirm("Are you sure you want to Suspend")) {
			suspend_id=this.id;
			 var info = 'suspend=' + suspend_id;
			 $.ajax({
			     type: "POST",
			     url: "<?php echo base_url()?>/Admin/suspendUser",
			     data: info,
			     success: function(data)
				 {
				 	//alert("Recorde Delete successfully");
				     $("#"+suspend_id+"").parent().parent().remove();
				 }
			});
		}
	});
	var rejoin_id;
	$(".rejoin").click(function(e)
	{
		if (confirm("Are you sure you want to Rejoin Employee")) {
			rejoin_id=this.id;
			 var info = 'rejoin=' + rejoin_id;
			 $.ajax({
			     type: "POST",
			     url: "<?php echo base_url()?>/Admin/rejoinUser",
			     data: info,
			     success: function(data)
				 {
				     $("#"+rejoin_id+"").parent().parent().remove();
				 }
			});
		}
	});
});
</script>
<script type="text/javascript">
function myFunction(role,id)
{
	
	
    $.post( "<?php echo base_url()?>admin/assignroles", { id: id,role:role } );
}



$( ".wholesale" ).change(function() {
    var $input = $( this );
    if(($input.is( ":checked" ))==true){
    $(".check").show();
    }else{
	 $(".check").hide();
    }
}).change();
</script>


</body>
</html>