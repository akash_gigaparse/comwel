<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	
	<meta name="viewport" content="width=device-width, initial-scale=1.0" />
	<meta name="description" content="Xenon Boostrap Admin Panel" />
	<meta name="author" content="" />

	<title>Xenon - Tables</title>
<link rel="stylesheet" href="<?php echo base_url()?>assets/css/custom2.css">
 
   
</head>
<body style="">

	
<div class="page-container"><!-- add class "sidebar-collapsed" to close sidebar by default, "chat-visible" to make chat appear always -->
			
		<!-- Add "fixed" class to make the sidebar fixed always to the browser viewport. -->
		<!-- Adding class "toggle-others" will keep only one menu item open at a time. -->
		<!-- Adding class "collapsed" collapse sidebar root elements and show only icons. -->
		
		
		
<div class="main-content">
			
			<h3></h3>
			<br />
			
			<div class="row">
			
				<div class="col-md-12" >
					

						
						<!--start html for register-->
							<div class="panel-body">
							

								<form  action="" method="post" class="form-horizontal" id="invoiceform" role="form">

<div class="form-group">
										<?PHP //echo "<pre>";
										//print_r($shopInfo);
										//echo $invoice[0]->item;
										// /die();
										$items= json_decode($invoice[0]->item);
?>
										<label for="field-1"   class="col-md-3 control-label"><?php if(!empty($shopInfo->logo)){?><image src="<?php if(!empty($shopInfo->logo)) echo base_url().'uploads/'.$shopInfo->logo;?>"  /><?php }?></label>
							
										<div class="col-sm-2" >
										</div>
										<label for="field-1"  class="col-sm-5 control-label"><h1>Tax Invoice</h1></label>
							
										
									
									</div>
									
									<div class="form-group-separator"></div>
							
									<div class="form-group">
										<label for="field-1" class="col-sm-3 control-label"><?php if(!empty($shopInfo->name)) echo $shopInfo->name;?></label>
							
										<div class="col-sm-2">
										</div>
										<label for="field-1" class="col-sm-2 control-label">Date</label>
							<input type="hidden" name="pdfgen" value="" id="pdfgen">
										<div class="col-sm-3">

											<label class="control-label"><?php echo $invoice[0]->date_ordered;?></label>
										</div>
							
									
									</div>
									
									<div class="form-group-separator"></div>
									<div class="form-group">
										<label for="field-1" class="col-sm-3 control-label"><?php echo $shopInfo->address;?></label>
							
										<div class="col-sm-2">
										</div>
										<label for="field-1" class="col-sm-2 control-label">Invoice</label>
							
										<div class="col-sm-3">

											<label class="control-label"><?php echo $invoice[0]->order_num?></label>
										</div>
							
									
									</div>
									
									<div class="form-group-separator"></div>
									<div class="form-group">
										<label for="field-1" class="col-sm-3 control-label"><?php echo $shopInfo->suburb.','.$shopInfo->state.','.$shopInfo->postcode;?></label>
							
										<div class="col-sm-2">
										</div>
										<label for="field-1" class="col-sm-2 control-label">Due Date</label>
							
										<div class="col-sm-3">

										<label class="control-label">	<?php echo $invoice[0]->date_required;?></label>

										</div>
							
									
									</div>
									
									<div class="form-group-separator"></div>
									<div class="form-group">
										<label for="field-1" class="col-sm-3 control-label"><?php echo $shopInfo->phone;?></label>
							
										<div class="col-sm-2">
										</div>
										<label for="field-1" class="col-sm-2 control-label"></label>
							
										<div class="col-sm-3">

											
										</div>
							
									
									</div>
									<div class="form-group-separator"></div>
									<div class="form-group">
										<label for="field-1" class="col-sm-2 control-label"><strong>BILL TO</strong></label>
							
										<div class="col-sm-3">
										</div>
										<label for="field-1" class="col-sm-2 control-label"></label>
							
										<div class="col-sm-3">

											
										</div>
							
									
									</div>
									<div class="form-group-separator"></div>
									<div class="form-group">
										<label for="field-1" class="col-sm-2 control-label">Customer Name</label>
							
										<div class="col-sm-3">
										<?php echo $customer->name;?>
										</div>
										<label for="field-1" class="col-sm-2 control-label"></label>
							
										<div class="col-sm-3">

											
										</div>
							
									
									</div>
									<div class="form-group-separator"></div>
									<div class="form-group">
										<label for="field-1" class="col-sm-2 control-label">Attn:contact</label>
							
										<div class="col-sm-3">
										<?php echo $customer->contact;?>
										</div>
										<label for="field-1" class="col-sm-2 control-label"></label>
							
										<div class="col-sm-3">

											
										</div>
							
									
									</div>
									<div class="form-group-separator"></div>
									<div class="form-group">
										<label for="field-1" class="col-sm-2 control-label">Customer Address</label>
							
										<div class="col-sm-3">

										<?php echo $customer->address;?>
										</div>
										<label for="field-1" class="col-sm-2 control-label"></label>
							
										<div class="col-sm-3">

											
										</div>
							
									
									</div>
									<div class="form-group-separator"></div>
									<div class="form-group">
										<label for="field-1" class="col-sm-2 control-label">City,state,zip</label>
							
										<div class="col-sm-3">
										<?php echo $customer->suburb.','.$customer->state;?>
										</div>
										<label for="field-1" class="col-sm-2 control-label"></label>
							
										<div class="col-sm-3">

											
										</div>
							
									
									</div>
									<div class="form-group-separator"></div>
									<div class="form-group">
										<label for="field-1" class="col-sm-2 control-label">Customer Phone</label>
							
										<div class="col-sm-3">
										<?php echo $customer->phone;?>
										</div>
										<label for="field-1" class="col-sm-2 control-label"></label>
							
										<div class="col-sm-3">

											
										</div>
							
									
									</div>
									<div class="form-group-separator"></div>
									<div class="form-group">
							<label for="field-1" class="col-sm-2 control-label">Customer Email</label>
										<div class="col-sm-3">
										<?php echo $customer->email;?>
										</div>
										<label for="field-1" class="col-sm-2 control-label"></label>
							
										<div class="col-sm-3">

											
										</div>
							
									
									</div>
									<div class="form-group-separator"></div>
									
									
									
									
								
									
										
									<div class="form-group-separator"></div>
									
									<div class="form-group">
										<div class="panel-body">
			

					
					<table  class="table table-striped table-bordered example-1" id="productTable" cellspacing="0" width="100%">
						<thead>
						<tr>
					
							<th>Name</th>
							<th>Price</th>
							<th>Qty</th>
							<th>Total</th>
						</tr>
					</thead>
					<tbody>
						
						<!--<tr>

							<td><input type="text" data-type="productName" name="itemName[]" id="itemName_1" class="form-control autocomplete_txt" autocomplete="off"></td>
							<td><input type="number" name="price[]" readonly="readonly" id="price_1" class="form-control changesNo" autocomplete="off" onkeypress="return IsNumeric(event);" ondrop="return false;" onpaste="return false;"></td>
							<td><input type="number" name="quantity[]" id="quantity_1" class="form-control changesNo" autocomplete="off" onkeypress="return IsNumeric(event);" ondrop="return false;" onpaste="return false;"></td>
							<td><input type="number" name="total[]" id="total_1" class="form-control totalLinePrice" autocomplete="off" onkeypress="return IsNumeric(event);" ondrop="return false;" onpaste="return false;"></td>
												</tr>	-->

						<?php foreach($items as $item){
							echo "<tr><td>$item->name</td><td>$item->price</td><td>$item->qty</td><td>$item->total</td></tr>";
							}?>			
					</tbody>
					
					</table>
				
			</div>
									</div>
<div class="form-group-separator"></div>
									
									<div class="form-group">
									 	<div class='row'>
      		<div class='col-xs-12 col-sm-3 col-md-3 col-lg-3'>
      			
      		</div>
      		<div class='col-xs-12 col-sm-offset-4 col-md-offset-4 col-lg-offset-4 col-sm-5 col-md-5 col-lg-5'>
				<form class="form-inline">
					<div class="form-group">
						<label>Subtotal: &nbsp;</label>
						<div class="input-group">
							<div class="input-group-addon">$</div><?php echo $invoice[0]->price;?>
						</div>
					</div>
				
				
					
				
				
			</div>
      	</div>
      	</div>

									<div class="form-group-separator"></div>
									
									
				
									
									
								</form>


								
								</div>
						

					
						
						
					
						
					</div>
					
					
				</div>
			</div>
			
			
			<!-- Main Footer -->
			<!-- Choose between footer styles: "footer-type-1" or "footer-type-2" -->
			<!-- Add class "sticky" to  always stick the footer to the end of page (if page contents is small) -->
			<!-- Or class "fixed" to  always fix the footer to the end of page -->

		</div>
		
	
		




</body>
</html>