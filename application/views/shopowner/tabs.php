<?php //echo '<pre>';print_r($users);die; ?>

<div class="main-content">
<?php $this->load->view('shopowner/page_header');?>
	<div class="page-title">
		<div class="title-env">
			<h1 class="title">Users</h1>
		</div>
		
	</div>
	
	<div class="row">
		<div class="col-md-12">
			<ul class="nav nav-tabs nav-tabs-justified">
				<li  <?php if((!empty($postData) && isset($postData) ) OR (!empty($postRoll) && isset($postRoll)) ){ echo 'class=""'; } else{ echo 'class="active"';} ?>>
					<a href="#home-3" data-toggle="tab">
						<span class="visible-xs"><i class="fa-home"></i></span>
						<span class="hidden-xs">Show Users List </span>
					</a>
				</li>
				<li  <?php if(!empty($postData) && isset($postData)){ echo 'class="active"'; } else{ echo 'class=""';} ?>>
					<a href="#profile-3" data-toggle="tab">
						<span class="visible-xs"><i class="fa-user"></i></span>
						<span class="hidden-xs">New Users</span>
					</a>
				</li>
			
				<li>
					<a href="#messages-3" data-toggle="tab">
						<span class="visible-xs"><i class="fa-envelope-o"></i></span>
						<span class="hidden-xs">Suspened Users List </span>
					</a>
				</li>
				<li  <?php if((!empty($postRoll) && isset($postRoll))) { echo 'class="active"'; } else{ echo 'class=""';} ?>>

					<a href="#settings-3" data-toggle="tab">
						<span class="visible-xs"><i class="fa-cog"></i></span>
						<span class="hidden-xs">Create Roles</span>
					</a>
				</li>
				<li>
					<a href="#inbox-3" data-toggle="tab">
						<span class="visible-xs"><i class="fa-bell-o"></i></span>
						<span class="hidden-xs">Roles Access</span>
					</a>
				</li>
				<!--
					<li>

					<a href="#settings-3" data-toggle="tab">
						<span class="visible-xs"><i class="fa-cog"></i></span>
						<span class="hidden-xs">Suspend shop</span>
					</a>
				</li>
				-->
			</ul>
			
			<div class="tab-content">
				<div <?php if( (!empty($postData) && isset($postData) ) OR ( !empty($postRoll) && isset($postRoll) ) ) { echo 'class="tab-pane"'; } else{ echo 'class="tab-pane active"';} ?>class="tab-pane active" id="home-3">
					<div class="panel-body">
					<?php //echo '<pre>';print_r($value);die;?>
						<table class="table table-model-2 table-hover" id="Suspendtb">
							<thead>
								<tr>
									<th>#</th>
									<th>Full Name</th>
									<th>Email</th>
									<th>Address</th>
									<th>Edit</th>
									<th>Suspend</th>
								</tr>
							</thead>
							<tbody>
							<?php

							foreach($users as $row){
					 			if( ($row->suspend==1) )
									{
								?>
								<tr>
									<td><?php echo $row->id;?></td>
									<td><?php echo $row->fname ."&nbsp;".$row->lname;?></td>
									<td><?php echo $row->email;?></td>
									<td><?php echo $row->address;?></td>
									<td><a class="btn btn-success" role="button" href="<?php echo base_url()?>ShopOwner/editUser?id=<?php echo $row->id;?>">Edit</a></td>
									<td><a id="<?php echo $row->id;?>" class="btn btn-danger suspendUser" values="<?php echo $row->id;?>" data-toggle="modal" role="button" href="#">Suspend user</a></td>									
								</tr>
							<?php }
							 }?>						
							</tbody>
						</table>
						
					</div>
					
				</div>

				<div 
				<?php if(!empty($postData) && isset($postData) ) {  echo 'class="tab-pane active"'; } else{ echo 'class="tab-pane"';} ?>
				id="profile-3">
				<!--start html for register-->
					<div class="panel-body">
						<form  action="<?php echo base_url();?>ShopOwner/register" method="post" class="form-horizontal" role="form">
					
							<div class="form-group">
								<label for="field-1" class="col-sm-2 control-label">Username(Email) *</label>
					
								<div class="col-sm-10">

									<input type="text" placeholder="Email" name="email" id="email" class="form-control" value="<?php if(!empty($postData)) echo $postData['email'];?>">

								</div>
					
								<?php echo form_error('email'); ?>
								<span id="email_error" class="error" style="display:none">Username Already Exists *</span>
							</div>
							
							<div class="form-group-separator"></div>
							
							<div class="form-group">
								<label for="field-2" class="col-sm-2 control-label">Password *</label>
								<div class="col-sm-10">
									<input type="password" placeholder="Password" name="password" id="field-2" class="form-control" value="<?php if(!empty($postData)) echo $postData['password'];?>">
								</div>
								<?php echo form_error('password'); ?>
							</div>

							<div class="form-group-separator"></div>
							
							<div class="form-group">
								<label for="field-2" class="col-sm-2 control-label">First Name *</label>
								<div class="col-sm-10">
									<input type="text" placeholder="First Name" name="fname" id="field-2" class="form-control" value="<?php if(!empty($postData)) echo $postData['fname'];?>">
								</div>
								<?php echo form_error('fname'); ?>
							</div>

							<div class="form-group-separator"></div>
							
							<div class="form-group">
								<label for="field-2" class="col-sm-2 control-label">Last Name</label>
								<div class="col-sm-10">
									<input type="text" placeholder="Last Name" name="lname" id="field-2" class="form-control" >
								</div>
									<?php echo form_error('lname'); ?>
							</div>

							<div class="form-group-separator"></div>
							
							<div class="form-group">
								<label for="field-2" class="col-sm-2 control-label">Phone *</label>
								<div class="col-sm-10">
									<input type="text" placeholder="Phone" name="mobile" id="field-2" class="form-control" value="<?php if(!empty($postData)) echo $postData['mobile'];?>">
								</div>
								<?php echo form_error('mobile'); ?>
							</div>

							<div class="form-group-separator"></div>
							
							<div class="form-group">
								<label for="field-2" class="col-sm-2 control-label">Address *</label>
								<div class="col-sm-10">
									<input type="text" placeholder="Address" name="address" id="field-2" class="form-control" value="<?php if(!empty($postData)) echo $postData['address'];?>">
								</div>
								<?php echo form_error('address'); ?>
							</div>

							<div class="form-group-separator"></div>
							
							<div class="form-group">
								<label for="field-2" class="col-sm-2 control-label">Employement Date*</label>
								<div class="col-sm-10">
									<input type="text" placeholder="Employement Date" name="employement" id="field-2" class="form-control datepicker" data-format="dd-mm-yyyy"  value="<?php if(!empty($postData)) echo $postData['employement'];?>">
								</div>
								<?php echo form_error('employement'); ?>
							</div>


							<div class="form-group-separator"></div>
							
							<div class="form-group">
								<label for="field-2" class="col-sm-2 control-label">Termination Date</label>
								<div class="col-sm-10">
									<input type="text" placeholder="Termination Date" name="termination" id="field-2" class="form-control datepicker" data-format="dd-mm-yyyy" value="<?php if(!empty($postData)) echo $postData['termination'];?>">
								</div>
								<?php echo form_error('termination'); ?>
							</div>

							<div class="form-group-separator"></div>
							
							<div class="form-group">
								<label for="field-2" class="col-sm-2 control-label">Position *</label>
								<div class="col-sm-10">
									<input type="text" placeholder="Position" name="position" id="field-2" class="form-control" value="<?php if(!empty($postData)) echo $postData['position'];?>">
								</div>
								<?php echo form_error('position'); ?>
							</div>
							<div class="form-group">
								<label class="control-label">Roll</label>
								
								<script type="text/javascript">
									jQuery(document).ready(function($)
									{
										$("#s2example-12").select2({
											placeholder: 'Select roll',
											allowClear: true
										}).on('select2-open', function()
										{
											// Adding Custom Scrollbar
											$(this).data('select2').results.addClass('overflow-hidden').perfectScrollbar();
										});
										
									});
								</script>
								<select class="form-control RollChange" id="s2example-12" name="roll">
								<optgroup label="Select">
								<option value="none">select</option>

									<!-- if roll is exsist-->
								<?php if(!empty($postData)) { ?>
								
									<?php foreach($roll as $key=>$val){
										if($postData['roll']== $val->id) { ?>
											<option value='<?php echo $val->id;?>' selected="selected"><?php echo $val->name;?></option>
											<?php }else{
											if( $val->id==5) { continue; }  ?>
											<option value='<?php echo $val->id;?>' ><?php echo $val->name;?></option>
									<?php  } } }else{ ?>
									<!-- if roll is Not select-->
									
									<?php foreach($roll as $key=>$val){ 
										if( $val->id==5)
										{
											continue;
										}

										?>
									<option value='<?php echo $val->id;?>' ><?php echo $val->name;?></option>
									<?php  } }?>
									
									</optgroup >
								</select>
									<?php echo form_error('roll'); ?>
							</div>
			
							<div class="form-group shopSelect" <?php if(!empty($postData['roll'] ) && ($postData['roll']==2 || $postData['roll']==6 || $postData['roll']==1 ) ){  }else{ echo 'style="display:none"';} ?> >
								<label class="control-label">Shop List</label>
									
								<script type="text/javascript">
									jQuery(document).ready(function($)
									{
										$("#s2example-2").select2({
											placeholder: 'Select shops',
											allowClear: true
										}).on('select2-open', function()
										{
											// Adding Custom Scrollbar
											$(this).data('select2').results.addClass('overflow-hidden').perfectScrollbar();
										});
										
									});
								</script>
								
								<select class="form-control" id="s2example-2" multiple name="shop[]">
									<optgroup label="Select">
									<?php foreach($shops as $value){

										 	foreach($value as $key=>$val){ ?>
											<option value='<?php echo $val->id;?>' <?php if(!empty($postData['shop'])){ if(in_array($val->id,$postData['shop']))  { echo "selected='selected'"; } } ?> ><?php echo $val->name;?></option>
										<?php  } }?>
									</optgroup>
								</select>
									<?php echo form_error('shop[]'); ?>
							</div>

							<div class="btn-group">					
								<input type="submit" name="submit" class="btn btn-success" value="Register">
							</div>
							
						</form>
						<script type="text/javascript">
						$( ".RollChange" ).change(function () {
								var roll=$( this ).val();
								if(roll==6 || roll==2 || roll==1)
								{
									$(".shopSelect").show();
								}else{
									$(".shopSelect").hide();
								}	
							})
						</script>
						
					</div>
				</div>

				<div  class="tab-pane" id="messages-3">
					<div class="panel-body">
						<table id="Rejointable" class="table table-model-2 table-hover" >
							<thead>
								<tr>
									<th>#</th>
									<th>Full Name</th>
									<th>Email</th>
									<th>Address</th>
									<th>Delete</th>
									<th>Rejoin User</th>
								</tr>
							</thead>
							<tbody>
							<?php

							foreach($users as $row){
					 			if( ($row->suspend==0) )
									{
								?>
								<tr>
									<td><?php echo $row->id;?></td>
									<td><?php echo $row->fname ."&nbsp;".$row->lname;?></td>
									<td><?php echo $row->email;?></td>
									<td><?php echo $row->address;?></td>
									<td><a id="<?php echo $row->id;?>" class="btn btn-danger removeUser" values="<?php echo $row->id;;?>" data-toggle="modal" role="button" href="#">Delete</a></td>
									<td><a id="<?php echo $row->id;?>" class="btn btn-success rejoin_user" values="<?php echo $row->id;?>" data-toggle="modal" role="button" href="#">Rejoin</a></td>
								</tr>
							<?php } } ?>						
							</tbody>
						</table>
					</div>
				</div>
				<div <?php if((!empty($postRoll) && isset($postRoll))) { echo 'class="active tab-pane"'; } else{ echo 'class="tab-pane"';} ?>

				id="settings-3">
						
					<div class="panel-body">
						<form  action="<?php echo base_url();?>ShopOwner/createroles" method="post" class="form-horizontal" role="form">
							<div class="form-group">
								<label for="field-2" class="col-sm-2 control-label">Role Name</label>
								<div class="col-sm-10">
									<input type="text" placeholder="Enter Role Name" name="name" id="field-2" class="form-control" value="<?php if(!empty($postRoll)) echo $postRoll['name'];?>">
								</div>
								<?php echo form_error('name'); ?>
							</div>

							<div class="btn-group">					
								<input type="submit" name="submit" class="btn btn-success" value="Submit">
							</div>
							
						</form>
					
					</div>
				</div>
			
				<div class="tab-pane" id="inbox-3">
					<div class="panel-body">
						<form  action="<?php echo base_url();?>shopowner/assignRoles" method="post" class="form-horizontal" role="form">
							<table class="table responsive">
									<thead>
										<tr>
											<th>Serial Number</th>
											<th>Page Name</th>
											<th>Assigned Role</th>
										</tr>
									</thead>
									
									<tbody>
									<?php $i=1;
									foreach($value2 as $pages){?>
										<tr>
											<td><?php echo $i;echo "<input type='hidden' name='ids[]' value='$pages[id]'/> ";?></td>
											<td><?php echo $pages['page'];?></td>
											<td><?php $selectedrole=explode(',',$pages['role_id']);
											//print_r($selectedrole);
											foreach($value3 as $droles){
												$rolevalue='R_'.$droles['id'];
									echo "<input type='checkbox' name='roles[]' onclick='myFunction(this.value,$pages[id])' value='$rolevalue'";
									if(in_array($rolevalue,$selectedrole))
									echo "checked='checked'";
									echo "/> $droles[name]";}?></td>
										</tr>
										
									<?php $i++;}?>
								</table>

							
							
						</form>
					
					</div>	
				
				</div>
				<!--
				<div  class="tab-pane" id="settings-3">
						
					<div class="panel-body">
						<table  id="Rejointb" class="table table-model-2 table-hover" >
							<thead>
								<tr>
									<th>#</th>
									<th>Name</th>
									<th>Address</th>
									<th>Phone</th>
									<th>Logo</th>
									<th>Deposite</th>
									<th>Suspend Shop</th>
								</tr>
							</thead>
							<tbody>
							<?php
							/*foreach($shops as $shop){
							//		if($shop->suspendShop==0)
									{
								?>
								<tr>
									<td><?php echo $shop->id;?></td>
									<td><?php echo $shop->name;?></td>
									<td><?php echo $shop->address;?></td>
									<td><?php echo $shop->phone;?></td>
									<td><img src="<?php echo base_url('uploads/'. $shop->logo);?>" hight="70px" width="70px"/></td>
									<td><?php echo $shop->deposite;?></td>
									<td><a id="<?php echo $shop->id;?>" class="btn btn-success rejoin" values="<?php echo $shop->id;?>" data-toggle="modal" role="button" href="#">Rejoin</a></td>
								
								</tr>
							<?php } } 
							*/ ?>						
							</tbody>
						</table>
					
					</div>
				</div>

				-->

				
			</div>
		</div>
	</div>
	<!-- Main Footer -->
	<!-- Choose between footer styles: "footer-type-1" or "footer-type-2" -->
	<!-- Add class "sticky" to  always stick the footer to the end of page (if page contents is small) -->
	<!-- Or class "fixed" to  always fix the footer to the end of page -->

</div>
	
</div>
	<!-- Bottom Scripts -->
	
	
<script type="text/javascript">
function myFunction(role,id)
{
	
	
    $.post( "<?php echo base_url()?>admin/assignroles", { id: id,role:role } );
}
</script>


</body>
</html>