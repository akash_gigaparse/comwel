<?php //echo '<pre>';print_r($shops);die;?>
<div class="main-content">
	<?php $this->load->view('shopowner/page_header');?>
		<div class="page-title">
			
			<div class="title-env">
				<h1 class="title">Customer</h1>
			</div>
			
			<div class="breadcrumb-env">
				<ol class="breadcrumb bc-1" >
					<li>
						<a href="ReportingDashboard.html"><i class="fa-home"></i>Home</a>
					</li>
					<li>
						<a href="ui-panels.html">UI Elements</a>
					</li>
					<li class="active">
						<strong>Tabs &amp; Accordions</strong>
					</li>
				</ol>
			</div>
					
		</div>
			<h3></h3>
			<br />
			
			<div class="row">
			
				<div class="col-md-12">
					
					<ul class="nav nav-tabs nav-tabs-justified">
						<li class="active">
							<a href="#home-3" data-toggle="tab">
								<span class="visible-xs"><i class="fa-home"></i></span>
								<span class="hidden-xs"> Create Customer </span>
							</a>
						</li>
						<!--<li>
							<a href="#profile-3" data-toggle="tab">
								<span class="visible-xs"><i class="fa-user"></i></span>
								<span class="hidden-xs">Invoice</span>
							</a>
						</li>
					
						<li>
							<a href="#messages-3" data-toggle="tab">
								<span class="visible-xs"><i class="fa-envelope-o"></i></span>
								<span class="hidden-xs">Suspend User </span>
							</a>
						</li>
						<li>
							<a href="#settings-3" data-toggle="tab">
								<span class="visible-xs"><i class="fa-cog"></i></span>
								<span class="hidden-xs">Create Roles</span>
							</a>
						</li>
						<li>
							<a href="#inbox-3" data-toggle="tab">
								<span class="visible-xs"><i class="fa-bell-o"></i></span>
								<span class="hidden-xs">Roles Access</span>
							</a>
						</li>-->
					</ul>
					
					<div class="tab-content">
						<div class="tab-pane active" id="home-3">
							<div class="panel-body">

							<form  action="<?php echo base_url();?>ShopOwner/newCustomer" method="post" class="form-horizontal" role="form">
							
									<div class="form-group">
										<label for="field-1" class="col-sm-2 control-label">Name *</label>
							
										<div class="col-sm-10">

											<input type="text" placeholder="Name" name="name" id="name" class="form-control" value="<?php if(!empty($postData)) echo $postData['name'];?>">

										</div>
							
										<?php echo form_error('name'); ?>
										<span id="email_error" class="error" style="display:none"> </span>
									</div>
									
									<div class="form-group-separator"></div>
									
									<div class="form-group">
										<label for="field-2" class="col-sm-2 control-label">Phone *</label>
										<div class="col-sm-10">
											<input type="text" placeholder="phone" name="phone" id="field-2" class="form-control" value="<?php if(!empty($postData)) echo $postData['phone'];?>">
										</div>
										<?php echo form_error('phone'); ?>
									</div>

									<div class="form-group-separator"></div>
									
									<div class="form-group">
										<label for="field-2" class="col-sm-2 control-label">Email </label>
										<div class="col-sm-10">
											<input type="text" placeholder="Email" name="email" id="field-2" class="form-control" value="<?php if(!empty($postData)) echo $postData['email'];?>">
										</div>
										<?php echo form_error('email'); ?>
									</div>
									
									<div class="form-group-separator"></div>
									
									<div class="form-group">
										<label for="field-2" class="col-sm-2 control-label">Wholesale </label>
										<div class="col-sm-10">
											
											<input type="checkbox" placeholder="Wholesale" name="wholesale" id="field-2" class="form-control wholesale" value="1" <?php if(!empty($postData['wholesale'])){ echo "checked";}?>>
										</div>
										<?php echo form_error('wholesale'); ?>
									</div>

									<div style="display:none" class="check">	
										<div class="form-group-separator"></div>
										
										<div class="form-group">
											<label for="field-2" class="col-sm-2 control-label">ABN*</label>
											<div class="col-sm-10">
												<input type="text" placeholder="ABN" name="abn" id="field-2" class="form-control" value="<?php if(!empty($postData)) echo $postData['abn'];?>">
											</div>
											<?php echo form_error('abn'); ?>
										</div>


										<div class="form-group-separator"></div>
										
										<div class="form-group">
											<label for="field-2" class="col-sm-2 control-label">Company Name*</label>
											<div class="col-sm-10">
												<input type="text" placeholder="Company Name" name="company_name" id="field-2" class="form-control" value="<?php if(!empty($postData)) echo $postData['company_name'];?>">
											</div>
											<?php echo form_error('company_name'); ?>
										</div>
										<div class="form-group-separator"></div>
										
										<div class="form-group">
											<label for="field-2" class="col-sm-2 control-label">Address *</label>
											<div class="col-sm-10">
												<input type="text" placeholder="Address" name="address" id="field-2" class="form-control" value="<?php if(!empty($postData)) echo $postData['address'];?>">
											</div>
											<?php echo form_error('address'); ?>
										</div>

										<div class="form-group-separator"></div>
										
										<div class="form-group">
											<label for="field-2" class="col-sm-2 control-label">Contact *</label>
											<div class="col-sm-10">
												<input type="text" placeholder="Contact" name="contact" id="field-2" class="form-control" value="<?php if(!empty($postData)) { echo $postData['contact']; } else { }?>">
											</div>
											<?php echo form_error('contact'); ?>
										</div>
									<div class="form-group-separator"></div>

									
									<div class="form-group">
										<label for="field-2" class="col-sm-2 control-label">Suburb </label>
										<div class="col-sm-10">
											<input type="text" placeholder="Suburb" name="suburb" id="field-2" class="form-control" value="<?php if(!empty($postData)) echo $postData['suburb'];?>">
										</div>
										<?php echo form_error('suburb'); ?>
									</div>


									<div class="form-group-separator"></div>
									
									<div class="form-group">
										<label for="field-2" class="col-sm-2 control-label">State </label>
										<div class="col-sm-10">
											<input type="text" placeholder="State" name="state" id="field-2" class="form-control" >
										</div>
											<?php echo form_error('state'); ?>
									</div>

									</div>
									<div class="form-group-separator"></div>
									
									<div class="form-group">
										<label for="field-2" class="col-sm-2 control-label">Mobile </label>
										<div class="col-sm-10">
											<input type="text" placeholder="Mobile" name="mobile" id="field-2" class="form-control" value="<?php if(!empty($postData)) echo $postData['mobile'];?>">
										</div>
										<?php echo form_error('mobile'); ?>
									</div>
									<div class="form-group-separator"></div>
									
									<div class="form-group">
										<label for="field-2" class="col-sm-2 control-label">Delivery Info </label>
										<div class="col-sm-10">
											<textarea class="form-control autogrow" name="delivery_info" id="about" data-validate="minlength[10]" rows="5" placeholder="" > </textarea>
										</div>
										<?php echo form_error('delivery_info'); ?>
									</div>

								<div class="form-group shopSelect">
								<label class="control-label">Shop List</label>
								
									
								<script type="text/javascript">
									jQuery(document).ready(function($)
									{
										$("#s2example-2").select2({
											placeholder: 'Select shops',
											allowClear: true
										}).on('select2-open', function()
										{
											// Adding Custom Scrollbar
											$(this).data('select2').results.addClass('overflow-hidden').perfectScrollbar();
										});
										
									});
								</script>
								
								<select class="form-control" id="s2example-2" multiple name="shop[]" >
									<optgroup label="Select">
									<?php foreach($shops as $value){ ?>
									<?php foreach($value as $key=>$val){ ?>
									<option value='<?php echo $val->id;?>' <?php if(!empty($postData['shop'])) { if(in_array($val->id,$postData['shop'])) { echo "selected='selected'"; } } ?> ><?php echo $val->name;?></option>
									<?php  } }?>	
									</optgroup>
								</select>
									<?php echo form_error('shop[]');?>
							</div>
								<div class="btn-group">					
										<input type="submit" name="submit" class="btn btn-success" value="Register">
									</div>
									
								</form>


				

							</div>
							
						</div>

						<div class="tab-pane" id="profile-3">
						<!--start html for register-->
							<div class="panel-body">
								
								
								</div>
						</div>

						<div class="tab-pane" id="messages-3">
							<div class="panel-body">
								
							</div>
							
						</div>
						
						<div class="tab-pane" id="settings-3">
								
							<div class="panel-body">
							
							
						</div>
						</div>
					
						<div class="tab-pane" id="inbox-3">
							<div class="panel-body">
							
							
						</div>	
						
						</div>
					</div>
					
					
				</div>
			</div>
			
			
			<!-- Main Footer -->
			<!-- Choose between footer styles: "footer-type-1" or "footer-type-2" -->
			<!-- Add class "sticky" to  always stick the footer to the end of page (if page contents is small) -->
			<!-- Or class "fixed" to  always fix the footer to the end of page -->

		</div>
		
			
		<div id="chat" class="fixed"><!-- start: Chat Section -->
			
			<div class="chat-inner">
			
				
				<h2 class="chat-header">
					<a  href="#" class="chat-close" data-toggle="chat">
						<i class="fa-plus-circle rotate-45deg"></i>
					</a>
					
					Chat
					<span class="badge badge-success is-hidden">0</span>
				</h2>
				
				<script type="text/javascript">
				// Here is just a sample how to open chat conversation box
				jQuery(document).ready(function($)
				{
					var $chat_conversation = $(".chat-conversation");
					
					$(".chat-group a").on('click', function(ev)
					{
						ev.preventDefault();
						
						$chat_conversation.toggleClass('is-open');
						
						$(".chat-conversation textarea").trigger('autosize.resize').focus();
					});
					
					$(".conversation-close").on('click', function(ev)
					{
						ev.preventDefault();
						$chat_conversation.removeClass('is-open');
					});
				});
				</script>
				
				
				<div class="chat-group">
					<strong>Favorites</strong>
					
					<a href="#"><span class="user-status is-online"></span> <em>Catherine J. Watkins</em></a>
					<a href="#"><span class="user-status is-online"></span> <em>Nicholas R. Walker</em></a>
					<a href="#"><span class="user-status is-busy"></span> <em>Susan J. Best</em></a>
					<a href="#"><span class="user-status is-idle"></span> <em>Fernando G. Olson</em></a>
					<a href="#"><span class="user-status is-offline"></span> <em>Brandon S. Young</em></a>
				</div>
				
				
				<div class="chat-group">
					<strong>Work</strong>
					
					<a href="#"><span class="user-status is-busy"></span> <em>Rodrigo E. Lozano</em></a>
					<a href="#"><span class="user-status is-offline"></span> <em>Robert J. Garcia</em></a>
					<a href="#"><span class="user-status is-offline"></span> <em>Daniel A. Pena</em></a>
				</div>
				
				
				<div class="chat-group">
					<strong>Other</strong>
					
					<a href="#"><span class="user-status is-online"></span> <em>Dennis E. Johnson</em></a>
					<a href="#"><span class="user-status is-online"></span> <em>Stuart A. Shire</em></a>
					<a href="#"><span class="user-status is-online"></span> <em>Janet I. Matas</em></a>
					<a href="#"><span class="user-status is-online"></span> <em>Mindy A. Smith</em></a>
					<a href="#"><span class="user-status is-busy"></span> <em>Herman S. Foltz</em></a>
					<a href="#"><span class="user-status is-busy"></span> <em>Gregory E. Robie</em></a>
					<a href="#"><span class="user-status is-busy"></span> <em>Nellie T. Foreman</em></a>
					<a href="#"><span class="user-status is-busy"></span> <em>William R. Miller</em></a>
					<a href="#"><span class="user-status is-idle"></span> <em>Vivian J. Hall</em></a>
					<a href="#"><span class="user-status is-offline"></span> <em>Melinda A. Anderson</em></a>
					<a href="#"><span class="user-status is-offline"></span> <em>Gary M. Mooneyham</em></a>
					<a href="#"><span class="user-status is-offline"></span> <em>Robert C. Medina</em></a>
					<a href="#"><span class="user-status is-offline"></span> <em>Dylan C. Bernal</em></a>
					<a href="#"><span class="user-status is-offline"></span> <em>Marc P. Sanborn</em></a>
					<a href="#"><span class="user-status is-offline"></span> <em>Kenneth M. Rochester</em></a>
					<a href="#"><span class="user-status is-offline"></span> <em>Rachael D. Carpenter</em></a>
				</div>
			
			</div>
			
			<!-- conversation template -->
			<div class="chat-conversation">
				
				<div class="conversation-header">
					<a href="#" class="conversation-close">
						&times;
					</a>
					
					<span class="user-status is-online"></span>
					<span class="display-name">Arlind Nushi</span> 
					<small>Online</small>
				</div>
				
				<ul class="conversation-body">	
					<li>
						<span class="user">Arlind Nushi</span>
						<span class="time">09:00</span>
						<p>Are you here?</p>
					</li>
					<li class="odd">
						<span class="user">Brandon S. Young</span>
						<span class="time">09:25</span>
						<p>This message is pre-queued.</p>
					</li>
					<li>
						<span class="user">Brandon S. Young</span>
						<span class="time">09:26</span>
						<p>Whohoo!</p>
					</li>
					<li class="odd">
						<span class="user">Arlind Nushi</span>
						<span class="time">09:27</span>
						<p>Do you like it?</p>
					</li>
				</ul>
				
				<div class="chat-textarea">
					<textarea class="form-control autogrow" placeholder="Type your message"></textarea>
				</div>
				
			</div>
			
		<!-- end: Chat Section -->
		</div>
		
	</div>
	<!-- Bottom Scripts -->
	<script src="assets/js/bootstrap.min.js"></script>
	<script src="assets/js/TweenMax.min.js"></script>
	<script src="assets/js/resizeable.js"></script>
	<script src="assets/js/joinable.js"></script>
	<script src="assets/js/xenon-api.js"></script>
	<script src="assets/js/xenon-toggles.js"></script>


	<!-- JavaScripts initializations and stuff -->
	<script src="assets/js/xenon-custom.js"></script>
	<script>
//account parmanently remove functionality
jQuery(document).ready(function()
    {
	var remove_id;
	$(".remove").click(function(e)
	{
		if (confirm("Are you sure you want to Delete")) {
			remove_id=this.id;
			 var info = 'remove=' + remove_id;
			 $.ajax({
			     type: "POST",
			     url: "<?php echo base_url()?>/Admin/deleteUser",
			     data: info,
			     success: function(data)
				 {
				 	//alert("Recorde Delete successfully");
				     $("#"+remove_id+"").parent().parent().remove();
				 }
			});
		}
	});
	var suspend_id;
	$(".suspend").click(function(e)
	{
		if (confirm("Are you sure you want to Suspend")) {
			suspend_id=this.id;
			 var info = 'suspend=' + suspend_id;
			 $.ajax({
			     type: "POST",
			     url: "<?php echo base_url()?>/Admin/suspendUser",
			     data: info,
			     success: function(data)
				 {
				 	//alert("Recorde Delete successfully");
				     $("#"+suspend_id+"").parent().parent().remove();
				 }
			});
		}
	});
	var rejoin_id;
	$(".rejoin").click(function(e)
	{
		if (confirm("Are you sure you want to Rejoin Employee")) {
			rejoin_id=this.id;
			 var info = 'rejoin=' + rejoin_id;
			 $.ajax({
			     type: "POST",
			     url: "<?php echo base_url()?>/Admin/rejoinUser",
			     data: info,
			     success: function(data)
				 {
				     $("#"+rejoin_id+"").parent().parent().remove();
				 }
			});
		}
	});
});
</script>
<script type="text/javascript">
function myFunction(role,id)
{
	
	
    $.post( "<?php echo base_url()?>admin/assignroles", { id: id,role:role } );
}



$( ".wholesale" ).change(function() {
    var $input = $( this );
    if(($input.is( ":checked" ))==true){
    $(".check").show();
    }else{
	 $(".check").hide();
    }
}).change();
</script>


</body>
</html>