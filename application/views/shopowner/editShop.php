<div class="main-content">
	<?php $this->load->view('shopowner/page_header');?>
	<div class="page-title">
		<div class="title-env">
			<h1 class="title">Edit   User</h1>
			
		</div>

		<!--<div class="breadcrumb-env">
			<ol class="breadcrumb bc-1" >
				<li>
					<a href="ReportingDashboard.html"><i class="fa-home"></i>Home</a>
				</li>
				<li>
					<a href="ui-panels.html">UI Elements</a>
				</li>
				<li class="active">
					<strong>Tabs &amp; Accordions</strong>
				</li>
			</ol>
		</div>-->
					
	</div>
	<h3></h3>
	<br />
			
			<div class="row">
			
				<div class="col-md-12">
					
					<ul class="nav nav-tabs nav-tabs-justified">
						<li <?php if(isset($till['update']) && !empty($till['update']) || isset($till['getSingleTill']) && !empty($till['getSingleTill'])|| isset($till['updateAcc']) && !empty($till['updateAcc'])) echo "class=''"; else echo "class='active'"; ?>>
							<a href="#edit-shop" data-toggle="tab">
								<span class="visible-xs"><i class="fa-home"></i></span>
								<span class="hidden-xs"> Edit Shop </span>
							</a>
						</li>
						<!--<li <?php if(!isset($_POST['tillid']) && !empty($_POST['tillid'])) echo "class=''"; ?>>
							<a href="#list-till" data-toggle="tab">
								<span class="visible-xs"><i class="fa-home"></i></span>
								<span class="hidden-xs"> Till List </span>
							</a>
						</li>-->
						<li <?php if(isset($till['update']) && !empty($till['update']) ) echo "class='active'"; else echo "class=''"; ?> >
							<a href="#add-till" data-toggle="tab">
								<span class="visible-xs"><i class="fa-user"></i></span>
								<span class="hidden-xs">Add Till</span>
							</a>
						</li>
					<li <?php if(isset($_REQUEST['editTill'])) echo "class='active'"; else echo "class=''"; ?> >
							<a href="#edit-till" data-toggle="tab">
								<span class="visible-xs"><i class="fa-user"></i></span>
								<span class="hidden-xs">Edit Till</span>
							</a>
						</li>
						<li <?php if(isset($till['updateAcc']) && !empty($till['updateAcc'])) echo "class='active'"; else echo "class=''";  ?> >
							<a href="#add-account" data-toggle="tab">
								<span class="visible-xs"><i class="fa-user"></i></span>
								<span class="hidden-xs">Accounting Info</span>
							</a>
						</li>
					</ul>
					
					<div class="tab-content">
					<div <?php  if(isset($till['update']) && !empty($till['update']) || isset($till['getSingleTill']) && !empty($till['getSingleTill'])|| isset($till['editTill']) && !empty($till['editTill']) || isset($till['updateAcc']) && !empty($till['updateAcc'])) echo "class='tab-pane'"; else echo "class='tab-pane active'";?> id="edit-shop">
					<div class="panel-body">
				<form  action="" method="post" class="form-horizontal" role="form">
					<div class="form-group-separator"></div>
					<div class="form-group">
						<label for="field-2" class="col-sm-2 control-label">Name</label>
						<div class="col-sm-10">
						<input type="hidden" placeholder="First Name" name="id" id="field-2" class="form-control" value="<?php echo $value->id;?>">
							<input type="text" placeholder="Name" name="name" id="field-2" class="form-control" value="<?php echo $value->name;?>">
						</div>
						<?php echo form_error('name'); ?>
					</div>
					<div class="form-group-separator"></div>
					<div class="form-group">
						<label for="field-2" class="col-sm-2 control-label">Address</label>
						<div class="col-sm-10">
							<input placeholder="Enter a location" name="address" id="pac-input" class="form-control" value="<?php echo $value->address;?>">
						</div>
						<div class="col-sm-12">
	    
							    <div id="type-selector" class="controls">
							      <input type="radio" name="type" id="changetype-all" checked="checked">
							      <label for="changetype-all">All</label>

							      <input type="radio" name="type" id="changetype-establishment">
							      <label for="changetype-establishment">Establishments</label>

							      <input type="radio" name="type" id="changetype-address">
							      <label for="changetype-address">Addresses</label>

							      <input type="radio" name="type" id="changetype-geocode">
							      <label for="changetype-geocode">Geocodes</label>
							       
    					</div>
   						 <div id="map-canvas" ></div>
    				</div>
						<?php echo form_error('address'); ?>
					</div>
					<div class="form-group-separator"></div>
					<div class="form-group">
						<label for="field-2" class="col-sm-2 control-label">Latitude</label>
						<div class="col-sm-10">
							<input type="text" placeholder="Latitude" name="latitude" id="cityLat" class="form-control" value="<?php echo $value->latitude;?>">
						</div>
						<?php echo form_error('latitude'); ?>
					</div>
					<div class="form-group-separator"></div>
					<div class="form-group">
						<label for="field-2" class="col-sm-2 control-label">Longitude</label>
						<div class="col-sm-10">
							<input type="text" placeholder="Longitude" name="longitude" id="cityLng" class="form-control" value="<?php echo $value->longitude;?>">
						</div>
						<?php echo form_error('longitude'); ?>
					</div>
					<div class="form-group-separator"></div>
								<div class="form-group">
									<label for="field-2" class="col-sm-2 control-label">  Contact Details</label>
									<div class="col-sm-10">
										<input type="text" placeholder="Contact Details" name="phone" id="field-3" value="<?php echo $value->phone;?>"class="form-control">
									</div>
									<?php echo form_error('phone'); ?>
								</div>
								
								<div class="form-group-separator"></div>
									<div class="form-group">
									<label for="field-2" class="col-sm-2 control-label"> logo </label>
									<div class="col-sm-10">
										<input type="file" placeholder="Contact Details" name="userfile" id="field-3" class="form-control">
									</div>
									<?php echo form_error('userfile'); ?>
								</div>
								
								
								<div class="form-group-separator"></div>
								
								
									<div class="form-group">
									<label for="field-2" class="col-sm-2 control-label"> Default Product Overhead cost</label>
									<div class="col-sm-10">
										<input type="text" placeholder=" Default Product Overhead cost" name="overhead" id="field-3" value="<?php echo $value->overhead;?>" class="form-control">
									</div>
									<?php echo form_error('overhead'); ?>
								</div>
								
					<div class="form-group-separator"></div>
					<div class="form-group">
						<label for="field-2" class="col-sm-2 control-label">Suburb</label>
						<div class="col-sm-10">
							<input type="text" placeholder="Suburb" name="suburb" id="field-2" class="form-control" value="<?php echo $value->suburb;?>">
						</div>
							<?php echo form_error('suburb'); ?>
					</div>
					<div class="form-group-separator"></div>
					
					<div class="form-group">
						<label for="field-2" class="col-sm-2 control-label">State</label>
						<div class="col-sm-10">
							<input type="text" placeholder="State" name="state" id="field-2" class="form-control" value="<?php echo $value->state;?>">
						</div>
						<?php echo form_error('state'); ?>
					</div>
					
					<div class="form-group-separator"></div>
					<div class="form-group">
						<label for="field-2" class="col-sm-2 control-label">Postcode</label>
						<div class="col-sm-10">
							<input type="text" placeholder="Employement Date" name="postcode" id="field-2" class="form-control" value="<?php echo $value->postcode;?>">
						</div>
						<?php echo form_error('postcode'); ?>
					</div>
					<div class="form-group-separator"></div>
					<div class="form-group">
						<label for="field-2" class="col-sm-2 control-label">Country</label>
						<div class="col-sm-10">
							<input type="text" placeholder="Country" name="country" id="field-2" class="form-control" value="<?php echo $value->country;?>">
						</div>
						<?php echo form_error('country'); ?>
					</div>
					
					
					<div class="btn-group">					
						<input type="submit" name="submit" class="btn btn-success" value="Update">
					</div>
				</form>
			</div>
					</div>
						

						<div <?php if(isset($till['update']) && !empty($till['update'])) echo "class='tab-pane active'"; else echo "class='tab-pane'";?> id="add-till">
						<!--start html for register-->
							<div class="panel-body">
								<form  action="" method="post" class="form-horizontal" role="form">
							
									<div class="form-group">
										<label for="field-1" class="col-sm-2 control-label">Till ID  *</label>
							
										<div class="col-sm-10">

											<input type="text" placeholder="Till ID" name="tillid" id="name" class="form-control" value="<?php if(!empty($till['tillid'])) echo $till['tillid'];?>">

										</div>
							
										<?php echo form_error('tillid'); ?>
										
									</div>
									
									<div class="form-group-separator"></div>
									
									<div class="form-group">
										<label for="field-2" class="col-sm-2 control-label">Till User Name *</label>
										<div class="col-sm-10">
											<input type="text" placeholder="Till User Name" name="tillUser" id="tillUser" class="form-control" value="<?php if(!empty($till['tillUser'])) echo $till['tillUser']?>">
										</div>
										<?php echo form_error('tillUser'); ?>
									</div>

									<div class="form-group-separator"></div>
									
									<div class="form-group">
										<label for="field-2" class="col-sm-2 control-label">Till Password *</label>
										<div class="col-sm-10">
											<input type="password" placeholder="Till Password" name="password" id="field-2" class="form-control" value="<?php if(!empty($till['password'])) echo $till['password'];?>">
										</div>
										<?php echo form_error('password'); ?>
									</div>

									<div class="form-group-separator"></div>
									
									<div class="form-group">
										<label for="field-2" class="col-sm-2 control-label">Port Number *</label>
										<div class="col-sm-10">
											<input type="text" placeholder="Port Number " name="port" id="port" class="form-control" value="<?php if(!empty($till['port'])) echo $till['port'];?>">
										</div>
											<?php echo form_error('port'); ?>
									</div>

									<div class="form-group-separator"></div>
									
									<div class="form-group">
										<label for="field-2" class="col-sm-2 control-label">IP Address *</label>
										<div class="col-sm-10">
											<input type="text" placeholder="IP address" name="ipadd" id="ipadd" class="form-control" value="<?php if(!empty($till['ipadd'])) echo $till['ipadd'];?>">
										</div>
										<?php echo form_error('ipadd'); ?>
									</div>

									
									<div class="btn-group">					
										<input type="submit" name="update" class="btn btn-success" value="Add Till">
									</div>
									
								</form>
								
								</div>
						</div>
						<div <?php if(isset($till['editTill']) && !empty($till['editTill']) || isset($till['getSingleTill']) && !empty($till['getSingleTill'])) echo "class='tab-pane active'"; else echo "class='tab-pane'";?> id="edit-till">
						<!--start html for register-->
							<div class="panel-body">
							<form  action="" method="post" class="form-horizontal" role="form">
							
									
								<div class="form-group">
									<label class="col-sm-2 control-label">Select list</label>
									
									<script type="text/javascript">
										jQuery(document).ready(function($)
										{
											$("#s2example-1").select2({
												placeholder: 'Select till...',
												allowClear: true
											})
											.on("change", function(e) {
										        //alert($('#s2example-1').val());
										         $.ajax({
												     type: "POST",
												     url: "<?php echo base_url()?>/till/getTill",
												     data: 'id='+$('#s2example-1').val(),
												     success: function(data)
													 {
													 	data=JSON.parse(data);
													 	$.each(data, function(key,valueObj){
													 		$('.error').empty();
    														$('#'+key+'').val(valueObj);
    														$('#edit_till_form').show();
														});
													 }
												});
        									})
											.on('select2-open', function()
											{
												// Adding Custom Scrollbar
												$(this).data('select2').results.addClass('overflow-hidden').perfectScrollbar();
											});
											
										});
									</script>
										<div class="col-sm-8">
									<select class="form-control" id="s2example-1" name="editTillId">
										<option></option>
										<?php foreach($editTill as $allTill){
											echo "<option value='$allTill->id'>$allTill->tillid</option>";
											echo  $allTill->id;
											}?>
									</select>
										</div>
									</div>
								</form>
									
									<div class="form-group-separator"></div>
									<div class="form-group-separator"></div>
									<br><br>
									
								<form  action="" method="post" class="form-horizontal" role="form" style="display:<?php if(isset($till) && !empty($till)) echo 'block'; else echo 'none';?>" id="edit_till_form">
							
									<div class="form-group">
										<label for="field-1" class="col-sm-2 control-label">Till ID  *</label>
										<div class="col-sm-10">
											<input type="text" placeholder="Till ID" name="edit_tillid" id="tillid" class="form-control" value="<?php if(!empty($till['edit_tillid'])) echo $till['edit_tillid'];?>">
											<input type="hidden" name="id" id="id" class="form-control">
										</div>
										<?php echo form_error('edit_tillid'); ?>
									</div>
									
									<div class="form-group-separator"></div>
									
									<div class="form-group">
										<label for="field-2" class="col-sm-2 control-label">Till User Name *</label>
										<div class="col-sm-10">
											<input type="text" placeholder="Till User Name" name="edit_tillUser" id="tillUsername" class="form-control" value="<?php if(!empty($till['edit_tillUser'])) echo $till['edit_tillUser'];?>">
										</div>
										<?php echo form_error('edit_tillUser'); ?>
									</div>

									<div class="form-group-separator"></div>
									
									<div class="form-group">
										<label for="field-2" class="col-sm-2 control-label">Till Password *</label>
										<div class="col-sm-10">
											<input type="password" placeholder="Till Password" name="edit_password" id="tillPassword" class="form-control" value="<?php if(!empty($till['edit_password'])) echo $till['edit_password'];?>">
										</div>
										<?php echo form_error('edit_password'); ?>
									</div>

									<div class="form-group-separator"></div>
									
									<div class="form-group">
										<label for="field-2" class="col-sm-2 control-label">Port Number *</label>
										<div class="col-sm-10">
											<input type="text" placeholder="Port Number " name="edit_port" id="portNumber" class="form-control" value="<?php if(!empty($till['edit_port'])) echo $till['edit_port'];?>">
										</div>
											<?php echo form_error('edit_port'); ?>
									</div>

									<div class="form-group-separator"></div>
									
									<div class="form-group">
										<label for="field-2" class="col-sm-2 control-label">IP Address *</label>
										<div class="col-sm-10">
											<input type="text" placeholder="IP address" name="edit_ipadd" id="ipAddress" class="form-control" value="<?php if(!empty($till['edit_ipadd'])) echo $till['edit_ipadd'];?>">
										</div>
										<?php echo form_error('edit_ipadd'); ?>
									</div>

									
									<div class="btn-group">					
										<input type="submit" name="editTill" class="btn btn-success" value="Update Till">
									</div>
									
								</form>
								
								</div>
						</div>
						<div <?php if(isset($till['updateAcc']) && !empty($till['updateAcc'])) echo "class='tab-pane active'"; else echo "class='tab-pane'";?> id="add-account">
						<!--start html for register-->
							<div class="panel-body">
								<form  action="" method="post" class="form-horizontal" role="form">
							
									<div class="form-group">
										<label for="field-1" class="col-sm-3 control-label">Bank name  *</label>
							
										<div class="col-sm-9">

											<input type="text" placeholder="Bank Name" name="bankName" id="bankName" class="form-control" value="<?php if(!empty($value->bankName)) echo $value->bankName;?>">

										</div>
							
										<?php echo form_error('bankName'); ?>
										
									</div>
									
									<div class="form-group-separator"></div>
									
									<div class="form-group">
										<label for="field-2" class="col-sm-3 control-label">Bank Account Name *</label>
										<div class="col-sm-9">
											<input type="text" placeholder="Bank Account Name" name="bankAccountName" id="bankAccountName" class="form-control" value="<?php if(!empty($value->bankAccountName)) echo $value->bankAccountName;?>">
										</div>
										<?php echo form_error('bankAccountName'); ?>
									</div>

									<div class="form-group-separator"></div>
									
									<div class="form-group">
										<label for="field-2" class="col-sm-3 control-label">BSB Number *</label>
										<div class="col-sm-9">
											<input type="text" placeholder="BSB Number" name="bsbNumber" id="bsbNumber" class="form-control" value="<?php if(!empty($value->bsbNumber)) echo $value->bsbNumber;?>">
										</div>
										<?php echo form_error('bsbNumber'); ?>
									</div>

									<div class="form-group-separator"></div>
									
									<div class="form-group">
										<label for="field-2" class="col-sm-3 control-label">Bank Account Number *</label>
										<div class="col-sm-9">
											<input type="text" placeholder="Bank Account Number " name="bankAccountNumber" id="bankAccountNumber" class="form-control" value="<?php if(!empty($value->accountNumber)) echo $value->accountNumber;?>">
										</div>
											<?php echo form_error('bankAccountNumber'); ?>
									</div>

									<div class="form-group-separator"></div>
									
									<div class="form-group">
										<label for="field-2" class="col-sm-3 control-label">Account Codes *</label>
										<div class="col-sm-9">
											<input type="text" placeholder="Account Codes" name="accountCodes" id="accountCodes" class="form-control" value="<?php if(!empty($value->accountCodes)) echo $value->accountCodes;?>">
										</div>
										<?php echo form_error('accountCodes'); ?>
									</div>

									<div class="form-group-separator"></div>

									<div class="form-group">
										<label for="field-2" class="col-sm-3 control-label">GST Free Sales *</label>
										<div class="col-sm-9">
											<input type="text" placeholder="GST Free Sales" name="gstFreeSales" id="gstFreeSales" class="form-control" value="<?php if(!empty($value->gstFreeSales)) echo $value->gstFreeSales;?>">
										</div>
										<?php echo form_error('gstFreeSales'); ?>
									</div>
									<div class="form-group-separator"></div>
									
									<div class="form-group">
										<label for="field-2" class="col-sm-3 control-label">GST Included Sales *</label>
										<div class="col-sm-9">
											<input type="text" placeholder="GST Included Sales" name="gstIncludedSales" id="gstIncludedSales" class="form-control" value="<?php if(!empty($value->gstIncludedSales)) echo $value->gstIncludedSales;?>">
										</div>
										<?php echo form_error('gstIncludedSales'); ?>
									</div>

									<div class="btn-group">					
										<input type="submit" name="updateAcc" class="btn btn-success" value="Update">
									</div>
									
								</form>
								
								</div>
						</div>

					</div>
					
					
				</div>
			</div>
			
			
			<!-- Main Footer -->
			<!-- Choose between footer styles: "footer-type-1" or "footer-type-2" -->
			<!-- Add class "sticky" to  always stick the footer to the end of page (if page contents is small) -->
			<!-- Or class "fixed" to  always fix the footer to the end of page -->

		</div>
		
			
		<div id="chat" class="fixed"><!-- start: Chat Section -->
			
			<div class="chat-inner">
			
				
				<h2 class="chat-header">
					<a  href="#" class="chat-close" data-toggle="chat">
						<i class="fa-plus-circle rotate-45deg"></i>
					</a>
					
					Chat
					<span class="badge badge-success is-hidden">0</span>
				</h2>
				
				<script type="text/javascript">
				// Here is just a sample how to open chat conversation box
				jQuery(document).ready(function($)
				{
					var $chat_conversation = $(".chat-conversation");
					
					$(".chat-group a").on('click', function(ev)
					{
						ev.preventDefault();
						
						$chat_conversation.toggleClass('is-open');
						
						$(".chat-conversation textarea").trigger('autosize.resize').focus();
					});
					
					$(".conversation-close").on('click', function(ev)
					{
						ev.preventDefault();
						$chat_conversation.removeClass('is-open');
					});
				});
				</script>
				
				
				<div class="chat-group">
					<strong>Favorites</strong>
					
					<a href="#"><span class="user-status is-online"></span> <em>Catherine J. Watkins</em></a>
					<a href="#"><span class="user-status is-online"></span> <em>Nicholas R. Walker</em></a>
					<a href="#"><span class="user-status is-busy"></span> <em>Susan J. Best</em></a>
					<a href="#"><span class="user-status is-idle"></span> <em>Fernando G. Olson</em></a>
					<a href="#"><span class="user-status is-offline"></span> <em>Brandon S. Young</em></a>
				</div>
				
				
				<div class="chat-group">
					<strong>Work</strong>
					
					<a href="#"><span class="user-status is-busy"></span> <em>Rodrigo E. Lozano</em></a>
					<a href="#"><span class="user-status is-offline"></span> <em>Robert J. Garcia</em></a>
					<a href="#"><span class="user-status is-offline"></span> <em>Daniel A. Pena</em></a>
				</div>
				
				
				<div class="chat-group">
					<strong>Other</strong>
					
					<a href="#"><span class="user-status is-online"></span> <em>Dennis E. Johnson</em></a>
					<a href="#"><span class="user-status is-online"></span> <em>Stuart A. Shire</em></a>
					<a href="#"><span class="user-status is-online"></span> <em>Janet I. Matas</em></a>
					<a href="#"><span class="user-status is-online"></span> <em>Mindy A. Smith</em></a>
					<a href="#"><span class="user-status is-busy"></span> <em>Herman S. Foltz</em></a>
					<a href="#"><span class="user-status is-busy"></span> <em>Gregory E. Robie</em></a>
					<a href="#"><span class="user-status is-busy"></span> <em>Nellie T. Foreman</em></a>
					<a href="#"><span class="user-status is-busy"></span> <em>William R. Miller</em></a>
					<a href="#"><span class="user-status is-idle"></span> <em>Vivian J. Hall</em></a>
					<a href="#"><span class="user-status is-offline"></span> <em>Melinda A. Anderson</em></a>
					<a href="#"><span class="user-status is-offline"></span> <em>Gary M. Mooneyham</em></a>
					<a href="#"><span class="user-status is-offline"></span> <em>Robert C. Medina</em></a>
					<a href="#"><span class="user-status is-offline"></span> <em>Dylan C. Bernal</em></a>
					<a href="#"><span class="user-status is-offline"></span> <em>Marc P. Sanborn</em></a>
					<a href="#"><span class="user-status is-offline"></span> <em>Kenneth M. Rochester</em></a>
					<a href="#"><span class="user-status is-offline"></span> <em>Rachael D. Carpenter</em></a>
				</div>
			
			</div>
			
			<!-- conversation template -->
			<div class="chat-conversation">
				
				<div class="conversation-header">
					<a href="#" class="conversation-close">
						&times;
					</a>
					
					<span class="user-status is-online"></span>
					<span class="display-name">Arlind Nushi</span> 
					<small>Online</small>
				</div>
				
				<ul class="conversation-body">	
					<li>
						<span class="user">Arlind Nushi</span>
						<span class="time">09:00</span>
						<p>Are you here?</p>
					</li>
					<li class="odd">
						<span class="user">Brandon S. Young</span>
						<span class="time">09:25</span>
						<p>This message is pre-queued.</p>
					</li>
					<li>
						<span class="user">Brandon S. Young</span>
						<span class="time">09:26</span>
						<p>Whohoo!</p>
					</li>
					<li class="odd">
						<span class="user">Arlind Nushi</span>
						<span class="time">09:27</span>
						<p>Do you like it?</p>
					</li>
				</ul>
				
				<div class="chat-textarea">
					<textarea class="form-control autogrow" placeholder="Type your message"></textarea>
				</div>
				
			</div>
			
		<!-- end: Chat Section -->
		</div>
		
	</div>
	<!-- Bottom Scripts -->
	<script src="assets/js/bootstrap.min.js"></script>
	<script src="assets/js/TweenMax.min.js"></script>
	<script src="assets/js/resizeable.js"></script>
	<script src="assets/js/joinable.js"></script>
	<script src="assets/js/xenon-api.js"></script>
	<script src="assets/js/xenon-toggles.js"></script>


	<!-- JavaScripts initializations and stuff -->
	<script src="assets/js/xenon-custom.js"></script>
	<script>
//account parmanently remove functionality
jQuery(document).ready(function()
    {
	var remove_id;
	$(".remove").click(function(e)
	{
		if (confirm("Are you sure you want to Delete")) {
			remove_id=this.id;
			 var info = 'remove=' + remove_id;
			 $.ajax({
			     type: "POST",
			     url: "<?php echo base_url()?>/Admin/deleteUser",
			     data: info,
			     success: function(data)
				 {
				 	//alert("Recorde Delete successfully");
				     $("#"+remove_id+"").parent().parent().remove();
				 }
			});
		}
	});
	var suspend_id;
	$(".suspend").click(function(e)
	{
		if (confirm("Are you sure you want to Suspend")) {
			suspend_id=this.id;
			 var info = 'suspend=' + suspend_id;
			 $.ajax({
			     type: "POST",
			     url: "<?php echo base_url()?>/Admin/suspendUser",
			     data: info,
			     success: function(data)
				 {
				 	//alert("Recorde Delete successfully");
				     $("#"+suspend_id+"").parent().parent().remove();
				 }
			});
		}
	});
	var rejoin_id;
	$(".rejoin").click(function(e)
	{
		if (confirm("Are you sure you want to Rejoin Employee")) {
			rejoin_id=this.id;
			 var info = 'rejoin=' + rejoin_id;
			 $.ajax({
			     type: "POST",
			     url: "<?php echo base_url()?>/Admin/rejoinUser",
			     data: info,
			     success: function(data)
				 {
				     $("#"+rejoin_id+"").parent().parent().remove();
				 }
			});
		}
	});
});
</script>
<script type="text/javascript">
function myFunction(role,id)
{
	
	
    $.post( "<?php echo base_url()?>admin/assignroles", { id: id,role:role } );
}
</script>


</body>
</html>