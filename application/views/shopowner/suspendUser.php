<?php
foreach($users as $row){
		if( ($row->suspend==0) )
		{
	?>
	<tr>
		<td><?php echo $row->id;?></td>
		<td><?php echo $row->fname ."&nbsp;".$row->lname;?></td>
		<td><?php echo $row->email;?></td>
		<td><?php echo $row->address;?></td>
		<td><a id="<?php echo $row->id;?>" class="btn btn-danger removeUser" values="<?php echo $row->id;?>" data-toggle="modal" role="button" href="#">Delete</a></td>
		<td><a id="<?php echo $row->id;?>" class="btn btn-success rejoin_user" values="<?php echo $row->id;?>" data-toggle="modal" role="button" href="#">Rejoin</a></td>
	</tr>
<?php } } ?>

<script>
var remove_id;
$(".removeUser").click(function(e)
{
	if (confirm("Are you sure you want to Delete")) {
		remove_id=this.id;
		 var info = 'remove=' + remove_id;
			 $.ajax({
			     type: "POST",
			     url: "<?php echo base_url()?>/ShopOwner/removeUser",
			     data: info,
			     success: function(data)
				 {
				 	//alert("Recorde Delete successfully");
				    $("#"+remove_id+"").parent().parent().remove();
				}
			});
		}
	});
	//rejoin user by Shop Owner
	var rejoin_id;
	$(".rejoin_user").click(function(e)
	{
		if (confirm("Are you sure you want to Rejoin User")) {
			rejoin_id=this.id;
			 var info = 'rejoinUser=' + rejoin_id;
			 $.ajax({
			     type: "POST",
			     url: "<?php echo base_url()?>/ShopOwner/rejoinUser",
			     data: info,
			     success: function(data)
				 {
				     $("#"+rejoin_id+"").parent().parent().remove();
				     $("#Suspendtb tbody").html(data);
				 }
			});
		}
	});

</script>