<?php $empShopIds=explode(',',$empshop->shop_ids);?>
<div class="main-content">
<?php
$this->load->view('admin/page_header');?>
	<div class="page-title">
		
		<div class="title-env">
			<h1 class="title">Stockage</h1>
		</div>	
	</div>
		<div>
			<ul class="nav nav-tabs nav-tabs-justified">
				<li class="active">
					<a href="#profile-3" data-toggle="tab">
						<span class="visible-xs"><i class="fa-user"></i></span>
						<span class="hidden-xs">Stockage</span>
					</a>
				</li>
			</ul>
			<div class="tab-content">
				<div class="tab-pane active" id="profile-3">
					<div class="panel-body">
						<form  action="" method="post" class="form-horizontal" role="form">
						<?php if($this->session->userdata('user_roll')==1){?>
						<div class="form-group">
							<label for="field-1" class="col-sm-2 control-label">Select Shop </label>
							<div class="col-sm-10">
								<select class="form-control" id="shop" name="shop_id">
									<option value="0">Select</option>
									<?php foreach($shops as $shop){
										if(in_array($shop->id,$empShopIds))
										echo "<option value='$shop->id'>$shop->name</option>";
									}?>
								</select>
							</div>
						</div>
						<div class="form-group-separator"></div>
						<?php }
								else{
									  $shopID=$this->session->userdata('shop_id');
									echo "<input type='hidden' id='shop' name='shop_id' value='$shopID' /> ";
								}?>
						<div class="form-group">
							<div class="panel-body">
								<table  class="table table-striped table-bordered example-1" id="productTable" cellspacing="0" width="100%">
									<thead>
										<tr>
											<th>Product Name</th>
											<th>Produce Quantity</th>
											<th>Sale</th>
											<th>Wastage</th>
											<th>Total</th>
										</tr>
									</thead>
									<tbody>
									</tbody>
								</table>
							</div>
						</div>
						<div class="form-group-separator"></div>
						<div class="btn-group" id="stockageSubmit">					
							<input type="submit" name="submit" id="submit" class="btn btn-success" value="Add Stockage">
						</div>
						</form>
					</div>
				</div>
			</div>
		</div>
</div>
	<!-- Bottom Scripts -->
	<script type="text/javascript">
	jQuery(document).ready(function()
    {
		$("#stockageSubmit").hide();
		$("#shop").change(function(e)
		{
			var shopId=this.value;
				var info='action=shop&shopId='+shopId;
				//alert(info);
				$.ajax({
					type: "POST",
					url: "<?php echo base_url()?>stockage/ajax",
					data: info,
					success: function(data)
					{
						$('#productTable tbody').html(data);
						var check=$('#checkStockage').val();
						var pid=$('#pid').val();
						if(check==0 && !typeof(pid)  === "undefined")
						$("#stockageSubmit").show();
					}
				});
			if(shopId==0)
				$("#stockageSubmit").hide();
			});
		$("#shop").ready(function(){
    	
    	var shopId=$("#shop").val();
				var info='action=shop&shopId='+shopId;
				//alert(info);
				$.ajax({
					type: "POST",
					url: "<?php echo base_url()?>stockage/ajax",
					data: info,
					success: function(data)
					{
						$('#productTable tbody').html(data);
						var check=$('#checkStockage').val();
						var pid=$('#pid').val();
						if(check==0 && !typeof(pid)  === "undefined")
						$("#stockageSubmit").show();
					}
				});
			if(shopId==0)
				$("#stockageSubmit").hide();
    });
	});
	</script>
	</body>
</html>