<script type="text/javascript">
$(document).ready(function() {
	/// make loader hidden in start
	$('#email').blur(function(){
		var email_val = $("#email").val();
		$.post( "<?php echo base_url()?>user/checkEmail", { email: email_val })
		  .done(function( data ) {
		    if(data == "true")
		    	$("#email_error").show();
		    else
		    	$("#email_error").hide();
		});
	});
});   
</script>
<div class="main-content">
			<?php $this->load->view('page_header');?>
			
			<div class="page-title">
				
				<div class="title-env">
					<h1 class="title">Registration Page</h1>
					<p class="description">Users can register from this page</p>
				</div>
				
					<div class="breadcrumb-env">
					
								<ol class="breadcrumb bc-1" >
									<li>
							<a href="ReportingDashboard.html"><i class="fa-home"></i>Home</a>
						</li>
								<li>
						
										<a href="tables-basic.html">Tables</a>
								</li>
							<li class="active">
						
										<strong>Basic Tables</strong>
								</li>
								</ol>
								
				</div>
					
			</div>
<div class="row">
<div class="col-sm-12">
					
					<div class="panel panel-default">
						<div class="panel-heading">
							<h3 class="panel-title">Default form inputs</h3>
							<div class="panel-options">
								<a data-toggle="panel" href="#">
									<span class="collapse-icon">&ndash;</span>
									<span class="expand-icon">+</span>
								</a>
								<a data-toggle="remove" href="#">
									×
								</a>
							</div>
						</div>
						<div class="panel-body">
							<form  action="<?php echo base_url();?>admin/register" method="post" class="form-horizontal" role="form">
								<div class="form-group">
									<label for="field-1" class="col-sm-2 control-label">Username(Email)</label>
									<div class="col-sm-10">
										<input type="text" placeholder="Email" name="email" id="email" class="form-control">

									</div>
									<?php echo form_error('email'); ?>
									<span id="email_error" class="error" style="display:none">Username Already Exists</span>
								</div>
								
								<div class="form-group-separator"></div>
								
								<div class="form-group">
									<label for="field-2" class="col-sm-2 control-label">Password</label>
									<div class="col-sm-10">
										<input type="password" placeholder="Password" name="password" id="field-2" class="form-control">
									</div>
									<?php echo form_error('password'); ?>
								</div>

								<div class="form-group-separator"></div>
								
								<div class="form-group">
									<label for="field-2" class="col-sm-2 control-label">First Name</label>
									<div class="col-sm-10">
										<input type="text" placeholder="First Name" name="fname" id="field-2" class="form-control">
									</div>
									<?php echo form_error('fname'); ?>
								</div>

								<div class="form-group-separator"></div>
								
								<div class="form-group">
									<label for="field-2" class="col-sm-2 control-label">Last Name</label>
									<div class="col-sm-10">
										<input type="text" placeholder="Last Name" name="lname" id="field-2" class="form-control">
									</div>
										<?php echo form_error('lname'); ?>
								</div>

								<div class="form-group-separator"></div>
								
								<div class="form-group">
									<label for="field-2" class="col-sm-2 control-label">Phone</label>
									<div class="col-sm-10">
										<input type="text" placeholder="Phone" name="phone" id="field-2" class="form-control">
									</div>
									<?php echo form_error('phone'); ?>
								</div>

								<div class="form-group-separator"></div>
								
								<div class="form-group">
									<label for="field-2" class="col-sm-2 control-label">Address</label>
									<div class="col-sm-10">
										<input type="text" placeholder="Address" name="address" id="field-2" class="form-control">
									</div>
									<?php echo form_error('address'); ?>
								</div>

								<div class="form-group-separator"></div>
								
								<div class="form-group">
									<label for="field-2" class="col-sm-2 control-label">Employement Date</label>
									<div class="col-sm-10">
										<input type="text" placeholder="Employement Date" name="employement_date" id="field-2" class="form-control">
									</div>
									<?php echo form_error('employement_date'); ?>
								</div>


								<div class="form-group-separator"></div>
								
								<div class="form-group">
									<label for="field-2" class="col-sm-2 control-label">Termination Date</label>
									<div class="col-sm-10">
										<input type="text" placeholder="Termination Date" name="termination_date" id="field-2" class="form-control">
									</div>
									<?php echo form_error('termination_date'); ?>
								</div>

								<div class="form-group-separator"></div>
								
								<div class="form-group">
									<label for="field-2" class="col-sm-2 control-label">Position</label>
									<div class="col-sm-10">
										<input type="text" placeholder="Position" name="position" id="field-2" class="form-control">
									</div>
									<?php echo form_error('position'); ?>
								</div>

								<div class="btn-group">					
									<input type="submit" name="submit" class="btn btn-success" value="Register">
								</div>
								
							</form>
							
						</div>
					</div>
					
				</div>  

</div>