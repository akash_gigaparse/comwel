<div class="main-content">
	<?php $this->load->view('admin/page_header');?>
		<div class="page-title">
			
			<div class="title-env">
				<h1 class="title">Customer</h1>
			</div>
		</div>
			<h3></h3>
			<br />
			
			<div class="row">
			
				<div class="col-md-12">
					
					<ul class="nav nav-tabs nav-tabs-justified">
						<li class="active">
							<a href="#home-3" data-toggle="tab">
								<span class="visible-xs"><i class="fa-home"></i></span>
								<span class="hidden-xs"> Create Customer </span>
							</a>
						</li>
						<li>
							<a href="#profile-3" data-toggle="tab">
								<span class="visible-xs"><i class="fa-user"></i></span>
								<span class="hidden-xs">Customer List</span>
							</a>
						</li>
					<!--
						<li>
							<a href="#messages-3" data-toggle="tab">
								<span class="visible-xs"><i class="fa-envelope-o"></i></span>
								<span class="hidden-xs">Suspend User </span>
							</a>
						</li>
						<li>
							<a href="#settings-3" data-toggle="tab">
								<span class="visible-xs"><i class="fa-cog"></i></span>
								<span class="hidden-xs">Create Roles</span>
							</a>
						</li>
						<li>
							<a href="#inbox-3" data-toggle="tab">
								<span class="visible-xs"><i class="fa-bell-o"></i></span>
								<span class="hidden-xs">Roles Access</span>
							</a>
						</li>-->
					</ul>
					
					<div class="tab-content">
						<div class="tab-pane active" id="home-3">
							<div class="panel-body">

							<form  action="<?php echo base_url();?>customer/newCustomer" method="post" class="form-horizontal" role="form">
							
									<div class="form-group">
										<label for="field-1" class="col-sm-2 control-label">Customer Name *</label>
							
										<div class="col-sm-10">

											<input type="text" placeholder="Customer Name" name="name" id="name" class="form-control" value="<?php if(!empty($postData)) echo $postData['name'];?>">

										</div>
							
										<?php echo form_error('name'); ?>
										<span id="email_error" class="error" style="display:none"> </span>
									</div>
									
									<div class="form-group-separator"></div>
									
									<div class="form-group">
										<label for="field-2" class="col-sm-2 control-label">Phone *</label>
										<div class="col-sm-10">
											<input type="text" placeholder="phone" name="phone" id="field-2" class="form-control" value="<?php if(!empty($postData)) echo $postData['phone'];?>">
										</div>
										<?php echo form_error('phone'); ?>
									</div>

									<div class="form-group-separator"></div>
									
									<div class="form-group">
										<label for="field-2" class="col-sm-2 control-label">Email </label>
										<div class="col-sm-10">
											<input type="text" placeholder="Email" name="email" id="field-2" class="form-control" value="<?php if(!empty($postData)) echo $postData['email'];?>">
										</div>
										<?php echo form_error('email'); ?>
									</div>
									
									<div class="form-group-separator"></div>
									
									<div class="form-group">
										<label for="field-2" class="col-sm-2 control-label">Wholesale </label>
										<div class="col-sm-10">
											
											<input type="checkbox" placeholder="Wholesale" name="wholesale" id="field-2" class="form-control wholesale" value="1" <?php if(!empty($postData['wholesale'])){ echo "checked";}?>>
										</div>
										<?php echo form_error('wholesale'); ?>
									</div>

									<div style="display:none" class="check">	
										<div class="form-group-separator"></div>
										
										<div class="form-group">
											<label for="field-2" class="col-sm-2 control-label">ABN*</label>
											<div class="col-sm-10">
												<input type="text" placeholder="ABN" name="abn" id="field-2" class="form-control" value="<?php if(!empty($postData)) echo $postData['abn'];?>">
											</div>
											<?php echo form_error('abn'); ?>
										</div>


										<div class="form-group-separator"></div>
										
										<div class="form-group">
											<label for="field-2" class="col-sm-2 control-label">Company Name*</label>
											<div class="col-sm-10">
												<input type="text" placeholder="Company Name" name="company_name" id="field-2" class="form-control" value="<?php if(!empty($postData)) echo $postData['company_name'];?>">
											</div>
											<?php echo form_error('company_name'); ?>
										</div>
										<div class="form-group-separator"></div>
										
										<div class="form-group">
											<label for="field-2" class="col-sm-2 control-label">Address *</label>
											<div class="col-sm-10">
												<input type="text" placeholder="Address" name="address" id="field-2" class="form-control" value="<?php if(!empty($postData)) echo $postData['address'];?>">
											</div>
											<?php echo form_error('address'); ?>
										</div>

										<div class="form-group-separator"></div>
										
										<div class="form-group">
											<label for="field-2" class="col-sm-2 control-label">Contact *</label>
											<div class="col-sm-10">
												<input type="text" placeholder="Contact" name="contact" id="field-2" class="form-control" value="<?php if(!empty($postData)) { echo $postData['contact']; } else { }?>">
											</div>
											<?php echo form_error('contact'); ?>
										</div>
									<div class="form-group-separator"></div>

									
									<div class="form-group">
										<label for="field-2" class="col-sm-2 control-label">Suburb </label>
										<div class="col-sm-10">
											<input type="text" placeholder="Suburb" name="suburb" id="field-2" class="form-control" value="<?php if(!empty($postData)) echo $postData['suburb'];?>">
										</div>
										<?php echo form_error('suburb'); ?>
									</div>


									<div class="form-group-separator"></div>
									
									<div class="form-group">
										<label for="field-2" class="col-sm-2 control-label">State </label>
										<div class="col-sm-10">
											<input type="text" placeholder="State" name="state" id="field-2" class="form-control" >
										</div>
											<?php echo form_error('state'); ?>
									</div>

									</div>
									<div class="form-group-separator"></div>
									
									<div class="form-group">
										<label for="field-2" class="col-sm-2 control-label">Mobile </label>
										<div class="col-sm-10">
											<input type="text" placeholder="Mobile" name="mobile" id="field-2" class="form-control" value="<?php if(!empty($postData)) echo $postData['mobile'];?>">
										</div>
										<?php echo form_error('mobile'); ?>
									</div>
									<div class="form-group-separator"></div>
									
									<div class="form-group">
										<label for="field-2" class="col-sm-2 control-label">Delivery Info </label>
										<div class="col-sm-10">
											<textarea class="form-control autogrow" name="delivery_info" id="about" data-validate="minlength[10]" rows="5" placeholder="" > </textarea>
										</div>
										<?php echo form_error('delivery_info'); ?>
									</div>

								<div class="form-group shopSelect">
								<label class="control-label">Shop List</label>
								
									
								<script type="text/javascript">
									jQuery(document).ready(function($)
									{
										$("#s2example-2").select2({
											placeholder: 'Select shops',
											allowClear: true
										}).on('select2-open', function()
										{
											// Adding Custom Scrollbar
											$(this).data('select2').results.addClass('overflow-hidden').perfectScrollbar();
										});
										
									});
								</script>
								
								<select class="form-control" id="s2example-2" multiple name="shop[]" >
									<optgroup label="Select">
									<?php foreach($shops as $key=>$val){ 
										if($this->session->userdata('user_roll')==5)
											{ ?>
												<option value='<?php echo $val->id;?>' <?php if(!empty($postData['shop'])) { if(in_array($val->id,$postData['shop'])) { echo "selected='selected'"; } } ?>><?php echo $val->name;?></option>

										<?php }else{ ?>
									<?php foreach($val as $keys=>$vals){ ?>
									<option value='<?php echo $vals->id;?>' <?php if(!empty($postData['shop'])) { if(in_array($vals->id,$postData['shop'])) { echo "selected='selected'"; } } ?>><?php echo $vals->name;?></option>
									<?php  } } }?>	
									</optgroup>
								</select>
									<?php echo form_error('shop[]'); ?>
							</div>

				
									<div class="btn-group">					
										<input type="submit" name="submit" class="btn btn-success" value="Register">
									</div>
									
								</form>
							</div>
						</div>

						<div class="tab-pane" id="profile-3">
						<!--start html for register-->
							<div class="panel-body">
								<table class="table table-model-2 table-hover" id="rejointable">
							<thead>
								<tr>
									<th>#</th>
									<th>Full Name</th>
									<th>Phone</th>
									<th>Email</th>
									<th>Edit</th>
									<th>Delete</th>
								</tr>
							</thead>
							<tbody>
							<?php

							//echo '<pre>';print_r($customerList);die;
							foreach($customerList as $row){
								?>
								<tr>
									<td><?php echo $row->id;?></td>
									<td><?php echo $row->name;?></td>
									<td><?php echo $row->phone;?></td>
									<td><?php echo $row->email;?></td>
									<td><a class="btn btn-success" role="button" href="<?php  echo base_url()?>Customer/editCustomer?id=<?php echo $row->id;?>">Edit</a></td>
									<td><a id="<?php echo $row->id;?>" class="btn btn-danger removeCustomer" values="<?php echo $row->id;?>" data-toggle="modal" role="button" href="#">Delete</a></td>
								</tr>
							<?php } ?>						
							</tbody>
						</table>
								
								</div>
						</div>

						<div class="tab-pane" id="messages-3">
							<div class="panel-body">
								
							</div>
							
						</div>
						
						<div class="tab-pane" id="settings-3">
								
							<div class="panel-body">
							
							
						</div>
						</div>
					
						<div class="tab-pane" id="inbox-3">
							<div class="panel-body">
							
							
						</div>	
						
						</div>
					</div>
					
					
				</div>
			</div>
			
			
			<!-- Main Footer -->
			<!-- Choose between footer styles: "footer-type-1" or "footer-type-2" -->
			<!-- Add class "sticky" to  always stick the footer to the end of page (if page contents is small) -->
			<!-- Or class "fixed" to  always fix the footer to the end of page -->

		</div>
		
			
		
	<!-- Bottom Scripts -->
	
	<script>
//account parmanently remove functionality
jQuery(document).ready(function()
    {
	var remove_id;
	$(".removeCustomer").click(function(e)
	{
		if (confirm("Are you sure you want to Delete")) {
			remove_id=this.id;
			 var info = 'remove=' + remove_id;
			 $.ajax({
			     type: "POST",
			     url: "<?php echo base_url()?>/Customer/deleteCustomer",
			     data: info,
			     success: function(data)
				 {
				 	//alert("Recorde Delete successfully");
				     $("#"+remove_id+"").parent().parent().remove();
				 }
			});
		}
	});
	/*
	var suspend_id;
	$(".suspend").click(function(e)
	{
		if (confirm("Are you sure you want to Suspend")) {
			suspend_id=this.id;
			 var info = 'suspend=' + suspend_id;
			 $.ajax({
			     type: "POST",
			     url: "<?php echo base_url()?>/Admin/suspendUser",
			     data: info,
			     success: function(data)
				 {
				 	//alert("Recorde Delete successfully");
				     $("#"+suspend_id+"").parent().parent().remove();
				 }
			});
		}
	});
	var rejoin_id;
	$(".rejoin").click(function(e)
	{
		if (confirm("Are you sure you want to Rejoin Employee")) {
			rejoin_id=this.id;
			 var info = 'rejoin=' + rejoin_id;
			 $.ajax({
			     type: "POST",
			     url: "<?php echo base_url()?>/Admin/rejoinUser",
			     data: info,
			     success: function(data)
				 {
				     $("#"+rejoin_id+"").parent().parent().remove();
				 }
			});
		}
	});*/
});
</script>
<script type="text/javascript">
function myFunction(role,id)
{
	
	
    $.post( "<?php echo base_url()?>admin/assignroles", { id: id,role:role } );
}



$( ".wholesale" ).change(function() {
    var $input = $( this );
    if(($input.is( ":checked" ))==true){
    $(".check").show();
    }else{
	 $(".check").hide();
    }
}).change();
</script>


</body>
</html>