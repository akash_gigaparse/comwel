<?php //echo '<pre>';print_r($customerData);
//echo '<pre>';print_r($postData);
//die("as");
 $shop_id=(explode(",",$customerData->shop_ids));
// echo '<pre>'; print_r($shop_id);die;
 //echo $shop_id;die;
?>
<div class="main-content">
<?php $this->load->view('admin/page_header');?>
	<div class="page-title">
		<div class="title-env">
			<h1 class="title">Edit Customer</h1>
			<p class="description"></p>
		</div>
		
	</div>
	<div class="row">
		<div class="col-sm-12">
			<div class="panel panel-default">
				<div class="panel-heading">
					<h3 class="panel-title"></h3>
					<!--<div class="panel-options">
						<a data-toggle="panel" href="#">
							<span class="collapse-icon">&ndash;</span>
							<span class="expand-icon">+</span>
						</a>
						<a data-toggle="remove" href="#">
							×
						</a>
					</div>-->
				</div>
				<div class="panel-body">
					<form  action="" method="post" class="form-horizontal" role="form">
						<div class="form-group">
							<label for="field-1" class="col-sm-2 control-label">Customer Name *</label>
				
							<div class="col-sm-10">

								<input type="text" placeholder="Customer Name" name="name" id="name" class="form-control" value="<?php if(!empty($postData)) { echo $postData['name']; }else { echo $customerData->name; }?>">

							</div>
				
							<?php echo form_error('name'); ?>
							<span id="email_error" class="error" style="display:none"> </span>
						</div>
						
						<div class="form-group-separator"></div>
						
						<div class="form-group">
							<label for="field-2" class="col-sm-2 control-label">Phone *</label>
							<div class="col-sm-10">
								<input type="text" placeholder="phone" name="phone" id="field-2" class="form-control" value="<?php if(!empty($postData)) { echo $postData['phone']; }else { echo $customerData->phone; }?>">
							</div>
							<?php echo form_error('phone'); ?>
						</div>

						<div class="form-group-separator"></div>
						
						<div class="form-group">
							<label for="field-2" class="col-sm-2 control-label">Email </label>
							<div class="col-sm-10">
								<input type="text" placeholder="Email" name="email" id="field-2" class="form-control" value="<?php if(!empty($postData)) { echo $postData['email']; }else { echo $customerData->email; } ?>">
							</div>
							<?php echo form_error('email'); ?>
						</div>
						
						<div class="form-group-separator"></div>
						
						<div class="form-group">
							<label for="field-2" class="col-sm-2 control-label">Wholesale </label>
							<div class="col-sm-10">
								
								<input type="checkbox" placeholder="Wholesale" name="wholesale" id="field-2" class="form-control wholesale" value="1" <?php  if(!empty($customerData->wholesale))   { echo "checked"; }else if(!empty($postData['wholesale'])){ echo "checked";}?>>
							</div>
							<?php echo form_error('wholesale'); ?>
						</div>

						<div <?php if(empty($postData->wholesale)){ echo 'style="display:none"'; }?> class="check">	
							<div class="form-group-separator"></div>
							
							<div class="form-group">
								<label for="field-2" class="col-sm-2 control-label">ABN*</label>
								<div class="col-sm-10">
									<input type="text" placeholder="ABN" name="abn" id="field-2" class="form-control" value="<?php if(!empty($postData)) { echo $postData['abn']; }else { echo $customerData->abn; } ?>">
								</div>
								<?php echo form_error('abn'); ?>
							</div>


							<div class="form-group-separator"></div>
							
							<div class="form-group">
								<label for="field-2" class="col-sm-2 control-label">Company Name*</label>
								<div class="col-sm-10">
									<input type="text" placeholder="Company Name" name="company_name" id="field-2" class="form-control" value="<?php if(!empty($postData)) { echo $postData['company_name']; }else { echo $customerData->company_name; }?>">
								</div>
								<?php echo form_error('company_name'); ?>
							</div>
							<div class="form-group-separator"></div>
							
							<div class="form-group">
								<label for="field-2" class="col-sm-2 control-label">Address *</label>
								<div class="col-sm-10">
									<input type="text" placeholder="Address" name="address" id="field-2" class="form-control" value="<?php if(!empty($postData)) { echo $postData['address']; }else {echo $customerData->address; }?>">
								</div>
								<?php echo form_error('address'); ?>
							</div>

							<div class="form-group-separator"></div>
							
							<div class="form-group">
								<label for="field-2" class="col-sm-2 control-label">Contact *</label>
								<div class="col-sm-10">
									<input type="text" placeholder="Contact" name="contact" id="field-2" class="form-control" value="<?php if(!empty($postData)) { echo $postData['contact']; }else { echo $customerData->contact; }?>">
								</div>
								<?php echo form_error('contact'); ?>
							</div>
						<div class="form-group-separator"></div>

						
						<div class="form-group">
							<label for="field-2" class="col-sm-2 control-label">Suburb </label>
							<div class="col-sm-10">
								<input type="text" placeholder="Suburb" name="suburb" id="field-2" class="form-control" value="<?php if(!empty($postData)) { echo $postData['suburb']; }else { echo $customerData->suburb; }?>">
							</div>
							<?php echo form_error('suburb'); ?>
						</div>


						<div class="form-group-separator"></div>
						
						<div class="form-group">
							<label for="field-2" class="col-sm-2 control-label">State </label>
							<div class="col-sm-10">
								<input type="text" placeholder="State" name="state" id="field-2" class="form-control" >
							</div>
								<?php echo form_error('state'); ?>
						</div>

						</div>
						<div class="form-group-separator"></div>
						
						<div class="form-group">
							<label for="field-2" class="col-sm-2 control-label">Mobile </label>
							<div class="col-sm-10">
								<input type="text" placeholder="Mobile" name="mobile" id="field-2" class="form-control" value="<?php if(!empty($postData)) { echo $postData['mobile']; }else { echo $customerData->mobile; }?>">
							</div>
							<?php echo form_error('mobile'); ?>
						</div>
						<div class="form-group-separator"></div>
						
						<div class="form-group">
							<label for="field-2" class="col-sm-2 control-label">Delivery Info </label>
							<div class="col-sm-10">
								<textarea class="form-control autogrow" name="delivery_info" id="about" data-validate="minlength[10]" rows="5" placeholder="" ><?php if(!empty($postData)) { echo $postData['delivery_info']; }else { echo $customerData->delivery_info; }?> </textarea>
							</div>
							<?php echo form_error('delivery_info'); ?>
						</div>

					<div class="form-group shopSelect">
					<label class="control-label">Shop List</label>
					
						
					<script type="text/javascript">
						jQuery(document).ready(function($)
						{
							$("#s2example-2").select2({
								placeholder: 'Select shops',
								allowClear: true
							}).on('select2-open', function()
							{
								// Adding Custom Scrollbar
								$(this).data('select2').results.addClass('overflow-hidden').perfectScrollbar();
							});
							
						});
					</script>

					
					<select class="form-control" id="s2example-2" multiple name="shop[]" >
						<optgroup label="Select">
						<?php foreach($shops as $key=>$val){ 
							if($this->session->userdata('user_roll')==5)
								{ ?>
									<option value='<?php echo $val->id;?>' <?php if(!empty($postData['shop'])) { if(in_array($val->id,$postData['shop'])) { echo "selected='selected'"; } } ?>><?php echo $val->name;?></option>

							<?php }else{ ?>
						<?php foreach($val as $keys=>$vals){ ?>
						<option value='<?php echo $vals->id;?>' <?php if(!empty($postData['shop'])) { if(in_array($vals->id,$postData['shop'])) { echo "selected='selected'"; } }else{ if(in_array($vals->id,$shop_id)) { echo "selected='selected'"; } } ?>><?php echo $vals->name;?></option>
						<?php  } } }?>	
						</optgroup>
					</select>
						<?php echo form_error('shop[]'); ?>
				</div>
				<div class="btn-group">					
					<input type="submit" name="submit" class="btn btn-success" value="Update">
				</div>
						
					</form>
				</div>

			</div>
		</div>  
	</div>
<script type="text/javascript">
$( ".RollChange" ).change(function () {
		var roll=$( this ).val();
		
			$(".shopSelect").show();
		
	})

$( ".wholesale" ).change(function() {
    var $input = $( this );
    if(($input.is( ":checked" ))==true){
    $(".check").show();
    }else{
	 $(".check").hide();
    }
}).change();
</script>
