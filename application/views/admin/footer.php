	<!-- Main Footer -->
	<!-- Choose between footer styles: "footer-type-1" or "footer-type-2" -->
	<!-- Add class "sticky" to  always stick the footer to the end of page (if page contents is small) -->
	<!-- Or class "fixed" to  always fix the footer to the end of page -->
	<footer class="main-footer sticky footer-type-1">
		
		<div class="footer-inner">
		
			<!-- Add your copyright text here -->
		<!--	<div class="footer-text">
				&copy; 2014 
				<strong>Xenon</strong> 
				theme by <a href="http://laborator.co" target="_blank">Laborator</a> - <a href="http://themeforest.net/item/xenon-bootstrap-admin-theme/9059661?ref=Laborator" target="_blank">Purchase for only <strong>23$</strong></a>
			</div>
		-->	
			
			<!-- Go to Top Link, just add rel="go-top" to any link to add this functionality -->
			
			
		</div>
		
	</footer>
</div>

		<!-- conversation template -->
	<div class="chat-conversation">
		
<!-- Imported styles on this page -->
	<link rel="stylesheet" href="assets/js/datatables/dataTables.bootstrap.css">


	<link rel="stylesheet" href="<?php echo base_url();?>assets/js/daterangepicker/daterangepicker-bs3.css">
	<link rel="stylesheet" href="<?php echo base_url();?>assets/js/select2/select2.css">
	<link rel="stylesheet" href="<?php echo base_url();?>assets/js/select2/select2-bootstrap.css">
	<link rel="stylesheet" href="<?php echo base_url();?>assets/js/multiselect/css/multi-select.css">
	<!-- Bottom Scripts -->
	<script src="<?php echo base_url();?>assets/js/bootstrap.min.js"></script>
	<script src="<?php echo base_url();?>assets/js/TweenMax.min.js"></script>
	<script src="<?php echo base_url();?>assets/js/resizeable.js"></script>
	<script src="<?php echo base_url();?>assets/js/joinable.js"></script>
	<script src="<?php echo base_url();?>assets/js/xenon-api.js"></script>
	<script src="<?php echo base_url();?>assets/js/xenon-toggles.js"></script>
	<script src="<?php echo base_url();?>assets/js/datepicker/bootstrap-datepicker.js"></script>

	</div>
		<!-- end: Chat Section -->
</div>
		
</div>

<script src="<?php echo base_url();?>assets/js/select2/select2.min.js"></script>
	<!-- JavaScripts initializations and stuff -->
<script src="<?php echo base_url();?>assets/js/xenon-custom.js"></script>





	<script src="<?php echo base_url();?>assets/js/datatables/js/jquery.dataTables.min.js"></script>
<!-- Imported scripts on this page -->
	<script src="<?php echo base_url();?>assets/js/datatables/dataTables.bootstrap.js"></script>
	<script src="<?php echo base_url();?>assets/js/datatables/yadcf/jquery.dataTables.yadcf.js"></script>
	<script src="<?php echo base_url();?>assets/js/datatables/tabletools/dataTables.tableTools.min.js"></script>

	<script> var ajax="<?php echo base_url()?>order/wholesale";</script>
	 <script src="<?php echo base_url();?>assets/js/jquery.min.js"></script>
	<script src="<?php echo base_url();?>assets/js/custom/jquery-ui.min.js"></script>
	
	<script> var ajaxVal="<?php echo base_url();?>"+"order/wholesale";</script>
	<script src="<?php echo base_url();?>assets/js/custom/auto.js"></script>
	
	<!-- Imported scripts on this page -->
	<script src="<?php echo base_url();?>assets/js/devexpress-web-14.1/js/globalize.min.js"></script>
	<script src="<?php echo base_url();?>assets/js/devexpress-web-14.1/js/dx.chartjs.js"></script>


<script type="text/javascript">
	jQuery(document).ready(function()
    {
	var remove_id;
	$("#customer").change(function(e)
	{
		
			var cutomerId=this.value;
		
			var info='action=lastorder&getCustomerId='+cutomerId;
			//alert(info);
			 $.ajax({
			     type: "POST",
			     url: "<?php echo base_url()?>order/wholesale",
			     data: info,
			     success: function(data)
				 {
				 	$('#productTable tbody').html(data);
				 
					calculateTotal();

				    var gt=$('#subTotal').val();
					//alert(gt);
					if(gt==0.00)
					{
						$('#addmore').addClass("addmore");
						$('.addmore').removeAttr("id");
						
						$('.changesNo1').attr("readonly","readonly");
						$('#customcheck').val(0);

					}
					else{
							$('.addmore').attr("id","addmore");
							$('#addmore').removeClass("addmore");
							$('.changesNo1').removeAttr("readonly");
							$('#customcheck').val(1);
					} 
				 }
			});

					
		
	});
});
</script>

</body>
</html>