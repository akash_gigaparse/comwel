<div class="main-content">
<?php $this->load->view('page_header');?>
	<div class="page-title">
	
		<div class="title-env">
			<h1 class="title">Employee List</h1>
			<p class="description"></p>
		</div>
	
		<div class="breadcrumb-env">
		
			<ol class="breadcrumb bc-1" >
				<li>
					<a href="ReportingDashboard.html"><i class="fa-home"></i>Home</a>
				</li>
				<li>

						<a href="tables-basic.html">Tables</a>
				</li>
				<li class="active">

					<strong>Basic Tables</strong>
				</li>
			</ol>
					
		</div>
		
	</div>
	<div class="row">
		<div class="col-sm-12">
					
			<div class="panel panel-default">
				<div class="panel-heading">
					<h3 class="panel-title"></h3>
					<div class="panel-options">
						<a data-toggle="panel" href="#">
							<span class="collapse-icon">&ndash;</span>
							<span class="expand-icon">+</span>
						</a>
						<a data-toggle="remove" href="#">
							×
						</a>
					</div>
				</div>
				<div class="panel-body">
					<table class="table table-model-2 table-hover">
						<thead>
							<tr>
								<th>#</th>
								<th>Full Name</th>
								<th>Email</th>
								<th>Address</th>
								<th>Edit</th>
								<th>Delete</th>
								<th>Suspend User</th>
							</tr>
						</thead>
						<tbody>
						<?php
	//					echo '<pre>';print_r($this->session->userdata());die;
						foreach($value as $row){
				 		if($this->session->userdata('user_id')==$row->id)
						{

						}else{ 
								if($row->suspend==1)
								{
							?>
							<tr>
								<td><?php echo $row->id;?></td>
								<td><?php echo $row->fname ."&nbsp;".$row->lname;?></td>
								<td><?php echo $row->email;?></td>
								<td><?php echo $row->address;?></td>
								<td><a class="btn btn-success" role="button" href="<?php echo base_url()?>Admin/editUser?id=<?php echo $row->id;?>">Edit</a></td>
								<td><a id="<?php echo $row->id;?>" class="btn btn-danger remove" values="<?php echo $row->id;?>" data-toggle="modal" role="button" href="#">Delete</a></td>
								<td><a id="<?php echo $row->id;?>" class="btn btn-danger suspend" values="<?php echo $row->id;?>" data-toggle="modal" role="button" href="#">Suspend</a></td>
							
							</tr>
						<?php } } }?>						
						</tbody>
					</table>
					
				</div>
			</div>
		
		</div>  

	</div>
<script>
//account parmanently remove functionality
jQuery(document).ready(function()
 {
	var remove_id;
	$(".remove").click(function(e)
	{
		if (confirm("Are you sure you want to Delete")) {
			remove_id=this.id;
			 var info = 'remove=' + remove_id;
			 $.ajax({
			     type: "POST",
			     url: "<?php echo base_url()?>/Admin/deleteUser",
			     data: info,
			     success: function(data)
				 {
				 	//alert("Recorde Delete successfully");
				     $("#"+remove_id+"").parent().parent().remove();
				 }
			});
		}
	});
	var suspend_id;
	$(".suspend").click(function(e)
	{
		if (confirm("Are you sure you want to Suspend")) {
			suspend_id=this.id;
			 var info = 'suspend=' + suspend_id;
			 $.ajax({
			     type: "POST",
			     url: "<?php echo base_url()?>/Admin/suspendUser",
			     data: info,
			     success: function(data)
				 {
				 	//alert("Recorde Delete successfully");
				     $("#"+suspend_id+"").parent().parent().remove();
				 }
			});
		}
	});
});
</script>


