<?php //die("a");//echo '<pre>';print_r($charts);die;?>
    <script type="text/javascript" src="<?php echo base_url()?>js/raphael.js"></script>
    <script type="text/javascript" src="<?php echo base_url()?>js/jquery.enumerable.js"></script>
    <script type="text/javascript" src="<?php echo base_url()?>js/jquery.tufte-graph.js"></script>
    <link rel="stylesheet" href="<?php echo base_url()?>css/tufte-graph.css" type="text/css" media="screen" charset="utf-8" />

<style>
.label.bar-label {
    color: #000000 !important;
}
.label.axis-label{
    color: #000000 !important;
}
</style>
<div class="main-content">
<?php $this->load->view('employee/page_header');?>

	<div class="page-title">

	<div class="title-env">
		<h1 class="title">Employee Sales</h1>
		<p class="description"></p>
	</div>
	<div class="breadcrumb-env">
		<ol class="breadcrumb bc-1" >
			<li>
				<a href="ReportingDashboard.html"><i class="fa-home"></i>Home</a>
			</li>
			<li>
				<a href="tables-basic.html">Tables</a>
			</li>
			<li class="active">
				<strong>Basic Tables</strong>
			</li>
		</ol>
					
	</div>
	
</div>
<div class="row">
	<div class="col-sm-12">
		<form  action="<?php echo base_url();?>Sales/searchByDates" method="post" class="form-horizontal" role="form">
		<div class="form-group">
			<!--<label for="field-1" class="col-sm-2 control-label">User Name *</label>
			<div class="col-sm-10">
				<select class="form-control" name="user">
				<option value="All">All</option>
					<?php //if((!empty($charts) && isset($charts))) { ?>
						<option value="<?php //echo $date['user']; ?>"><?php //echo $date['user']; ?></option>
						<?php //} ?>
					<?php //foreach($users as $user){ 
						 //echo "<option value='$user->sales_person_name'>$user->sales_person_name</option>";
						// } ?>
				</select>
			</div>-->
			<br><br>
			<label for="field-1" class="col-sm-2 control-label">From Date *</label>
			<div class="col-sm-9">
				<div class="input-group">
					<input type="text" class="form-control datepicker" data-format="dd-mm-yyyy" name="date1" required value="<?php if((!empty($charts) && isset($charts))) { echo $date['date1']; }?>">
					
					<div class="input-group-addon">
						<a href="#"><i class="linecons-calendar"></i></a>
					</div>
				</div>
			</div>
			<br><br>
			<label for="field-1" class="col-sm-2 control-label">To Date *</label>
			<div class="col-sm-9">
				<div class="input-group">
					<input type="text" class="form-control datepicker" data-format="dd-mm-yyyy" name="date2" required value="<?php if((!empty($charts) && isset($charts))) { echo $date['date2']; }?>">
					
					<div class="input-group-addon">
						<a href="#"><i class="linecons-calendar"></i></a>
					</div>
				</div>
			</div>
			<br><br>
			<label for="field-1" class="col-sm-2 control-label"></label>
			<div class="col-sm-9">
				<div class="input-group">
				<input type="submit" name="submit" class="btn btn-success" value="Search">
				</div>
			</div>
			</form>
		</div>
	</div>
</div>
<br><br><br>

<?php if((!empty($charts) && isset($charts)))
{
foreach($charts as $key=>$value)
{
	$array[]=$value->totalSales;
}	
	$total=max($array)/2;

?>
<div class="row">
	<div class="col-sm-12">
	<div class="panel panel-default">
		    <div class="panel-heading">
				<h3 class="panel-title">Standard Bar</h3>
				<div class="panel-options">
					<a href="#" data-toggle="panel">
						<span class="collapse-icon">&ndash;</span>
						<span class="expand-icon">+</span>
					</a>
					<a href="#" data-toggle="remove">
						&times;
					</a>
				</div>
			</div>
			
	    <script type="text/javascript">
	      $(document).ready(function () {
	        jQuery('#awesome-graph').tufteBar({
	          data: [
	         <?php  $i=1;
	         foreach($charts as $key=>$value){ 
         		if($value->totalSales < 0)
 					continue;
				if(($value->totalSales <= $total) && ($value->totalSales > 0)) { 
					$color[$i]="#e60707";
				}
				else{
					$color[$i]="#68b828";
				}

/*					if($i!=1){
					if($total >= $value->totalSales) { 
					$color[$i]="#e60707";
					
				}
				
					else
					$color[$i]="#68b828";
				}
				else
				{
					$color[$i]="#68b828";
				}*/



				 ?>
	            [<?php  echo $value->totalSales; ?>, {label: '<?php  echo $value->sales_person_name; ?>'}],

	            <?php $i++; } ?>
	          ],
	          barWidth: 0.8,
	         // barLabel:  function(index) { return this[0] + 'x' },
	          axisLabel: function(index) { return this[1].label },
	          color:     function(index) { return [<?php for($j=1;$j<$i;$j++){ echo "'".$color[$j]."'".",";}?>][index % 10] }	

	        });
	      });
	    </script>
	    <div id='awesome-graph' class='graph' style='height: 440px; width: 100%;'></div>
		</div>

	</div>
</div>
	<?php } else {?>
<?php if((!empty($totalSales) && isset($totalSales)))
{
foreach($totalSales as $key=>$value)
{
	$array[]=$value->totalSales;
}	
	$total=max($array)/2;
/*
	$i = 1;
	foreach($totalSales as $key=>$value){
	 	if($value->totalSales < 0)
	 		continue;
	 	echo $value->totalSales;
	 	if(($value->totalSales <= $total)) {
			$color[$i]="red";
		}
		else{
			$color[$i]="green";			
		}
	$i++;
	 	echo "<br>";
	}
	echo "<pre>";print_r($color);
	//echo $total;die;
die('lfjds');	
*/ 
 ?>
<!--Default  bar graph load--> 
<div class="row">
	<div class="col-sm-12">
	    <div class="panel panel-default">
		    <div class="panel-heading">
				<h3 class="panel-title">Standard Bar</h3>
				<div class="panel-options">
					<a href="#" data-toggle="panel">
						<span class="collapse-icon">&ndash;</span>
						<span class="expand-icon">+</span>
					</a>
					<a href="#" data-toggle="remove">
						&times;
					</a>
				</div>
			</div>
			
	    <script type="text/javascript">
	      $(document).ready(function () {
	        jQuery('#awesome-graph').tufteBar({
	          data: [
	         <?php  $i=1;
	         foreach($totalSales as $key=>$value){ 
	         	//if($i!=1){
	         		if($value->totalSales < 0)
	 					continue;
					if(($value->totalSales <= $total) && ($value->totalSales > 0)) { 
						$color[$i]="#e60707";
					}
					else{
						$color[$i]="#68b828";
					}
				//}
				//else{
					//$color[$i]="#68b828";
				//}
				?>
				
	            [<?php  echo $value->totalSales; ?>, {label: '<?php  echo $value->sales_person_name; ?>'}],

	            <?php $i++;} ?>
	          ],
	          barWidth: 0.8,
	         // barLabel:  function(index) { return this[0] + 'x' },
	          axisLabel: function(index) { return this[1].label },
	          color:     function(index) { return [<?php for($j=1;$j<$i;$j++){ echo "'".$color[$j]."'".",";}?>][index % 10] }	

	        });
	      });
	    </script>
	    <div id='awesome-graph' class='graph' style='height: 440px; width: 100%;'></div>
		</div>
	</div>
</div>




<?php }	} ?>
</div>
