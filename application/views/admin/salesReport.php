<?php 
$empShopIds=explode(',',$empshop->shop_ids);
//echo '<pre>';print_r($customer_sales);die;
?>
<div class="main-content">
<?php $this->load->view('admin/page_header');?>

	<div class="page-title">

	<div class="title-env">
		<h1 class="title">Employee Sales Report</h1>
		<p class="description"></p>
	</div>
	
	
</div>
<div class="row">
	<div class="col-sm-12">
		<form  action="<?php echo base_url();?>Sales/searchByDateAndCustomer" method="post" class="form-horizontal" role="form">
		<?php if($this->session->userdata('user_roll')==1 || $this->session->userdata('user_roll')==5){?>
		<div class="form-group">
			<label for="field-1" class="col-sm-2 control-label">Shop Name *</label>
			<div class="col-sm-10">
				<select class="form-control ShopId" id="shop" name="shop_id">
				 <?php
		          	if((!empty($date['shop_id']) && isset($date['shop_id'])))
		          	{
		            foreach($shops as $shop){
		              	if($this->session->userdata('user_roll')==5)
		              	{
		                	if($shop->id==$date['shop_id']) echo "<option value=".$shop->id.">"; echo $shop->name; echo '</option>';
		              	}else{
			              	if(in_array($shop->id,$empShopIds))
			                	if($shop->id==$date['shop_id']) echo "<option value=".$shop->id.">"; echo $shop->name; echo '</option>';
			            	}
		            	}
		          	}else{ ?>
		          	<option>Select</option>
		          	<?php
		          	}
		           foreach($shops as $shop){
						if($this->session->userdata('user_roll')==5)
				            echo "<option value='$shop->id'>$shop->name</option>";
				            else{  
				            	if(in_array($shop->id,$empShopIds))
								echo "<option value='$shop->id'>$shop->name</option>";
							}
					}?>
				
				</select>
			</div>
<?php }
								else{
									  $shopID=$this->session->userdata('shop_id');
									echo "<input type='hidden' id='shop' name='shop_id' value='$shopID' /> ";
								}
								?>

			<br><br>
			<label for="field-1" class="col-sm-2 control-label">Employee Name *</label>
			<div class="col-sm-10">
				<select class="form-control" id="customer" name="customer">
						<option value="<?php if((!empty($customer_sales) && isset($customer_sales))) { echo $date['customer']; }?>"><?php if((!empty($customer_sales) && isset($customer_sales))) { echo $date['customer']; }?></option>
					
				</select>
			</div>
			

			<br><br>
			<label for="field-1" class="col-sm-2 control-label">From Date *</label>
			<div class="col-sm-9">
				<div class="input-group">
					<input type="text" class="form-control datepicker" data-format="dd-mm-yyyy" name="date1" required value="<?php if((!empty($customer_sales) && isset($customer_sales))) { echo $date['date1']; }?>">
					
					<div class="input-group-addon">
						<a href="#"><i class="linecons-calendar"></i></a>
					</div>
				</div>
			</div>
			<br><br>
			<label for="field-1" class="col-sm-2 control-label">To Date *</label>
			<div class="col-sm-9">
				<div class="input-group">
					<input type="text" class="form-control datepicker" data-format="dd-mm-yyyy" name="date2" required value="<?php if((!empty($customer_sales) && isset($customer_sales))) { echo $date['date2']; }?>">
					
					<div class="input-group-addon">
						<a href="#"><i class="linecons-calendar"></i></a>
					</div>
				</div>
			</div>
			<br><br>
			<label for="field-1" class="col-sm-2 control-label"></label>
			<div class="col-sm-9">
				<div class="input-group">
				<input type="submit" name="submit" class="btn btn-success" value="Search">
				</div>
			</div>
			</form>
		</div>
	</div>
</div>
<br><br><br>

<?php if((!empty($customer_sales) && isset($customer_sales)))
{
?>
<div class="row">
	<div class="col-sm-12">
	
		<div class="panel panel-default">
			<div class="panel-heading">

				<h4 class="panel-title">Shop Name -&nbsp;&nbsp;<?php if((!empty($date) && isset($date))) {
					foreach($shops as $shop){
						if(in_array($shop->id,$empShopIds))
							if($shop->id==$date['shop_id'])
								echo $shop->name;
							}
					 }?></h4>
				<br>
				<h4 class="panel-title">Employee Name -&nbsp;&nbsp;<?php if((!empty($date) && isset($date))) { echo $date['customer']; }?></h4>
				<br>
				<h4 class="panel-title">Date Range From -&nbsp;&nbsp; <?php if((!empty($date) && isset($date))) { echo $date['date1']."&nbsp;&nbsp;To&nbsp;&nbsp;".$date['date2']; }?></h4>
				<div class="panel-options">
					<a href="#" data-toggle="panel">
						<span class="collapse-icon">&ndash;</span>
						<span class="expand-icon">+</span>
					</a>
					<a href="#" data-toggle="remove">
						&times;
					</a>
				</div>
			</div>
			<div>
			<form name="" method="post" action="<?php echo base_url();?>Sales/csvforadmin">
			<?php if((!empty($date) && isset($date))) { foreach($shops as $shop){ if(in_array($shop->id,$empShopIds)) if($shop->id==$date['shop_id']) $shopname=$shop->name; } } ?>
				<input name='shopname' type='hidden' value="<?php echo $shopname;?>">
				<input name='date1' type='hidden' value="<?php if((!empty($date) && isset($date))) { echo $date['date1']; }?>">
				<input name='date2' type='hidden' value="<?php if((!empty($date) && isset($date))) { echo $date['date2']; }?>">
				<input name='customer' type='hidden' value="<?php if((!empty($date) && isset($date))) { echo $date['customer']; }?>">
				<input name='shop_id' type='hidden' value="<?php if((!empty($date) && isset($date))) { echo $date['shop_id']; }?>">
				<input name="csv" type="submit" value="Export CSV" class="btn btn-primary btn-small">
			</form>
			</div>
			<?php //echo "<pre>";print_r($customer_sales);die;?>
			<div class="panel-body">	
				<script type="text/javascript">
					jQuery(document).ready(function($)
					{
						if( ! $.isFunction($.fn.dxChart))
							return;
						$("#bar-1").dxChart({
							<?php if($date['customer']=='All') {?>
							dataSource: [
								
									<?php
									 foreach($customer_sales as $key=>$value)
									{ ?>
										{ day :"<?php  echo $value->sales_person_name; ?>", Totel : <?php  echo $value->Total_Dollar; ?>} ,
									<?php  } ?>
							],
						 <?php } else { ?>
						dataSource: [
								
									<?php
									 foreach($customer_sales as $key=>$value)
									{ ?>
										{ day :"<?php  echo $value->transaction_date; ?>", Totel : <?php  echo  $value->Total_Dollar; ?>} ,
									<?php  } ?>
							],
						<?php } ?>	
							series: {
								argumentField: "day",
								valueField: "Totel",
								type: "bar",
								color: '#68b828',
								name: "Totel Amount",
								label: { visible: true, }
							}
						});
						
					
					});
					
					
				</script>
				<div id="bar-1" style="height: 440px; width: 100%;"></div>
				<br />


				<!--<a href="#" id="bar-1-randomize" class="btn btn-primary btn-small">Randomize</a>-->
		</div>
		</div>
			
	</div>
	<?php 	} ?>
</div>
<script type="text/javascript">
	jQuery(document).ready(function($)
	{
		if( ! $.isFunction($.fn.dxChart))
			$(".dx-warning").removeClass('hidden');
	});
</script>

<script>
	var xenonPalette = ['#68b828','#7c38bc','#0e62c7','#fcd036','#4fcdfc','#00b19d','#ff6264','#f7aa47'];
</script>
<script>
	$('.ShopId').change(function(){
		var info = 'shopId=' + $(this).val();
		$.ajax({
		     type: "POST",
		     url: "<?php echo base_url()?>/Sales/selectCustomerById",
		     data: info,
		     success: function(data)
			 {
			 	 $('#customer').html(data);
			 }
		});
    });
    $("#shop").ready(function(){
    	var info = 'shopId=' + $("#shop").val();
    	//var productclass=$("#productclass").val();
    	//alert(productclass);
    	$.ajax({
		     type: "POST",
		     url: "<?php echo base_url()?>/Sales/selectCustomerById",
		     data: info,
		     success: function(data)
			 {
			 	 $('#customer').html(data);
			 }
		});
    });

</script>
