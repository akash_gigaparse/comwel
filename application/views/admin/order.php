<div class="main-content">
	<?php $this->load->view('page_header');?>
	<div class="page-title">
		<div class="title-env">
			<h1 class="title">Employee</h1>
		</div>
		
	</div>
	<div class="row">
			
		<div class="col-md-12">
					
			<ul class="nav nav-tabs nav-tabs-justified">
				<li class="active">
					<a href="#home-3" data-toggle="tab">
						<span class="visible-xs"><i class="fa-home"></i></span>
						<span class="hidden-xs"> Employee List </span>
					</a>
				</li>
				<li>
					<a href="#profile-3" data-toggle="tab">
						<span class="visible-xs"><i class="fa-user"></i></span>
						<span class="hidden-xs">Register</span>
					</a>
				</li>
			
				<li>
					<a href="#messages-3" data-toggle="tab">
						<span class="visible-xs"><i class="fa-envelope-o"></i></span>
						<span class="hidden-xs">Suspend User </span>
					</a>
				</li>
				<li>
					<a href="#settings-3" data-toggle="tab">
						<span class="visible-xs"><i class="fa-cog"></i></span>
						<span class="hidden-xs">Create Roles</span>
					</a>
				</li>
				<li>
					<a href="#inbox-3" data-toggle="tab">
						<span class="visible-xs"><i class="fa-bell-o"></i></span>
						<span class="hidden-xs">Roles Access</span>
					</a>
				</li>
			</ul>
					
			<div class="tab-content">
				<div class="tab-pane active" id="home-3">
					<div class="panel-body">
						<table class="table table-model-2 table-hover">
							<thead>
								<tr>
									<th>#</th>
									<th>Full Name</th>
									<th>Email</th>
									<th>Address</th>
									<th>Edit</th>
									<th>Delete</th>
									<th>Suspend User</th>
								</tr>
							</thead>
							<tbody>
							<?php
		//					echo '<pre>';print_r($this->session->userdata());die;
							foreach($value as $row){
					 		if($this->session->userdata('user_id')==$row->id)
							{

							}else{ 
									if($row->suspend==1)
									{
								?>
								<tr>
									<td><?php echo $row->id;?></td>
									<td><?php echo $row->fname ."&nbsp;".$row->lname;?></td>
									<td><?php echo $row->email;?></td>
									<td><?php echo $row->address;?></td>
									<td><a class="btn btn-success" role="button" href="<?php echo base_url()?>Admin/editUser?id=<?php echo $row->id;?>">Edit</a></td>
									<td><a id="<?php echo $row->id;?>" class="btn btn-danger remove" values="<?php echo $row->id;?>" data-toggle="modal" role="button" href="#">Delete</a></td>
									<td><a id="<?php echo $row->id;?>" class="btn btn-danger suspend" values="<?php echo $row->id;?>" data-toggle="modal" role="button" href="#">Suspend</a></td>
								
								</tr>
							<?php } } }?>						
							</tbody>
						</table>
		
					</div>
					
				</div>

				<div class="tab-pane" id="profile-3">
				<!--start html for register-->
					<div class="panel-body">
						<form  action="<?php echo base_url();?>admin/register" method="post" class="form-horizontal" role="form">
					
							<div class="form-group">
								<label for="field-1" class="col-sm-2 control-label">Username(Email) *</label>
					
								<div class="col-sm-10">

									<input type="text" placeholder="Email" name="email" id="email" class="form-control" value="<?php if(!empty($postData)) echo $postData['email'];?>">

								</div>
					
								<?php echo form_error('email'); ?>
								<span id="email_error" class="error" style="display:none">Username Already Exists *</span>
							</div>
							
							<div class="form-group-separator"></div>
							
							<div class="form-group">
								<label for="field-2" class="col-sm-2 control-label">Password *</label>
								<div class="col-sm-10">
									<input type="password" placeholder="Password" name="password" id="field-2" class="form-control" value="<?php if(!empty($postData)) echo $postData['password'];?>">
								</div>
								<?php echo form_error('password'); ?>
							</div>

							<div class="form-group-separator"></div>
							
							<div class="form-group">
								<label for="field-2" class="col-sm-2 control-label">First Name *</label>
								<div class="col-sm-10">
									<input type="text" placeholder="First Name" name="fname" id="field-2" class="form-control" value="<?php if(!empty($postData)) echo $postData['fname'];?>">
								</div>
								<?php echo form_error('fname'); ?>
							</div>

							<div class="form-group-separator"></div>
							
							<div class="form-group">
								<label for="field-2" class="col-sm-2 control-label">Last Name</label>
								<div class="col-sm-10">
									<input type="text" placeholder="Last Name" name="lname" id="field-2" class="form-control" >
								</div>
									<?php echo form_error('lname'); ?>
							</div>

							<div class="form-group-separator"></div>
							
							<div class="form-group">
								<label for="field-2" class="col-sm-2 control-label">Phone *</label>
								<div class="col-sm-10">
									<input type="text" placeholder="Phone" name="phone" id="field-2" class="form-control" value="<?php if(!empty($postData)) echo $postData['phone'];?>">
								</div>
								<?php echo form_error('phone'); ?>
							</div>

							<div class="form-group-separator"></div>
							
							<div class="form-group">
								<label for="field-2" class="col-sm-2 control-label">Address *</label>
								<div class="col-sm-10">
									<input type="text" placeholder="Address" name="address" id="field-2" class="form-control" value="<?php if(!empty($postData)) echo $postData['address'];?>">
								</div>
								<?php echo form_error('address'); ?>
							</div>

							<div class="form-group-separator"></div>
							
							<div class="form-group">
								<label for="field-2" class="col-sm-2 control-label">Employement Date*</label>
								<div class="col-sm-10">
									<input type="text" placeholder="Employement Date" name="employement_date" id="field-2" class="form-control" value="<?php if(!empty($postData)) echo $postData['employement_date'];?>">
								</div>
								<?php echo form_error('employement_date'); ?>
							</div>


							<div class="form-group-separator"></div>
							
							<div class="form-group">
								<label for="field-2" class="col-sm-2 control-label">Termination Date*</label>
								<div class="col-sm-10">
									<input type="text" placeholder="Termination Date" name="termination_date" id="field-2" class="form-control" value="<?php if(!empty($postData)) echo $postData['termination_date'];?>">
								</div>
								<?php echo form_error('termination_date'); ?>
							</div>

							<div class="form-group-separator"></div>
							
							<div class="form-group">
								<label for="field-2" class="col-sm-2 control-label">Position *</label>
								<div class="col-sm-10">
									<input type="text" placeholder="Position" name="position" id="field-2" class="form-control" value="<?php if(!empty($postData)) echo $postData['position'];?>">
								</div>
								<?php echo form_error('position'); ?>
							</div>
							<div class="form-group">
								<label for="field-2" class="col-sm-2 control-label">Roll</label>
								<div class="col-sm-10">
								<select name="roll">
									<?php foreach($roll as $key=>$val){ ?>

									<option value='<?php echo $val->id;?>' ><?php echo $val->name;?></option>
									<?php } ?>
								</select>
								</div>
								<?php echo form_error('position'); ?>
							</div>

							<div class="btn-group">					
								<input type="submit" name="submit" class="btn btn-success" value="Register">
							</div>
							
						</form>
						
					</div>
				</div>

				<div class="tab-pane" id="messages-3">
					<div class="panel-body">
						<table class="table table-model-2 table-hover">
							<thead>
								<tr>
									<th>#</th>
									<th>Full Name</th>
									<th>Email</th>
									<th>Address</th>
									<th>Suspend Date</th>
									<th>Delete</th>
									<th>Suspend User</th>
								</tr>
							</thead>
							<tbody>
							<?php
		//					echo '<pre>';print_r($this->session->userdata());die;
							foreach($value as $row){
					 		if($this->session->userdata('user_id')==$row->id)
							{

							}else{ 
									if($row->suspend==0)
									{
								?>
								<tr>
									<td><?php echo $row->id;?></td>
									<td><?php echo $row->fname ."&nbsp;".$row->lname;?></td>
									<td><?php echo $row->email;?></td>
									<td><?php echo $row->address;?></td>
									<td><?php echo $row->suspend_date;?></td>
									<td><a id="<?php echo $row->id;?>" class="btn btn-danger remove" values="<?php echo $row->id;?>" data-toggle="modal" role="button" href="#">Delete</a></td>
									<td><a id="<?php echo $row->id;?>" class="btn btn-success rejoin" values="<?php echo $row->id;?>" data-toggle="modal" role="button" href="#">Rejoin</a></td>
								
								</tr>
							<?php } } }?>						
							</tbody>
						</table>
					</div>
					
				</div>
				
				<div class="tab-pane" id="settings-3">
						
					<div class="panel-body">
						<form  action="<?php echo base_url();?>admin/createroles" method="post" class="form-horizontal" role="form">
							<div class="form-group">
								<label for="field-2" class="col-sm-2 control-label">Role Name</label>
								<div class="col-sm-10">
									<input type="text" placeholder="Enter Role Name" name="name" id="field-2" class="form-control">
								</div>
								<?php echo form_error('name'); ?>
							</div>

							<div class="btn-group">					
								<input type="submit" name="submit" class="btn btn-success" value="Submit">
							</div>
							
						</form>
					
					</div>
				</div>
			
				<div class="tab-pane" id="inbox-3">
					<div class="panel-body">
						<form  action="<?php echo base_url();?>admin/assignRoles" method="post" class="form-horizontal" role="form">
							<table class="table responsive">
								<thead>
									<tr>
										<th>Serial Number</th>
										<th>Page Name</th>
										<th>Assigned Role</th>
									</tr>
								</thead>
								
								<tbody>
								<?php $i=1;
								foreach($value2 as $pages){?>
									<tr>
										<td><?php echo $i;echo "<input type='hidden' name='ids[]' value='$pages[id]'/> ";?></td>
										<td><?php echo $pages['page'];?></td>
										<td><?php $selectedrole=explode(',',$pages['role_id']);
										//print_r($selectedrole);
										foreach($value3 as $droles){
											$rolevalue='R_'.$droles['id'];
								echo "<input type='checkbox' name='roles[]' onclick='myFunction(this.value,$pages[id])' value='$rolevalue'";
								if(in_array($rolevalue,$selectedrole))
								echo "checked='checked'";
								echo "/> $droles[name]";}?></td>
									</tr>
									
								<?php $i++;}?>
							</table>

						</form>
					</div>	
				
				</div>
			</div>
		</div>
	</div>
			<!-- Main Footer -->
			<!-- Choose between footer styles: "footer-type-1" or "footer-type-2" -->
			<!-- Add class "sticky" to  always stick the footer to the end of page (if page contents is small) -->
			<!-- Or class "fixed" to  always fix the footer to the end of page -->
</div>
</div>
	<!-- Bottom Scripts -->
	<script src="assets/js/bootstrap.min.js"></script>
	<script src="assets/js/TweenMax.min.js"></script>
	<script src="assets/js/resizeable.js"></script>
	<script src="assets/js/joinable.js"></script>
	<script src="assets/js/xenon-api.js"></script>
	<script src="assets/js/xenon-toggles.js"></script>


	<!-- JavaScripts initializations and stuff -->
	<script src="assets/js/xenon-custom.js"></script>
	<script>
//account parmanently remove functionality
jQuery(document).ready(function()
 {
	var remove_id;
	$(".remove").click(function(e)
	{
		if (confirm("Are you sure you want to Delete")) {
			remove_id=this.id;
			 var info = 'remove=' + remove_id;
			 $.ajax({
			     type: "POST",
			     url: "<?php echo base_url()?>/Admin/deleteUser",
			     data: info,
			     success: function(data)
				 {
				 	//alert("Recorde Delete successfully");
				     $("#"+remove_id+"").parent().parent().remove();
				 }
			});
		}
	});
	var suspend_id;
	$(".suspend").click(function(e)
	{
		if (confirm("Are you sure you want to Suspend")) {
			suspend_id=this.id;
			 var info = 'suspend=' + suspend_id;
			 $.ajax({
			     type: "POST",
			     url: "<?php echo base_url()?>/Admin/suspendUser",
			     data: info,
			     success: function(data)
				 {
				 	//alert("Recorde Delete successfully");
				     $("#"+suspend_id+"").parent().parent().remove();
				 }
			});
		}
	});
	var rejoin_id;
	$(".rejoin").click(function(e)
	{
		if (confirm("Are you sure you want to Rejoin Employee")) {
			rejoin_id=this.id;
			 var info = 'rejoin=' + rejoin_id;
			 $.ajax({
			     type: "POST",
			     url: "<?php echo base_url()?>/Admin/rejoinUser",
			     data: info,
			     success: function(data)
				 {
				     $("#"+rejoin_id+"").parent().parent().remove();
				 }
			});
		}
	});
});
</script>
<script type="text/javascript">
function myFunction(role,id)
{
	
	
    $.post( "<?php echo base_url()?>admin/assignroles", { id: id,role:role } );
}
</script>
</body>
</html>