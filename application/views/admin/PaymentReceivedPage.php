<?php  /*echo "<pre>";
print_r($selectShop);
die();*/?>
<div class="main-content">
<?php $this->load->view('admin/page_header');?>
	<div class="page-title">
		<div class="title-env">
			<h1 class="title">Payment Recieve Page</h1>
		</div>
	</div>

		<?php  if($this->session->userdata('user_roll')==1){
		foreach ($selectShop as $selectShopID) 
		{

			$id= $selectShopID->shop_ids;
			$shopData=explode(',', $id);

		}
		}?>

			
		<ul class="nav nav-tabs nav-tabs-justified">
			<li class="active">
							<a href="#inbox-3" data-toggle="tab">
								<span class="visible-xs"><i class="fa-user"></i></span>
								<span class="hidden-xs"></span>
							</a>
						</li>
		</ul>
		<div class="tab-content">
	<div class="tab-pane active" id="inbox-3">
	

					<div class="panel-body">
						<form id="frmUpdateData"  action="" method="post" class="form-horizontal" role="form">
							
 <?php if($this->session->userdata('user_roll')==1){?>
								<div class="form-group">
									<label for="field-1" class="col-sm-2 control-label">Shop Name </label>
									<div class="col-sm-10">
										<select class="form-control shop_select" name="shopId">
											<option>Select</option>
										<?php 	foreach ($shops as $val) {
													//iff check shop Id is match or not for login user 
												if(in_array($val->id, $shopData)) {?>
												
												<option value="<?php echo $val->id; ?>"><?php echo $val->name;?></option>
												
										<?php }else{ continue;}}?>
										
										</select>
									</div>
								</div>	
<?php }
else{
                    $shopID=$this->session->userdata('shop_id');
                    echo "<input type='hidden' id='shop' name='shop' value='$shopID' /> ";
                }?>
								<div class="form-group">
									<label for="field-1" class="col-sm-2 control-label">Customer Name </label>
									<div class="col-sm-10">
										<select class="form-control custId" id="custId" name="custId">
											<option>Select</option>
																	
										</select>
									</div>
								</div>	
								<script type="text/javascript">
										
											jQuery(document).ready(function($){
												
												//select shop on basis of multiple shopId which is assign to login user
												/*alert(user_id);

												jQuery.ajax({
															type: "POST",
															url: "<?php echo base_url(); ?>" + "/admin/selectShopIdByUserId",
															dataType: 'text',
															data: {user_id: user_id},
															success: function(response){
																
															$(".shop_select").html(response);
															} 

														});*/
												//this ajax calling for selecting shop from shop table
													$("#shop").ready(function(){
													var shopId=$("#shop").val();
													//alert(shopId);
														jQuery.ajax({
															type: "POST",
															url: "<?php echo base_url(); ?>" + "/PaymentReceivedPage/selectCustById",
															dataType: 'text',
															data: {shopId: shopId},
															success: function(response){
																
															$(".custId").html(response);
															} 

														});
													});
													$(".shop_select").change(function(){
													var shopId=$(this).val();
														jQuery.ajax({
															type: "POST",
															url: "<?php echo base_url(); ?>" + "/PaymentReceivedPage/selectCustById",
															dataType: 'text',
															data: {shopId: shopId},
															success: function(response){
																
															$(".custId").html(response);
															} 

														});
													});
													
													//this ajax is calling for selecting all pages from Page table from database
													$("#custId").change(function(){

														
														var custId=$(this).val();
														//alert(custId);
														jQuery.ajax({
															type: "POST",
															url: "<?php echo base_url(); ?>" + "/PaymentReceivedPage/orderPaided",
															dataType: 'text',
															data: {custId: custId},
															
															success: function(response){
																
																$(".hee").html(response);
															
															} 

														});
													});
													

											});
								//function for update for orders table using Orders_Id and customer_ID without loading ...
								function updateOrdersPaid(orders_Id){
									
									//Confirmation check for order is successfully paying
									alert("Your payment is successfully done..!");
									
									var custId=$('#custId').val();

										jQuery.ajax({
											type: "POST",
											url: "<?php echo base_url(); ?>" + "/PaymentReceivedPage/orderUpdatePaided",
											dataType: 'text',
											data: {custId: custId,orderid:orders_Id},
											
											success: function(response){
												
												$(".hee").html(response);
											
											} 


										});


							}
							</script> 
<table class="table responsive hee" id="hee">
          
       </table>
							
						</form>
					
					</div>	
					</div>
				</div>
</body>
</html>