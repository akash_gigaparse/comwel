<?php //echo '<pre>';print_r($shops);die; ?>

<div class="main-content">
<?php $this->load->view('admin/page_header');?>
	<div class="page-title">
		<div class="title-env">
			<h1 class="title">Shops</h1>
		</div>
		
	</div>
	
	<div class="row">
		<div class="col-md-12">
			<ul class="nav nav-tabs nav-tabs-justified">
				<li <?php if(isset($postData['submit']) && !empty($postData['submit'])) echo "class=''"; else echo "class='active'"; ?>>
					<a href="#all-shop" data-toggle="tab">
						<span class="visible-xs"><i class="fa-home"></i></span>
						<span class="hidden-xs"> Shops List </span>
					</a>
				</li>
				<?php /*<li <?php if(isset($postData['submit']) && !empty($postData['submit'])) echo "class='active'"; else echo "class=''"; ?> >
					<a href="#add-shop" data-toggle="tab">
						<span class="visible-xs"><i class="fa-user"></i></span>
						<span class="hidden-xs">Add New Shop</span>
					</a>
				</li>
				*/ 
			?>
				<li>
					<a href="#messages-3" data-toggle="tab">
						<span class="visible-xs"><i class="fa-envelope-o"></i></span>
						<span class="hidden-xs"> Suspend shops List</span>
					</a>
				</li>
				<!--
					<li>

					<a href="#settings-3" data-toggle="tab">
						<span class="visible-xs"><i class="fa-cog"></i></span>
						<span class="hidden-xs">Suspend shop</span>
					</a>
				</li>-->
			</ul>
			
			<div class="tab-content">
				<div <?php  if(isset($postData['submit']) && !empty($postData['submit'])) echo "class='tab-pane'"; else echo "class='tab-pane active'";?> id="all-shop">
								<div class="panel-body">
									<table class="table table-model-2 table-hover" id="Suspendtb">
										<thead>
											<tr>
												<th>Name </th>
												
												<th>Edit</th>
												<th>Suspend</th>
											</tr>
										</thead>
										<tbody>
										<?php
										foreach($value as $array){
											foreach($array as $row){
											if($row->suspendShop==1)
											{ ?>
											<tr>
												<td><?php echo $row->name ;?></td>
												<td><a class="btn btn-success" role="button" href="<?php echo base_url()?>ListShop/editShop?id=<?php echo $row->id;?>">Edit</a></td>
												<td><a id="<?php echo $row->id;?>" class="btn btn-danger suspend_shop" values="<?php echo $row->id;?>" data-toggle="modal" role="button" href="#">Suspend</a></td>
											</tr>
										<?php } } }?>						
										</tbody>
									</table>
								
								</div>
							</div>
							<div <?php /* if(isset($postData['submit']) && !empty($postData['submit'])) echo "class='tab-pane active'"; else echo "class='tab-pane'";?> id="add-shop">
								<div class="panel-body">
									<form  action="" method="post" class="form-horizontal" enctype="multipart/form-data" role="form">
										<div class="form-group">
											<label for="field-1" class="col-sm-2 control-label">Name</label>
											<div class="col-sm-10">
												<input type="text" placeholder="Name" name="name" id="name" value="<?php if(!empty($postData)) echo $postData['name'];?>" class="form-control">

											</div>
											<?php echo form_error('name'); ?>
										</div>

										<div class="form-group-separator"></div>
										
										<div class="form-group">
											<label for="field-2" class="col-sm-2 control-label"> Address</label>
											<div class="col-sm-10">
												<input type="text" placeholder="Enter a location" name="address" id="pac-input" class="form-control" value="<?php if(!empty($postData))echo $postData['address'];?>">
											</div>
											<?php echo form_error('address'); ?>
											<div class="col-sm-12">
			    
											    <div id="type-selector" class="controls">
												      <input type="radio" name="type" id="changetype-all" checked="checked">
												      <label for="changetype-all">All</label>

												      <input type="radio" name="type" id="changetype-establishment">
												      <label for="changetype-establishment">Establishments</label>

												      <input type="radio" name="type" id="changetype-address">
												      <label for="changetype-address">Addresses</label>

												      <input type="radio" name="type" id="changetype-geocode">
												      <label for="changetype-geocode">Geocodes</label>
											       
											    </div>
												<div id="map-canvas" ></div>
		    								</div>
											
									
										</div>
										<div class="form-group-separator"></div>
										<div class="form-group">
											<label for="field-2" class="col-sm-2 control-label">Latitude</label>
											<div class="col-sm-10">
												<input type="text" placeholder="Latitude" name="latitude" id="cityLat" class="form-control" value="<?php if(!empty($postData)) echo $postData['latitude'];?>">
											</div>
											<?php echo form_error('latitude'); ?>
										</div>								
										<div class="form-group-separator"></div>
										
										<div class="form-group">
											<label for="field-2" class="col-sm-2 control-label">Longitude</label>
											<div class="col-sm-10">
												<input type="text" placeholder="Longitude" name="longitude" id="cityLng" class="form-control" value="<?php if(!empty($postData)) echo $postData['longitude'];?>">
											</div>
											<?php echo form_error('longitude'); ?>
										</div>
										<div class="form-group-separator"></div>
										<div class="form-group">
											<label for="field-2" class="col-sm-2 control-label">  Contact Details</label>
											<div class="col-sm-10">
												<input type="text" placeholder="Contact Details" name="phone" id="field-3" class="form-control" value="<?php if(!empty($postData)) echo $postData['phone'];?>">
											</div>
											<?php echo form_error('phone'); ?>
										</div>
										
										<div class="form-group-separator"></div>
											<div class="form-group">
											<label for="field-2" class="col-sm-2 control-label"> logo </label>
											<div class="col-sm-10">
												<input type="file" placeholder="Contact Details" name="userfile" id="field-3" class="form-control">
											</div>
											<?php echo form_error('userfile'); ?>
										</div>
										

										<div class="form-group-separator"></div>
										
										
											<div class="form-group">
											<label for="field-2" class="col-sm-2 control-label"> Default Product Overhead cost</label>
											<div class="col-sm-10">
												<input type="text" placeholder=" Default Product Overhead cost" name="overhead" id="field-3" class="form-control" value="<?php if(!empty($postData)) echo $postData['overhead'];?>">
											</div>
											<?php echo form_error('overhead'); ?>
										</div>
										<div class="form-group-separator"></div>
										<div class="form-group">
											<label for="field-2" class="col-sm-2 control-label">Suburb</label>
											<div class="col-sm-10">
												<input type="text" placeholder="Suburb" name="suburb" id="field-2" class="form-control" value="<?php if(!empty($postData)) echo $postData['suburb'];?>"> 
											</div>
											<?php echo form_error('suburb'); ?>
										</div>
										<div class="form-group-separator"></div>
								
		     

										<div class="form-group-separator"></div>
										
										<div class="form-group">
											<label for="field-2" class="col-sm-2 control-label">State</label>
											<div class="col-sm-10">
												<input type="text" placeholder="State" name="state" id="field-2" class="form-control" value="<?php if(!empty($postData)) echo $postData['state'];?>">
											</div>
											<?php echo form_error('state'); ?>
										</div>

										<div class="form-group-separator"></div>
										
										<div class="form-group">
											<label for="field-2" class="col-sm-2 control-label">Postcode</label>
											<div class="col-sm-10">
												<input type="text" placeholder="Postcode" name="postcode" id="field-2" class="form-control" value="<?php if(!empty($postData)) echo $postData['postcode'];?>">
											</div>
												<?php echo form_error('postcode'); ?>
										</div>

										<div class="form-group-separator"></div>
										
										<div class="form-group">
											<label for="field-2" class="col-sm-2 control-label">Country</label>
											<div class="col-sm-10">
												<input type="text" placeholder="Country" name="country" id="field-2" class="form-control" value="<?php if(!empty($postData)) echo $postData['country'];?>">
											</div>
											<?php echo form_error('country'); ?>
										</div>
										<div class="form-group-separator"></div>
										
										
										<div class="btn-group">					
											<input type="submit" name="submit" class="btn btn-success" value="Create shop">
										</div>
										
									</form>
								
								</div>
							</div>*/?>
					<div  class="tab-pane" id="messages-3">
					<div class="panel-body">
						<table class="table table-model-2 table-hover" id="Rejointb">
							<thead>
								<tr>
									<th>Name </th>
									
									<th>Rejoin</th>
								</tr>
							</thead>
							<tbody>
							<?php
		//					echo '<pre>';print_r($this->session->userdata());die;
							foreach($value as $array){
								foreach($array as $row){
								if($row->suspendShop==0)
								{
					 		?>
								<tr>
									<td><?php echo $row->name ;?></td>
									<!--<td><a id="<?php //echo $row->id;?>" class="btn btn-danger removeShop" values="<?php //echo $row->id;?>" data-toggle="modal" role="button" href="#">Delete</a></td>
									--><td><a id="<?php echo $row->id;?>" class="btn btn-success rejoin_shop" values="<?php echo $row->id;?>" data-toggle="modal" role="button" href="#">Rejoin</a></td>
								</tr>	
							<?php } } }?>						
							</tbody>
						</table>
					</div>
					
				</div>
<!--				<div  class="tab-pane" id="settings-3">
						
					<div class="panel-body">
					
					</div>
				</div>
-->


				
			</div>
		</div>
	</div>
	<!-- Main Footer -->
	<!-- Choose between footer styles: "footer-type-1" or "footer-type-2" -->
	<!-- Add class "sticky" to  always stick the footer to the end of page (if page contents is small) -->
	<!-- Or class "fixed" to  always fix the footer to the end of page -->

</div>
	<div id="chat" class="fixed"><!-- start: Chat Section -->
			
		<div class="chat-inner">
		
			
			<h2 class="chat-header">
				<a  href="#" class="chat-close" data-toggle="chat">
					<i class="fa-plus-circle rotate-45deg"></i>
				</a>
				
				Chat
				<span class="badge badge-success is-hidden">0</span>
			</h2>
			
			<script type="text/javascript">
			// Here is just a sample how to open chat conversation box
			jQuery(document).ready(function($)
			{
				var $chat_conversation = $(".chat-conversation");
				
				$(".chat-group a").on('click', function(ev)
				{
					ev.preventDefault();
					
					$chat_conversation.toggleClass('is-open');
					
					$(".chat-conversation textarea").trigger('autosize.resize').focus();
				});
				
				$(".conversation-close").on('click', function(ev)
				{
					ev.preventDefault();
					$chat_conversation.removeClass('is-open');
				});
			});
			</script>
			
			
			
			
			
			
		</div>
	
		<!-- conversation template -->
		<div class="chat-conversation">
			
			
		</div>
	<!-- end: Chat Section -->
	</div>

</div>
	<!-- Bottom Scripts -->
	<script src="assets/js/bootstrap.min.js"></script>
	<script src="assets/js/TweenMax.min.js"></script>
	<script src="assets/js/resizeable.js"></script>
	<script src="assets/js/joinable.js"></script>
	<script src="assets/js/xenon-api.js"></script>
	<script src="assets/js/xenon-toggles.js"></script>


	<!-- JavaScripts initializations and stuff -->
	<script src="assets/js/xenon-custom.js"></script>
	
<script type="text/javascript">
function myFunction(role,id)
{
	
	
    $.post( "<?php echo base_url()?>admin/assignroles", { id: id,role:role } );
}
</script>


</body>
</html>