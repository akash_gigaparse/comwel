	<?php //echo '<pre>';print_r($this->session->userdata('user_roll'));die;?>
<div class="sidebar-menu toggle-others fixed">
			
			<div class="sidebar-menu-inner">	
				
				<header class="logo-env">
					
					<!-- logo -->
					<div class="logo">
						<a href="#" class="logo-expanded">
							<img src="<?php echo base_url()?>assets/images/logo@2x.png" width="80" alt="" />
						</a>
						
						<a href="#" class="logo-collapsed">
							<img src="<?php echo base_url()?>assets/images/logo-collapsed@2x.png" width="40" alt="" />
						</a>
					</div>
					
					<!-- This will toggle the mobile menu and will be visible only on mobile devices -->
					<div class="mobile-menu-toggle visible-xs">
						<a href="#" data-toggle="user-info-menu">
							<i class="fa-bell-o"></i>
							<span class="badge badge-success">7</span>
						</a>
						
						<a href="#" data-toggle="mobile-menu">
							<i class="fa-bars"></i>
						</a>
					</div>
					
					<!-- This will open the popup with user profile settings, you can use for any purpose, just be creative -->
					
					
								
				</header>
						
				<?php if($this->session->userdata('user_roll')==1 || $this->session->userdata('user_roll')==6){ ?>
						
				<ul id="main-menu" class="main-menu">
					<!-- add class "multiple-expanded" to allow multiple submenus to open -->
					<!-- class "auto-inherit-active-class" will automatically add "active" class for parent elements who are marked already with class "active" -->
					<li>
						<?php if($this->session->userdata('user_roll')==1){ ?>
							<a href="<?php echo base_url();?>admin">
						<?php } else { ?>
							<a href="<?php echo base_url();?>ShopOwner">
						<?php } ?>
							<i class="linecons-cog"></i>
							<span class="title">Dashboard</span>
						</a>
						
					</li>
					<li>
						<a href="<?php echo base_url();?>employee">
							<i class="linecons-note"></i>
							<span class="title">Users</span>
						</a>
						
					</li>
					
					
					<li class="">
						<a href="<?php echo base_url();?>ListShop">
						<i class="linecons-database"></i>
							<span class="title">Shop List</span>
						</a>
					</li>

					<li class="has-sub">
						<a href="">
							<i class="linecons-database"></i>
							<span class="title">Order</span>
						</a>
						<ul style="display: none;">
						<!--<li class="">
							<a href="<?php //echo base_url();?>order/specialOrder">
								<span class="title">Special Order</span>
						</a>
						</li>-->
						<li class="">
						<a href="<?php echo base_url();?>customer">
								<span class="title">Create Customer</span>
							</a>
						</li>
						<li class="">
						<!--	<?php //if($this->session->userdata('user_roll')==1){ ?>
								<a href="<?php //echo base_url();?>order/specialOrder">

							<?php //} else if ($this->session->userdata('user_roll')==6) { ?>
						-->
							<a href="<?php echo base_url();?>Order/order">
						<?php // } ?>
								<span class="title">Special Order</span>
							</a>
						</li>
							
						</ul>
						
					</li>
					<li class="">
						<a href="<?php echo base_url();?>PaymentReceivedPage">
						<i class="linecons-database"></i>
							<span class="title">Payment Received </span>
						</a>
					</li>
					<li class="">

						<a href="<?php echo base_url();?>product">
						<i class="linecons-database"></i>
							<span class="title">Product</span>
						</a>
					</li>	

					<li class="">

						<a href="<?php echo base_url();?>production">
						<i class="linecons-database"></i>
							<span class="title">production</span>
						</a>
					</li>	
					<li class="">
						<a href="<?php echo base_url();?>stockage">
						<i class="linecons-database"></i>
							<span class="title">Stockage</span>
						</a>
					</li>	
					<!--<li class="has-sub">
						<a href="tables-basic.html">
							<i class="linecons-database"></i>
							<span class="title">Report</span>
						</a>
						<ul style="display: none;">
							<li class="">
								<a href="<?php echo base_url();?>pages/reports1">
									<span class="title">Report1</span>
								</a>
							</li>
							<li class="">
								<a href="<?php echo base_url();?>pages/reports2">
									<span class="title">Report2</span>
								</a>
							<li class="">
								<a href="<?php echo base_url();?>pages/reports3">
									<span class="title">Report3</span>
								</a>
							</li>
							<li class="">
								<a href="<?php echo base_url();?>pages/reports4">
									<span class="title">Report4</span>
								</a>
							</li>
							</li>
							
						</ul>

					</li>-->
					<li class="">

						<a href="<?php echo base_url();?>sales">
						<i class="linecons-database"></i>
							<span class="title">Employee Sales Report </span>
						</a>
					</li>
					
					<li class="">

						<a href="<?php echo base_url();?>sales/sales">
						<i class="linecons-database"></i>
							<span class="title">Worst and Best sales report </span>
						</a>
					</li>
					<li class="">

						<a href="<?php echo base_url();?>sales/salesReport">
						<i class="linecons-database"></i>
							<span class="title">Sales Reports</span>
						</a>
					</li>
					<li class="">

						<a href="<?php echo base_url();?>ProfitMargin">
						<i class="linecons-database"></i>
							<span class="title">Profit Margin Report</span>
						</a>
					</li>
					<li class="">
						<a href="<?php echo base_url();?>Wastage/index">
						<i class="linecons-database"></i>
							<span class="title">Wastage Report</span>
						</a>
					</li>
							
						</ul>
					</li>

					
				</ul>
				<?php } else{?>
				<ul id="main-menu" class="main-menu">
					<!-- add class "multiple-expanded" to allow multiple submenus to open -->
					<!-- class "auto-inherit-active-class" will automatically add "active" class for parent elements who are marked already with class "active" -->
					<li>
						<a href="<?php echo base_url();?>employee/empdashboard">
							<i class="linecons-cog"></i>
							<span class="title">Dashboard</span>
						</a>
						
					</li>
					<?php foreach($pages as $page){
						if(in_array($page['id'],$empPage)){?>
					<li>
						<a href="<?php echo base_url().$page['url'];?>">
							<i class="linecons-desktop"></i>
							<span class="title"><?php echo $page['page']?></span>
						</a>
						
					</li>
					<?php }
					}?>
					
					
				</ul>
				<?php }?>
						
			</div>
			
		</div>
