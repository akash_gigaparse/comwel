<?php //echo '<pre>';print_r($charts);
	
$empShopIds=explode(',',$user->shop_ids);
//echo '<pre>';print_r($empShop);die;
?>
<div class="main-content">
<?php $this->load->view('employee/page_header');?>

	<div class="page-title">

	<div class="title-env">
		<h1 class="title">Worst and Best sales report </h1>
		<p class="description"></p>
	</div>
	
	
</div>
<div class="row">
	<div class="col-sm-12">
		<form  action="<?php echo base_url();?>Sales/searchByDates" method="post" class="form-horizontal" role="form">
		<div class="form-group">
			<label for="field-1" class="col-sm-2 control-label">Shop Name </label>
			<div class="col-sm-10">
				<select class="form-control" name="shop">
					<option>Select</option>
				<?php foreach($shops as $shop){
						if(in_array($shop->id,$empShopIds))
						echo "<option value='$shop->id'>$shop->name</option>";
						
						}?>
				</select>
			</div>
			<br><br>
			<label for="field-1" class="col-sm-2 control-label">From Date *</label>
			
			<div class="col-sm-9">
				<div class="input-group">
					<input type="text" class="form-control datepicker" data-format="dd-mm-yyyy" name="date1" required value="<?php if((!empty($charts) && isset($charts))) { echo $date['date1']; }?>">
					
					<div class="input-group-addon">
						<a href="#"><i class="linecons-calendar"></i></a>
					</div>
				</div>
			</div>
			<br><br>
			<label for="field-1" class="col-sm-2 control-label">To Date *</label>
			<div class="col-sm-9">
				<div class="input-group">
					<input type="text" class="form-control datepicker" data-format="dd-mm-yyyy" name="date2" required value="<?php if((!empty($charts) && isset($charts))) { echo $date['date2']; }?>">
					
					<div class="input-group-addon">
						<a href="#"><i class="linecons-calendar"></i></a>
					</div>
				</div>
			</div>
			<br><br>
			<label for="field-1" class="col-sm-2 control-label"></label>
			<div class="col-sm-9">
				<div class="input-group">
				<input type="submit" name="submit" class="btn btn-success" value="Search">
				</div>
			</div>
			</form>
		</div>
	</div>
</div>
<br><br><br>

<?php if((!empty($charts) && isset($charts)))
{
	//die("asd");
	//echo $date['user'];
	//echo '<pre>';print_r($charts);die;
$chart_max=array_reverse($charts);
//echo '<pre>';print_r($charts);die;
?>
<div class="row">
	<div class="col-sm-12">
	
		<div class="panel panel-default">
			<div class="panel-heading">
				<h3 class="panel-title"></h3>
				<div class="panel-options">
					<a href="#" data-toggle="panel">
						<span class="collapse-icon">&ndash;</span>
						<span class="expand-icon">+</span>
					</a>
					<a href="#" data-toggle="remove">
						&times;
					</a>
				</div>
			</div>
			<div>

				<form name="" method="post" action="<?php echo base_url();?>Sales/csv">
				<input name='shop' type='hidden' value="<?php if((!empty($date) && isset($date))) { echo $date['shop']; }?>">
					<input name='date1' type='hidden' value="<?php if((!empty($date) && isset($date))) { echo $date['date1']; }?>">
					<input name='date2' type='hidden' value="<?php if((!empty($date) && isset($date))) { echo $date['date2']; }?>">
					<input name="csv" type="submit" value="Export CSV" class="btn btn-primary btn-small">
				</form>

			</div>
			<?php //echo "<pre>";print_r($charts);die;?>
			<div class="panel-body">	
				<script type="text/javascript">
					jQuery(document).ready(function($)
					{
						if( ! $.isFunction($.fn.dxChart))
							return;
						$("#bar-1").dxChart({
						
							dataSource: [
								
									<?php
									
									 foreach($charts as $key=>$value)
									{
										if($key <=4)
										{	

									 ?>
										{ day :"<?php  echo $value->item; ?>", Total : <?php  echo $value->totalProductCount; ?>} ,
									<?php  } } 

									 foreach($chart_max as $key=>$value)
									{
										if($key <=4)
										{	

									 ?>
										{ day :"<?php  echo $value->item; ?>", Total : <?php  echo $value->totalProductCount; ?>} ,
									<?php  } } ?>
							],
							series: {
								argumentField: "day",
								valueField: "Total",
								name: "Total",
								type: "bar",
								color: '#68b828'
							}
						});
					});
				</script>
				<div id="bar-1" style="height: 440px; width: 100%;"></div>
				<br />


				<!--<a href="#" id="bar-1-randomize" class="btn btn-primary btn-small">Randomize</a>-->
		</div>
		</div>
			
	</div>
	<?php } ?>
</div>
<script type="text/javascript">
	jQuery(document).ready(function($)
	{
		if( ! $.isFunction($.fn.dxChart))
			$(".dx-warning").removeClass('hidden');
	});
</script>

<script>
	var xenonPalette = ['#68b828','#7c38bc','#0e62c7','#fcd036','#4fcdfc','#00b19d','#ff6264','#f7aa47'];
</script>




	
