<?php //print_r($value2); die();?>
<script type="text/javascript">
function myFunction(role,id)
{
    $.post( "<?php echo base_url()?>admin/assignroles", { id: id,role:role } );
}
</script>
<div class="main-content">
	<?php $this->load->view('page_header');?>
	<div class="page-title">
		
		<div class="title-env">
			<h1 class="title">Registration Page</h1>
			<p class="description">Users can register from this page</p>
		</div>
		
		<div class="breadcrumb-env">
		
			<ol class="breadcrumb bc-1" >
				<li>
					<a href="ReportingDashboard.html"><i class="fa-home"></i>Home</a>
				</li>
				<li>
		
						<a href="tables-basic.html">Tables</a>
				</li>
				<li class="active">
					<strong>Basic Tables</strong>
				</li>
			</ol>

		</div>
			
	</div>
	<div class="row">
		<div class="col-sm-12">
			<div class="panel panel-default">
				<div class="panel-heading">
					<h3 class="panel-title">Default form inputs</h3>
					<div class="panel-options">
						<a data-toggle="panel" href="#">
							<span class="collapse-icon">&ndash;</span>
							<span class="expand-icon">+</span>
						</a>
						<a data-toggle="remove" href="#">
							×
						</a>
					</div>
				</div>
				<div class="panel-body">
					<form  action="<?php echo base_url();?>admin/assignRoles" method="post" class="form-horizontal" role="form">
						<table class="table responsive">
								<thead>
									<tr>
										<th>Serial Number</th>
										<th>Page Name</th>
										<th>Assigned Role</th>
									</tr>
								</thead>
								
								<tbody>
								<?php $i=1;
								foreach($value2 as $pages){?>
									<tr>
										<td><?php echo $i;echo "<input type='hidden' name='ids[]' value='$pages[id]'/> ";?></td>
										<td><?php echo $pages['page'];?></td>
										<td><?php $selectedrole=explode(',',$pages['role_id']);
										//print_r($selectedrole);
										foreach($value as $droles){
											$rolevalue='R_'.$droles['id'];
								echo "<input type='checkbox' name='roles[]' onclick='myFunction(this.value,$pages[id])' value='$rolevalue'";
								if(in_array($rolevalue,$selectedrole))
								echo "checked='checked'";
								echo "/> $droles[name]";}?></td>
									</tr>
									
								<?php $i++;}?>
								</tbody>
							</table>
					</form>
					
				</div>
			</div>
					
		</div>  
	</div>
