<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	
	<meta name="viewport" content="width=device-width, initial-scale=1.0" />
	<meta name="description" content="Xenon Boostrap Admin Panel" />
	<meta name="author" content="" />
	
	<title>Comwel - Tables</title>

	<link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Arimo:400,700,400italic">
	<link rel="stylesheet" href="<?php echo base_url()?>assets/css/fonts/linecons/css/linecons.css">
	<link rel="stylesheet" href="<?php echo base_url()?>assets/css/fonts/fontawesome/css/font-awesome.min.css">
	<link rel="stylesheet" href="<?php echo base_url()?>assets/css/bootstrap.css">
	<link rel="stylesheet" href="<?php echo base_url()?>assets/css/xenon-core.css">
	<link rel="stylesheet" href="<?php echo base_url()?>assets/css/xenon-forms.css">
	<link rel="stylesheet" href="<?php echo base_url()?>assets/css/xenon-components.css">
	<link rel="stylesheet" href="<?php echo base_url()?>assets/css/xenon-skins.css">
	<link rel="stylesheet" href="<?php echo base_url()?>assets/css/custom.css">

	<script src="<?php echo base_url()?>assets/js/jquery-1.11.1.min.js"></script>
	
	<script src="https://maps.googleapis.com/maps/api/js?v=3.exp&signed_in=true&libraries=places"></script>
 <link href="<?php echo base_url()?>assets/css/jquery-ui.min.css" rel="stylesheet">
    <script>

		function initialize() {
			var lat=document.getElementById('cityLat').value;
		    var lng=document.getElementById('cityLng').value ;
		   
		    if(lng=="" && lat==""){
		  	var mapOptions = {
		    	center: new google.maps.LatLng(-33.8688, 151.2195),
		    	zoom: 13
		  		};
			}
			else{
				var mapOptions = {
			    center: new google.maps.LatLng(lat,lng),
			    zoom: 13
			  	};
			}
		  	var map = new google.maps.Map(document.getElementById('map-canvas'),
		    mapOptions);
		  	var input = /** @type {HTMLInputElement} */(
		    document.getElementById('pac-input'));
		  	var autocomplete = new google.maps.places.Autocomplete(input);
		  	autocomplete.bindTo('bounds', map);

		  	var infowindow = new google.maps.InfoWindow();
		  	var marker = new google.maps.Marker({
			    map: map,
			    anchorPoint: new google.maps.Point(0, -29)
		  	});

		  	google.maps.event.addListener(autocomplete, 'place_changed', function() {
			    infowindow.close();
			    marker.setVisible(false);
			    var place = autocomplete.getPlace();
			     document.getElementById('cityLat').value = place.geometry.location.lat();
			            document.getElementById('cityLng').value = place.geometry.location.lng();
			    if (!place.geometry) {
			      return;
			    }
			    // If the place has a geometry, then present it on a map.
			    if (place.geometry.viewport) {
			      map.fitBounds(place.geometry.viewport);
			    } else {
			      map.setCenter(place.geometry.location);
			      map.setZoom(17);  // Why 17? Because it looks good.
			    }
			    marker.setIcon(/** @type {google.maps.Icon} */({
			      url: place.icon,
			      size: new google.maps.Size(71, 71),
			      origin: new google.maps.Point(0, 0),
			      anchor: new google.maps.Point(17, 34),
			      scaledSize: new google.maps.Size(35, 35)
			    }));
			    marker.setPosition(place.geometry.location);
			    marker.setVisible(true);

			    var address = '';
			    if (place.address_components) {
			      address = [
			        (place.address_components[0] && place.address_components[0].short_name || ''),
			        (place.address_components[1] && place.address_components[1].short_name || ''),
			        (place.address_components[2] && place.address_components[2].short_name || '')
			      ].join(' ');
			    }
			    infowindow.setContent('<div><strong>' + place.name + '</strong><br>' + address);
			    infowindow.open(map, marker);
			});

		  // Sets a listener on a radio button to change the filter type on Places
		  // Autocomplete.
			function setupClickListener(id, types) {
				var radioButton = document.getElementById(id);
				google.maps.event.addDomListener(radioButton, 'click', function() {
				  autocomplete.setTypes(types);
				});
			}
		  setupClickListener('changetype-all', []);
		  setupClickListener('changetype-address', ['address']);
		  setupClickListener('changetype-establishment', ['establishment']);
		  setupClickListener('changetype-geocode', ['geocode']);
		}
		google.maps.event.addDomListener(window, 'load', initialize);
    </script>
	<script>
	//account parmanently remove functionality
	jQuery(document).ready(function()
	{
		var remove_id;
		$(".remove").click(function(e)
		{
			if (confirm("Are you sure you want to Delete")) {
				remove_id=this.id;
				 var info = 'remove=' + remove_id;
				 $.ajax({
				     type: "POST",
				     url: "<?php echo base_url()?>/Employee/deleteUser",
				     data: info,
				     success: function(data)
					 {
					 	//alert("Recorde Delete successfully");
					     $("#"+remove_id+"").parent().parent().remove();
					 }
				});
			}
		});
		$(".removeShop").click(function(e)
		{
			if (confirm("Are you sure you want to Delete")) {
				remove_id=this.id;
				 var info = 'remove=' + remove_id;
				 $.ajax({
				     type: "POST",
				     url: "<?php echo base_url()?>/Admin/deleteShop",
				     data: info,
				     success: function(data)
					 {
					 	//alert("Recorde Delete successfully");
					     $("#"+remove_id+"").parent().parent().remove();
					 }
				});
			}
		});
		var suspend_id;
		$(".suspend").click(function(e)
		{
			if (confirm("Are you sure you want to Suspend")) {
				suspend_id=this.id;
				 var info = "action='suspenduser'&suspend=" + suspend_id;
				 $.ajax({
				     type: "POST",
				     url: "<?php echo base_url()?>/Employee/suspendUser",
				     data: info,
				     success: function(data)
					 {
					 	
					 	$("#"+suspend_id+"").parent().parent().remove();
					 	//alert("Successfully Suspend");
					 	$("#suspendtable tbody").html(data);

					 }
				});
			}
		});
		var rejoin_id;
		$(".rejoin").click(function(e)
		{
			if (confirm("Are you sure you want to Rejoin Employee")) {
				rejoin_id=this.id;
				 var info = 'rejoin=' + rejoin_id;
				  $.ajax({
				     type: "POST",
				     url: "<?php echo base_url()?>/Employee/rejoinUser",
				     data: info,
				     success: function(data)
					 {
					 	
					     $("#"+rejoin_id+"").parent().parent().remove();
					     //alert("Successfully Rejoin");
					 	$("#rejointable tbody").html(data);
					 }
				});
			}
		});
		//remove product form table
		$(".removeProduct").click(function(e)
			{
				if(confirm("Are you sure you want to delete Product")){
					var productRemove_id=this.id;	
					var info = 'productRemove_id=' + productRemove_id;
					$.ajax({
						type : "POST",
						url  : "<?php echo base_url()?>/product/deleteProduct",
						data : info,
						success : function (data)
						{
							$("#"+productRemove_id).parent().parent().remove();
							//alert("Recorde Delete successfully");
						}
					});	
				}
			});

			$(".rejoin_shop").click(function(e)
			{
				if (confirm("Are you sure you want to Rejoin Employee")) {
					rejoin_id=this.id;
					 var info = 'rejoin=' + rejoin_id;
					 $.ajax({
					     type: "POST",
					     url: "<?php echo base_url()?>/ListShop/rejoinShop",
					     data: info,
					     success: function(data)
						 {
						 	$("#"+rejoin_id+"").parent().parent().remove();
						    $("#Suspendtb tbody").html(data);
						 }
					});
				}
			});
			var suspend_id;
			$(".suspend_shop").click(function(e)
			{ 
				if (confirm("Are you sure you want to Suspend")) {
					suspend_id=this.id;
					 var info = "action='suspendShop'&suspendShop=" + suspend_id;
					 $.ajax({
					     type: "POST",
					     url: "<?php echo base_url()?>/ListShop/suspendShop",
					     data: info,
					     success: function(data)
						 {
						 	$("#"+suspend_id+"").parent().parent().remove();
						 	$("#Rejointb tbody").html(data);
						 }
					});
				}
			});
		});
	</script>
</head>
<body class="page-body">

	<div class="settings-pane">
			
		<a href="#" data-toggle="settings-pane" data-animate="true">
			&times;
		</a>
		
		<div class="settings-pane-inner">
			
			<div class="row">
				
				<div class="col-md-4">
					
					<div class="user-info">
						
						<div class="user-image">
							<a href="extra-profile.html">
								<img src="<?php //echo base_url()?>assets/images/user-2.png" class="img-responsive img-circle" />
							</a>
						</div>
						
						
						
					</div>
					
				</div>
				
				<div class="col-md-8 link-blocks-env">
					
					<div class="links-block left-sep">
						<h4>
							<span>Notifications</span>
						</h4>
						
						<ul class="list-unstyled">
							<li>
								<input type="checkbox" class="cbr cbr-primary" checked="checked" id="sp-chk1" />
								<label for="sp-chk1">Messages</label>
							</li>
							<li>
								<input type="checkbox" class="cbr cbr-primary" checked="checked" id="sp-chk2" />
								<label for="sp-chk2">Events</label>
							</li>
							<li>
								<input type="checkbox" class="cbr cbr-primary" checked="checked" id="sp-chk3" />
								<label for="sp-chk3">Updates</label>
							</li>
							<li>
								<input type="checkbox" class="cbr cbr-primary" checked="checked" id="sp-chk4" />
								<label for="sp-chk4">Server Uptime</label>
							</li>
						</ul>
					</div>
					
					<div class="links-block left-sep">
						<h4>
							<a href="#">
								<span>Help Desk</span>
							</a>
						</h4>
						
						<ul class="list-unstyled">
							<li>
								<a href="#">
									<i class="fa-angle-right"></i>
									Support Center
								</a>
							</li>
							<li>
								<a href="#">
									<i class="fa-angle-right"></i>
									Submit a Ticket
								</a>
							</li>
							<li>
								<a href="#">
									<i class="fa-angle-right"></i>
									Domains Protocol
								</a>
							</li>
							<li>
								<a href="#">
									<i class="fa-angle-right"></i>
									Terms of Service
								</a>
							</li>
						</ul>
					</div>
					
				</div>
				
			</div>
		
		</div>
		
	</div>
	
<div class="page-container">
		
		
		