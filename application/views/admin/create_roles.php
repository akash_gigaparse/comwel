<div class="main-content">
<?php $this->load->view('page_header');?>

	<div class="page-title">
	
		<div class="title-env">
			<h1 class="title">Inserting Role</h1>
			<p class="description">Admin can create new role from this page</p>
		</div>
	
		<div class="breadcrumb-env">
		
			<ol class="breadcrumb bc-1" >
				<li>
					<a href="ReportingDashboard.html"><i class="fa-home"></i>Home</a>
				</li>
				<li>
				
					<a href="tables-basic.html">Tables</a>
				</li>
				<li class="active">
	
					<strong>Basic Tables</strong>
				</li>
			</ol>
					
		</div>
		
	</div>
	<div class="row">
		<div class="col-sm-12">
					
			<div class="panel panel-default">
				<div class="panel-heading">
					<h3 class="panel-title">Default form inputs</h3>
					<div class="panel-options">
						<a data-toggle="panel" href="#">
							<span class="collapse-icon">&ndash;</span>
							<span class="expand-icon">+</span>
						</a>
						<a data-toggle="remove" href="#">
							×
						</a>
					</div>
				</div>
				<div class="panel-body">
					<form  action="<?php echo base_url();?>admin/createroles" method="post" class="form-horizontal" role="form">
						<div class="form-group">
							<label for="field-2" class="col-sm-2 control-label">Role Name</label>
							<div class="col-sm-10">
								<input type="text" placeholder="Enter Role Name" name="name" id="field-2" class="form-control">
							</div>
							<?php echo form_error('name'); ?>
						</div>

						<div class="btn-group">					
							<input type="submit" name="submit" class="btn btn-success" value="Submit">
						</div>
						
					</form>
					
				</div>
			</div>
		</div>  

	</div>
