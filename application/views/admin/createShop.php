
<div class="main-content">
<?php $this->load->view('page_header');?>
			
	<div class="page-title">
		
		<div class="title-env">
			<h1 class="title">Create Shop</h1>
			<p class="description">Admin can Create Shop from this page</p>
		</div>
		
		<div class="breadcrumb-env">
		
			<ol class="breadcrumb bc-1" >
				<li>
					<a href="ReportingDashboard.html"><i class="fa-home"></i>Home</a>
				</li>
				<li>
		
						<a href="tables-basic.html">Tables</a>
				</li>
				<li class="active">
				
					<strong>Basic Tables</strong>
				</li>
			</ol>
					
		</div>
			
	</div>
	<div class="row">
		<div class="col-sm-12">
					
			<div class="panel panel-default">
				<div class="panel-heading">
					<h3 class="panel-title">Default form inputs</h3>
					<div class="panel-options">
						<a data-toggle="panel" href="#">
							<span class="collapse-icon">&ndash;</span>
							<span class="expand-icon">+</span>
						</a>
						<a data-toggle="remove" href="#">
							×
						</a>
					</div>
				</div>
				<div class="panel-body">
					<form  action="<?php echo base_url();?>admin/createShop" method="post" class="form-horizontal" enctype="multipart/form-data" role="form">
						<div class="form-group">
							<label for="field-1" class="col-sm-2 control-label">Name</label>
							<div class="col-sm-10">
								<input type="text" placeholder="Name" name="name" id="name" value="<?php if(!empty($value))echo $value['name'];?>" class="form-control">

							</div>
							<?php echo form_error('name'); ?>
						</div>

						<div class="form-group-separator"></div>
						
						<div class="form-group">
							<label for="field-2" class="col-sm-2 control-label"> Address</label>
							<div class="col-sm-10">
								<input type="text" placeholder="Enter a location" name="address" id="pac-input" class="form-control">
							</div>
							<?php echo form_error('address'); ?>
							<div class="col-sm-12">

								    <div id="type-selector" class="controls">
									      <input type="radio" name="type" id="changetype-all" checked="checked">
									      <label for="changetype-all">All</label>

									      <input type="radio" name="type" id="changetype-establishment">
									      <label for="changetype-establishment">Establishments</label>

									      <input type="radio" name="type" id="changetype-address">
									      <label for="changetype-address">Addresses</label>

									      <input type="radio" name="type" id="changetype-geocode">
									      <label for="changetype-geocode">Geocodes</label>
								       
								    </div>
									<div id="map-canvas" ></div>
							</div>
							
					
						</div>
						<div class="form-group-separator"></div>
						<div class="form-group">
							<label for="field-2" class="col-sm-2 control-label">Latitude</label>
							<div class="col-sm-10">
								<input type="text" placeholder="Latitude" name="latitude" id="cityLat" class="form-control">
							</div>
							<?php echo form_error('latitude'); ?>
						</div>								
						<div class="form-group-separator"></div>
						
						<div class="form-group">
							<label for="field-2" class="col-sm-2 control-label">Longitude</label>
							<div class="col-sm-10">
								<input type="text" placeholder="Longitude" name="longitude" id="cityLng" class="form-control">
							</div>
							<?php echo form_error('longitude'); ?>
						</div>
						<div class="form-group-separator"></div>
						<div class="form-group">
							<label for="field-2" class="col-sm-2 control-label">  Contact Details</label>
							<div class="col-sm-10">
								<input type="text" placeholder="Contact Details" name="phone" id="field-3" class="form-control">
							</div>
							<?php echo form_error('phone'); ?>
						</div>
						
						<div class="form-group-separator"></div>
							<div class="form-group">
							<label for="field-2" class="col-sm-2 control-label"> logo </label>
							<div class="col-sm-10">
								<input type="file" placeholder="Contact Details" name="userfile" id="field-3" class="form-control">
							</div>
							<?php echo form_error('userfile'); ?>
						</div>
						
						<div class="form-group-separator"></div>
						
						
							<div class="form-group">
							<label for="field-2" class="col-sm-2 control-label"> Direct deposite details</label>
							<div class="col-sm-10">
								<input type="text" placeholder=" Direct deposite details" name="deposite" id="field-3" class="form-control">
							</div>
							<?php echo form_error('deposite'); ?>
						</div>
						<div class="form-group-separator"></div>
						
						
							<div class="form-group">
							<label for="field-2" class="col-sm-2 control-label"> Default Product Overhead cost</label>
							<div class="col-sm-10">
								<input type="text" placeholder=" Default Product Overhead cost" name="overhead" id="field-3" class="form-control">
							</div>
							<?php echo form_error('overhead'); ?>
						</div>
						<div class="form-group-separator"></div>
						<div class="form-group">
							<label for="field-2" class="col-sm-2 control-label">Suburb</label>
							<div class="col-sm-10">
								<input type="text" placeholder="Suburb" name="suburb" id="field-2" class="form-control">
							</div>
							<?php echo form_error('suburb'); ?>
						</div>
						<div class="form-group-separator"></div>
				


						<div class="form-group-separator"></div>
						
						<div class="form-group">
							<label for="field-2" class="col-sm-2 control-label">State</label>
							<div class="col-sm-10">
								<input type="text" placeholder="State" name="state" id="field-2" class="form-control">
							</div>
							<?php echo form_error('state'); ?>
						</div>

						<div class="form-group-separator"></div>
						
						<div class="form-group">
							<label for="field-2" class="col-sm-2 control-label">Postcode</label>
							<div class="col-sm-10">
								<input type="text" placeholder="Postcode" name="postcode" id="field-2" class="form-control">
							</div>
								<?php echo form_error('postcode'); ?>
						</div>

						<div class="form-group-separator"></div>
						
						<div class="form-group">
							<label for="field-2" class="col-sm-2 control-label">Country</label>
							<div class="col-sm-10">
								<input type="text" placeholder="Country" name="country" id="field-2" class="form-control">
							</div>
							<?php echo form_error('country'); ?>
						</div>
						<div class="form-group-separator"></div>
						
						
						<div class="btn-group">					
							<input type="submit" name="submit" class="btn btn-success" value="Create shop">
						</div>
						
					</form>
				
				</div>
			</div>
	
		</div>  

	</div>
