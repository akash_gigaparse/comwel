<div class="sidebar-menu toggle-others fixed">
			
			<div class="sidebar-menu-inner">	
				
				<header class="logo-env">
					
					<!-- logo -->
					<div class="logo">
						<a href="ReportingDashboard.html" class="logo-expanded">
							<img src="assets/images/logo@2x.png" width="80" alt="" />
						</a>
						
						<a href="ReportingDashboard.html" class="logo-collapsed">
							<img src="assets/images/logo-collapsed@2x.png" width="40" alt="" />
						</a>
					</div>
					
					<!-- This will toggle the mobile menu and will be visible only on mobile devices -->
					<div class="mobile-menu-toggle visible-xs">
						<a href="#" data-toggle="user-info-menu">
							<i class="fa-bell-o"></i>
							<span class="badge badge-success">7</span>
						</a>
						
						<a href="#" data-toggle="mobile-menu">
							<i class="fa-bars"></i>
						</a>
					</div>
					
					<!-- This will open the popup with user profile settings, you can use for any purpose, just be creative -->
					<div class="settings-icon">
						<a href="#" data-toggle="settings-pane" data-animate="true">
							<i class="linecons-cog"></i>
						</a>
					</div>
					
								
				</header>
						
				
						
				<ul id="main-menu" class="main-menu">
					<!-- add class "multiple-expanded" to allow multiple submenus to open -->
					<!-- class "auto-inherit-active-class" will automatically add "active" class for parent elements who are marked already with class "active" -->
					<li>
						<a href="<?php echo base_url();?>admin">
							<i class="linecons-cog"></i>
							<span class="title">Dashboard</span>
						</a>
						
					</li>
					<li>
						<a href="<?php echo base_url();?>admin/employee">
							<i class="linecons-note"></i>
							<span class="title">Employee</span>
						</a>
						
					</li>
					
					
					<li class="">

						<a href="<?php echo base_url();?>admin/ListShop">
						<i class="linecons-database"></i>
							<span class="title">Shop List</span>
						</a>
					</li>

					<li class="has-sub">
						<a href="<?php echo base_url();?>">
							<i class="linecons-database"></i>
							<span class="title">Order</span>
						</a>
						<ul style="display: none;">
						<!--<li class="">
							<a href="<?php echo base_url();?>order/specialOrder">
								<span class="title">Special Order</span>
						</a>
						</li>-->
						<li class="">
							<a href="<?php echo base_url();?>customer">
								<span class="title">Create Customer</span>
							</a>
						</li>
						<li class="">
							<a href="<?php echo base_url();?>order/specialOrder">
								<span class="title">Special Order</span>
							</a>
						</li>
							
						</ul>
						
					</li>
					<li class="">

						<a href="<?php echo base_url();?>product">
						<i class="linecons-database"></i>
							<span class="title">Product</span>
						</a>
					</li>	

					<li class="">

						<a href="<?php echo base_url();?>production">
						<i class="linecons-database"></i>
							<span class="title">production</span>
						</a>
					</li>	
<li class="">

						<a href="<?php echo base_url();?>stockage">
						<i class="linecons-database"></i>
							<span class="title">Stockage</span>
						</a>
					</li>	
					<li class="has-sub">
						<a href="tables-basic.html">
							<i class="linecons-database"></i>
							<span class="title">Report</span>
						</a>
						<ul style="display: none;">
							<li class="">
								<a href="<?php echo base_url();?>pages/reports1">
									<span class="title">Report1</span>
								</a>
							</li>
							<li class="">
								<a href="<?php echo base_url();?>pages/reports2">
									<span class="title">Report2</span>
								</a>
							<li class="">
								<a href="<?php echo base_url();?>pages/reports3">
									<span class="title">Report3</span>
								</a>
							</li>
							<li class="">
								<a href="<?php echo base_url();?>pages/reports4">
									<span class="title">Report4</span>
								</a>
							</li>
							</li>
							
						</ul>

					</li>
					<li class="">

						<a href="<?php echo base_url();?>sales">
						<i class="linecons-database"></i>
							<span class="title">Employee Sales Report </span>
						</a>
					</li>
					<li class="">

						<a href="<?php echo base_url();?>sales/sales">
						<i class="linecons-database"></i>
							<span class="title">Employee Sales </span>
						</a>
					</li>
					<li class="">

						<a href="<?php echo base_url();?>sales/salesReport">
						<i class="linecons-database"></i>
							<span class="title">Sales Reports</span>
						</a>
					</li>
							
						</ul>
					</li>
					
				</ul>
						
			</div>
			
		</div>
