<?php
foreach($value as $row){
	if($row->suspend==1)
	{
	?>
	<tr>
		<td><?php echo $row->id;?></td>
		<td><?php echo $row->fname ."&nbsp;".$row->lname;?></td>
		<td><?php echo $row->email;?></td>
		<td><?php echo $row->address;?></td>
		<td><a class="btn btn-success" role="button" href="<?php echo base_url()?>Admin/editUser?id=<?php echo $row->id;?>">Edit</a></td>
		<td><a id="<?php echo $row->id;?>" class="btn btn-danger suspend" values="<?php echo $row->id;?>" data-toggle="modal" role="button" href="#">Suspend</a></td>

	</tr>
	<?php } } ?>
<script>
//account parmanently remove functionality
jQuery(document).ready(function()
{
	var suspend_id;
	$(".suspend").click(function(e)
	{
		if (confirm("Are you sure you want to Suspend")) {
			suspend_id=this.id;
			 var info = "action='suspenduser'&suspend=" + suspend_id;
			 $.ajax({
			     type: "POST",
			     url: "<?php echo base_url()?>/Admin/suspendUser",
			     data: info,
			     success: function(data)
				 {
				 	
				 	$("#"+suspend_id+"").parent().parent().remove();
				 	//alert("Successfully Suspend");
				 	$("#suspendtable tbody").html(data);

				 }
			});
		}
	});
});
</script>