<?php 
class ProfitMargin extends CI_Controller{
  public function __construct(){
    parent::__construct();
    $this->load->database();
    $this->load->model('shop_model');
    $this->load->model('transaction_model');
    $this->load->model('User_model');
    $this->load->model('Profit_model');
      if(!$this->session->userdata('logged_in'))
      {
          redirect('', 'refresh');
      }
    }

  public function index($data=NULL){
    //$data['users'] = $this->transaction_model->getSalesPersonName();
    $data['empshop']=$this->User_model->currentUserInfo();
    $data['shops']=$this->shop_model->get_all();
    //echo '<pre>';print_r($data);die;

    if($this->session->userdata('user_roll')==5)
    {
      $this->load->view('admin/header');
      $this->load->view('superadmin/left_menu');
      $this->load->view('superadmin/profitmargin',$data);
      $this->load->view('admin/footer');
    }else{
        $this->load->view('admin/header');
        if($this->session->userdata('user_roll')!=1){
          
            page();
        }
        else{
          $this->load->view('admin/left_menu');
        }
      $this->load->view('admin/profitmargin',$data);
      $this->load->view('admin/footer');

    }
          
  }
  public function searchByShopDates()
  {
    if($this->input->POST()){
      $data['charts']=$this->Profit_model->getProfitBySingleShopDates();
      $date1=$this->input->POST('date1');
      $date2=$this->input->POST('date2');
      $shop=$this->input->POST('shop');
      $data['date']=array('date1' => $date1,'date2' => $date2,'shop' => $shop);
//echo '<pre>';print_r($data);die;
      return $this->index($data);
    }else{
      return $this->index();
    }
  }

   //create csv for profit margin
  public function csvForProfitMargin($query=NULL, $filename = 'CSV_Report.csv')
  {
      $shopname=$this->input->POST('shopname');
      $shopname =str_replace(" ","_",$shopname);
      $date1 = date("Y-m-d", strtotime($this->input->POST('date1')));
      $date2 = date("Y-m-d", strtotime($this->input->POST('date2')));
      $shop=$this->input->POST('shop');
      $this->load->dbutil();
      $this->load->helper('download');
      $delimiter = ",";
      $newline = "\n";
      $query = $this->db->query("SELECT sum(products.qty) as pqty,products.price, product.cost,sum(products.qty)*products.price as totalretailprice, sum(products.qty)*product.cost as totalcostprice,((((products.price-product.cost)* sum(products.qty))/sum(products.qty))*100) as `profitperctenge_%`,products.item FROM transaction,products,product WHERE transaction.shopid =products.shop_id and transaction.id =products.transactionid and products.shop_id =product.shop_id and products.item =product.name and transaction.shopid = '$shop' AND transaction.transaction_date BETWEEN '$date1' AND '$date2' and product.filter=0 GROUP BY products.item ORDER BY `profitperctenge_%` DESC ");
//    $query = $this->db->query("SELECT products.qty as pqty,products.price, product.cost,CONCAT((((products.price-product.cost)* sum(products.qty))/sum(products.qty))*100,' %') as profitperctenge,products.item FROM transaction,products,product WHERE transaction.shopid =products.shop_id and transaction.id =products.transactionid and products.shop_id =product.shop_id and products.item =product.name and transaction.shopid = '$shop' AND transaction.transaction_date BETWEEN '$date1' AND '$date2' and product.filter=0 GROUP BY products.item ORDER BY profitperctenge DESC ");
      $data = $this->dbutil->csv_from_result($query, $delimiter,$newline);
      $var="Shop_Name ".$shopname."\nProfitMargin ".$date1."_To_".$date2."\n\n".$data;
      force_download($filename, $var);
  }

}
