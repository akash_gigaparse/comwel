<?php 
class Order extends CI_Controller{
    public function __construct(){
        parent::__construct();
        ini_set('max_execution_time', 600000);
        $this->load->library('form_validation');
        //$this->acl_auth->restrict_access('admin');
        $this->load->model('shop_model'); 
        $this->load->model('Customer_model');   
        $this->load->model('Product_model');  
        $this->load->model('Order_model');  
        $this->load->model('User_model');
        $this->load->model('Wastage_model'); 
        if(!$this->session->userdata('logged_in'))
        {
            redirect('', 'refresh');
        }
    }
    
    //function for order
    public function specialOrder($data=null){

        if($this->input->post('submit')){
                 $jsonArr=array();
                 $pid=$this->input->post('pid');
                 $name=$this->input->post('itemName');
                 $price=$this->input->post('price');
                 $qty=$this->input->post('quantity');
                 $total=$this->input->post('total');
                 $size=sizeof($name);
                 for($i=0;$i<$size;$i++){
                    if(!empty($name[$i]))
                    array_push($jsonArr ,array('pid'=>$pid[$i],'name'=>$name[$i],'price'=>$price[$i],'qty'=>$qty[$i],'total'=>$total[$i]));
                 }
                 $item=json_encode($jsonArr);

                
                 $date=date('Y-m-d');
                $data =array('date_ordered' => $date,'order_type'=>$this->input->POST('order_type'),'date_required' => $this->input->POST('orderdate'),'shop_id'=>$this->input->POST('shop_id'),'paided'=> $this->input->POST('paid'),'item'=> $item,'customer_id'=> $this->input->POST('customer'),'order_num'=> $this->input->POST('ordnum'),'price'=> $this->input->POST('grandtotal'),'employee_id'=>$this->session->userdata('user_id'));


                 $success= $this->Order_model->addorder($data);
                 

                
        }
        if($this->input->post('search')){
        			$this->form_validation->set_rules('customer', 'Customer Name', 'required');
        			if ($this->form_validation->run() == FALSE){
                         $data['searchResults']	=array();
                    }else{
                    	$arrsearch=array();
                    	$shop_id=$this->input->post('shop_id');
                    	$customer=$this->input->post('customer');
                    	$requiredate=$this->input->post('requiredate');
                    	$orderdate=$this->input->post('orderdate');
                    	$paid=$this->input->post('paid');
                    	if(!empty($shop_id))
                    		$arrsearch['shop_id']=$shop_id;
                    	if(!empty($customer))
                    		$arrsearch['customer_id']=$customer;
                    	if(!empty($requiredate))
                    		$arrsearch['date_required']=$requiredate;
                    		//array_push($arrsearch, 'date_required'=>$requiredate);
                    	if(!empty($orderdate))
                    		$arrsearch['date_required']=$orderdate;
                    		//array_push($arrsearch, 'date_required'=>$orderdate);
                    	if(!empty($paid))
                    		$arrsearch['paided']=$paid;
                    		

                    	
                    	$data['searchResults']	=$this->Order_model->get_invoice($arrsearch);
						
					}
					echo "<pre>";
					print_r($data['searchResults']);
					die();

        }
         if($this->input->post('pdfgen')){
      
            $this->load->helper(array('dompdf', 'file'));
    
            $orderid=$this->input->post('pdfgen');
            $update=array('paided'=>'1');
            $Success=$this->Order_model->update_order($update,array('id' => $orderid));
            $data['invoice']=$this->Order_model->get_invoice(array('order_num' => $orderid)); 
            $data['customer']=$this->Customer_model->getSingleCustomer($data['invoice'][0]->customer_id);
            $data['shopInfo']=$this->shop_model->singleShop($data['invoice'][0]->shop_id);
          
     		$html = $this->load->view('order/invoice', $data, true);
         /*  $html = "
                    <html>
                      <head>
                        <link rel='stylesheet' href='$path'>
                      </head>
                      <body>
                        <div class='myclass col-md-12'></div>
                      </body>
                    </html>
                  ";*/
         // echo $html;die;


               
           
     // page info here, db calls, etc.     
   
     pdf_create($html,'filename');

            }
            else{
        
                                $data['customers']=$this->Customer_model->getCustomer();
                                $data['shops']=$this->shop_model->get_all();
                                $data['products']=$this->Product_model->getProduct();
                                $data['orders']=$this->Order_model->get_order();
                                $data['orderNum']=$this->Order_model->all_order();
                                $data['empshop']=$this->User_model->currentUserInfo();
                                //echo '<pre>';print_r($data);die("d");
                                $this->load->view('admin/header');
                                $this->load->view('admin/left_menu');
                                $this->load->view('order/index',$data);
                                $this->load->view('admin/footer');
                }
    }
     public function wholesale()
     {
         $start=$this->input->POST('name_startsWith');
          $data['action']=$this->input->POST('action');
            if($data['action']=='lastorder'){

            	 $data['wholesale']=$this->Customer_model->getSingleCustomer($this->input->POST('getCustomerId'));
            	 $data['lastorder']=$this->Order_model->get_invoice(array('customer_id' => $this->input->POST('getCustomerId'))); 
             

            }
          elseif($data['action']=='invoice')
          {
                 
                  $where=array('shop_id'=>$this->input->POST('shop_id'));
            	 
                  $data['wholesale']=$this->Customer_model->getSingleCustomer($this->input->POST('getCustomerId'));
                  $data['products']=$this->Product_model->getAutoProduct($start,$this->input->POST('shop_id'));
                 //$data['wastages']=$this->Wastage_model->getShopWastage($where);

        }
        else{

                    $data['products']=$this->Product_model->getAutoProduct($start,$this->input->POST('shop_id'));
        }

        $this->load->view('order/ajax',$data);

     }

   public function invoice()
     {

         if($this->input->post('orderNum'))
        {
            $orderid=$this->input->post('orderNum');
           $data['invoice']=$this->Order_model->get_invoice(array('order_num' => $orderid)); 
           $data['customer']=$this->Customer_model->getSingleCustomer($data['invoice'][0]->customer_id);
           $data['shopInfo']=$this->shop_model->singleShop($data['invoice'][0]->shop_id);
           /*echo "<pre>";
           print_r($data['customer']);
           echo $data['invoice'][0]->customer_id;
           die(); */
           $this->load->view('order/wholesale',$data);
      
        }
        // invoice pdf generation
        if($this->input->post('pdfgen')){

               $this->load->helper(); 
               $this->load->helper(array('dompdf', 'file'));
               $this->load->helper('dompdf'); 
               $this->load->helper('file'); 
               $orderid=$this->input->post('pdfgen');
               $data['invoice']=$this->Order_model->get_invoice(array('order_num' => $orderid)); 
               $data['customer']=$this->Customer_model->getSingleCustomer($data['invoice'][0]->customer_id);
               $data['shopInfo']=$this->shop_model->singleShop($data['invoice'][0]->shop_id);
          
               $html= $this->load->view('admin/header');
               $html .=  $this->load->view('order/invoice',$data);
               $html .= $this->load->view('admin/footer');
 
               pdf_create($html, 'filename');

            }

        

     }
  //function for order
  public function order($data=null){
    if($this->input->post('submit')){
      $this->form_validation->set_rules('shop_id','Shop','required');
      $this->form_validation->set_rules('customer','customer','required');
      $this->form_validation->set_error_delimiters('<div class="error">', '</div>');
      if ($this->form_validation->run() == FALSE){
      }
      else{
      //echo '<pre>';print_r($this->input->post());die; 
        $jsonArr=array();
       $pid=$this->input->post('pid');
       $name=$this->input->post('itemName');
       $price=$this->input->post('price');
       $qty=$this->input->post('quantity');
       $total=$this->input->post('total');
       $size=sizeof($name);
       for($i=0;$i<$size;$i++){
          if(!empty($name[$i]))
          array_push($jsonArr ,array('pid'=>$pid[$i],'name'=>$name[$i],'price'=>$price[$i],'qty'=>$qty[$i],'total'=>$total[$i]));
       }
        $item=json_encode($jsonArr);
        $date=date('Y-m-d');
        $data =array('date_ordered' => $date,'date_required' => $this->input->POST('orderdate'),'shop_id'=>$this->input->POST('shop_id'),'paided'=> $this->input->POST('paid'),'item'=> $item,'customer_id'=> $this->input->POST('customer'),'order_num'=> $this->input->POST('ordnum'),'price'=> $this->input->POST('grandtotal'),'employee_id'=>$this->session->userdata('user_id'));
        $success= $this->Order_model->addorder($data);
        if($success)
        {
        $data['shops']=$this->shop_model->get_all();
        $data['order']=$this->input->post();
        $data['Customers']=$this->Customer_model->getSingleCustomer($data['order']['customer']);
        $this->load->view('order/order_pdf',$data);
          if($data['Customers']->email!="")
          {
           $this->load->view('order/sendpdf.php',$data);
          }
        }
        //$this->load->view('order/sendPdf');
        }
       }
if($this->input->post('search')){
  $this->form_validation->set_rules('customer', 'Customer Name', 'required');
  if ($this->form_validation->run() == FALSE){
  	$data['searchResults']=array();
    }else{
      $arrsearch=array();
       $shop_id=$this->input->post('shop_id');
      $customer=$this->input->post('customer');
      $requiredate=$this->input->post('requiredate');
      $orderdate=$this->input->post('orderdate');
      $paid=$this->input->post('paid');

      if(!empty($shop_id))
          $arrsearch['shop_id']=$shop_id;
        if(!empty($customer))
          $arrsearch['customer_id']=$customer;
        if(!empty($requiredate))
          $arrsearch['date_required']=$requiredate;
        //array_push($arrsearch, 'date_required'=>$requiredate);
        if(!empty($orderdate))
          $arrsearch['date_required']=$orderdate;
        //array_push($arrsearch, 'date_required'=>$orderdate);
        if(!empty($paid))
          $arrsearch['paided']=$paid;
          $data['searchResults']  =$this->Order_model->get_invoice($arrsearch);
        }
      }
       if($this->input->post('pdfgen')){
    
          $this->load->helper(array('dompdf', 'file'));
  
          $orderid=$this->input->post('pdfgen');
          $update=array('paided'=>'1');
          $Success=$this->Order_model->update_order($update,array('id' => $orderid));
         $data['invoice']=$this->Order_model->get_invoice(array('order_num' => $orderid)); 
         $data['customer']=$this->Customer_model->getSingleCustomer($data['invoice'][0]->customer_id);
         $data['shopInfo']=$this->shop_model->singleShop($data['invoice'][0]->shop_id);
        
   $html = $this->load->view('order/invoice', $data, true);
   pdf_create($html,'filename');
  }
  else{
    //$data['customers']=$this->Customer_model->getCustomer();
    //echo '<pre>';print_r($data['customers']);die;
    
    $data['shops']=$this->shop_model->get_all();
    $data['products']=$this->Product_model->getProduct();
    $data['orders']=$this->Order_model->get_order();
    $data['orderNum']=$this->Order_model->all_order();
    $data['empshop']=$this->User_model->currentUserInfo();
    //echo '<pre>';print_r($data);die("d");
    $this->load->view('admin/header');
    if($this->session->userdata('user_roll')!=1){
          
        page();
    }
    else{
        $this->load->view('admin/left_menu');
    }
    $this->load->view('order/shopOwnerOrder',$data);
    $this->load->view('admin/footer');
      }
    }
    function selectCustomerById()
    {
      $data['SelectCustomers']=$this->Customer_model->getCustomerListById();
      $data['allCustomer']=$this->Customer_model->getCustomer();
      $this->load->view('order/ajaxCustomerList',$data);
    }
}