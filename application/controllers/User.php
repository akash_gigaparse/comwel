<?php 
class User extends CI_Controller{
    public function __construct(){
        parent::__construct();
        $this->load->helper('url');
        $this->load->library('form_validation');
        $this->load->model('user_role_model');
        $this->load->library('session');
        // only allow users with 'admin' role to access all methods in this controller
        //$this->acl_auth->restrict_access('admin');
        //echo '<pre>';print_r($this->session->userdata());die;
    }
    public function index(){
        if(($this->session->userdata('logged_in')==1) && ($this->session->userdata('user_roll')==1))
            redirect('/admin', 'refresh');
        elseif(($this->session->userdata('logged_in')==1) && ($this->session->userdata('user_roll')==2))
            redirect('/employee/empdashboard', 'refresh');
        elseif(($this->session->userdata('logged_in')==1) && ($this->session->userdata('user_roll')==3))
            redirect('/employee/empdashboard', 'refresh');
        elseif(($this->session->userdata('logged_in')==1) && ($this->session->userdata('user_roll')==5))
            redirect('/SuperAdmin', 'refresh');
        elseif(($this->session->userdata('logged_in')==1)  && ($this->session->userdata('user_roll')==6))
            redirect('/ShopOwner', 'refresh');
        else
            $this->load->view('login');
    }
    
    //Ajax check if username(email) already exits
    public function checkEmail(){
        if(! $this->form_validation->is_unique($this->input->post('email'), 'users.email')){
           echo "true";
        }
        else
            echo "false";
    }

    //login form action
    public function process(){
        $remember = FALSE;
        $session_data = array('fname', 'email');
        $success = $this->acl_auth->login( $this->input->post('username'), $this->input->post('password'), $remember, $session_data );
        //if user exists then get role of that user
        if($success){
            $data=$this->user_role_model->get_role($this->session->userdata('user_id'));
            $this->session->set_userdata(array( 'user_roll'=> $data->roles_id));
            //echo '<pre>';print_r($data);die;
            if($data->roles_id==1)
                redirect('/admin', 'refresh');
            elseif($data->roles_id==5)
                redirect('/SuperAdmin', 'refresh');
            elseif($data->roles_id==6)
                redirect('/ShopOwner', 'refresh');
            else
                redirect('/employee/empdashboard', 'refresh');

        }
        else{
            redirect('', 'refresh');
        }
    }

    public function logout(){
        //$success = $this->acl_auth->logout();
        if($this->acl_auth->logout())
            redirect('', 'refresh');
    }
   
}

?>