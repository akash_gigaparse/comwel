<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Page extends CI_Controller {

 /**
  * Index Page for this controller.
  *
  * Maps to the following URL
  *   http://example.com/index.php/welcome
  * - or -
  *   http://example.com/index.php/welcome/index
  * - or -
  * Since this controller is set as the default controller in
  * config/routes.php, it's displayed at http://example.com/
  *
  * So any other public methods not prefixed with an underscore will
  * map to /index.php/welcome/<method_name>
  * @see http://codeigniter.com/user_guide/general/urls.html
  */

 public function page(){
  $this->load->helper('url');
  $this->load->database();
  $this->load->library('session');
  $this->load->model('user_role_model');
  $this->load->model('shop_model');
  $this->load->model('Page_model');
    
        $auth['shopEmpPage']=$this->shop_model->empShopPage($_SESSION['shop_id'],$this->session->userdata('user_roll'));
        $data=$this->user_role_model->get_role($this->session->userdata('user_id'));
        $auth['value']=$this->Page_model->get_pages($data,'7');
        $auth['pages']=$this->Page_model->fetch_pages();
          
         $this->load->view('left_menu',$auth);
    }

 public function custom_a(){
    die("akash");
 }
}