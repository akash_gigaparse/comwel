<?php 
class ListShop extends CI_Controller{
	public function __construct(){
        parent::__construct();
        //$this->load->library('session');
        $this->load->library('form_validation');
        $this->load->model('shop_model');  
        $this->load->model('role_model');  
        $this->load->model('user_role_model');
        //$this->acl_auth->restrict_access('admin');
         $this->load->model('User_model');
        $this->load->model('super_admin_model');
        $this->load->model('transaction_model');
        $this->load->helper('date');
        $this->load->helper('url');
        if(!$this->session->userdata('logged_in'))
        {
            redirect('', 'refresh');
        }
        
    }

     public function index()
     {
        if($this->input->POST())
        {
             $this->form_validation->set_rules('name', 'Name', 'required');
             $this->form_validation->set_rules('address', 'Address', 'required');
             $this->form_validation->set_rules('phone', 'Contact Details', 'required|regex_match[/^[0-9]+$/]');
            
            if (empty($_FILES['userfile']['name']))
            {
                    $this->form_validation->set_rules('userfile', 'Document', 'required');
            }
        
            $this->form_validation->set_rules('overhead', 'Default Product Overhead cost', 'required');
            $this->form_validation->set_rules('suburb', 'Suburb', 'required');
            $this->form_validation->set_rules('state', 'State', 'required');
            $this->form_validation->set_rules('postcode', 'Postcode', 'required');
            $this->form_validation->set_rules('country', 'Country', 'required');
            $this->form_validation->set_rules('longitude', 'Longitude', 'required');
            $this->form_validation->set_rules('latitude', 'Latitude', 'required');
            $this->form_validation->set_error_delimiters('<div class="error">', '</div>');
            
            if ($this->form_validation->run() == FALSE)
            {
            }
            else
            {

                $this->load->library('upload');
                if (!empty($_FILES['userfile']['name'])) 
                {
        
                        $config['upload_path'] = './uploads/';
                        $config['upload_path'];
                        $config['allowed_types'] = 'gif|jpg|png|jpeg';     
                      
                        $this->upload->initialize($config);
                      
                        if ($this->upload->do_upload('userfile'))
                        {
                                    $img = $this->upload->data();
                                    $file_name = $img['file_name'];
                            
                        }
                        else
                        {
                                echo $this->upload->display_errors();
                                     die();

                        }
                        
                        $data =array('name' => $this->input->POST('name'),'address' => $this->input->POST('address'),'phone'=> $this->input->POST('phone'),'logo'=> $file_name,'overhead'=> $this->input->POST('overhead'),'state' => $this->input->POST('state'),'suburb' => $this->input->POST('suburb'),'postcode' => $this->input->POST('postcode'),'country' => $this->input->POST('country'),'longitude' => $this->input->POST('longitude'),'latitude' => $this->input->POST('latitude'));
                        $success= $this->shop_model->insertShop($data);
                            
                        if($success)
                        {
                                    
                                    redirect('ListShop','refresh');
                        }
                        else
                              echo "<script>alert('There is some problem in inserting Shop');</script>";    
                 }
             }
        }
            $data['shops']=$this->shop_model->getById();
            $data['postData']=$this->input->POST();
            $data['shops']=$this->shop_model->get_all();
            $user=$this->session->userdata();
            $data['empshop']=$this->User_model->currentUserInfo();

            //fetch user data and get shop id
            $data['value']=$this->shop_model->getById();

            $this->load->view('admin/header');
             if($this->session->userdata('user_roll')!=1){
         // $data=empInfo();
            page();
          }
          else{
            $this->load->view('admin/left_menu');
        }
            $this->load->view('admin/showShops',$data);
            $this->load->view('admin/footer'); 

    }
    public function editShop()
    {
            $this->load->model('till_model');
            $id=$this->input->GET('id');
            if($this->input->POST('submit'))
            {
                    $this->form_validation->set_rules('name', 'Name', 'required');
                    $this->form_validation->set_rules('address', 'Address', 'required');
                    $this->form_validation->set_rules('phone', 'Contact Details', 'required|regex_match[/^[0-9]+$/]');
                  
                    $this->form_validation->set_rules('overhead', 'Default Product Overhead cost', 'required');
                    $this->form_validation->set_rules('suburb', 'Suburb', 'required');
                    $this->form_validation->set_rules('state', 'State', 'required');
                    $this->form_validation->set_rules('postcode', 'Postcode', 'required');
                    $this->form_validation->set_rules('country', 'Country', 'required');
                    $this->form_validation->set_rules('longitude', 'Longitude', 'required');
                    $this->form_validation->set_rules('latitude', 'Latitude', 'required');
                    $this->form_validation->set_error_delimiters('<div class="error">', '</div>');
                    if ($this->form_validation->run() == FALSE)
                    {
                         
                    }
                    else
                    {

                             $this->load->library('upload');
                             if (!empty($_FILES['userfile']['name'])) 
                             {
                                        $config['upload_path'] = './uploads/';
                                        echo $config['upload_path'];
                                        $config['allowed_types'] = 'gif|jpg|png|jpeg';     
                                        $this->upload->initialize($config);
  
                                        if ($this->upload->do_upload('userfile'))
                                        {
                                                $img = $this->upload->data();
                                                $file_name = $img['file_name'];
                                            
                                        }
                                        else
                                        {
                                                echo $this->upload->display_errors();
                                                die();

                                        }
                            }
                               
  
                            if (!empty($_FILES['userfile']['name']))
                            {
                                            $data =array('name' => $this->input->POST('name'),'address' => $this->input->POST('address'),'phone'=> $this->input->POST('phone'),'logo'=> $file_name,'overhead'=> $this->input->POST('overhead'),'state' => $this->input->POST('state'),'suburb' => $this->input->POST('suburb'),'postcode' => $this->input->POST('postcode'),'country' => $this->input->POST('country'),'longitude' => $this->input->POST('longitude'),'latitude' => $this->input->POST('latitude'));
          
                            }
                            else
                            {
                                            $data =array('name' => $this->input->POST('name'),'address' => $this->input->POST('address'),'phone'=> $this->input->POST('phone'),'deposite'=> $this->input->POST('deposite'),'overhead'=> $this->input->POST('overhead'),'state' => $this->input->POST('state'),'suburb' => $this->input->POST('suburb'),'postcode' => $this->input->POST('postcode'),'country' => $this->input->POST('country'),'longitude' => $this->input->POST('longitude'),'latitude' => $this->input->POST('latitude'));
                            }

                            $success= $this->shop_model->updateShop($data,$id);
                            if($success)
                            {
                                            redirect("ListShop/editShop?id=$id",'refresh');
                            }
                            else
                                echo "<script>alert('There is some problem in inserting Shop');</script>";  
                    } 
            }
            if($this->input->POST('update'))
            {
                
                    $this->form_validation->set_rules('tillid', 'Till ID', 'required');
                    $this->form_validation->set_rules('tillUser', 'Till User Name', 'required');
                    $this->form_validation->set_rules('password', 'Till Password', 'required');
                    $this->form_validation->set_rules('port', 'Port Number', 'required');
                    $this->form_validation->set_rules('ipadd', 'IP Address', 'required');
                    if ($this->form_validation->run() == FALSE){
                         
                    }
                    else
                    {
                        $data =array('tillid' => $this->input->POST('tillid'),'tillUsername' => $this->input->POST('tillUser'),'tillPassword'=> $this->input->POST('password'),'portNumber'=> $this->input->POST('port'),'ipAddress'=> $this->input->POST('ipadd'),'shopid'=>$id);
                        $success= $this->till_model->addTill($data,$id);
                                if($success)
                                {
                                      
                                        redirect("ListShop/editShop?id=$id",'refresh');

                                }
                                 else
                                echo "<script>alert('There is some problem in inserting Shop');</script>"; 
                    }
            }
               
            if($this->input->POST('editTill'))
            {
                    $this->form_validation->set_rules('edit_tillid', 'Till ID', 'required');
                    $this->form_validation->set_rules('edit_tillUser', 'Till User Name', 'required');
                    $this->form_validation->set_rules('edit_password', 'Till Password', 'required');
                    $this->form_validation->set_rules('edit_port', 'Port Number', 'required');
                    $this->form_validation->set_rules('edit_ipadd', 'IP Address', 'required');
                    $this->form_validation->set_error_delimiters('<div class="error">', '</div>');
                    if ($this->form_validation->run() == FALSE){
                         
                    }
                    else
                    {
                        $id2=$this->input->POST('id');
                        $data =array('tillid' => $this->input->POST('edit_tillid'),'tillUsername' => $this->input->POST('edit_tillUser'),'tillPassword'=> $this->input->POST('edit_password'),'portNumber'=> $this->input->POST('edit_port'),'ipAddress'=> $this->input->POST('edit_ipadd'));
                        $success= $this->till_model->editTill($id2,$data);
                        if($success)
                        {
                                        echo "<script>alert('Till Update successfully');</script>";
                                        //redirect("admin/editShop?id=$id",'refresh');
                        }
                        else
                        echo "<script>alert('There is some problem in inserting Shop');</script>"; 
                    }
            }
                 
            if($this->input->POST('updateAcc'))
            {
                    $this->form_validation->set_rules('bankName', 'BanK Name', 'required');
                    $this->form_validation->set_rules('bankAccountName', 'Bank Account Name', 'required');
                    $this->form_validation->set_rules('bsbNumber', 'BSB Number', 'required');
                    $this->form_validation->set_rules('bankAccountNumber', 'Bank Account Number', 'required');
                    $this->form_validation->set_rules('accountCodes', 'Account Codes', 'required');
                    $this->form_validation->set_rules('gstFreeSales', 'Gst Free Sales', 'required');
                    $this->form_validation->set_rules('gstIncludedSales', 'GST Free Sales', 'required');
                    if ($this->form_validation->run() == FALSE)
                    {
                         
                    }
                    else
                    {
                        $data =array('bankName' => $this->input->POST('bankName'),'bankAccountName' => $this->input->POST('bankAccountName'),'bsbNumber'=> $this->input->POST('bsbNumber'),'accountNumber'=> $this->input->POST('bankAccountNumber'),'accountCodes'=> $this->input->POST('accountCodes'),'gstFreeSales'=> $this->input->POST('gstFreeSales'),'gstIncludedSales'=> $this->input->POST('gstIncludedSales'));
                        $success= $this->shop_model->updateShop($data,$id);
                                if($success)
                                {
                                        //echo "<script>alert('Shop Update successfully');</script>";
                                        redirect("ListShop/editShop?id=$id",'refresh');

                                }
                                else
                                 echo "<script>alert('There is some problem in inserting Shop');</script>"; 
                    }
            }

            $data['till']=$this->input->POST();
            $data['value']=$this->shop_model->editshop();
            $data['editTill']=$this->till_model->getTillWhere(array('shopid' => $id));

            if($this->input->POST('editTillId'))
            {
       
                $pid=$this->input->POST('editTillId');
                $data['editTillInfo']=$this->till_model->getTill($pid);

    
            }
            else if($this->input->POST('id') )
            {

                $data['editTillInfo']=$this->till_model->getTill($this->input->POST('id'));
                $data['editTillInfo']=$this->till_model->getTill($this->input->POST('id'));

            }
            //$data['empshop']=$this->User_model->currentUserInfo();
            $this->load->view('admin/header');
            $this->load->view('admin/left_menu');
            $this->load->view('admin/editShop',$data);
            $this->load->view('admin/footer');
    }
    //suspend shop  by  admin
    public function suspendShop()
    { 
        $this->super_admin_model->suspend($this->input->post('suspendShop'));
        //fetch all show 
        $data['shops']=$this->shop_model->getById();
        $this->load->view('admin/suspendAjaxResponce',$data);

    } 
    //suspend shop by  admin
    public function rejoinShop()
    {
        
        $this->super_admin_model->rejoin($this->input->post('rejoin'));
        //show all user 
        $data['shops']=$this->shop_model->getById();
        
        $this->load->view("admin/rejoinAjaxResponce",$data);

    }

    

}
