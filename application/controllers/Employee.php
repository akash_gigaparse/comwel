<?php 
class Employee extends CI_Controller{
	public function __construct(){
        parent::__construct();
        $this->load->library('form_validation');

        $this->load->model('shop_model');  
        $this->load->model('role_model');  
        $this->load->model('user_role_model');
        $this->load->model('product_model'); 
        $this->load->model('shop_model');
        //$this->acl_auth->restrict_access('admin');
        $this->load->model('super_admin_model');
        $this->load->model('transaction_model');
        $this->load->model('User_model');
        $this->load->helper('url');
        if(!$this->session->userdata('logged_in'))
        {
            redirect('', 'refresh');
        }
       
    }

    public function index(){
       

        if($this->input->post('empShopPagePost'))
        {
         
            $this->form_validation->set_rules('shop', 'Shop name', 'required');
            $this->form_validation->set_rules('User', 'User name', 'required'); 
            $this->form_validation->set_rules('pageId[]', 'Pages ', 'required');
        
        //echo"<pre>";print_r($this->input->post('pageId')); die("hello");
             if ($this->form_validation->run() == FALSE){
            }else{
                 $pageid=implode(',',$this->input->post('pageId'));
                 $this->transaction_model->shopEmpPageInsert($pageid);
             }
                $data['rolesPostData']=$this->input->POST();
        }
        if($this->input->POST('creeatrole')){
            $this->form_validation->set_rules('name', 'Role', 'required|is_unique[roles.name]');
            $this->form_validation->set_error_delimiters('<div class="error">', '</div>');
            if ($this->form_validation->run() == FALSE){
                 
            }else{
                $data =array('name' => $this->input->POST('name') );
                $success=$this->role_model->insertRole($data);
                if($success)
                    echo "<script>alert('Role inserted successfully');</script>";
                else
                    echo "<script>alert('There is some problem in inserting Role');</script>";    
            }
             $data['postRoll']=$this->input->POST();
        }
        
          if($this->input->POST('submit')){
            $this->form_validation->set_rules('email', 'Email', 'trim|required|valid_email|is_unique[users.email]');
            $this->form_validation->set_rules('password', 'Password', 'required|max_length[30]');
            $this->form_validation->set_rules('fname', 'First Name', 'required|max_length[30]');
            $this->form_validation->set_rules('mobile', 'Phone', 'required|regex_match[/^[0-9]+$/]|min_length[10]|max_length[12]');
            $this->form_validation->set_rules('address', 'Address', 'required|max_length[250]');
            //$this->form_validation->set_rules('termination', 'Termination Date', 'required');

            $this->form_validation->set_rules('employement', 'Employement Date', 'required');
            $this->form_validation->set_rules('position', 'Position', 'required|max_length[30]');
            $shop = $this->input->post('shop');
               
            $this->form_validation->set_rules('roll', 'roll', 'required');
            if(sizeof($shop)==0)
            $this->form_validation->set_rules('shop', 'Shop name', 'required');  // Validating select option field.
            
            $this->form_validation->set_error_delimiters('<div class="error">', '</div>');
            if ($this->form_validation->run() == FALSE){
                $data['postData']=$this->input->POST();
            }else{

                 
                    $shop=$this->input->POST('shop');
                    $shop_id=implode(",",$shop);
                    $data =array('add_user_ids'=>$this->session->userdata('user_id'),'shop_ids'=>$shop_id,'email' => $this->input->POST('email'),'password' => $this->input->POST('password'),'fname' => $this->input->POST('fname'),'lname' => $this->input->POST('lname'),'mobile' => $this->input->POST('mobile'),'address' => $this->input->POST('address'),'termination' => $this->input->POST('termination'),'employement' => $this->input->POST('employement'),'position' => $this->input->POST('position'),'suspend'=>1);
               

            $last_insert_id = $this->acl_auth->register($data);
            
            $roll=array('roles_id'=>$this->input->POST('roll'),'users_id'=>$last_insert_id);
            $success=$this->user_role_model->registerRole($roll);
            if($last_insert_id)
                echo "<script>alert('Registration done successfully');</script>";
                else
                echo "<script>alert('There is some problem in Registration');</script>";    
            }
             
       }
       

        $data['pages']=$this->transaction_model->getPages();
        
        $this->load->model('page_model');
        //select all employee of selected shop in dropdown
         $data['allCustomer']=$this->transaction_model->getSalesPersonNameByshopId();
        //show add  user 
        $data['users']=$this->User_model->getListById();
        //fetch all roll
        $data['roll']=$this->role_model->get_roll();

         //fetch all show 
        $data['shops']=$this->shop_model->getById();
        // select empshops
        $data['empshop']=$this->User_model->currentUserInfo();
        //fetch all pages
        $data['value2']=$this->page_model->fetch_pages();
        $data['value3']=$this->page_model->show_roles2();
//echo '<pre>';print_r($data);die;

        $data['empshop']=$this->User_model->currentUserInfo();

            $this->load->view('admin/header');
            if($this->session->userdata('user_roll')==5)
            $this->load->view('superadmin/left_menu');
            else{  if($this->session->userdata('user_roll')!=1){
          
            page();
        }
        else{
          $this->load->view('admin/left_menu');
        }
    }
            //$this->load->view('admin/left_menu');
            $this->load->view('admin/tabs',$data);
            $this->load->view('admin/footer');
        
        
        
           
    }

    public function empDashboard(){
        $data['value']=$this->product_model->getProduct();
        $data['shops']=$this->shop_model->get_all();
        $user=$this->session->userdata();
        $data['empshop']=$this->User_model->currentUserInfo();

        $this->load->view('admin/header');
        page();
        //$this->page();
        $this->load->view('employee/dashboard',$data);
        $this->load->view('employee/footer');
    }

    //Function for Employee Edit
    public function employeeEdit(){
        $data['value']=$this->product_model->getProduct();
        $this->load->view('employee/header');
        $this->load->helper('left_menu');
        $this->left_menu->page();
        //$this->page();
        $this->load->view('employee/showDetail',$data);
        $this->load->view('employee/footer');   
        //$this->load->model('');
    }
	
      public function editUser($data=NULL)
    {
        if(!$this->input->GET('id'))
        {
        redirect('employee');
        }
        
        $this->load->model('User_model');
        $data =array('shop'=>$this->input->POST('shop'),'users_id' => $this->input->POST('id'),'email' => $this->input->POST('email'),'fname' => $this->input->POST('fname'),'lname' => $this->input->POST('lname'),'mobile' => $this->input->POST('mobile'),'address' => $this->input->POST('address'),'termination' => $this->input->POST('termination'),'employement' => $this->input->POST('employement'),'position' => $this->input->POST('position'),'suspend'=>1);
       
        if($this->input->POST('submit')){
            $this->form_validation->set_rules('email', 'Email', 'trim|required|valid_email');
            //$this->form_validation->set_rules('password', 'Password', 'required|max_length[30]');
            $this->form_validation->set_rules('fname', 'First Name', 'required|max_length[30]');
            $this->form_validation->set_rules('mobile', 'Phone', 'required|regex_match[/^[0-9]+$/]|min_length[10]|max_length[12]');
            $this->form_validation->set_rules('address', 'Address', 'required|max_length[250]');
            //$this->form_validation->set_rules('termination', 'Termination Date', 'required');
            $this->form_validation->set_rules('employement', 'Employement Date', 'required');
            $this->form_validation->set_rules('position', 'Position', 'required|max_length[30]');
            $this->form_validation->set_rules('shop[]', 'Shop', 'required');
            $this->form_validation->set_error_delimiters('<div class="error">', '</div>');
            if($this->form_validation->run() == FALSE){
               
                $data['postUserData']=$this->input->POST();
            //die("sdddd");
            }else{
                $this->load->model('User_model');
                $update=$this->User_model->update_user();
                if($update)
                {
                    echo "<script>alert('Updation done successfully');</script>";
                } 
                ?>
                <script>window.location.replace("<?php echo base_url() ?>employee");</script>

                <?php
                //$this->_redirect('employee','refresh');
                //$this->index();
                //redirect('employee');
            }
        }   
    $data['empshop']=$this->User_model->currentUserInfo();    
    $data['value']=$this->User_model->editUser();
    $data['roll']=$this->role_model->get_roll();
    $data['shops']=$this->shop_model->getById();
    $this->load->view('admin/header');
    if($this->session->userdata('user_roll')==5)
    $this->load->view('superadmin/left_menu');
    else
    $this->load->view('admin/left_menu');
    $this->load->view('admin/editUser',$data);
    $this->load->view('admin/footer');
    
    } 
    public function sessionchange(){

        $_SESSION['shop_id']=$_POST['shop'];
        echo 1;

    }
    //suspend user by admin or shop owner
    public function suspendUser()
    {

        $this->load->model('User_model');
        $this->User_model->suspend($this->input->post('suspend'));
          //show all user 
        $data['value']=$this->User_model->getListById();
        $this->load->view('admin/ajaxCall',$data);

    }
     //suspend user by admin or shop owner
    public function rejoinUser()
    {
        $this->load->model('User_model');
        $this->User_model->rejoin($this->input->post('rejoin'));
        $data['value']=$this->User_model->getListById();
        $this->load->view("admin/ajax",$data);

    }
     //delete user by admin
    public function deleteUser()
    {
        $this->load->model('User_model');
        $data=$this->User_model->delete_rol($this->input->post('remove'));
        $data=$this->User_model->delete($this->input->post('remove'));
    }


}
