<?php 
class PaymentReceivedPage extends CI_Controller{
	public function __construct(){
        parent::__construct();
        //$this->load->library('session');
        $this->load->library('form_validation');
        $this->load->model('shop_model');  
        $this->load->model('role_model');  
        $this->load->model('user_role_model');
        //$this->acl_auth->restrict_access('admin');
        $this->load->model('super_admin_model');
        $this->load->model('transaction_model');
        $this->load->helper('date');
        $this->load->helper('url');
        if(!$this->session->userdata('logged_in'))
        {
            redirect('', 'refresh');
        }
        
    }

    //payment recieved page      
    public function index(){
        //code for select user shop where user_id in users table
        $user_id=$this->session->userdata('user_id');
        
            $data['selectShop']=$this->transaction_model->selectUserShop($user_id);
       if($this->input->post())
        {
                 
        $this->transaction_model->orderPaidUpdate();

        }
        $this->transaction_model->updatePaid();
        $data['shops']=$this->transaction_model->getAllShop();
        //echo '<pre>';print_r($data);die("shops data");
        $this->load->view('admin/header');
           if($this->session->userdata('user_roll')!=1){
                $data['selectShop']="";
                $data=empInfo();
                page();
          }
          else{

            $this->load->view('admin/left_menu');
       }
        $this->load->view('admin/PaymentReceivedPage',$data);
        $this->load->view('admin/footer'); 

    }
     //select customer for payment unPaid
   public function selectCustById(){ 
            //echo '<pre>';print_r($this->input->post());die("Asdf");
            if($this->input->post('shopId')=="Select"){ echo "<option>Select</option>";?><script>window.location.reload();</script><?php }
            $shop_id=$this->input->post('shopId');
            $data['custName']=$this->transaction_model->getCustNameByshopId();
            
            $i=0;

            foreach ($data['custName'] as $custName) {
                if($i==0){
                    echo "<option>Select</option>";
                }
                echo "<option value='$custName->id'>$custName->name</option>";
            $i++;
            }
    }
        //select order where paided =2 
    public function orderPaided(){

$grandtotal=0;
$id=0;

           //echo $this->input->post('custId');die('hre');
            $data['order']=$this->transaction_model->orderPaidedSelect();
          //echo '<td>'.foreach(json_decode($row['item']) as $item){ print_r($item); }.'</td>';
                    echo '<tr>';
                    echo '<td>'.'Date Ordered'.'</td>';
                    echo '<td>'.'total'.'</td>';
                    echo '<td>'.'Paided'.'</td>';
                    echo'</tr>';

            foreach ($data as $result)
            {
                        foreach($result as $row)
                        {
                    
                            if($row['item'])
                            {

                                     $item=json_decode(($row['item']));
                                    foreach ($item as $value) 
                                    {
                                

                                $grandtotal+=$value->total;
//admin/PaymentReceivedPage/
                            }
                            echo $id=$row['id'];
                    echo '<tr>';
                                  echo '<td>'.$row['date_ordered'].'</td>';
                                  echo '<td>'.$grandtotal.'</td>';
                                  /*$linkAttr = array('title'=>'update Paided','class'=>'update');
                                echo anchor('admin/PaymentReceivedPage/'.$row['id'], 'Paid','Paid');
                                */  echo "<td><a onclick='updateOrdersPaid($id)' class='customdelete btn btn-success' title='Paying' href='#'>Paying</a></td>";
                    echo'</tr>';

                            }
                        }            
           }
    


    }

    //update order table where paided =2
    public function orderUpdatePaided(){
    $orderid=$this->input->post('orderid');
    $result=$this->transaction_model->updatePaid($orderid);
    $grandtotal=0;
    $id=0;
   //echo $this->input->post('custId');die('hre');
    $data['order']=$this->transaction_model->orderPaidedSelect();
    echo '<tr>';
    echo '<td>'.'Date Ordered'.'</td>';
    echo '<td>'.'total'.'</td>';
    echo '<td>'.'Paided'.'</td>';
    echo'</tr>';
        foreach ($data as $result) {
            foreach($result as $row){
                if($row['item']){
                    $item=json_decode(($row['item']));
                        foreach ($item as $value) {
                            $grandtotal+=$value->total;
                        }
                        echo $id=$row['id'];
                    echo '<tr>';
                        echo '<td>'.$row['date_ordered'].'</td>';
                        echo '<td>'.$grandtotal.'</td>';
                        echo "<td><a onclick='updateOrdersPaid($id)' class='customdelete btn btn-success' title='Paying' href='#'>Paying</a></td>";
                    echo'</tr>';
                   
                }
            }
        }
    }
}
