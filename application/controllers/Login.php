<?php 
class Login extends CI_Controller{
    public function __construct(){
        parent::__construct();
        $this->load->helper('url');
        $this->load->library('acl_auth');
    }
    public function index(){
        //$data = array( 'fname' => 'testuser', 'password' => '123456', 'email' => 'testuser@gmail.com','lname' => 'ttest' );
        //$success = $this->acl_auth->register( $data );
        //echo $success;die;
        $this->load->view('login');
    }
    public function process(){
       // echo "<pre>"; print_r($this->input->post());die;
        // This logs the user in and writes user_name, user_email and user_phone into session. Also sets a cookie to remember the user.
        $remember = TRUE;
        $session_data = array('fname', 'email');
        $success = $this->acl_auth->login( $this->input->post('username'), $this->input->post('password'), $remember, $session_data );
        echo $success;die;
    }
}

?>