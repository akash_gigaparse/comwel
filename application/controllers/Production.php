<?php 
class Production extends CI_Controller {
  public $aObj;
	public function __construct(){
  parent::__construct();
  ini_set('memory_limit', '1024M');

  $this->load->library('form_validation');
 
  $this->load->model('Products_model'); 
  $this->load->model('Production_model'); 
  $this->load->model('product_model');
  $this->load->model('Order_model'); 
  $this->load->model('User_model');
  $this->load->model('shop_model');

  if(!$this->session->userdata('logged_in'))
  {
      redirect('', 'refresh');
    }
  }



  //function for order
  public function index($data=null){
  //add production products for a shop previous case
    if($this->input->post('submit')){
    $productID=$this->input->post('pid');
    $shopID=$this->input->post('shop_id');
    $class=$this->input->post('class');
    $qty=$this->input->post('total');
    $size=sizeof($productID);
    for($i=0;$i<$size;$i++){
      if(!empty($productID[$i])){
        $data =array('product_id' => $productID[$i],'pqty' => $qty[$i],'shop_id' => $shopID,'date'=>date('Y-m-d'),'employee_id'=>$this->session->userdata('user_id'),'class'=>$class);
        //$data =array('pqty' => $qty[$i],'shop_id' => $shopID,'date'=>date('Y-m-d'),'employee_id'=>$this->session->userdata('user_id'));
        $this->Production_model->addProduction($data);
        }
      }
    }
     if($this->input->post('generate')){
      //add production products for a shop: current case
      $data['value']=$this->Products_model->getProducts();
      // $data['productionReports']=$this->Production_model->productionReport($shopID=$this->input->post('shop_id'),$this->input->post('fromdate'),$this->input->post('todate'));
      $data['productionReports2']=$this->Products_model->productionReport($shopID=$this->input->post('shop_id'),$this->input->post('fromdate'),$this->input->post('todate'));
     
      $data['products']=$this->Products_model->getShopProducts($this->input->post('shop_id'));
      $data['shoporder']=$this->Order_model->get_invoice(array('shop_id'=>$this->input->post('shop_id')))  ;
     // echo date("YY-mm-dd");
      $data['class']=$this->Production_model->getProductClassByshopId($this->input->POST('shop_id'));
     
      $data['specialOrder']=$this->Order_model->get_invoice(array('shop_id'=>$this->input->post('shop_id'),'order_type'=>'0','date_ordered'=>date("Y-m-d",strtotime("-1 days")))) ;
      $data['wholeSale']=$this->Order_model->get_invoice(array('shop_id'=>$this->input->post('shop_id'),'order_type'=>'1','date_ordered'=>date("Y-m-d",strtotime("-1 days"))))  ;
      $data['checkProduction']=$this->Production_model->checkCurrentProduction(date('Y-m-d'),$this->input->post('shop_id'),$this->input->post('product_class'));
     }
     else{
      $data['products']=array();
      $data['checkProduction']=array();
      $data['productionReports']=array();
    }     
  //get all product in product table
    $data['value']=$this->Products_model->getProducts();
    $data['shops']=$this->shop_model->get_all();
    $data['empshop']=$this->User_model->currentUserInfo();
  //       echo '<pre>';print_r($data);die("jere");
    $this->load->view('admin/header');
    
if($this->session->userdata('user_roll')!=1){
                $data['selectShop']="";
              //  $data=empInfo();
                $shop_id_emp=$this->session->userdata('shop_id');
                //echo $shop_id_emp;
                $data['shopRecord']=$this->product_model->getProductBySingleShop($shop_id_emp);
               
                page();
        }
        else{

            $this->load->view('admin/left_menu');
       }
    $this->load->view('production/index',$data);
    $this->load->view('admin/footer');
  }
  function selectProductByShopId()
    { 
      $data['class']=$this->Production_model->getProductClassByshopId($this->input->POST('shopId'));
      $this->load->view('production/ajaxProductList',$data);
    }


}