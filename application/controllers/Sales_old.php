<?php 
class Sales extends CI_Controller{
	public function __construct(){
        parent::__construct();
        $this->load->model('shop_model');
        $this->load->database();
        $this->load->helper('url');
        $this->load->helper('csv');
   
    }

    public function index($data=NULL){
        //show all user 
        $data['users']=$this->user_model->get_all();
        //echo '<pre>';print_r($data);die;
        $this->load->view('admin/header');
        $this->load->view('admin/left_menu');
        $this->load->view('admin/sales',$data);
        $this->load->view('admin/footer');    
    }

    public function searchByDate()
    {
        if($this->input->POST()){
            $data['chart']=$this->shop_model->getSearchByDate();
            $date1=$this->input->POST('date1');
            $date2=$this->input->POST('date2');
            $user=$this->input->POST('user');
            $data['date']=array('date1' => $date1,'date2' => $date2, 'user'=>$user);
            return $this->index($data);
        }else{
           return $this->index();
         }
    }

    function csv($query, $filename = 'CSV_Report.csv')
    {
        $date1=$this->input->POST('date1');
        $date2=$this->input->POST('date2');
        $user=$this->input->POST('user');
        $this->load->dbutil();
        $this->load->helper('download');
        $delimiter = ",";
        $newline = "\n";
        $query = $this->db->query('SELECT * FROM orders where employee_id="'.$user.'" AND (date_ordered BETWEEN "'.$date1.'" AND "'.$date2.'")');
        $data = $this->dbutil->csv_from_result($query, $delimiter, $newline);
        force_download($filename, $data);
    }
}
