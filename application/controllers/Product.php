<?php 
class Product extends CI_Controller{
	public function __construct(){
        parent::__construct();
        $this->load->library('form_validation');
        //$this->acl_auth->restrict_access('admin');
         $this->load->model('product_model'); 
         $this->load->model('Wastage_model'); 
           $this->load->model('User_model');
        $this->load->model('shop_model');
        if(!$this->session->userdata('logged_in'))
        {
            redirect('', 'refresh');
        }
    }

    //function for order
    public function index($data=null){
    //add wastage products for a shop
     if($this->input->post('submit')){
       // echo '<pre>';print_r($this->input->post());die;
        $productID=$this->input->post('pid');

        $shopID=$this->input->post('shop_id');
        $notes=$this->input->post('notes');
        $qty=$this->input->post('qty');
        $size=sizeof($productID);
            //echo '<pre>';print_r($this->input->POST());die;     
        for($i=0;$i<$size;$i++){
            if(!empty($productID[$i])){
            $data =array('product_id' => $productID[$i],'qty' => $qty[$i],'shop_id' => $shopID,'employee_id'=>$this->session->userdata('user_id'),'note' => $notes[$i],'date'=>date('Y-m-d'));
            $this->Wastage_model->addWastage($data);}
        }
     }
    if($this->input->post('search')){
        $shopId=$this->input->POST('shop');
            //echo '<pre>';print_r($this->input->POST());die;
        $data['shopRecord']=$this->product_model->getProductBySingleShop($shopId);

    }
           

    //get all product in product table
        $data['shops']=$this->shop_model->get_all();
        $data['empshop']=$this->User_model->currentUserInfo();
        $empShopIds=explode(',',$data['empshop']->shop_ids);
        if(count($empShopIds)==1)
        {
            $data['value']=$this->product_model->getProductBySingleShop($data['empshop']->shop_ids);
        }    
        
        $this->load->view('admin/header');
        if($this->session->userdata('user_roll')!=1){
            $data['selectShop']="";
            $shop_id_emp=$this->session->userdata('shop_id');
            $data['shopRecord']=$this->product_model->getProductBySingleShop($shop_id_emp);
            //$data=empInfo();
            page();
        }
        else{

            $this->load->view('admin/left_menu');
       }
       //echo '<pre>';print_r($data);die;

        $this->load->view('product/index',$data);
        $this->load->view('admin/footer');
        }
        public function searchByShopid(){
             
        }
    //function for create new Customer
    public function newPoduct(){
        if($this->input->POST()){
            $this->form_validation->set_rules('name', 'Name', 'required');
            $this->form_validation->set_rules('class', 'Class', 'required');
            $this->form_validation->set_rules('cost', 'cost', 'required|regex_match[/^[0-9]+$/]');
            $this->form_validation->set_rules('wholesale','Wholesale', 'required|regex_match[/^[0-9]+$/]');
            $this->form_validation->set_rules('retail', 'Retail', 'required|regex_match[/^[0-9]+$/]');

            $this->form_validation->set_error_delimiters('<div class="error">', '</div>');
            if ($this->form_validation->run() == FALSE){
            
            }else{
            $data =array('name' => $this->input->POST('name'),'class' => $this->input->POST('class'),'cost' => $this->input->POST('cost'),'retail' => $this->input->POST('retail'),'wholesale' => $this->input->POST('wholesale'));
            $this->product_model->newPoduct($data);
            redirect('product/newPoduct');
            }
       }
       $data['postData']=$this->input->POST();
        return $this->index($data);
    }
    //fucntion for delete Product
    public function deleteProduct()
    {
        $data=$this->product_model->deleteById();
    }
    //show specific edit user
    public function editProduct($data=NULL)
    {

    $data['value']=$this->product_model->editProduct();
    $this->load->view('admin/header');
      if($this->session->userdata('user_roll')!=1){
        $data['empshop']=$this->User_model->currentUserInfo();
        page();
      }
      else{
    $this->load->view('admin/left_menu');
    
    }
    $this->load->view('product/editProduct',$data);
    $this->load->view('admin/footer');
    }   
        //update Product 
    public function updatePorduct(){

    $data =array('id' => $this->input->POST('id'),'name' => $this->input->POST('name'),'class' => $this->input->POST('class'),'cost' => $this->input->POST('cost'),'wholesale' => $this->input->POST('wholesale'),'filter' => $this->input->POST('filter'));
    if($this->input->POST()){
            $this->form_validation->set_rules('name', 'Name', 'required');
            $this->form_validation->set_rules('class', 'Class', 'required');
            $this->form_validation->set_rules('cost', 'cost', 'required|numeric');
            $this->form_validation->set_rules('wholesale', 'Wholesale', 'required|numeric');
            //$this->form_validation->set_rules('retail', 'Retail', 'required|regex_match[/^[0-9]+$/]');
            $this->form_validation->set_error_delimiters('<div class="error">', '</div>');
            if ($this->form_validation->run() == FALSE){
            $data['postProductData']=$this->input->POST();
            return $this->editProduct($data);
            }else{
                //$this->load->model('User_model');
                $result=$this->product_model->updateProduct($data);
                $this->index();
            }

        }
    }

    public function query(){
        $this->product_model->query();
         return $this->index();
    }

}