<?php 
class Stockage extends CI_Controller{
	public function __construct(){
        parent::__construct();
         $this->load->library('form_validation');
         //$this->acl_auth->restrict_access('admin');
         $this->load->model('product_model'); 
          $this->load->model('products_model');
         $this->load->model('Wastage_model'); 
         $this->load->model('Production_model'); 
         $this->load->model('User_model');
         $this->load->model('shop_model');
         $this->load->model('Stockage_model');
      $this->load->model('order_model');
      if(!$this->session->userdata('logged_in'))
        {
            redirect('', 'refresh');
        }
    }

    //function for order
    public function index($data=null){
        //add wastage products for a shop
        if($this->input->post('submit')){
            $productID=$this->input->post('pid');
            $shopID=$this->input->post('shop_id');
            $qty=$this->input->post('totalQty');
            $size=sizeof($productID);
            for($i=0;$i<$size;$i++){
                if(!empty($productID[$i]))
                  $data =array('product_id' => $productID[$i],'pqty' => $qty[$i],'shop_id' => $shopID,'date'=>date('Y-m-d'),'employee_id'=>$this->session->userdata('user_id'));
                  $this->Stockage_model->addStockage($data);
            }
        }
        $data['value']=$this->product_model->getProduct();
        $data['shops']=$this->shop_model->get_all();
        $data['empshop']=$this->User_model->currentUserInfo();
        //echo '<pre>';print_r($data);die;
        $this->load->view('admin/header');
           
if($this->session->userdata('user_roll')!=1){
                $data['selectShop']="";
              //  $data=empInfo();
                $shop_id_emp=$this->session->userdata('shop_id');
                //echo $shop_id_emp;
                $data['shopRecord']=$this->product_model->getProductBySingleShop($shop_id_emp);
               
                page();
        }
        else{

            $this->load->view('admin/left_menu');
       }
        $this->load->view('stockage/index',$data);
        $this->load->view('admin/footer');
    }
         
 
         public function ajax($data=null){
            $data['action']=$this->input->POST('action');
                //add wastage products for a shop
           if($this->input->post('shopId')){
                     $orderqty=array();
                     $checkStockage=$this->Stockage_model->checkCurrentStockage(date('Y-m-d'),$this->input->post('shopId'));
                    
                    // $product=$this->product_model->getShopProduct($this->input->post('shopId'));
                      $products=$this->products_model->getShopProducts($this->input->post('shopId'));
                     /* echo "<pre>";

print_r($products);
die();     */         $shoporder=$this->order_model->get_invoice(array('shop_id'=>$this->input->post('shopId')))  ;
                  
                    foreach($shoporder as $item){
                    
                    $orders=json_decode($item->item);
                    $num=0;
                            foreach ($orders as $order) {
                                
                                        $order = (array)($order);
                                        $num=0;
                                        for($i=0;$i<count($orderqty);$i++){

                                                        if($orderqty[$i]['pid']==$order['pid']){
                                                        
                                                             $orderqty[$i]['qty']=$orderqty[$i]['qty']+$order['qty'];  
                                                             $num=1;
                                                         }
                             
                                        }


                        
                                         if(!isset($orderqty) || $num==0)
                     
                                                        array_push($orderqty,$order);
                                                            
                             }
                  }


                  
                 $shopProduct = $this->product_model->getShopProduct($this->input->post('shopId'));
                 $shopId=$this->input->post('shopId');
                 echo "<input type='hidden' name='shop_id' value='$shopId' />";
                 if(count($checkStockage)==0){
                        echo "<input name='checkStockage' type='hidden' value='0' id='checkStockage'/>";
                 }
                 else
                 {
                      echo "<input name='checkStoage' type='hidden' value='1' id='checkStockage' />";
                 }
                 foreach($shopProduct as $product){
                    //echo $product->shop_id;
                     $productionQty=$this->Production_model->getProdution($product->id,$product->shop_id);
                     

                     if(count($productionQty)==0)
                        $proQty=0;
                    else
                        $proQty=$productionQty[0]->pqty;

                      $wastageQty=$this->Wastage_model->getShopWastage($product->id);
                      if(count($wastageQty)==0)
                        $wasteQty=0;
                     else
                        $wasteQty=$wastageQty[0]->qty;

                    $stockOrderQty=0;

                    for($i=0;$i<count($orderqty);$i++){

                                                        if($orderqty[$i]['pid']==$product->id){
                                                        
                                                             $stockOrderQty=$orderqty[$i]['qty'];
                                                         }
                                                         
                    }
                    for($i=0;$i<count($products);$i++){
                        

                                                        if($products[$i]->item==$product->name){
                                                        
                                                             $stockOrderQty= $stockOrderQty+$products[$i]->qty;
                                                         }
                                                         
                    }


                     $stockTotalQty=0;

                    for($i=0;$i<count($checkStockage);$i++){

                                                        if($checkStockage[$i]->product_id==$product->id){
                                                        
                                                             $stockTotalQty=$checkStockage[$i]->pqty;
                                                         }
                                                         
                    }


                          $remainQty=$proQty-$stockOrderQty-$wasteQty;
                          if(count($checkStockage)==0){
                            
                            $totalStockhtml='<input type="text" name="totalQty[]" value="'.$remainQty.'" />';
                          }
                          else{
                            $totalStockhtml=$stockTotalQty;

                          }            
                          echo "<input type='hidden' name='pid[]' value='$product->id' id='pid'/>";
                         echo '<tr>

                            <td>'.$product->name.'</td>
                            <td>'.$proQty.'</td>
                            <td>'.$stockOrderQty.'</td>
                            <td>'.$wasteQty.'</td>
                            <td>'.$totalStockhtml.'</td>
                        </tr>';
                      

                
                    }
        }
    }
   

}