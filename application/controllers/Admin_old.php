<?php 
class Admin extends CI_Controller{
	
	public function __construct(){
        parent::__construct();
        $this->acl_auth->restrict_access('admin');
    }

    public function index(){
    	echo "This is admin section";
    	echo "<br>";
    	echo "<a href='user/logout'>Logout</a>";
    }

    public function register(){
        if($this->input->post()){
            $success = $this->acl_auth->register( $this->input->post());
            if($success)
                echo "<script>alert('Registration done successfully');</script>";
            else
                echo "<script>alert('There is some problem in Registration');</script>";    
        }
        $this->load->view('header');
        $this->load->view('left_menu');
        $this->load->view('register');
        $this->load->view('footer');    
    }
}
