<?php 
class Customer extends CI_Controller{
	public function __construct(){
    parent::__construct();
    $this->load->library('form_validation');
     $this->load->model('customer_model');  
     $this->load->model('Customer_model');  

     $this->load->model('shop_model');
        if(!$this->session->userdata('logged_in'))
        {
            redirect('', 'refresh');
        }
    }

    //function for order
    public function index($data=null){
        $this->load->view('admin/header');
        if($this->session->userdata('user_roll')==5)
        {
            $data['customerList']=$this->customer_model->getCustomer();
            $this->load->view('superadmin/left_menu');
        }else{
            $data['customerList']=$this->customer_model->getCustomerDetail();
            $this->load->view('admin/left_menu');
        }
        //fetch all shops show 
        $data['shops']=$this->shop_model->getById();
        $this->load->view('customer/index',$data);
        $this->load->view('admin/footer');
    }
    //function for create new Customer
    public function newCustomer(){
       // echo '<pre>';print_r($this->input->POST());die("herer");
        if($this->input->POST()){
            $this->form_validation->set_rules('name', 'Name', 'required');
            $this->form_validation->set_rules('phone', 'Phone', 'required|regex_match[/^[0-9]+$/]');
            $this->form_validation->set_rules('shop[]','Shop','required');
            //$shop=$this->input->POST('shop');
            //$this->form_validation->set_rules('shop','School groups','required|callback_check_default');
            //$this->form_validation->set_message('check_default', 'You need to select something other than the default');
            
            //$this->form_validation->set_rules('email', 'Email', 'required');
            //$this->form_validation->set_rules('suburb', 'Suburb', 'required');
            //$this->form_validation->set_rules('state', 'State', 'required');
            //$this->form_validation->set_rules('wholesale', 'Wholesale', 'required');
            if($this->input->POST('wholesale')==1){
            $this->form_validation->set_rules('abn', 'abn', 'required');
            $this->form_validation->set_rules('company_name', 'Company Name', 'required');
            //$this->form_validation->set_rules('mobile', 'Mobile', 'required|regex_match[/^[0-9]+$/]');
            $this->form_validation->set_rules('contact', 'Contact', 'required');
            $this->form_validation->set_rules('address', 'Address', 'required');
            }
            //$this->form_validation->set_rules('delivery_info', 'Delivery Info', 'required');
            


            $this->form_validation->set_error_delimiters('<div class="error">', '</div>');
            if ($this->form_validation->run() == FALSE){
            
            }else{
                
                if($this->input->POST('wholesale') == 1)
                {
                    $wholesale=$this->input->POST('wholesale');
                }else{
                    $wholesale=0;
                }
            $shop=$this->input->POST('shop');
            $shop_id=implode(",",$shop);
            $data =array('shop_ids'=>$shop_id,'name' => $this->input->POST('name'),'phone' => $this->input->POST('phone'),'email' => $this->input->POST('email'),'suburb' => $this->input->POST('suburb'),'state' => $this->input->POST('state'),'wholesale' => $wholesale,'abn' => $this->input->POST('abn'),'address' => $this->input->POST('address'),'company_name' => $this->input->POST('company_name'),'mobile' => $this->input->POST('mobile'),'contact' => $this->input->POST('contact'),'delivery_info' => $this->input->POST('delivery_info'),'add_user_ids'=>$this->session->userdata('user_id'));
            $this->customer_model->newCustomer($data);
            redirect('customer/newCustomer');
            }
       }
       $data['postData']=$this->input->POST();
        return $this->index($data);
    }
    //delete customer 
    public function deleteCustomer()
    {
        $data=$this->Customer_model->deleteCustomer();
    }

    //function for edit customer
    public function editCustomer() {
//        $id=$this->input->GET('id'); 
         //$data['shops']=$this->shop_model->getById();
if(!$this->input->GET('id'))
{
    redirect('customer/');
}
        $data['customerData']=$this->Customer_model->getSingleCustomer($this->input->GET('id'));
        if($this->input->POST('submit')){
        $this->form_validation->set_rules('name', 'Name', 'required');
            $this->form_validation->set_rules('phone', 'Phone', 'required|regex_match[/^[0-9]+$/]');
            $this->form_validation->set_rules('shop[]','Shop','required');
            //$shop=$this->input->POST('shop');
            //$this->form_validation->set_rules('shop','School groups','required|callback_check_default');
            //$this->form_validation->set_message('check_default', 'You need to select something other than the default');
            
            //$this->form_validation->set_rules('email', 'Email', 'required');
            //$this->form_validation->set_rules('suburb', 'Suburb', 'required');
            //$this->form_validation->set_rules('state', 'State', 'required');
            //$this->form_validation->set_rules('wholesale', 'Wholesale', 'required');
            if($this->input->POST('wholesale')==1){
            $this->form_validation->set_rules('abn', 'abn', 'required');
            $this->form_validation->set_rules('company_name', 'Company Name', 'required');
            $this->form_validation->set_rules('contact', 'Contact', 'required');
            $this->form_validation->set_rules('address', 'Address', 'required');
            }
            $this->form_validation->set_error_delimiters('<div class="error">', '</div>');
            if ($this->form_validation->run() == FALSE){

            $data['postData']=$this->input->POST();
            }else{
                $shop=$this->input->POST('shop');
                $shop_id=implode(",",$shop);
                if($this->input->POST('wholesale') == 1)
                {
                    $wholesale=$this->input->POST('wholesale');
                    $data =array('shop_ids'=>$shop_id,'name' => $this->input->POST('name'),'phone' => $this->input->POST('phone'),'email' => $this->input->POST('email'),'suburb' => $this->input->POST('suburb'),'state' => $this->input->POST('state'),'wholesale' => $wholesale,'abn' => $this->input->POST('abn'),'address' => $this->input->POST('address'),'company_name' => $this->input->POST('company_name'),'mobile' => $this->input->POST('mobile'),'contact' => $this->input->POST('contact'),'delivery_info' => $this->input->POST('delivery_info'));
                }else{
                    $wholesale=0;
                    $data =array('shop_ids'=>$shop_id,'name' => $this->input->POST('name'),'phone' => $this->input->POST('phone'),'email' => $this->input->POST('email'),'suburb' => '','state' => '','wholesale' => $wholesale,'abn' => '','address' => '','company_name' => '','mobile' => $this->input->POST('mobile'),'contact' => '','delivery_info' => $this->input->POST('delivery_info'));
                    //$data =array('shop_ids'=>$shop_id,'name' => $this->input->POST('name'),'phone' => $this->input->POST('phone'),'email' => $this->input->POST('email'),'wholesale' => $wholesale,'mobile' => $this->input->POST('mobile'),'delivery_info' => $this->input->POST('delivery_info'),'add_user_ids'=>$this->session->userdata('user_id'));
                }

            $this->Customer_model->updateCustomer($data,$this->input->GET('id'));
            redirect('customer/');
            }
        }
        
        //fetch all shops show 
        $data['shops']=$this->shop_model->getById();

        $this->load->view('admin/header');
        if($this->session->userdata('user_roll')==5)
        {
            $this->load->view('superadmin/left_menu');
        }else{
            $this->load->view('admin/left_menu');   
        }
        $this->load->view('customer/editCustomer',$data);
        $this->load->view('admin/footer');

    }


  
}