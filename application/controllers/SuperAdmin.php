<?php 
class SuperAdmin extends CI_Controller{
	public function __construct(){
        parent::__construct();
        $this->load->library('form_validation');
        $this->load->model('role_model');  
        $this->load->model('user_role_model');
        $this->load->model('super_admin_model');
        $this->load->model('shop_model');
        $this->load->helper(array('form', 'url'));
        $this->load->model('User_model');
        $this->load->model('customer_model'); 
        //echo '<pre>';print_r($this->session->userdata());die;
        //echo $this->session->userdata('user_roll');
        if(!$this->session->userdata('logged_in'))
        {
            redirect('', 'refresh');
        }
        if($this->session->userdata('user_roll')!=5)
        {
            redirect('', 'refresh');
        }

    }

    public function index(){
        $this->load->view('superadmin/header');
        $this->load->view('superadmin/left_menu');
        $this->load->view('superadmin/dashboard');
        $this->load->view('superadmin/footer');    
    }
    //update Employee information
    public function editEmployee(){ 

    $this->load->model('User_model');
    $data =array('shop'=>$this->input->POST('shop'),'users_id' => $this->input->POST('id'),'email' => $this->input->POST('email'),'fname' => $this->input->POST('fname'),'lname' => $this->input->POST('lname'),'mobile' => $this->input->POST('mobile'),'address' => $this->input->POST('address'),'termination' => $this->input->POST('termination'),'employement' => $this->input->POST('employement'),'position' => $this->input->POST('position'),'suspend'=>1);
   
    $data['value']=$this->User_model->editUser();
    $data['roll']=$this->role_model->get_roll();
    $data['shops']=$this->shop_model->getById();
    $this->load->view('superadmin/header');
    $this->load->view('superadmin/left_menu');
    $this->load->view('superadmin/editUser',$data);
    $this->load->view('superadmin/footer');
    
    }
    // show all shop owner list
    public function owner($data=NULL) {
        if($this->input->post('empShopPagePost'))
        {
        $this->form_validation->set_rules('shop', 'Shop name', 'required'); 
        $this->form_validation->set_rules('employee', 'Employee Name', 'required'); 
        $this->form_validation->set_rules('pages', 'pageId', 'required');
        if ($this->form_validation->run() == FALSE){
        }else{
        $pageid=implode(',',$this->input->post('pageId'));
        $this->transaction_model->shopEmpPageInsert($pageid);
        }
 
        $data['rolesPostData']=$this->input->POST();
        }

        if($this->input->POST('creeatrole')){

                    $this->form_validation->set_rules('name', 'Role', 'required|is_unique[roles.name]');
                    $this->form_validation->set_error_delimiters('<div class="error">', '</div>');
                    
                    if ($this->form_validation->run() == FALSE){
                 
                    }else{
                            $data =array('name' => $this->input->POST('name') );
                            $success=$this->role_model->insertRole($data);
                            if($success)
                                    echo "<script>alert('Role inserted successfully');</script>";
                             else
                                    echo "<script>alert('There is some problem in inserting Role');</script>";    
                    }
                    $data['postRoll']=$this->input->POST();
        }
        
        if($this->input->POST('submit')){
            $this->form_validation->set_rules('email', 'Email', 'trim|required|valid_email');
            $this->form_validation->set_rules('password', 'Password', 'required');
            $this->form_validation->set_rules('fname', 'First Name', 'required');
            $this->form_validation->set_rules('mobile', 'Phone', 'required|regex_match[/^[0-9]+$/]');
            $this->form_validation->set_rules('address', 'Address', 'required');
            //$this->form_validation->set_rules('termination', 'Termination Date', 'required');

            $this->form_validation->set_rules('employement', 'Employement Date', 'required');
            $this->form_validation->set_rules('position', 'Position', 'required');
            $roll = $this->input->post('roll');
           
            if($roll=='none')
            {    
            $this->form_validation->set_rules('roll', 'roll', 'required|callback_select_validate'); // Validating select option field.
            }
           
            $this->form_validation->set_error_delimiters('<div class="error">', '</div>');
            
            if ($this->form_validation->run() == FALSE){
            }else{
	                if($this->input->POST('shop') && ($roll==1 || $roll==2 || $roll==6))
	                 {    
                        $shop=$this->input->POST('shop');
                        $shop_id=implode(",",$shop);
                        $data =array('add_user_ids'=>$this->session->userdata('user_id'),'shop_ids'=>$shop_id,'email' => $this->input->POST('email'),'password' => $this->input->POST('password'),'fname' => $this->input->POST('fname'),'lname' => $this->input->POST('lname'),'mobile' => $this->input->POST('mobile'),'address' => $this->input->POST('address'),'termination' => $this->input->POST('termination'),'employement' => $this->input->POST('employement'),'position' => $this->input->POST('position'),'suspend'=>1);
	                 }else{
	                 $data =array('add_user_ids'=>$this->session->userdata('user_id'),'email' => $this->input->POST('email'),'password' => $this->input->POST('password'),'fname' => $this->input->POST('fname'),'lname' => $this->input->POST('lname'),'mobile' => $this->input->POST('mobile'),'address' => $this->input->POST('address'),'termination' => $this->input->POST('termination'),'employement' => $this->input->POST('employement'),'position' => $this->input->POST('position'),'suspend'=>1);
                 }
            
            $last_insert_id = $this->acl_auth->register($data);
            $roll=array('roles_id'=>$this->input->POST('roll'),'users_id'=>$last_insert_id);
            $success=$this->user_role_model->registerRole($roll);
            
            if($last_insert_id)
                echo "<script>alert('Registration done successfully');</script>";
                else
                echo "<script>alert('There is some problem in Registration');</script>";    
            return $this->employee();
           
            }
             $data['postData']=$this->input->POST();
       }
       
         $this->load->model('transaction_model');
        //show all owner 
         $data['pages']=$this->transaction_model->getPages();
        $this->load->model('User_model');
        $this->load->model('page_model');
        //select all employee of selected shop in dropdown
         $data['allCustomer']=$this->transaction_model->getSalesPersonNameByshopId();
        //show add  user 
        $data['users']=$this->User_model->getListById();
        //fetch all roll
        $data['roll']=$this->role_model->get_roll();
         //fetch all show 
        $data['shops']=$this->shop_model->getById();
        //fetch all pages
        $data['value2']=$this->page_model->fetch_pages();
        $data['value3']=$this->page_model->show_roles2();

        $this->load->view('superadmin/header');
        $this->load->view('superadmin/left_menu');
        $this->load->view('superadmin/tabs',$data);
        $this->load->view('superadmin/footer');
    }

    //function for create new owner
    public function register(){
        if($this->input->POST()){
            //echo '<pre>';print_r($this->input->POST());die("dd");

            $this->form_validation->set_rules('email', 'Email', 'trim|required|valid_email');
            $this->form_validation->set_rules('password', 'Password', 'required');
            $this->form_validation->set_rules('fname', 'First Name', 'required');
            $this->form_validation->set_rules('mobile', 'Phone', 'required|regex_match[/^[0-9]+$/]');
            $this->form_validation->set_rules('address', 'Address', 'required');
            //$this->form_validation->set_rules('termination', 'Termination Date', 'required');
            $this->form_validation->set_rules('employement', 'Employement Date', 'required');
            $this->form_validation->set_rules('position', 'Position', 'required');
            
            $roll = $this->input->post('roll');
            if($roll=='none')
            {    
            $this->form_validation->set_rules('roll', 'roll', 'required|callback_select_validate'); // Validating select option field.
            
            }
            $this->form_validation->set_error_delimiters('<div class="error">', '</div>');
            if ($this->form_validation->run() == FALSE){
            }else{

                if($this->input->POST('shop') && ($roll==1 || $roll==2 || $roll==6))
                 {  
                    $shop=$this->input->POST('shop');
                    $shop_id=implode(",",$shop);
                    $data =array('add_user_ids'=>$this->session->userdata('user_id'),'shop_ids'=>$shop_id,'email' => $this->input->POST('email'),'password' => $this->input->POST('password'),'fname' => $this->input->POST('fname'),'lname' => $this->input->POST('lname'),'mobile' => $this->input->POST('mobile'),'address' => $this->input->POST('address'),'termination' => $this->input->POST('termination'),'employement' => $this->input->POST('employement'),'position' => $this->input->POST('position'),'suspend'=>1);
                }else{
                    $data =array('add_user_ids'=>$this->session->userdata('user_id'),'email' => $this->input->POST('email'),'password' => $this->input->POST('password'),'fname' => $this->input->POST('fname'),'lname' => $this->input->POST('lname'),'mobile' => $this->input->POST('mobile'),'address' => $this->input->POST('address'),'termination' => $this->input->POST('termination'),'employement' => $this->input->POST('employement'),'position' => $this->input->POST('position'),'suspend'=>1);
                }
            $last_insert_id = $this->acl_auth->register($data);
            $roll=array('roles_id'=>$this->input->POST('roll'),'users_id'=>$last_insert_id);
            $success=$this->user_role_model->registerRole($roll);
            if($last_insert_id)
                echo "<script>alert('Registration done successfully');</script>";
                else
                echo "<script>alert('There is some problem in Registration');</script>";    
            
            return $this->owner();
            }
       }
        $data['postData']=$this->input->POST();
        return $this->owner($data);
    }
    // Below function is called for validating select option field.
    function select_validate($roll)
    {
    // 'none' is the first option that is default "-------Choose Roll-------"
        if($roll=="none"){
        $this->form_validation->set_message('select_validate', 'Please Select Roll.');
        return false;
        } else{
        // User picked something.
        return true;
        }
    }
   
        //show specific edit user
    public function editUser($data=NULL)
    {
    $data['value']=$this->user_model->editUser();
    $data['roll']=$this->role_model->get_roll();
    //fetch all show 
    $data['shops']=$this->shop_model->get_all();
    //echo '<pre>';print_r($data);die;
    $this->load->view('superadmin/header');
    $this->load->view('superadmin/left_menu');
    $this->load->view('superadmin/editUser',$data);
    $this->load->view('superadmin/footer');
}    
    //update user by super admin 
   public function userUpdate(){
    $data =array('shop'=>$this->input->POST('shop'),'users_id' => $this->input->POST('id'),'email' => $this->input->POST('email'),'fname' => $this->input->POST('fname'),'lname' => $this->input->POST('lname'),'mobile' => $this->input->POST('mobile'),'address' => $this->input->POST('address'),'termination' => $this->input->POST('termination'),'employement' => $this->input->POST('employement'),'position' => $this->input->POST('position'),'suspend'=>1);
            
    if($this->input->POST('shop')){
            $this->form_validation->set_rules('email', 'Email', 'trim|required|valid_email');
            $this->form_validation->set_rules('fname', 'First Name', 'required');
            $this->form_validation->set_rules('mobile', 'Phone', 'required|regex_match[/^[0-9]+$/]');
            $this->form_validation->set_rules('address', 'Address', 'required');
            $this->form_validation->set_rules('termination', 'Termination Date', 'required');
            $this->form_validation->set_rules('employement', 'Employement Date', 'required');
            $this->form_validation->set_rules('position', 'Position', 'required');
            $this->form_validation->set_rules('shop[]', 'Shop', 'required');
            $this->form_validation->set_error_delimiters('<div class="error">', '</div>');
            if ($this->form_validation->run() == FALSE){
                $data['postUserData']=$this->input->POST();
            //return $this->editUser($data);
            }else{
                 $this->load->model('User_model');
                 $result=$this->super_admin_model->update_user();
                //return $this->owner();
                //$result=$this->super_admin_model->update_user();
                 redirect('SuperAdmin/owner');
            }
        }else{

            $this->form_validation->set_rules('email', 'Email', 'trim|required|valid_email');
            $this->form_validation->set_rules('fname', 'First Name', 'required');
            $this->form_validation->set_rules('mobile', 'Phone', 'required|regex_match[/^[0-9]+$/]');
            $this->form_validation->set_rules('address', 'Address', 'required');
            $this->form_validation->set_rules('termination', 'Termination Date', 'required');
            $this->form_validation->set_rules('employement', 'Employement Date', 'required');
            $this->form_validation->set_rules('position', 'Position', 'required');
            $this->form_validation->set_error_delimiters('<div class="error">', '</div>');
            if ($this->form_validation->run() == FALSE){
            $data['postUserData']=$this->input->POST();
            //return $this->editUser($data);
            }else{
                 $this->load->model('User_model');
                 $result=$this->super_admin_model->update_user();
                //return $this->owner();
                //$result=$this->super_admin_model->update_user();
                 redirect('SuperAdmin/owner');
            }



        }

        
    }
    //suspend shop  by super admin
    public function suspendShop()
    { 

        $this->super_admin_model->suspend($this->input->post('suspendShop'));
        //fetch all show 
        $data['shops']=$this->shop_model->get_all();
        $this->load->view('superadmin/suspendAjaxResponce',$data);

    }
     //suspend shop by Super admin
    public function rejoinShop()
    {
        
        $this->super_admin_model->rejoin($this->input->post('rejoin'));
        //show all user 
        $data['shops']=$this->shop_model->get_all();
        
        $this->load->view("superadmin/rejoinAjaxResponce",$data);

    }
    // show all shop list
    public function listShop(){
        if($this->input->POST()){
            $this->form_validation->set_rules('name', 'Name', 'required');
            $this->form_validation->set_rules('address', 'Address', 'required');
            $this->form_validation->set_rules('phone', 'Contact Details', 'required|regex_match[/^[0-9]+$/]');
            
            if (empty($_FILES['userfile']['name'])){
                $this->form_validation->set_rules('userfile', 'Document', 'required');
            }

            $this->form_validation->set_rules('overhead', 'Default Product Overhead cost', 'required');
            $this->form_validation->set_rules('suburb', 'Suburb', 'required');
            $this->form_validation->set_rules('state', 'State', 'required');
            $this->form_validation->set_rules('postcode', 'Postcode', 'required');
            $this->form_validation->set_rules('country', 'Country', 'required');
            $this->form_validation->set_rules('longitude', 'Longitude', 'required');
            $this->form_validation->set_rules('latitude', 'Latitude', 'required');
            $this->form_validation->set_error_delimiters('<div class="error">', '</div>');
            if ($this->form_validation->run() == FALSE){
                 
            }else{
                $this->load->library('upload');
               if (!empty($_FILES['userfile']['name'])) {

                     $config['upload_path'] = './uploads/';
                    echo $config['upload_path'];
                    $config['allowed_types'] = 'gif|jpg|png|jpeg';     
                    $this->load->library('upload', $config);
                    $this->upload->initialize($config);

          
                    if ($this->upload->do_upload('userfile'))
                    {
                        chmod($config['upload_path'], 777);
                        $img = $this->upload->data();
                        $file_name = $img['file_name'];
                
                    }
                    else
                    {
                        echo $this->upload->display_errors();
                         die();

                    }
                $data =array('name' => $this->input->POST('name'),'address' => $this->input->POST('address'),'phone'=> $this->input->POST('phone'),'logo'=> $file_name,'overhead'=> $this->input->POST('overhead'),'state' => $this->input->POST('state'),'suburb' => $this->input->POST('suburb'),'postcode' => $this->input->POST('postcode'),'country' => $this->input->POST('country'),'longitude' => $this->input->POST('longitude'),'latitude' => $this->input->POST('latitude'));
                $success= $this->shop_model->insertShop($data);
                
                if($success){
                        //echo "<script>alert('Shop inserted successfully');</script>";
                        redirect('SuperAdmin/listShop','refresh');
                    }
                else
                  echo "<script>alert('There is some problem in inserting Shop');</script>";    
                 }
             }
        }

    $data['postData']=$this->input->POST();
    $data['value']=$this->shop_model->get_all();
    $this->load->view('superadmin/header');
    $this->load->view('superadmin/left_menu');
    $this->load->view('superadmin/showShops',$data);
    $this->load->view('superadmin/footer'); 

    }
    //show specific edit Shop
    public function editShop()
    {
        $this->load->model('till_model');
         $id=$this->input->GET('id');
         if($this->input->POST('submit')){
            $this->form_validation->set_rules('name', 'Name', 'required');
            $this->form_validation->set_rules('address', 'Address', 'required');
            $this->form_validation->set_rules('phone', 'Contact Details', 'required|regex_match[/^[0-9]+$/]');
          
            $this->form_validation->set_rules('overhead', 'Default Product Overhead cost', 'required');
            $this->form_validation->set_rules('suburb', 'Suburb', 'required');
            $this->form_validation->set_rules('state', 'State', 'required');
            $this->form_validation->set_rules('postcode', 'Postcode', 'required');
            $this->form_validation->set_rules('country', 'Country', 'required');
            $this->form_validation->set_rules('longitude', 'Longitude', 'required');
            $this->form_validation->set_rules('latitude', 'Latitude', 'required');
            $this->form_validation->set_error_delimiters('<div class="error">', '</div>');
            if ($this->form_validation->run() == FALSE){
                 
            }else{
                     if (!empty($_FILES['userfile']['name'])) {
             
             $config['upload_path'] = './uploads/';
            //echo $config['upload_path'];
            $config['allowed_types'] = 'gif|jpg|png|jpeg';     
            $this->load->library('upload', $config);
            $this->upload->initialize($config);

  
            if ($this->upload->do_upload('userfile'))
            {   
                $img = $this->upload->data();
                $file_name = $img['file_name'];
        
            }
            else
            {
                echo $this->upload->display_errors();
                 die();

            }
                }
               

                if (!empty($_FILES['userfile']['name'])){
                            $data =array('name' => $this->input->POST('name'),'address' => $this->input->POST('address'),'phone'=> $this->input->POST('phone'),'logo'=> $file_name,'overhead'=> $this->input->POST('overhead'),'state' => $this->input->POST('state'),'suburb' => $this->input->POST('suburb'),'postcode' => $this->input->POST('postcode'),'country' => $this->input->POST('country'),'longitude' => $this->input->POST('longitude'),'latitude' => $this->input->POST('latitude'));

                }
                else{
                            $data =array('name' => $this->input->POST('name'),'address' => $this->input->POST('address'),'phone'=> $this->input->POST('phone'),'deposite'=> $this->input->POST('deposite'),'overhead'=> $this->input->POST('overhead'),'state' => $this->input->POST('state'),'suburb' => $this->input->POST('suburb'),'postcode' => $this->input->POST('postcode'),'country' => $this->input->POST('country'),'longitude' => $this->input->POST('longitude'),'latitude' => $this->input->POST('latitude'));
                }

                $success= $this->shop_model->updateShop($data,$id);
                if($success){
                       // echo "<script>alert('Shop Update successfully');</script>";
                            redirect("SuperAdmin/editShop?id=$id",'refresh');
                    }
                 else
                echo "<script>alert('There is some problem in inserting Shop');</script>";  
                    } 
            }
            if($this->input->POST('update')){
                    $this->form_validation->set_rules('tillid', 'Till ID', 'required');
                    $this->form_validation->set_rules('tillUser', 'Till User Name', 'required');
                    $this->form_validation->set_rules('password', 'Till Password', 'required');
                    $this->form_validation->set_rules('port', 'Port Number', 'required');
                    $this->form_validation->set_rules('ipadd', 'IP Address', 'required');
                    if ($this->form_validation->run() == FALSE){

                    }else{

                        $data =array('tillid' => $this->input->POST('tillid'),'tillUsername' => $this->input->POST('tillUser'),'tillPassword'=> $this->input->POST('password'),'portNumber'=> $this->input->POST('port'),'ipAddress'=> $this->input->POST('ipadd'),'shopid'=>$id);
                   
                    $success= $this->till_model->addTill($data,$id);

                                if($success){
                                       // echo "<script>alert('Shop Update successfully');</script>";
                                        redirect("SuperAdmin/editShop?id=$id",'refresh');

                                    }
                                 else
                                echo "<script>alert('There is some problem in inserting Shop');</script>"; 
                    }
                }
                if($this->input->POST('editTill')){
                    $this->form_validation->set_rules('edit_tillid', 'Till ID', 'required');
                    $this->form_validation->set_rules('edit_tillUser', 'Till User Name', 'required');
                    $this->form_validation->set_rules('edit_password', 'Till Password', 'required');
                    $this->form_validation->set_rules('edit_port', 'Port Number', 'required');
                    $this->form_validation->set_rules('edit_ipadd', 'IP Address', 'required');
                    $this->form_validation->set_error_delimiters('<div class="error">', '</div>');
                    if ($this->form_validation->run() == FALSE){
                         
                    }else{
                        $id2=$this->input->POST('id');
                        $data =array('tillid' => $this->input->POST('edit_tillid'),'tillUsername' => $this->input->POST('edit_tillUser'),'tillPassword'=> $this->input->POST('edit_password'),'portNumber'=> $this->input->POST('edit_port'),'ipAddress'=> $this->input->POST('edit_ipadd'));
                    $success= $this->till_model->editTill($id2,$data);
                                if($success){
                                        echo "<script>alert('Till Update successfully');</script>";
                                        //redirect("admin/editShop?id=$id",'refresh');
                                    }
                                 else
                                echo "<script>alert('There is some problem in inserting Shop');</script>"; 
                    }
                }
                 if($this->input->POST('updateAcc')){
                    $this->form_validation->set_rules('bankName', 'BanK Name', 'required');
                    $this->form_validation->set_rules('bankAccountName', 'Bank Account Name', 'required');
                    $this->form_validation->set_rules('bsbNumber', 'BSB Number', 'required');
                    $this->form_validation->set_rules('bankAccountNumber', 'Bank Account Number', 'required');
                    $this->form_validation->set_rules('accountCodes', 'Account Codes', 'required');
                    $this->form_validation->set_rules('gstFreeSales', 'Gst Free Sales', 'required');
                    $this->form_validation->set_rules('gstIncludedSales', 'GST Free Sales', 'required');
                    if ($this->form_validation->run() == FALSE){
                         
                    }else{
                        $data =array('bankName' => $this->input->POST('bankName'),'bankAccountName' => $this->input->POST('bankAccountName'),'bsbNumber'=> $this->input->POST('bsbNumber'),'accountNumber'=> $this->input->POST('bankAccountNumber'),'accountCodes'=> $this->input->POST('accountCodes'),'gstFreeSales'=> $this->input->POST('gstFreeSales'),'gstIncludedSales'=> $this->input->POST('gstIncludedSales'));
                        $success= $this->shop_model->updateShop($data,$id);
                                if($success){
                                        //echo "<script>alert('Shop Update successfully');</script>";
                                        redirect("SuperAdmin/editShop?id=$id",'refresh');

                                    }
                                 else
                                echo "<script>alert('There is some problem in inserting Shop');</script>"; 
                    }
                }

    $data['till']=$this->input->POST();
    $data['value']=$this->shop_model->editshop();
    $data['editTill']=$this->till_model->getTillWhere(array('shopid' => $id));

    if($this->input->POST('editTillId')){
        $pid=$this->input->POST('editTillId');
        $data['editTillInfo']=$this->till_model->getTill($pid);
    }
    else if($this->input->POST('id') ){
        $data['editTillInfo']=$this->till_model->getTill($this->input->POST('id'));
        $data['editTillInfo']=$this->till_model->getTill($this->input->POST('id'));
    }
    //echo '<pre>'; print_r($data);die;
    $this->load->view('superadmin/header');
    $this->load->view('superadmin/left_menu');
    $this->load->view('superadmin/editShop',$data);
    $this->load->view('superadmin/footer');
    } 
    //delete shop by SuperAdmin
    public function deleteShop()
    {
        $data=$this->shop_model->delete($this->input->post('remove'));
    }
    //suspend user by admin
    public function suspendUser()
    { 
    $this->User_model->suspend($this->input->post('suspendUser'));
    //show all user 
    $data['value']=$this->User_model->get_all();
    $this->load->view('superadmin/suspendUserAjaxResponce',$data);
    }
    //delete user by Super admin
    public function removeUser()
    {
        $data=$this->User_model->delete_rol($this->input->post('remove'));
        $data=$this->User_model->delete($this->input->post('remove'));
    }
    //suspend user by admin
    public function rejoinUser()
    {

        $this->User_model->rejoin($this->input->post('rejoin'));
         //show all user 
        $data['value']=$this->User_model->get_all();
        $this->load->view("superadmin/rejoinUserAjaxResponce",$data);

    }
      //function for create customer
    public function customer($data=null){
        $this->load->view('superadmin/header');
        $this->load->view('superadmin/left_menu');
        //fetch all shops show 
        $data['shops']=$this->shop_model->get_all();

        $this->load->view('superadmin/customer',$data);
        $this->load->view('superadmin/footer');
    }
    //function for create new Customer
    public function newCustomer(){

       // echo '<pre>';print_r($this->input->POST());die("herer");
        if($this->input->POST()){
            $this->form_validation->set_rules('name', 'Name', 'required');
            $this->form_validation->set_rules('phone', 'Phone', 'required|regex_match[/^[0-9]+$/]');
            //$array=$this->input->POST('shop');

            //$this->form_validation->set_rules('shop[]','shop','required');
            $this->form_validation->set_rules('shop[]','Shop','required');
            
            //$this->form_validation->set_message('check_default', 'You need to select something other than the default');
            
            //$this->form_validation->set_rules('email', 'Email', 'required');
            //$this->form_validation->set_rules('suburb', 'Suburb', 'required');
            //$this->form_validation->set_rules('state', 'State', 'required');
            //$this->form_validation->set_rules('wholesale', 'Wholesale', 'required');
            if($this->input->POST('wholesale')==1){
            $this->form_validation->set_rules('abn', 'abn', 'required');
            $this->form_validation->set_rules('company_name', 'Company Name', 'required');
            //$this->form_validation->set_rules('mobile', 'Mobile', 'required|regex_match[/^[0-9]+$/]');
            $this->form_validation->set_rules('contact', 'Contact', 'required');
            $this->form_validation->set_rules('address', 'Address', 'required');
            }
            //$this->form_validation->set_rules('delivery_info', 'Delivery Info', 'required');
            


            $this->form_validation->set_error_delimiters('<div class="error">', '</div>');
            if ($this->form_validation->run() == FALSE){
            
            }else{
                
                if($this->input->POST('wholesale') == 1)
                {
                    $wholesale=$this->input->POST('wholesale');
                }else{
                    $wholesale=0;
                }
            $shop=$this->input->POST('shop');
            $shop_id=implode(",",$shop);

            $data =array('shop_ids'=>$shop_id,'name' => $this->input->POST('name'),'phone' => $this->input->POST('phone'),'email' => $this->input->POST('email'),'suburb' => $this->input->POST('suburb'),'state' => $this->input->POST('state'),'wholesale' => $wholesale,'abn' => $this->input->POST('abn'),'address' => $this->input->POST('address'),'company_name' => $this->input->POST('company_name'),'mobile' => $this->input->POST('mobile'),'contact' => $this->input->POST('contact'),'delivery_info' => $this->input->POST('delivery_info'));
            $this->customer_model->newCustomer($data);
            redirect('SuperAdmin/newCustomer');
            }
       }
       $data['postData']=$this->input->POST();
        return $this->customer($data);
    }
    
}
