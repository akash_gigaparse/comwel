<?php 
class Admin extends CI_Controller{
	public function __construct(){
        parent::__construct();
        //$this->load->library('session');
        $this->load->library('form_validation');
        $this->load->model('shop_model');  
        $this->load->model('role_model');  
        $this->load->model('user_role_model');
        //$this->acl_auth->restrict_access('admin');
        $this->load->model('super_admin_model');
        $this->load->model('transaction_model');
        $this->load->helper('date');
        $this->load->helper('url');
        if(!$this->session->userdata('logged_in'))
        {
            redirect('', 'refresh');
        }
        if($this->session->userdata('user_roll')!=1)
        {
            redirect('', 'refresh');
        }
    }

    public function index(){
        $this->acl_auth->restrict_access('admin');
        $this->load->view('admin/header');
        $this->load->view('admin/left_menu');
        $this->load->view('admin/dashboard');
        $this->load->view('admin/footer'); 
         //$this->load->view('admin/PaymentReceivedPage');    
    }
    // show all user list
    public function showList(){
        $this->load->model('User_model');
        $data['value']=$this->User_model->get_all();
        $this->load->view('admin/header');
        $this->load->view('admin/left_menu');
        $this->load->view('admin/show_Employe',$data);
        $this->load->view('admin/footer'); 

    }
    //update order table where paided =2
    public function orderUpdatePaided(){
$orderid=$this->input->post('orderid');
        $result=$this->transaction_model->updatePaid($orderid);


$grandtotal=0;
$id=0;

           //echo $this->input->post('custId');die('hre');
            $data['order']=$this->transaction_model->orderPaidedSelect();

            //echo '<td>'.foreach(json_decode($row['item']) as $item){ print_r($item); }.'</td>';
                    echo '<tr>';
                    echo '<td>'.'Date Ordered'.'</td>';
                    echo '<td>'.'total'.'</td>';
                    echo '<td>'.'Paided'.'</td>';
                    echo'</tr>';

            foreach ($data as $result) {
                foreach($result as $row){
                    
                    if($row['item']){

                        $item=json_decode(($row['item']));
                            foreach ($item as $value) {
                                
                                $grandtotal+=$value->total;
//admin/PaymentReceivedPage/
                            }
                            echo $id=$row['id'];
                    echo '<tr>';
                                  echo '<td>'.$row['date_ordered'].'</td>';
                                  echo '<td>'.$grandtotal.'</td>';
                                  /*$linkAttr = array('title'=>'update Paided','class'=>'update');
                                echo anchor('admin/PaymentReceivedPage/'.$row['id'], 'Paid','Paid');
                                */  echo "<td><a onclick='updateOrdersPaid($id)' class='customdelete btn btn-success' title='Paying' href='#'>Paying</a></td>";
                    echo'</tr>';
                       
                }
          }  }
    }
    //select order where paided =2 
 /*   public function orderPaided(){
$grandtotal=0;
$id=0;

           //echo $this->input->post('custId');die('hre');
            $data['order']=$this->transaction_model->orderPaidedSelect();

            //echo '<td>'.foreach(json_decode($row['item']) as $item){ print_r($item); }.'</td>';
                    echo '<tr>';
                    echo '<td>'.'Date Ordered'.'</td>';
                    echo '<td>'.'total'.'</td>';
                    echo '<td>'.'Paided'.'</td>';
                    echo'</tr>';

            foreach ($data as $result)
            {
                        foreach($result as $row)
                        {
                    
                            if($row['item'])
                            {

                                     $item=json_decode(($row['item']));
                                    foreach ($item as $value) 
                                    {
                                

                                $grandtotal+=$value->total;
//admin/PaymentReceivedPage/
                            }
                            echo $id=$row['id'];
                    echo '<tr>';
                                  echo '<td>'.$row['date_ordered'].'</td>';
                                  echo '<td>'.$grandtotal.'</td>';
                                  /*$linkAttr = array('title'=>'update Paided','class'=>'update');
                                echo anchor('admin/PaymentReceivedPage/'.$row['id'], 'Paid','Paid');
                                */  /*echo "<td><a onclick='updateOrdersPaid($id)' class='customdelete btn btn-success' title='Paying' href='#'>Paying</a></td>";
                    echo'</tr>';

                            }
                        }            
           }
    }
*/
    //selectShopIdByUserId
    public function selectShopIdByUserId(){
        //echo '<pre>';print_r($this->session->userdata());die;

        //echo $this->session->userdata('users_id');
        //echo"<pre>";print_r($this->input->post());die("hello");


    }
   
    //payment recieved page      
    public function PaymentReceivedPage(){
        //code for select user shop where user_id in users table
        $user_id=$this->session->userdata('user_id');
        $data['selectShop']=$this->transaction_model->selectUserShop($user_id);
            
       if($this->input->post())
        {
                 
        $this->transaction_model->orderPaidUpdate();

        }
        $this->transaction_model->updatePaid();
        $data['shops']=$this->transaction_model->getAllShop();
        //echo '<pre>';print_r($data);die("shops data");
        $this->load->view('admin/header');
        $this->load->view('admin/left_menu');
        $this->load->view('admin/PaymentReceivedPage',$data);
        $this->load->view('admin/footer'); 

    }
    //delete user by admin
    public function deleteUser()
    {
        $this->load->model('User_model');
        $data=$this->User_model->delete_rol($this->input->post('remove'));
        $data=$this->User_model->delete($this->input->post('remove'));
    }
    //Function to create Shop
    public function createShop(){
        if($this->input->POST()){
        $this->form_validation->set_rules('name', 'Name', 'required');
        $this->form_validation->set_rules('address', 'Address', 'required');
        $this->form_validation->set_rules('phone', 'Contact Details', 'required|regex_match[/^[0-9]+$/]');
        
        if (empty($_FILES['userfile']['name'])){
            $this->form_validation->set_rules('userfile', 'Document', 'required');
        }

       
        $this->form_validation->set_rules('overhead', 'Default Product Overhead cost', 'required');
        $this->form_validation->set_rules('suburb', 'Suburb', 'required');
        $this->form_validation->set_rules('state', 'State', 'required');
        $this->form_validation->set_rules('postcode', 'Postcode', 'required');
        $this->form_validation->set_rules('country', 'Country', 'required');
        $this->form_validation->set_rules('longitude', 'Longitude', 'required');
        $this->form_validation->set_rules('latitude', 'Latitude', 'required');
        $this->form_validation->set_error_delimiters('<div class="error">', '</div>');
        if ($this->form_validation->run() == FALSE){
             
        }else{

        	       $this->load->library('upload');

                   if (!empty($_FILES['userfile']['name'])) {
    		//die('kk');
                                    $config['upload_path'] = './uploads/';
                                    echo $config['upload_path'];

                                    $config['allowed_types'] = 'gif|jpg|png|jpeg';     
                          
                                   $this->upload->initialize($config);
                          
                                    if ($this->upload->do_upload('userfile'))
                                    {
                                        $img = $this->upload->data();
                                        $file_name = $img['file_name'];
                                
                                    }
                                    else
                                    {
                                        echo $this->upload->display_errors();
                                         die();

                                    }
                                $data =array('name' => $this->input->POST('name'),'address' => $this->input->POST('address'),'phone'=> $this->input->POST('phone'),'logo'=> $file_name,'overhead'=> $this->input->POST('overhead'),'state' => $this->input->POST('state'),'suburb' => $this->input->POST('suburb'),'postcode' => $this->input->POST('postcode'),'country' => $this->input->POST('country'),'longitude' => $this->input->POST('longitude'),'latitude' => $this->input->POST('latitude'));
                                $success= $this->shop_model->insertShop($data);
                                
                                if($success){
                                       // echo "<script>alert('Shop inserted successfully');</script>";
                                        redirect('admin/ListShop','refresh');
                                    }
                                else
                                  echo "<script>alert('There is some problem in inserting Shop');</script>";    
                     }
                 }
            }

            $data['value']=$this->input->POST();
            $this->load->view('admin/header');

            $this->load->view('admin/left_menu',$data);
            $this->load->view('admin/createShop');
            $this->load->view('admin/footer');
    }
    // show all shop list
    public function listShop(){
        if($this->input->POST()){
        $this->form_validation->set_rules('name', 'Name', 'required');
        $this->form_validation->set_rules('address', 'Address', 'required');
        $this->form_validation->set_rules('phone', 'Contact Details', 'required|regex_match[/^[0-9]+$/]');
        if (empty($_FILES['userfile']['name'])){
            $this->form_validation->set_rules('userfile', 'Document', 'required');
        }
        $this->form_validation->set_rules('overhead', 'Default Product Overhead cost', 'required');
        $this->form_validation->set_rules('suburb', 'Suburb', 'required');
        $this->form_validation->set_rules('state', 'State', 'required');
        $this->form_validation->set_rules('postcode', 'Postcode', 'required');
        $this->form_validation->set_rules('country', 'Country', 'required');
        $this->form_validation->set_rules('longitude', 'Longitude', 'required');
        $this->form_validation->set_rules('latitude', 'Latitude', 'required');
        $this->form_validation->set_error_delimiters('<div class="error">', '</div>');
        if ($this->form_validation->run() == FALSE){
        }else{

               $this->load->library('upload');

               if (!empty($_FILES['userfile']['name'])) {
        //die('kk');
                                $config['upload_path'] = './uploads/';
                                echo $config['upload_path'];

                                $config['allowed_types'] = 'gif|jpg|png|jpeg';     
                      
                               $this->upload->initialize($config);
                      
                                if ($this->upload->do_upload('userfile'))
                                {
                                    $img = $this->upload->data();
                                    $file_name = $img['file_name'];
                            
                                }
                                else
                                {
                                    echo $this->upload->display_errors();
                                     die();

                                }
                            $data =array('name' => $this->input->POST('name'),'address' => $this->input->POST('address'),'phone'=> $this->input->POST('phone'),'logo'=> $file_name,'overhead'=> $this->input->POST('overhead'),'state' => $this->input->POST('state'),'suburb' => $this->input->POST('suburb'),'postcode' => $this->input->POST('postcode'),'country' => $this->input->POST('country'),'longitude' => $this->input->POST('longitude'),'latitude' => $this->input->POST('latitude'));
                            $success= $this->shop_model->insertShop($data);
                            
                            if($success){
                                    //echo "<script>alert('Shop inserted successfully');</script>";
                                    redirect('admin/ListShop','refresh');
                                }
                            else
                              echo "<script>alert('There is some problem in inserting Shop');</script>";    
                 }
             }
        }

            $data['postData']=$this->input->POST();
            
            //fetch user data and get shop id
            $data['value']=$this->shop_model->getById();
//reprint_r($this->session->userdata());die;
//echo '<pre>';print_r($data);die;
            $this->load->view('admin/header');
            $this->load->view('admin/left_menu');
            $this->load->view('admin/showShops',$data);
            $this->load->view('admin/footer'); 

    }
    //delete user by admin
    public function deleteShop()
    {
        $data=$this->shop_model->delete($this->input->post('remove'));
    }
    
    //show specific edit Shop
    public function editShop()
    {
        $this->load->model('till_model');
         $id=$this->input->GET('id');
         if($this->input->POST('submit')){
                    $this->form_validation->set_rules('name', 'Name', 'required');
                    $this->form_validation->set_rules('address', 'Address', 'required');
                    $this->form_validation->set_rules('phone', 'Contact Details', 'required|regex_match[/^[0-9]+$/]');
                  
                    $this->form_validation->set_rules('overhead', 'Default Product Overhead cost', 'required');
                    $this->form_validation->set_rules('suburb', 'Suburb', 'required');
                    $this->form_validation->set_rules('state', 'State', 'required');
                    $this->form_validation->set_rules('postcode', 'Postcode', 'required');
                    $this->form_validation->set_rules('country', 'Country', 'required');
                    $this->form_validation->set_rules('longitude', 'Longitude', 'required');
                    $this->form_validation->set_rules('latitude', 'Latitude', 'required');
                    $this->form_validation->set_error_delimiters('<div class="error">', '</div>');
                    if ($this->form_validation->run() == FALSE){
                         
                    }else{

                             $this->load->library('upload');

                             if (!empty($_FILES['userfile']['name'])) {
                //die('kk');
                                                $config['upload_path'] = './uploads/';
                                                echo $config['upload_path'];
                                                $config['allowed_types'] = 'gif|jpg|png|jpeg';     
                                                $this->upload->initialize($config);
  
                                                if ($this->upload->do_upload('userfile')){
                                                        $img = $this->upload->data();
                                                        $file_name = $img['file_name'];
                                            
                                                }
                                                else{
                                                    echo $this->upload->display_errors();
                                                     die();

                                                }
                                }
                               
  
                                if (!empty($_FILES['userfile']['name'])){
                                            $data =array('name' => $this->input->POST('name'),'address' => $this->input->POST('address'),'phone'=> $this->input->POST('phone'),'logo'=> $file_name,'overhead'=> $this->input->POST('overhead'),'state' => $this->input->POST('state'),'suburb' => $this->input->POST('suburb'),'postcode' => $this->input->POST('postcode'),'country' => $this->input->POST('country'),'longitude' => $this->input->POST('longitude'),'latitude' => $this->input->POST('latitude'));
          
                                }
                                else{
                                            $data =array('name' => $this->input->POST('name'),'address' => $this->input->POST('address'),'phone'=> $this->input->POST('phone'),'deposite'=> $this->input->POST('deposite'),'overhead'=> $this->input->POST('overhead'),'state' => $this->input->POST('state'),'suburb' => $this->input->POST('suburb'),'postcode' => $this->input->POST('postcode'),'country' => $this->input->POST('country'),'longitude' => $this->input->POST('longitude'),'latitude' => $this->input->POST('latitude'));
                                }

                                $success= $this->shop_model->updateShop($data,$id);
                                if($success){
                                       // echo "<script>alert('Shop Update successfully');</script>";
                                            redirect("admin/editShop?id=$id",'refresh');
                                    }
                                 else
                                echo "<script>alert('There is some problem in inserting Shop');</script>";  
                    } 
            }
            if($this->input->POST('update')){
                    $this->form_validation->set_rules('tillid', 'Till ID', 'required');
                    $this->form_validation->set_rules('tillUser', 'Till User Name', 'required');
                    $this->form_validation->set_rules('password', 'Till Password', 'required');
                    $this->form_validation->set_rules('port', 'Port Number', 'required');
                    $this->form_validation->set_rules('ipadd', 'IP Address', 'required');
                    if ($this->form_validation->run() == FALSE){
                         
                    }else{
                        $data =array('tillid' => $this->input->POST('tillid'),'tillUsername' => $this->input->POST('tillUser'),'tillPassword'=> $this->input->POST('password'),'portNumber'=> $this->input->POST('port'),'ipAddress'=> $this->input->POST('ipadd'),'shopid'=>$id);
                    $success= $this->till_model->addTill($data,$id);
                                if($success){
                                       // echo "<script>alert('Shop Update successfully');</script>";
                                        redirect("admin/editShop?id=$id",'refresh');

                                    }
                                 else
                                echo "<script>alert('There is some problem in inserting Shop');</script>"; 
                    }
                }
                if($this->input->POST('editTill')){
                    $this->form_validation->set_rules('edit_tillid', 'Till ID', 'required');
                    $this->form_validation->set_rules('edit_tillUser', 'Till User Name', 'required');
                    $this->form_validation->set_rules('edit_password', 'Till Password', 'required');
                    $this->form_validation->set_rules('edit_port', 'Port Number', 'required');
                    $this->form_validation->set_rules('edit_ipadd', 'IP Address', 'required');
                    $this->form_validation->set_error_delimiters('<div class="error">', '</div>');
                    if ($this->form_validation->run() == FALSE){
                         
                    }else{
                        $id2=$this->input->POST('id');
                        $data =array('tillid' => $this->input->POST('edit_tillid'),'tillUsername' => $this->input->POST('edit_tillUser'),'tillPassword'=> $this->input->POST('edit_password'),'portNumber'=> $this->input->POST('edit_port'),'ipAddress'=> $this->input->POST('edit_ipadd'));
                    $success= $this->till_model->editTill($id2,$data);
                                if($success){
                                        echo "<script>alert('Till Update successfully');</script>";
                                        //redirect("admin/editShop?id=$id",'refresh');
                                    }
                                 else
                                echo "<script>alert('There is some problem in inserting Shop');</script>"; 
                    }
                }
                 if($this->input->POST('updateAcc')){
                    $this->form_validation->set_rules('bankName', 'BanK Name', 'required');
                    $this->form_validation->set_rules('bankAccountName', 'Bank Account Name', 'required');
                    $this->form_validation->set_rules('bsbNumber', 'BSB Number', 'required');
                    $this->form_validation->set_rules('bankAccountNumber', 'Bank Account Number', 'required');
                    $this->form_validation->set_rules('accountCodes', 'Account Codes', 'required');
                    $this->form_validation->set_rules('gstFreeSales', 'Gst Free Sales', 'required');
                    $this->form_validation->set_rules('gstIncludedSales', 'GST Free Sales', 'required');
                    if ($this->form_validation->run() == FALSE){
                         
                    }else{
                        $data =array('bankName' => $this->input->POST('bankName'),'bankAccountName' => $this->input->POST('bankAccountName'),'bsbNumber'=> $this->input->POST('bsbNumber'),'accountNumber'=> $this->input->POST('bankAccountNumber'),'accountCodes'=> $this->input->POST('accountCodes'),'gstFreeSales'=> $this->input->POST('gstFreeSales'),'gstIncludedSales'=> $this->input->POST('gstIncludedSales'));
                        $success= $this->shop_model->updateShop($data,$id);
                                if($success){
                                        //echo "<script>alert('Shop Update successfully');</script>";
                                        redirect("admin/editShop?id=$id",'refresh');

                                    }
                                 else
                                echo "<script>alert('There is some problem in inserting Shop');</script>"; 
                    }
                }

    $data['till']=$this->input->POST();
    $data['value']=$this->shop_model->editshop();
    $data['editTill']=$this->till_model->getTillWhere(array('shopid' => $id));

    if($this->input->POST('editTillId')){
       
             $pid=$this->input->POST('editTillId');

            $data['editTillInfo']=$this->till_model->getTill($pid);

    
    }
    else if($this->input->POST('id') ){

                $data['editTillInfo']=$this->till_model->getTill($this->input->POST('id'));

        $data['editTillInfo']=$this->till_model->getTill($this->input->POST('id'));

    }
    $this->load->view('admin/header');
    $this->load->view('admin/left_menu');
    $this->load->view('admin/editShop',$data);
    $this->load->view('admin/footer');
    } 
    //update user by admin 
   
	
    
    // show Suspend user list
    public function suspendList(){
        $this->load->model('User_model');
        $data['value']=$this->User_model->get_all();
        $this->load->view('admin/header');
        $this->load->view('admin/left_menu');
        $this->load->view('admin/suspend_list',$data);
        $this->load->view('admin/footer'); 

    }
    public function assignroles(){
        $this->load->model('Page_model');
        if($this->input->POST()){
                $data=$_POST['role'];
                $where=$_POST['id'];
                $success=$this->Page_model->update_roles($data,$where);
                if($success)
                {
                    echo "<script>alert('Role inserted successfully');</script>";
                }
                else{
                    echo "<script>alert('There is some problem in inserting Role');</script>";    
                }
                redirect('admin/employee','refresh');

            
        }
        $this->load->view('admin/header');
        $this->load->view('admin/left_menu');
        $this->page();
        $this->load->view('admin/footer');
    }
    public function page(){
    $auth['value2']=$this->Page_model->fetch_pages();
    $auth['value']=$this->Page_model->show_roles2();
    $this->load->view('admin/assignRoles',$auth);
    }
    //this is insertData from roles access view

    public function shopEmpPageData(){

        //$data=array('shopId' =>$this->input->post('shopId'),'empid'=>$this->input->post('empId'),'pageId'=>$this->input->post('pageId') );
   

    }

    //select Employee Name and ShopId from user table 
   function selectEmpById()
    { 
        $shop_id=$this->input->post('shopId');
        $data['empName']=$this->transaction_model->getEmpNameByshopId();
        foreach ($data['empName'] as $employee) {

            //echo '<pre>';echo $employee->shop_ids;print_r($employee);die("user table data");
            $empshopid=explode(',',$employee->shop_ids);
            //echo '<pre>';print_r($empshopid);die("user table data");
            if(in_array($shop_id, $empshopid)){
                echo "<option value='$employee->id'>$employee->fname</option>";
            }
        }
        
    } 

    // show all user list
    public function employee($data=NULL) { 
        if($this->input->post('empShopPagePost'))
        {

                
        
                    $this->form_validation->set_rules('shop', 'Shop name', 'required'); 
                    $this->form_validation->set_rules('employee', 'Employee Name', 'required'); 
                    $this->form_validation->set_rules('pages', 'pageId', 'required');
        //echo"<pre>";print_r($this->input->post('pageId')); die("hello");
             if ($this->form_validation->run() == FALSE){
            }else{
                 $pageid=implode(',',$this->input->post('pageId'));
                 $this->transaction_model->shopEmpPageInsert($pageid);
                 
                  
             }
             
                $data['rolesPostData']=$this->input->POST();
//print_r($data['postData']);
            //die();
        }
        if($this->input->POST('creeatrole')){
            $this->form_validation->set_rules('name', 'Role', 'required|is_unique[roles.name]');
            $this->form_validation->set_error_delimiters('<div class="error">', '</div>');
            if ($this->form_validation->run() == FALSE){
                 
            }else{
                $data =array('name' => $this->input->POST('name') );
                $success=$this->role_model->insertRole($data);
                if($success)
                    echo "<script>alert('Role inserted successfully');</script>";
                else
                    echo "<script>alert('There is some problem in inserting Role');</script>";    
            }
             $data['postRoll']=$this->input->POST();
        }
        
          if($this->input->POST('submit')){
            $this->form_validation->set_rules('email', 'Email', 'trim|required|valid_email');
            $this->form_validation->set_rules('password', 'Password', 'required');
            $this->form_validation->set_rules('fname', 'First Name', 'required');
            $this->form_validation->set_rules('mobile', 'Phone', 'required|regex_match[/^[0-9]+$/]');
            $this->form_validation->set_rules('address', 'Address', 'required');
            //$this->form_validation->set_rules('termination', 'Termination Date', 'required');

            $this->form_validation->set_rules('employement', 'Employement Date', 'required');
            $this->form_validation->set_rules('position', 'Position', 'required');
            $roll = $this->input->post('roll');
            if($roll=='none')
            {    
            $this->form_validation->set_rules('roll', 'roll', 'required|callback_select_validate'); // Validating select option field.
            }
            $this->form_validation->set_error_delimiters('<div class="error">', '</div>');
            if ($this->form_validation->run() == FALSE){
            }else{
                if($this->input->POST('shop') && ($roll==1 || $roll==2 || $roll==6))
                 {    
                    $shop=$this->input->POST('shop');
                    $shop_id=implode(",",$shop);
                    $data =array('add_user_ids'=>$this->session->userdata('user_id'),'shop_ids'=>$shop_id,'email' => $this->input->POST('email'),'password' => $this->input->POST('password'),'fname' => $this->input->POST('fname'),'lname' => $this->input->POST('lname'),'mobile' => $this->input->POST('mobile'),'address' => $this->input->POST('address'),'termination' => $this->input->POST('termination'),'employement' => $this->input->POST('employement'),'position' => $this->input->POST('position'),'suspend'=>1);
                }else{
                     $data =array('add_user_ids'=>$this->session->userdata('user_id'),'email' => $this->input->POST('email'),'password' => $this->input->POST('password'),'fname' => $this->input->POST('fname'),'lname' => $this->input->POST('lname'),'mobile' => $this->input->POST('mobile'),'address' => $this->input->POST('address'),'termination' => $this->input->POST('termination'),'employement' => $this->input->POST('employement'),'position' => $this->input->POST('position'),'suspend'=>1);
                }
            $last_insert_id = $this->acl_auth->register($data);
            $roll=array('roles_id'=>$this->input->POST('roll'),'users_id'=>$last_insert_id);
            $success=$this->user_role_model->registerRole($roll);
            if($last_insert_id)
                echo "<script>alert('Registration done successfully');</script>";
                else
                echo "<script>alert('There is some problem in Registration');</script>";    
            return $this->employee();
           
            }
             $data['postData']=$this->input->POST();
       }
       

        $data['pages']=$this->transaction_model->getPages();

        $this->load->model('User_model');
        $this->load->model('page_model');

        //select all employee of selected shop in dropdown
         $data['allCustomer']=$this->transaction_model->getSalesPersonNameByshopId();
        //show add  user 
        $data['users']=$this->User_model->getListById();
        //fetch all roll
        $data['roll']=$this->role_model->get_roll();
         //fetch all show 
        $data['shops']=$this->shop_model->getById();
        //fetch all pages
        $data['value2']=$this->page_model->fetch_pages();
        $data['value3']=$this->page_model->show_roles2();
        //select all pages from Pages table
        $data['page']=$this->transaction_model->getPages();
        //echo '<pre>';print_r($data['page']);die;

        $this->load->view('admin/header');
        $this->load->view('admin/left_menu');
        $this->load->view('admin/tabs',$data);
        $this->load->view('admin/footer');
        //$this->load->view('admin/ajaxCall'); 
    }
    //show specific edit user
    public function editUser($data=NULL)
    {
        $this->load->model('User_model');
        $data =array('shop'=>$this->input->POST('shop'),'users_id' => $this->input->POST('id'),'email' => $this->input->POST('email'),'fname' => $this->input->POST('fname'),'lname' => $this->input->POST('lname'),'mobile' => $this->input->POST('mobile'),'address' => $this->input->POST('address'),'termination' => $this->input->POST('termination'),'employement' => $this->input->POST('employement'),'position' => $this->input->POST('position'),'suspend'=>1);
       
        if($this->input->POST('shop')){
            
                $this->form_validation->set_rules('email', 'Email', 'trim|required|valid_email');
                $this->form_validation->set_rules('fname', 'First Name', 'required');
                $this->form_validation->set_rules('mobile', 'Phone', 'required|regex_match[/^[0-9]+$/]');
                $this->form_validation->set_rules('address', 'Address', 'required');
                $this->form_validation->set_rules('termination', 'Termination Date', 'required');
                $this->form_validation->set_rules('employement', 'Employement Date', 'required');
                $this->form_validation->set_rules('position', 'Position', 'required');
                $this->form_validation->set_rules('shop[]', 'Shop', 'required');
                $this->form_validation->set_error_delimiters('<div class="error">', '</div>');
                if ($this->form_validation->run() == FALSE){
                    
                    $data['postUserData']=$this->input->POST();
                
                }else{
                    $this->load->model('User_model');
                    $this->User_model->update_user();
                    redirect('employee');
                }
            }else{

                $this->form_validation->set_rules('email', 'Email', 'trim|required|valid_email');
                $this->form_validation->set_rules('fname', 'First Name', 'required');
                $this->form_validation->set_rules('mobile', 'Phone', 'required|regex_match[/^[0-9]+$/]');
                $this->form_validation->set_rules('address', 'Address', 'required');
                $this->form_validation->set_rules('termination', 'Termination Date', 'required');
                $this->form_validation->set_rules('employement', 'Employement Date', 'required');
                $this->form_validation->set_rules('position', 'Position', 'required');
                $this->form_validation->set_error_delimiters('<div class="error">', '</div>');
                if ($this->form_validation->run() == FALSE){
                    
                    $data['postUserData']=$this->input->POST();
                
                }else{
                    $this->load->model('User_model');
                    $this->User_model->update_user();
                    redirect('employee');
                }

            }
        
    $data['value']=$this->User_model->editUser();
    $data['roll']=$this->role_model->get_roll();
    $data['shops']=$this->shop_model->getById();
    $this->load->view('admin/header');
    $this->load->view('admin/left_menu');
    $this->load->view('admin/editUser',$data);
    $this->load->view('admin/footer');
    
    }    
    //update user by admin 
    public function userUpdate(){


        


    $this->employee();
    }
        //Function to create Roles
    public function createRoles(){
        
         return $this->employee($data);
    }

    //function for create new employee
    public function register(){
        //$this->acl_auth->restrict_access('admin');       
      
        return $this->employee($data);
    }
        // Below function is called for validating select option field.
    function select_validate($roll)
    {
    // 'none' is the first option that is default "-------Choose Roll-------"
        if($roll=="none"){
        $this->form_validation->set_message('select_validate', 'Please Select Roll.');
        return false;
        } else{
        // User picked something.
        return true;
        }
    }
    

}
