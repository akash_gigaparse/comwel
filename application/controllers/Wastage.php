<?php 
class Wastage extends CI_Controller{
	public function __construct(){
        parent::__construct();
        $this->load->library('form_validation');
        //$this->acl_auth->restrict_access('admin');
        $this->load->model('product_model');  
        $this->load->model('User_model');
        $this->load->model('shop_model');
        $this->load->model('Wastage_model');
        if(!$this->session->userdata('logged_in'))
        {
            redirect('', 'refresh');
        }
    }

    //function for order
    public function index($data=null){
        if($this->input->post('submit'))
        {
            $data['productWastage']=$this->Wastage_model->shopWastageReport($this->input->post('shop_id'),$this->input->post('date1'),$this->input->post('date2'));
            $data['totalWastage']=$this->Wastage_model->totalWastageReport($this->input->post('shop_id'),$this->input->post('date1'),$this->input->post('date2'));
           $data['date']=$this->input->post();
            
        }
        //get all product in product table
        $data['value']=$this->product_model->getProduct();
        $data['shops']=$this->shop_model->get_all();
        $data['empshop']=$this->User_model->currentUserInfo();
 //       echo '<pre>';print_r($data);die("jere");
    	$this->load->view('admin/header');
        if($this->session->userdata('user_roll')==5)
        {
            $this->load->view('superadmin/left_menu');
        }else{
            if($this->session->userdata('user_roll')!=1){
          
            page();
        }
        else{
          $this->load->view('admin/left_menu');
        }
        }    
        $this->load->view('wastage_report/index',$data);
        $this->load->view('admin/footer');
    }
    //function for generate csv for wastage report
     function wastageReportCsv($query=NULL, $filename = 'Wasatage_CSV_Report.csv')
    {

        $shopname =str_replace(" ","_",$this->input->POST('shopname'));
        $date1 =$this->input->POST('date1');
        $date2 = $this->input->POST('date2');
        $shop=$this->input->POST('shop_id');
        //    echo "select sum(qty) as wqty,(sum(qty)*product.cost) as totalwastageamount,note from wastages,product where product.shop_id=wastages.shop_id and product.id=wastages.product_id and wastages.shop_id='$shop' and date between '$date1' and '$date2' group by note";die;
        $query=$this->db->query("select product.name,sum(wastages.qty) as wqty,product.cost,(sum(wastages.qty)*product.cost) as total_retail_sales_dollars,(sum(wastages.qty)/(select sum(qty) as wqt from wastages where shop_id='$shop' and date between '$date1' and '$date2'))*100 as `percentage_%`,note from wastages,product where product.shop_id=wastages.shop_id and product.id=wastages.product_id and  wastages.shop_id='$shop' and wastages.date between '$date1' and '$date2' and product.filter='0' group by wastages.product_id,wastages.note");

        $this->load->dbutil();
        $this->load->helper('download');
        $delimiter = ",";
        $newline = "\n";
        $data = $this->dbutil->csv_from_result($query, $delimiter, $newline);
        $var="\tShop_Name-\t".$shopname."\tWastage_Report-\t".$date1."-To-".$date2."\n\n".$data;

        force_download($filename, $var);
    }
    
}