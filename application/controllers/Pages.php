<?php 
class Pages extends CI_Controller{
	public function __construct(){
        parent::__construct();
        $this->load->model('user_role_model');
        }
        public function reports1(){
        $this->load->view('pages/header');
        $this->load->view('pages/left_menu');
        $this->load->view('pages/reports1');
        $this->load->view('pages/footer');    
        }
        public function reports2(){
        $this->load->view('pages/header');
        $this->load->view('pages/left_menu');
        $this->load->view('pages/reports2');
        $this->load->view('pages/footer');    
        }
        public function reports3(){
        $this->load->view('pages/header');
        $this->load->view('pages/left_menu');
        $this->load->view('pages/reports3');
        $this->load->view('pages/footer');    
        }
        public function reports4(){
        $this->load->view('pages/header');
        $this->load->view('pages/left_menu');
        $this->load->view('pages/reports4');
        $this->load->view('pages/footer');    
        }


}