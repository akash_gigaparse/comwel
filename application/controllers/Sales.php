<?php 
class Sales extends CI_Controller{
    public function __construct(){
        parent::__construct();
        $this->load->model('shop_model');
         $this->load->model('User_model');
        $this->load->model('transaction_model');
        $this->load->database();
        $this->load->helper('url');
        $this->load->model('Customer_model');  
        $this->load->model('Profit_model');
       // $this->load->helper('csv');
        if(!$this->session->userdata('logged_in'))
        {
            redirect('', 'refresh');
        }
   
    }

    public function index($data=NULL){
        $data['users'] = $this->transaction_model->getSalesPersonName();
        $data['totalSales'] = $this->transaction_model->getTotalSales();
        $data['shops']=$this->shop_model->get_all();
        $data['empshop']=$this->User_model->currentUserInfo();
        $this->load->view('admin/header'); 
          
          
        if($this->session->userdata('user_roll')==5)
        {
          $this->load->view('superadmin/left_menu');
        }
        elseif($this->session->userdata('user_roll')!=1){
          
            page();
          }
        else{
          $this->load->view('admin/left_menu');
        }
        //echo '<pre>';print_r($data);die;
        $this->load->view('admin/salesReport',$data);
        $this->load->view('admin/footer');
            
    }
    function selectCustomerById()
    { 
      $data['allCustomer']=$this->transaction_model->getSalesPersonNameByshopId();
      $this->load->view('admin/ajaxCustomerList',$data);
    }

    public function sales($data=NULL){
        //fetch current user information
        $data['user']=$this->User_model->currentUserInfo();
         //fetch all show 
        if($this->input->POST()){
      
            $date1=$this->input->POST('date1');
            $date2=$this->input->POST('date2');
             if($this->session->userdata('user_roll')!=1)
            $shop=$this->session->userdata('shop_id');
        else
              $shop=$this->input->POST('shop');
            $limit=$this->input->POST('limit');
            $data['date']=array('date1' => $date1,'date2' => $date2,'shop' => $shop,'limit' => $limit);
          //for first (product) bar 
            $data['charts']=$this->transaction_model->getTotalSalesByEmployee($data['date']);
            $data['charts2']=$this->transaction_model->getTotalSalesbycost($data['date']);

            $data['charts3']=$this->Profit_model->getProfitBySingleShopDates();


           
           
          // return $this->sales($data);
        }

        if($this->session->userdata('user_roll')==5)
        {
            $this->load->view('admin/header');
            $this->load->view('superadmin/left_menu');
            $this->load->view('superadmin/sales',$data);
            $this->load->view('admin/footer');
        }else{

            //fetch user data and get shop id
            //$data['empshop']=$this->User_model->currentUserInfo();
            $data['shops']=$this->shop_model->get_all();
            $this->load->view('admin/header');
            $data['empshop']=$this->User_model->currentUserInfo();
            //$data['shops']=$this->shop_model->get_all();
            if($this->session->userdata('user_roll')!=1){
                page();
            }
            else
            {
            
                $this->load->view('admin/left_menu');
            }

            $this->load->view('admin/sales',$data);
            $this->load->view('admin/footer');
        }    
    }
    public function searchByDates()
    { 
        if($this->input->POST()){
          //echo '<pre>'; print_r($this->input->POST());die("a");
          //for first (product) bar 
            $data['charts']=$this->transaction_model->getTotalSalesByEmployee($this->input->POST());
            $data['charts2']=$this->transaction_model->getTotalSalesbycost($this->input->POST());

            $data['charts3']=$this->Profit_model->getProfitBySingleShopDates();

        //echo '<pre>';print_r($data);die;
        //echo '<pre>';print_r($data);die;
            $date1=$this->input->POST('date1');
            $date2=$this->input->POST('date2');
            $shop=$this->input->POST('shop');
            $limit=$this->input->POST('limit');
            $data['date']=array('date1' => $date1,'date2' => $date2,'shop' => $shop,'limit' => $limit);
           return $this->sales($data);
        }else{
           return $this->sales();
         }

    }
      public function salesReport($data=NULL){
        if($this->input->POST()){
         $date12=date_create($this->input->POST('date1'));
         $date22=date_create($this->input->POST('date2'));
         $diff=date_diff($date12,$date22);
         $datedif=$diff->format("%a");
         $data['charts']=$this->transaction_model->getByShopID($this->input->POST('shop_id'),$this->input->POST('date1'),$this->input->POST('date2'));
         $data['getTemp']=$this->transaction_model->getTemp($this->input->POST('shop_id'),$this->input->POST('date1'),$this->input->POST('date2'));
         $data['datedif']=$datedif; 
         $data['date']=$this->input->POST();
            //array_push($data['charts'],);
            //array_push($data['charts'],);
           // array_merge($data['total'],$data['notGstTotal']);
      /* echo "<pre>";

        echo $this->input->POST('shop_id');
        print_r($data['getTemp']);
        die();*/
        }
        $data['shops']=$this->shop_model->get_all();
        $data['empshop']=$this->User_model->currentUserInfo();
        //echo '<pre>';print_r($data);die;
        if($this->session->userdata('user_roll')==5)
        {
        $this->load->view('admin/header');
        $this->load->view('superadmin/left_menu');
        $this->load->view('sales/index',$data);
        $this->load->view('admin/footer');  

        }else{
            $this->load->view('admin/header');
            $this->load->view('admin/left_menu');
            $this->load->view('sales/index',$data);
            $this->load->view('admin/footer');  
        }
          
    }

    public function searchByDate()
    {
       if($this->input->POST()){
            $data['charts']=$this->transaction_model->getTotalSales($this->input->POST());
            //echo '<pre>';print_r($data);die;
          $date1=$this->input->POST('date1');
            $date2=$this->input->POST('date2');
            $user=$this->input->POST('shop');
            $data['date']=array('date1' => $date1,'date2' => $date2, 'user'=>$user);
            return $this->index($data);
        }else{
           return $this->index();
         }
    }
    public function searchByDateAndCustomer()
    {
      
        if($this->input->POST()){
            $data['customer_sales']=$this->transaction_model->getTotalSalesCustomer();
            //echo '<pre>';print_r($this->input->POST());die;
            $date1=$this->input->POST('date1');
            $date2=$this->input->POST('date2');
            $customer=$this->input->POST('customer');
            $shop_id=$this->input->POST('shop_id');
            $data['date']=array('date1' => $date1,'date2' => $date2, 'customer'=>$customer,'shop_id'=>$shop_id);
            return $this->index($data);
        }else{
           return $this->index();
         }
    }
    
    //csv create for user using date 
    function csvforadmin($query=NULL, $filename = 'CSV_Report.csv')
    {
      $date1 = date("Y-m-d", strtotime($this->input->POST('date1')));
      $date2 = date("Y-m-d", strtotime($this->input->POST('date2')));
      $customer=$this->input->POST('customer');
      $shop_id=$this->input->POST('shop_id');
      $shopname=$this->input->POST('shopname');
 //     $name=(explode(" ",$shopname));
      $shopname =str_replace(" ","_",$shopname);
      $this->load->dbutil();
      $this->load->helper('download');
      $delimiter = ",";
      $newline = "\n";
      if($customer=='All')
      {
      $query = $this->db->query('SELECT sales_person_name,sum(total) as Total_Dollar FROM transaction where sales_person_name!=" " and shopid="'.$shop_id.'"  AND (transaction_date BETWEEN "'.$date1.'" AND "'.$date2.'")  group by sales_person_name ');
      }else{
      //csv create for singal user
      $query = $this->db->query('SELECT sales_person_name,transaction_date,sum(total) as Total_Dollar FROM transaction where sales_person_name!=" " and shopid="'.$shop_id.'" AND sales_person_name="'.$customer.'" AND (transaction_date BETWEEN "'.$date1.'" AND "'.$date2.'") group by transaction_date DESC');
      }
      $data = $this->dbutil->csv_from_result($query, $delimiter, $newline);
      $var="Shop_Name ".$shopname."\nEmployee_Sale_Report ".$date1."_To_".$date2."\n\n".$data;
      force_download($filename, $var);
      

      }  

    function salesReportCsv($query=NULL, $filename = 'Sales_CSV_Report.csv')
    {
      $shopname =str_replace(" ","_",$this->input->POST('shopname'));
      $date1 =$this->input->POST('date1');
      $date2 = $this->input->POST('date2');
      $shop_id=$this->input->POST('shop_id');
      $data=array('startdate' => $date1,'enddate' => $date2, 'userid' => $this->session->userdata('user_id') , 'shopid' => $shop_id);
      $value=$this->shop_model->dateRange($data);
      if(!$value)
        {
          echo "<script>alert('2)  previous date range as this report is imported into the clients accounting software and needs to be 100% correct.');</script>";
          return $this->salesReport();
        } else {
         $date12=date_create($date1);
          $date22=date_create($date2);
          $diff=date_diff($date12,$date22);
          $datedif=$diff->format("%a");
          if($datedif==0){
          $query = $this->db->query("select hourtem as Time,totalsale,gstIncludedSales,gstFreeSales,mintem as Temp from(select HOUR(datetime) as hourtem ,DATE(datetime) as date,temp_c as mintem,shop_id from weather where shop_id='$shop_id' and datetime like '$date1%' group by hourtem) as wh left join (select HOUR(transaction.time) as hour,TRUNCATE(sum(transaction.total),2) as totalSale,shops.accountNumber,shops.gstFreeSales,transaction.transaction_date,shops.gstIncludedSales,shopid from transaction,shops where transaction.shopid=shops.id and transaction.shopid='$shop_id' and transaction.transaction_date ='$date1' group by hour ORDER BY hour DESC) as tr on hourtem=hour");
          }
          else{
          $query = $this->db->query("select date,transaction_date,totalsale,gstIncludedSales,accountNumber,mintem as Temp from (select DATE(datetime) as date,min(temp_c) as mintem, max(temp_c) as maxtem, avg(temp_c) as avgtem from weather where shop_id='$shop_id' and datetime BETWEEN '$date1' AND '$date2' group by date) as wh left join (select shops.gstFreeSales,shops.gstIncludedSales,shops.accountNumber,TRUNCATE(sum(transaction.total),2) as totalSale, TRUNCATE(SUM(IF(transaction.gst_inc = '', total, 0)),2) AS notGst,sum(transaction.total)-SUM(IF(transaction.gst_inc = '', transaction.total, 0)) as Gst ,transaction.transaction_date from transaction,shops where transaction.shopid=shops.id and transaction.shopid='$shop_id' and transaction.transaction_date BETWEEN '$date1' AND '$date2' group by transaction.transaction_date ORDER BY transaction.transaction_date DESC) as tr on date=transaction_date");
          }
          if($datedif==0){
          $query2 = $this->db->query("select HOUR(datetime) as hour ,min(temp_c) as mintem, max(temp_c) as maxtem, avg(temp_c) as avgtem  from weather  where shop_id='$shop_id' and datetime like '$date1%' group by hour");
          }
          else{
          $query2 = $this->db->query("select DATE(datetime) as date ,min(temp_c) as mintem, max(temp_c) as maxtem, avg(temp_c) as avgtem  from weather  where shop_id='$shop_id' and datetime BETWEEN '$date1' AND '$date2' group by date");
          } 
          $this->load->dbutil();
          $this->load->helper('download');
          $delimiter = ",";
          $newline = "\n";
          $data = $this->dbutil->csv_from_result($query, $delimiter, $newline);
          $data1 = $this->dbutil->csv_from_result($query2, $delimiter, $newline);
          $var="\tShop Name \t".$shopname ."\t Sales Report for \t".$date1." To ".$date2."\n\n".$data;
          force_download($filename, $var);
      }
    }
  //csv create for user using date 
  function csv($query=NULL, $filename = 'CSV_Report.csv')
  {
      $date1=date("Y-m-d", strtotime($this->input->POST('date1') ) );
      $date2=date("Y-m-d", strtotime($this->input->POST('date2') ) );
      $shop=$this->input->POST('shop');
      $shopname =str_replace(" ","_",$this->input->POST('shopname'));
      $limit=$this->input->POST('limit');
      $this->load->dbutil();
      $this->load->helper('download');
      $delimiter = ",";
      $newline = "\n";
    //csv create for singal user
      if($shop && $date1 && $date2)
      {
        //$query = $this->db->query("SELECT products.item,sum(products.qty) AS totalquantityCount FROM products left  join transaction on products.shop_id = transaction.shopid WHERE products.transactionid = transaction.id AND transaction.shopid =  '$shop'  AND transaction.transaction_date BETWEEN  '$date1' AND '$date2' GROUP BY products.item ORDER BY totalquantityCount DESC limit ".$limit."");
        //$query2 = $this->db->query("SELECT products.item,sum(products.qty) AS totalquantityCount FROM products left  join transaction on products.shop_id = transaction.shopid WHERE products.transactionid = transaction.id AND transaction.shopid =  '$shop'  AND transaction.transaction_date BETWEEN  '$date1' AND '$date2' GROUP BY products.item ORDER BY totalquantityCount ASC limit ".$limit."");
          $query = $this->db->query("SELECT products.item,sum(products.qty) AS totalquantityCount FROM transaction,products,product   WHERE products.shop_id = transaction.shopid AND products.transactionid = transaction.id AND transaction.shopid =  '$shop'  AND transaction.transaction_date BETWEEN  '$date1' AND '$date2' and product.shop_id=products.shop_id and product.name=products.item and product.filter=0 GROUP BY products.item ORDER BY totalquantityCount DESC limit ".$limit."");
          $query2 = $this->db->query("SELECT products.item,sum(products.qty) AS totalquantityCount FROM transaction,products,product   WHERE products.shop_id = transaction.shopid AND products.transactionid = transaction.id AND transaction.shopid =  '$shop'  AND transaction.transaction_date BETWEEN  '$date1' AND '$date2' and product.shop_id=products.shop_id and product.name=products.item and product.filter=0 GROUP BY products.item ORDER BY totalquantityCount ASC limit ".$limit."");

      }
      $data = $this->dbutil->csv_from_result($query, $delimiter, $newline);
      $data1 = $this->dbutil->csv_from_result($query2, $delimiter, $newline);
//      $var="Shop_Name ".$shopname."\nEmployee_Sale_Report ".$date1."_To_".$date2."\n\n".$data;

      $var="Shop_Name ".$shopname."\nWorst_and_Best_Sales_Report ".$date1."_To_".$date2."\nBy_Product_Count \n\n"."top_".$limit."_elemtnts\n".$data."\nbottom_".$limit."_elemtnts\n".$data1;
      force_download($filename, $var);
  }  
  //create csv for profit margin
  public function csvForProfitMargin($query=NULL, $filename = 'CSV_Report.csv')
  { 
  $shopname =str_replace(" ","_",$this->input->POST('shopname'));
  $date1=date("Y-m-d", strtotime($this->input->POST('date1') ) );
  $date2=date("Y-m-d", strtotime($this->input->POST('date2') ) );
  $shop=$this->input->POST('shop');
  $limit=$this->input->POST('limit');
  $this->load->dbutil();
  $this->load->helper('download');
  $delimiter = ",";
  $newline = "\n";
  $query = $this->db->query("SELECT sum(products.qty) as pqty,products.price, product.cost,sum(products.qty)*products.price as totalretailprice, sum(products.qty)*product.cost as totalcostprice,((((products.price-product.cost)* sum(products.qty))/sum(products.qty))*100) as `profitperctenge_%`,products.item FROM transaction,products,product WHERE transaction.shopid =products.shop_id and transaction.id =products.transactionid and products.shop_id =product.shop_id and products.item =product.name and transaction.shopid = '$shop' AND transaction.transaction_date BETWEEN '$date1' AND '$date2' and product.filter=0 GROUP BY products.item ORDER BY `profitperctenge_%` DESC limit ".$limit."");
  $query2 = $this->db->query("SELECT sum(products.qty) as pqty,products.price, product.cost,sum(products.qty)*products.price as totalretailprice, sum(products.qty)*product.cost as totalcostprice,((((products.price-product.cost)* sum(products.qty))/sum(products.qty))*100) as `profitperctenge_%`,products.item FROM transaction,products,product WHERE transaction.shopid =products.shop_id and transaction.id =products.transactionid and products.shop_id =product.shop_id and products.item =product.name and transaction.shopid = '$shop' AND transaction.transaction_date BETWEEN '$date1' AND '$date2' and product.filter=0 GROUP BY products.item ORDER BY `profitperctenge_%` ASC limit ".$limit."");

//  $query = $this->db->query("SELECT sum(products.qty) as pqty,products.price, product.cost,((((products.price-product.cost)* sum(products.qty))/sum(products.qty))*100) as profitperctenge,products.item FROM transaction,products,product WHERE transaction.shopid =products.shop_id and transaction.id =products.transactionid and products.shop_id =product.shop_id and products.item =product.name and transaction.shopid = '$shop' AND transaction.transaction_date BETWEEN '$date1' AND '$date2' and product.filter=0 GROUP BY products.item ORDER BY profitperctenge DESC limit ".$limit."");
//  $query2 = $this->db->query("SELECT sum(products.qty) as pqty,products.price, product.cost,((((products.price-product.cost)* sum(products.qty))/sum(products.qty))*100) as profitperctenge,products.item FROM transaction,products,product WHERE transaction.shopid =products.shop_id and transaction.id =products.transactionid and products.shop_id =product.shop_id and products.item =product.name and transaction.shopid = '$shop' AND transaction.transaction_date BETWEEN '$date1' AND '$date2' and product.filter=0 GROUP BY products.item ORDER BY profitperctenge ASC limit ".$limit."");
   
    $data = $this->dbutil->csv_from_result($query, $delimiter, $newline);
    $data1 = $this->dbutil->csv_from_result($query2, $delimiter, $newline);
    $var="Shop_Name ".$shopname."\nWorst_and_Best_Sales_Report".$date1."_To_".$date2."\nProfit_Margin\n\n"."top_".$limit."_elemtnts\n".$data."\nbottom_".$limit."_elemtnts\n".$data1;
    force_download($filename, $var);

  }
  //create csv for totalpricecount
  public function totalPriceCount($query=NULL, $filename = 'CSV_Report.csv')
  { 
    $shopname =str_replace(" ","_",$this->input->POST('shopname'));
    $date1=date("Y-m-d", strtotime($this->input->POST('date1') ) );
    $date2=date("Y-m-d", strtotime($this->input->POST('date2') ) );
    $shop=$this->input->POST('shop');
    $limit=$this->input->POST('limit');
    $this->load->dbutil();
    $this->load->helper('download');
    $delimiter = ",";
    $newline = "\n";
    //$query = $this->db->query("SELECT products.item,TRUNCATE(sum(products.price),2) AS totalPriceCount FROM products left  join transaction on products.shop_id = transaction.shopid WHERE products.transactionid = transaction.id AND transaction.shopid =  '$shop'  AND transaction.transaction_date BETWEEN  '$date1' AND '$date2' GROUP BY products.item ORDER BY totalPriceCount DESC limit ".$limit."");
    $query = $this->db->query("SELECT products.item,TRUNCATE(sum(products.price),2) AS totalPriceCount FROM products,transaction,product where products.shop_id = transaction.shopid AND products.transactionid = transaction.id AND transaction.shopid =  '$shop'  AND transaction.transaction_date BETWEEN  '$date1' AND '$date2' AND products.shop_id =product.shop_id and products.item =product.name And product.filter=0 GROUP BY products.item ORDER BY totalPriceCount DESC limit ".$limit."");
    
    //$query2 = $this->db->query("SELECT products.item,TRUNCATE(sum(products.price),2) AS totalPriceCount FROM products left  join transaction on products.shop_id = transaction.shopid WHERE products.transactionid = transaction.id AND transaction.shopid =  '$shop'  AND transaction.transaction_date BETWEEN  '$date1' AND '$date2' GROUP BY products.item ORDER BY totalPriceCount ASC limit ".$limit."");
    $query2 = $this->db->query("SELECT products.item,TRUNCATE(sum(products.price),2) AS totalPriceCount FROM products,transaction,product where products.shop_id = transaction.shopid AND products.transactionid = transaction.id AND transaction.shopid =  '$shop'  AND transaction.transaction_date BETWEEN  '$date1' AND '$date2' AND products.shop_id =product.shop_id and products.item =product.name And product.filter=0 GROUP BY products.item ORDER BY totalPriceCount ASC limit ".$limit."");
    
    $data = $this->dbutil->csv_from_result($query, $delimiter, $newline);
    $data1 = $this->dbutil->csv_from_result($query2, $delimiter, $newline);
    $var="Shop_Name ".$shopname."\nWorst_and_Best_Sales_Report ".$date1."_To_".$date2."\nProduct_Sales\n\n"."top_".$limit."_elemtnts\n".$data."\nbottom_".$limit."_elemtnts\n".$data1;
    

    force_download($filename, $var);

  }

}
