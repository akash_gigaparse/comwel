<?php
class till extends CI_Controller{
	public function __construct(){
        parent::__construct();
        $this->load->library('form_validation');
        $this->load->model('shop_model');  
        $this->load->model('role_model');  
        $this->load->model('user_role_model');
        $this->load->model('till_model');
    
    }
	public function index(){
		  if($this->input->POST()){
            $this->form_validation->set_rules('tillid', 'Till ID', 'required');
            $this->form_validation->set_rules('address', 'Address', 'required');
            $this->form_validation->set_rules('phone', 'Contact Details', 'required|regex_match[/^[0-9]+$/]');
           $this->form_validation->set_rules('deposite', 'Direct deposite details', 'required');
           $this->form_validation->set_rules('overhead', 'Default Product Overhead cost', 'required');
            $this->form_validation->set_rules('suburb', 'Suburb', 'required');
            $this->form_validation->set_rules('state', 'State', 'required');
            $this->form_validation->set_rules('postcode', 'Postcode', 'required');
            $this->form_validation->set_rules('country', 'Country', 'required');
            $this->form_validation->set_rules('longitude', 'Longitude', 'required');
            $this->form_validation->set_rules('latitude', 'Latitude', 'required');
            $this->form_validation->set_error_delimiters('<div class="error">', '</div>');
            if ($this->form_validation->run() == FALSE){
                 
            }else{
            	 $data =array('name' => $this->input->POST('name'),'address' => $this->input->POST('address'),'phone'=> $this->input->POST('phone'),'logo'=> $file_name,'deposite'=> $this->input->POST('deposite'),'overhead'=> $this->input->POST('overhead'),'state' => $this->input->POST('state'),'suburb' => $this->input->POST('suburb'),'postcode' => $this->input->POST('postcode'),'country' => $this->input->POST('country'),'longitude' => $this->input->POST('longitude'),'latitude' => $this->input->POST('latitude'));
          
            }
		      
	}
        $data['value']=$this->till_model->getTill();
		$this->acl_auth->restrict_access('admin');
        $this->load->view('admin/header');
        $this->load->view('admin/left_menu',$data);
        $this->load->view('till');
        $this->load->view('admin/footer'); 
    }

    //Getting till data on the basis of id
    public function getTill(){
       $data=$this->till_model->getTillWhere(array('id' => $this->input->post('id')));
       echo json_encode($data[0]);die;
    }

}
?>