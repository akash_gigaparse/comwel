<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Shopowner_model extends MY_Model
{
   
   /* // this function return all shop owner list
    public function getList(){
    $this->db->select('*'); 
    $this->db->from('users a');
    $this->db->join('user_roles b', 'a.id = b.users_id', 'left'); 
    $query = $this->db->get();
    $value=$query->result();
    return $query->result();
    }

    */
    public function suspend($suspend_id)
    {
         return $this->db->update('shops', array('suspendShop' => 0),array('id' => $suspend_id));
    }

    public function rejoin($rejoin_id)
    {
    return $this->db->update('shops', array('suspendShop'=>1),array('id' => $rejoin_id));
    }
    public function getListById(){
       return $this->db->get_where('users',array('add_user_ids'=>$this->session->userdata('user_id') ) )->result();
    }
    public function register($data)
    {
        $this->db->insert('users',$data);
       return $insert_id = $this->db->insert_id();
    }
    public function suspendUser($suspend_id)
    {
         return $this->db->update('users', array('suspend' => 0),array('id' => $suspend_id));
    }

    public function rejoinUser($rejoin_id)
    {
    return $this->db->update('users', array('suspend'=>1),array('id' => $rejoin_id));
    }
     public function editUser(){
        $user_id=$this->input->get('id');
        $this->db->join( 'user_roles ur', 'users.id = ur.users_id' );
        $this->db->join( 'roles r', 'r.id = ur.roles_id' );
        return $this->get_by( array( 'ur.users_id' => $user_id ) );
    }
     public function currentUserInfo(){
     
        return $this->get_by( array( 'id' => $this->session->userdata('user_id') ) );
    }
    public function update_user(){
    $id=$this->input->POST('id');
    $email=$this->input->POST('email');
    $fname=$this->input->POST('fname');
    $lname=$this->input->POST('lname');
    $mobile=$this->input->POST('mobile');
    $address=$this->input->POST('address');
    $employement=$this->input->POST('employement');
    $termination=$this->input->POST('termination');
    $position=$this->input->POST('position');
    $shops=implode(",",$this->input->POST('shop'));
    $data=array('email'=>$email,'shop_ids'=>$shops,'fname'=>$fname,'lname'=>$lname,'address'=>$address,'mobile'=>$mobile,'employement'=>$employement,'termination'=>$termination,'position'=>$position);
    return $this->db->update('users', $data,array('id' => $id));
    }
    public function delete_rol($id)
    {
    return $this->db->delete('user_roles',array('users_id' => $id));
    }


}