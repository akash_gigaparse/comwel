<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Super_admin_model extends MY_Model
{
    // this function return all shop owner list
    public function getList(){
    $this->db->select('*'); 
    $this->db->from('users a');
    $this->db->join('user_roles b', 'a.id = b.users_id', 'left'); 
    $query = $this->db->get();
    $value=$query->result();
    return $query->result();
    }

    public function editUser(){
        $user_id=$this->input->get('id');
        $this->db->join( 'user_roles ur', 'users.id = ur.users_id' );
        $this->db->join( 'roles r', 'r.id = ur.roles_id' );
        return $this->get_by( array( 'ur.users_id' => $user_id ) );
    }
     public function currentUserInfo(){
     
        return $this->get_by( array( 'id' => $this->session->userdata('user_id') ) );
    }
    public function update_user(){
     $id=$this->input->POST('id');
    $email=$this->input->POST('email');
    $fname=$this->input->POST('fname');
    $lname=$this->input->POST('lname');
    $mobile=$this->input->POST('mobile');
    $address=$this->input->POST('address');
    $employement=$this->input->POST('employement');
    $termination=$this->input->POST('termination');
    $position=$this->input->POST('position');
    $roll=$this->input->POST('roll');
    if($roll==1|| $roll==2 || $roll==6)
    {
        $shops=$this->input->POST('shop');
        $empShop=implode(',',$shops);
        $data=array('email'=>$email,'fname'=>$fname,'lname'=>$lname,'address'=>$address,'mobile'=>$mobile,'employement'=>$employement,'termination'=>$termination,'position'=>$position,'shop_ids'=>$empShop);
    
    }else{
    $data=array('email'=>$email,'fname'=>$fname,'lname'=>$lname,'address'=>$address,'mobile'=>$mobile,'employement'=>$employement,'termination'=>$termination,'position'=>$position,'shop_ids'=>"");
    
    }
    //echo '<pre>';print_r($empShop);die;
    $this->db->update('user_roles', array('roles_id'=>$roll),array('users_id' => $id));
    return $this->db->update('users', $data,array('id' => $id));
}


    public function suspend($suspend_id)
    {
    return $this->db->update('shops', array('suspendShop' => 0),array('id' => $suspend_id));
    }

    public function rejoin($rejoin_id)
    {
    return $this->db->update('shops', array('suspendShop'=>1),array('id' => $rejoin_id));
    }

    public function delete_rol($id)
    {
    return $this->db->delete('user_roles',array('users_id' => $id));
    }
}