<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Transaction_model extends MY_Model{
    
    public function getSalesPersonName(){
        $this->db->select('sales_person_name');
        $this->db->distinct();
        $query=$this->db->get('transaction');
        return $query->result();
    }   

    public function getTotalSales($employee=NULL){
        if($employee === NULL){
            $query = $this->db->query('select TRUNCATE(sum(total),2) as totalSales,sales_person_name from transaction group by sales_person_name');
        }
        else{

            //echo '<pre>';print_r($this->input->POST());die;
            //$date1=$this->input->POST('date1');
            //$date2=$this->input->POST('date2');
            $user=$this->input->POST('user');
            $date1=date("Y-m-d", strtotime($this->input->POST('date1') ) );
            $date2=date("Y-m-d", strtotime($this->input->POST('date2') ) );
            if($user=='All')
            {
            $query = $this->db->query("select TRUNCATE(sum(total),2) as totalSales,sales_person_name,transaction_date from transaction  where transaction_date BETWEEN '$date1' AND '$date2' group by sales_person_name");
            //echo $query123="select * from transaction where transaction_date BETWEEN '2015-01-01' AND '2015-01-01'";
            //$query1 = $this->db->query("select * from transaction where transaction_date BETWEEN '24-02-2015' AND '28-02-2015'");   
            //$value=$query->result();
            //echo '<pre>';print_r($value);die("hjhj");
            return $query->result();

            }else{
            $this->db->select('*');
            $this->db->select_sum('total', 'total');
            $this->db->where("transaction_date BETWEEN '$date1' AND '$date2'");
            $this->db->where('sales_person_name', $this->input->POST('user'));
            return $this->db->get('transaction')->result();
            }    
            
         

        }
        return $query->result();
    }

 //get All shop from shops table 
    public function getAllShop(){
        
        $query=$this->db->get('shops');
        return $query->result();

    }

    //select all pages from page table 
    public function getPages(){


        $query=$this->db->get('pages');
        return $query->result();


    }
    //select customer name from suctomer table 
    public function getCustNameByshopId(){
//echo $this->input->POST("shopId");die("trasaction model calling brajuuu");;
     
        $this->db->where('shop_ids', $this->input->POST('shopId'));
        $query=$this->db->get('customers');
        return $query->result();




    }
    //update order where order id=$id
    public function updatePaid($id=''){

            //$id=$this->uri->segment(3);
            $date=date('Y-m-d');
            $time=date('h:i:s');
            $data=array('paided'=>1,'date'=>$date,'time'=>$time);
            //print_r($data);die;
            
            $this->db->where('id',$id);
        $result=$this->db->update('orders',$data);

    }
//select order where paided =2
    public function orderPaidedSelect(){

        $data = array('customer_id' => $this->input->post('custId'),'paided'      => 2);

        $this->db->select('*');
        $this->db->from('orders');
        $this->db->where($data);

        $query = $this->db->get();
        return $query->result_array();
            
    }
    //seleced order is now Paided=1
    public function orderPaidUpdate(){
        //  echo"<pre>"; print_r($this->input->post());die;
    $this->db->set('paided',1);
    $this->db->where('customer_id',2);
    $this->db->update('orders');



    }
    // select shop id where login user_id from users table
    public function selectUserShop($id){
        //echo $id;
        $this->db->select('shop_ids');
        $this->db->where('id',$id);
        $query=$this->db->get('users');
        return $query->result();

    }
    //insert "shopId , empId , pages multiple" into 
    public function shopEmpPageInsert($pageid){
       $str = $this->db->delete('shopemppage',array('shopId'=>$this->input->post('shop'),'empId'=>$this->input->post('User'))); 

       // echo $pageid;die;
        $data=array('shopId'=>$this->input->post('shop'),'empId'=>$this->input->post('User'),'pageId'=>$pageid);
        //$shopId=$this->input->post('shopId');
        //$empId=$this->input->post('empId');

        //$this->db->set('shopId', $this->input->post('shopId'));
        //$this->db->set('empId', $this->input->post('empId'));
        //$this->db->set('pageId', $pageid);
        
        $str = $this->db->insert('shopemppage',$data); 
       
        
    }

    public function getTotalSalesByEmployee($employee=NULL){

            $shop=$this->input->POST('shop');
            $date1=date("Y-m-d", strtotime($this->input->POST('date1') ) );
            $date2=date("Y-m-d", strtotime($this->input->POST('date2') ) );
          //without filter
            //$query = $this->db->query("SELECT products.item,sum(products.qty) AS totalProductCount FROM products left  join transaction on products.shop_id = transaction.shopid WHERE products.transactionid = transaction.id AND transaction.shopid =  '$shop'  AND transaction.transaction_date BETWEEN  '$date1' AND '$date2' GROUP BY products.item ORDER BY totalProductCount DESC ");
            //add filter
            $query = $this->db->query("SELECT products.item,sum(products.qty) AS totalProductCount FROM transaction,products,product   WHERE products.shop_id = transaction.shopid AND products.transactionid = transaction.id AND transaction.shopid =  '$shop'  AND transaction.transaction_date BETWEEN  '$date1' AND '$date2' and product.shop_id=products.shop_id and product.name=products.item and product.filter=0 GROUP BY products.item ORDER BY totalProductCount DESC ");
            
            //$query = $this->db->query("SELECT products.qty as pqty,products.price, product.cost,(((products.price-product.cost)* sum(products.qty))/sum(products.qty))*100 as profitperctenge,products.item FROM transaction,products,product WHERE transaction.shopid =products.shop_id and transaction.id =products.transactionid and products.shop_id =product.shop_id and products.item =product.name and transaction.shopid = '$shopid' AND transaction.transaction_date BETWEEN '$date1' AND '$date2' and product.filter=0 GROUP BY products.item ORDER BY profitperctenge DESC ");
          
//          echo $this->db->last_query();die();
            return $query->result();
    }
    public function getTotalSalesbycost($employee=NULL){
    $shop=$this->input->POST('shop');
    $limit=$this->input->POST('limit');
    $date1=date("Y-m-d", strtotime($this->input->POST('date1') ) );
    $date2=date("Y-m-d", strtotime($this->input->POST('date2') ) );
    //without filter
    //$query = $this->db->query("SELECT products.item,TRUNCATE(sum(products.price),2) AS totalPriceCount FROM products left  join transaction on products.shop_id = transaction.shopid WHERE products.transactionid = transaction.id AND transaction.shopid =  '$shop'  AND transaction.transaction_date BETWEEN  '$date1' AND '$date2' GROUP BY products.item ORDER BY totalPriceCount DESC ");
    $query = $this->db->query("SELECT products.item,TRUNCATE(sum(products.price),2) AS totalPriceCount FROM products,transaction,product where products.shop_id = transaction.shopid and products.transactionid = transaction.id AND transaction.shopid =  '$shop'  AND transaction.transaction_date BETWEEN  '$date1' AND '$date2' and product.shop_id=products.shop_id and product.name=products.item and product.filter=0 GROUP BY products.item ORDER BY totalPriceCount DESC ");
    
    //echo $this->db->last_query();die();
    return $query->result();
    }


    public function getSalesPersonNameByshopId(){
        //echo $this->input->POST("shopId");die("asdf");;
        $this->db->select('sales_person_name');
        $this->db->distinct();
        $this->db->where('shopid', $this->input->POST('shopId'));
        $query=$this->db->get('transaction');
        return $query->result();
    }  
    public function getTotalSalesCustomer(){
        //echo '<pre>';print_r($this->input->POST());die;
        //$date1=$this->input->POST('date1');
        //$date2=$this->input->POST('date2');
        $customer=$this->input->POST('customer');
        $date1=date("Y-m-d", strtotime($this->input->POST('date1') ) );
        $date2=date("Y-m-d", strtotime($this->input->POST('date2') ) );

        $customer=$this->input->POST('customer');
        $shop_id=$this->input->POST('shop_id');
        if($customer=='All')
      {
      $query = $this->db->query('SELECT sales_person_name,TRUNCATE(sum(total),2) as Total_Dollar FROM transaction where sales_person_name!=" " and shopid="'.$shop_id.'"  AND (transaction_date BETWEEN "'.$date1.'" AND "'.$date2.'")  group by sales_person_name ');
      }else{
      
      $query = $this->db->query('SELECT sales_person_name,TRUNCATE(sum(total),2) as Total_Dollar,transaction_date FROM transaction where sales_person_name!=" " and shopid="'.$shop_id.'" AND sales_person_name="'.$customer.'" AND (transaction_date BETWEEN "'.$date1.'" AND "'.$date2.'") group by transaction_date DESC');
      }
   return $query->result();

    }
     public function getByShopID($shop_id,$date1,$date2){
              $date12=date_create($date1);
            $date22=date_create($date2);
            $diff=date_diff($date12,$date22);
            $datedif= $diff->format("%a");
           
            if($datedif==0){
                         $query = $this->db->query("select HOUR(time) as hour,TRUNCATE(sum(total),2) as totalSale, SUM(IF(gst_inc = '', total, 0)) AS notGst ,transaction_date from transaction  where shopid='$shop_id' and transaction_date ='$date1'  group by hour  ORDER BY hour DESC");

            }
             else{
                    $query = $this->db->query("select TRUNCATE(sum(total),2) as totalSale, SUM(IF(gst_inc = '', total, 0)) AS notGst ,transaction_date from transaction  where shopid='$shop_id' and transaction_date BETWEEN '$date1' AND '$date2' group by transaction_date  ORDER BY transaction_date DESC");
            }
            //echo $this->db->last_query(); die;
         return $query->result();
     }
   
 public function getTemp($shop_id,$date1,$date2){
         $date12=date_create($date1);
         $date22=date_create($date2);
         $diff=date_diff($date12,$date22);
         $datedif=$diff->format("%a");
           
            if($datedif==0){
            $query = $this->db->query("select HOUR(datetime) as hour ,min(temp_c) as mintem, max(temp_c) as maxtem, avg(temp_c) as avgtem  from weather  where shop_id='$shop_id' and datetime like '$date1%' group by hour");
               // echo "o";
               // echo $this->db->last_query();
            }
            else{
                    $query = $this->db->query("select DATE(datetime) as date ,min(temp_c) as mintem, max(temp_c) as maxtem, avg(temp_c) as avgtem  from weather  where shop_id='$shop_id' and DATE(datetime) BETWEEN '$date1' AND '$date2' group by date");
               // echo "1";
        
             } 
             return $query->result();
     }

}
