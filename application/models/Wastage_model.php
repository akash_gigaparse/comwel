<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Wastage_model extends MY_Model
{
    // model for add wastage products
            public function addWastage($data){
            return $this->insert($data);
            }
    // model for get all wastage products
            public function getShopWastage($where,$date=""){
                    
        				$this->db->select('sum(qty) as qty');
        				$this->db->where('product_id',$where); 
                          
                        if(!empty($date))
                        $this->db->where('date',$date); 
        				
                        $this->db->group_by('product_id');
        				$query=$this->db->get('wastages');
    
            			return $query->result();
            }
            public function shopWastageReport($shop,$date1,$date2){
               $query=$this->db->query("select sum(qty) as wqty,(select sum(qty) as wqt from wastages where shop_id='$shop' and date between '$date1' and '$date2') as totalSale,date,note from wastages,product where product.shop_id=wastages.shop_id and product.id=wastages.product_id and  wastages.shop_id='$shop' and wastages.date between '$date1' and '$date2' and product.filter='0' group by note");
                return $query->result();
            }

            public function totalWastageReport($shop,$date1,$date2){
               $query=$this->db->query("select sum(qty) as wqty,date from wastages where shop_id='$shop' and date between '$date1' and '$date2'");
               return $query->result();
            }


}