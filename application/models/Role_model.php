<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Role_model extends MY_Model
{
    //Inserting Role
    public function insertRole($data){
    	return $this->insert($data);
    }

	public function getRole($id){
    	$data=$this->get_by('id',$id);
    	return $data->name;
    }    
    //Get All Type Of Role
    public function get_roll(){
    	$query=$this->db->get('roles');
    	return $query->result();
    }
   
}