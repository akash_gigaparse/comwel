<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class User_model extends MY_Model
{
    public function has_role( $user, $role )
    {
        $this->db->join( 'user_roles ur', 'users.id = ur.users_id' );
        $this->db->join( 'roles r', 'r.id = ur.roles_id' );
        return $this->get_by( array( 'r.name' => $role, 'ur.users_id' => $user ) );
    }

    public function field_exists( $field )
    {
        return $this->db->field_exists( $field, $this->config->item('user_table', 'acl_auth') );
    }

    public function get_user( $identity )
    {
        if( ! $identity )
        {
            return false;
        }
        return $this->get_by( $this->config->item( 'identity_field', 'acl_auth' ), $identity );
    }

    public function check_token( $token )
    {
        return ( $token === $this->reset_code );
    }


    public function editUser(){
        $user_id=$this->input->get('id');

        $this->db->join( 'user_roles ur', 'users.id = ur.users_id' );
        $this->db->join( 'roles r', 'r.id = ur.roles_id' );
        return $this->get_by( array( 'ur.users_id' => $user_id ) );
    }
     public function currentUserInfo(){
     
        return $this->get_by( array( 'id' => $this->session->userdata('user_id') ) );
    }
    public function update_user(){
        //echo"<pre>"; print_r($this->input->POST());die('this');
    $id=$this->input->POST('id');
    $email=$this->input->POST('email');
    $fname=$this->input->POST('fname');
    $lname=$this->input->POST('lname');
    $mobile=$this->input->POST('mobile');
    $address=$this->input->POST('address');
    $employement=$this->input->POST('employement');
    $termination=$this->input->POST('termination');
    $position=$this->input->POST('position');
    $roll=$this->input->POST('roll');
    
    $shops=$this->input->POST('shop');
    $empShop=implode(',',$shops);
    $data=array('email'=>$email,'fname'=>$fname,'lname'=>$lname,'address'=>$address,'mobile'=>$mobile,'employement'=>$employement,'termination'=>$termination,'position'=>$position,'shop_ids'=>$empShop);

    //echo '<pre>';print_r($empShop);die;
    $this->db->update('user_roles', array('roles_id'=>$roll),array('users_id' => $id));
    return $this->db->update('users', $data,array('id' => $id));
}

	public function get_pages($data)
    {

		$this->load->database(); 
        $this->db->where("role_id",$data->roles_id);
        $query=$this->db->get("page");
   	    return $aResult = $query->result_array();
    }

    public function suspend($suspend_id)
    {
        return $this->db->update('users', array('suspend'=>0,'suspend_date'=>date("d-m-Y")),array('id' => $suspend_id));
    }

    public function rejoin($rejoin_id)
    {
    return $this->db->update('users', array('suspend'=>1),array('id' => $rejoin_id));
    }

    public function delete_rol($id)
    {
    return $this->db->delete('user_roles',array('users_id' => $id));
    }
     public function getListById(){
        if($this->session->userdata('user_roll')==5)
        return $this->db->get_where('users')->result();
        else
        return $this->db->get_where('users',array('add_user_ids'=>$this->session->userdata('user_id') ) )->result();
    }

}