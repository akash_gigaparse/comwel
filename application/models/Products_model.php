<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Products_model extends MY_Model
{

	
	//get all product 
    public function getProducts()
    {
        $query=$this->db->get('products');
        
        return $query->result();
        
    }
    public function getShopProducts($shop_id){
            $this->db->select('products.item,sum(products.qty) as qty');
            //$this->db->join('product pr', 'pr.name = products.item','right');
            //$this->db->where('products.item', 'pr.name');
            $this->db->where('products.shop_id', $shop_id);
            $this->db->group_by('products.item');
            //$this->db->where('pr.filter','0');
            $query=$this->db->get('products');
        return $query->result();
        }    
    public function getAutoProducts($start,$shop_id)
    {
                $value=strtoupper($start);
                $this->db->like('item', $value, 'both'); 
                $this->db->where('shop_id', $shop_id); 
                $query=$this->db->get('products');
    
                return $query->result();  
        
    }
	
     public function productionReport($shop_id,$date1,$date2){
        $classname=$this->input->POST("product_class");
         $query = $this->db->query("SELECT sum(products.qty) as pqty, products.item, transaction.transaction_date, product.filter,product.id FROM transaction,products,product WHERE transaction.shopid =products.shop_id and transaction.id =products.transactionid and products.shop_id =product.shop_id and products.item =product.name and transaction.shopid = '$shop_id' AND product.class='$classname' AND transaction.transaction_date BETWEEN '$date2' AND '$date1' GROUP BY products.item");
        return $query->result();
    }     
           
}