<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Production_model extends MY_Model
{
    // model for add produce products
    public function addProduction($data){
        return $this->insert($data);
    }
    // model get prodution detail for shop
    public function productionDetail($id){
        $this->db->select('productions.pqty,pr.name,pr.id,wt.qty wqty');
        $this->db->from('productions');
        $this->db->join('shops sh', 'productions.shop_id = sh.id');
        $this->db->join('product pr', 'productions.product_id = pr.id');
        $this->db->join( 'wastages wt', 'pr.id = wt.product_id' );
        $this->db->where('wt.shop_id', $id);
        $query = $this->db->get();
        return  $query->result();
        
        /*$user_id=$this->input->get('id');
        $this->db->join('shops sh', 'productions.shop_id = sh.id' );
        $this->db->join('product pr', 'productions.product_id = pr.id');
        $this->db->join( 'orders or', 'productions.shop_id= or.shop_id' );
        $this->db->join( 'wastages wt', 'productions.shop_id= wt.shop_id' );
        return $this->get_many_by( array( 'productions.shop_id => $id') );
        */
    }     
           
         public function getProdution($where){
            
       
        $this->db->select('sum(pqty) as pqty');
        $this->db->where('product_id',$where); 
        
        $this->db->group_by('product_id');
        $query=$this->db->get('productions');
    
            return $query->result();
            

        }  
         public function checkCurrentProduction($where,$shopid,$class){
            
       
        $this->db->where('date',$where); 
        $this->db->where('shop_id',$shopid);
       $this->db->where('class',$class);
        $query=$this->db->get('productions');
    
            return $query->result();
            

        }      
        public function productionReport($shop_id,$date1,$date2){
            $this->db->select('sum(productions.pqty) as pqty,product.name');
            $this->db->join('productions', 'productions.product_id = product.id');
            $this->db->where("productions.date BETWEEN '$date1' AND '$date2'");
            $this->db->where('product.shop_id', $shop_id);
            $this->db->group_by('productions.product_id');
            $query=$this->db->get('product');
            return $query->result();
        } 
        public function getProductClassByshopId($shopid){
        $this->db->select('class');
        $this->db->group_by('class');
        $this->db->where('shop_id',$shopid);
        $query=$this->db->get('product');
        return $query->result();
    } 
      
}