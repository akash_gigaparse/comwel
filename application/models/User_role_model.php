<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class User_role_model extends MY_Model
{
	//Return role of given user
    public function get_role($user_id){
        return $this->get_by('users_id',$user_id);
    }
     //Inserting Role
    public function registerRole($data){
        return $this->insert($data);
    }


}