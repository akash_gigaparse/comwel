<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Profit_model extends MY_Model
{
    //get shop by shop id 
    public function getProfitBySingleShopDates()
    {
        $shopid=$this->input->POST('shop');
        $date1=date("Y-m-d", strtotime($this->input->POST('date1') ) );
        $date2=date("Y-m-d", strtotime($this->input->POST('date2') ) );
//        $query = $this->db->query("SELECT sum(products.qty) as pqty,products.price, product.cost,products.item FROM transaction,products,product WHERE transaction.shopid =products.shop_id and transaction.id =products.transactionid and products.shop_id =product.shop_id and products.item =product.name and transaction.shopid = '$shopid' AND transaction.transaction_date BETWEEN '$date1' AND '$date2' GROUP BY products.item DESC");
        $query = $this->db->query("SELECT sum(products.qty) as pqty,products.price, product.cost,(((products.price-product.cost)* sum(products.qty))/sum(products.qty))*100 as profitperctenge,products.item FROM transaction,products,product WHERE transaction.shopid =products.shop_id and transaction.id =products.transactionid and products.shop_id =product.shop_id and products.item =product.name and transaction.shopid = '$shopid' AND transaction.transaction_date BETWEEN '$date1' AND '$date2' and product.filter=0 GROUP BY products.item ORDER BY profitperctenge DESC ");

        //echo $this->db->last_query();die();
//        $val=$query->result();  echo '<pre>';print_r($val);die;
        return $query->result();  
   }
   
           
}
