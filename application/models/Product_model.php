<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Product_model extends MY_Model
{

	//add new product
	public function newPoduct($data)
    {
       $value=$this->db->insert('product', $data); 
    }
	
	//get all product 
    public function getProduct()
    {
        $query=$this->db->get('product');
        return $query->result();
    }

    //get all product 
    public function getProductByWhere($where)
    {
        $query = $this->db->get_where('product',$where);
        return $query->result();
    }

    public function getShopProduct($shop_id){
            

            $this->db->where('shop_id', $shop_id); 
            $this->db->order_by("name", "asc");
            $query=$this->db->get('product');
    
            return $query->result();
            

        }    
    public function getAutoProduct($start,$shop_id)
    {
                $value=strtoupper($start);
                $this->db->like('name', $value, 'both'); 
                $this->db->where('shop_id', $shop_id); 
                $query=$this->db->get('product');
    
                return $query->result();  
    }
	//Delete  product by id 
    public function deleteById()
    {
    	$this->db->delete('product', array('id' => $this->input->post('productRemove_id'))); 
    }
    
    //Edit product record show
	public function editProduct(){
		return $this->db->get_where('product',array('id' => $this->input->get('id')))->result();
    }
    //update product 
    public function updateProduct($data){
    return  $this->db->update('product',$data,array('id' => $data['id']));
    }
    public function query()
    {
    $query = $this->db->query('INSERT product (name, retail, gst,shop_id) SELECT DISTINCT item, price, gst,shop_id FROM products where qty = 1');
    }
     public function productionDetail($id){
        $user_id=$this->input->get('id');
        $this->db->join('productions pr', 'product.id = pr.product_id');
        $this->db->join( 'wastages wt', 'product.id= wt.product_id' );
        $this->db->where('product.shop_id', $id); 
        $query=$this->db->get('product');
          return $query->result();
    }     

    //get shop by shop id 
    public function getProductBySingleShop($shopId)
    {
       //$shopId=$this->input->POST('shop');
        $this->db->where('shop_id',$shopId); 
        $this->db->order_by('name','asc'); 
        $query=$this->db->get('product');
        return $query->result();
   
    }
           
}