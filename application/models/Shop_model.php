<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Shop_model extends MY_Model
{
    //Inserting Shop 
    public function insertShop($data){
        return $this->insert($data);
    }
    //Get All Type Of Shop
    public function get_all(){
    	$query=$this->db->get('shops');
        return $query->result();
    }
    //Get shop by id
    public function editShop(){
    return $this->get($this->input->get('id'));
    }
     public function singleShop($id){
    return $this->get($id);
    }
    //Update shop
    public function updateShop($data,$id){
        return $this->db->update('shops', $data,array('id' => $id));
                  
    }
    //search by date comparison
    public function getSearchByDate(){
        $date1=$this->input->POST('date1');
        $date2=$this->input->POST('date2');
        $this->db->where("date_ordered BETWEEN '$date1' AND '$date2'");
        $this->db->where('employee_id', $this->input->POST('user'));
        return $this->db->get('orders')->result();
    }
    //Get All Type Of Shop
    public function getById(){
        //if superadmin is exist
        if($this->session->userdata('user_roll')==5)
           {
            return $this->db->get('shops')->result();
           }else{
                $shopsData=array();
                $query=$this->db->get_where('users',array('id'=>$this->session->userdata('user_id')))->result();
                $shop_id=(explode(",",$query[0]->shop_ids));
                foreach ($shop_id as $key => $value) {
                    $shopsData[$key]=$this->db->get_where('shops',array('id'=>$value))->result();
                    }
                return $shopsData;
           } 
    }
     //Get All Type Of Shop by id
    public function getUserById($id){
        $shopsData=array();
        $query=$this->db->get_where('users',array('id'=>$id))->result();
        $shop_id=(explode(",",$query[0]->shop_ids));
        foreach ($shop_id as $key => $value) {
        $shopsData[$key]=$this->db->get_where('shops',array('id'=>$value))->result();        }
        //echo '<pre>';print_r($shopsData);die;
        return $shopsData;
    }
    // get user shop pages
    public function empShopPage($shopid,$userid){
         $this->db->where('shopId',$shopid);
        $this->db->where('empId', $userid);
        $query=$this->db->get('shopemppage');
        return $query->result();

    }
     //check if date range is already exist then not insert otherwise insert
    public function dateRange($data){
        $query=$this->db->get_where('salesreport',$data)->result();
        if(!$query){
            $this->db->insert('salesreport',$data);
            return true;
        }else{
            return false;
        }
    }
 
}