<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Customer_model extends MY_Model
{

	public function newCustomer($data)
    {
       $value=$this->db->insert('customers', $data); 
    }
    public function getCustomer()
    {
        $query=$this->db->get('customers');
        return $query->result();
    }
    public function getSingleCustomer($id)
    {
        $query=$this->get_by(array('id' => $id));
        
        return $query;
        
    }
    public function getCustomerListById()
    {
        $CustomerId=$this->input->post('customerId');

        $this->db->select('id,shop_ids');
        $query = $this->db->get('customers')->result();
        foreach($query as $key=>$val)
        {
            $array=explode(",",$val->shop_ids);
            if(in_array($CustomerId,$array))
            {
                $customer_id[]=$val->id;
            }
        }   
        return $customer_id; 
    }
    public function getCustomerDetail()
    {
    $query=$this->db->get_where('customers',array('add_user_ids' => $this->session->userdata('user_id')))->result();
        
        return $query;
    }
    public function deleteCustomer()
    {
    return $this->db->delete('customers',array('id' => $this->input->POST('remove')));
    }
    //update customer functionality
    public function updateCustomer($data,$id)
    {
        $this->db->update('customers', $data,array('id' => $id));
    }


}