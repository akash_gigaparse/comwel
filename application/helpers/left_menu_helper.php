<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
function page(){

	$CI = get_instance();
   	$CI->load->library('session');
   	$CI->load->model('user_role_model');
    $CI->load->model('User_model');
    $CI->load->model('shop_model');
   	//$data = $CI->user_role_model->get_role($CI->session->userdata('user_id'));
   	$CI->load->model('Page_model');

    
   	if(isset($_SESSION['shop_id'])){
        $auth['shopEmpPage'] = $CI->shop_model->empShopPage($_SESSION['shop_id'],$CI->session->userdata('user_roll'));
   	}
   	else{
		$auth['shopEmpPage'] = $CI->shop_model->empShopPage(1,$CI->session->userdata('user_roll'));
   	}
	$auth['pages']=$CI->Page_model->fetch_pages();
	$CI->load->view('left_menu',$auth);
}
function empInfo(){
    $CI = get_instance();
    $CI->load->library('session');
    $CI->load->model('user_role_model');
    $CI->load->model('User_model');
    $CI->load->model('shop_model');
    $data['shops']=$CI->shop_model->get_all();
    $data['user']=$CI->session->userdata();
    $data['empshop']=$CI->User_model->currentUserInfo();
    return $data;
}
?>