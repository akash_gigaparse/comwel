/**
 * Site : http:www.smarttutorials.net
 * @author muni
 */
	      
//adds extra table rows
$("#addmore").on('click',function(){
var i=$('table tr').length;

	html = '<tr>';
	html += '<td><input class="case" type="checkbox"/></td>';
	html += '<td><input type="text" data-type="productName" name="itemName[]" id="itemName_'+i+'" class="form-control autocomplete_txt" autocomplete="off"></td>';
	
	html += '<td><input type="text" name="price[]" id="price_'+i+'"  class="form-control changesNo" autocomplete="off" onkeypress="return IsNumeric(event);" ondrop="return false;" onpaste="return false;"></td>';
	html += '<td><input type="text" name="quantity[]" id="quantity_'+i+'" class="form-control changesNo" autocomplete="off" onkeypress="return IsNumeric(event);" ondrop="return false;" onpaste="return false;"></td>';
	html += '<td><input type="text" name="total[]" id="total_'+i+'" class="form-control totalLinePrice" autocomplete="off" readonly="readonly" onkeypress="return IsNumeric(event);" ondrop="return false;" onpaste="return false;"></td>';
	html += '</tr>';
	$('table').append(html);
	i++;
});
$(".addmore").on('click',function(){
	var i=$('table tr').length;

	html = '<tr>';
	html += '<td><input class="case" type="checkbox"/></td>';
	html += '<input type="hidden"  name="pid[]" id="pid_'+i+'" value=""  autocomplete="off">';
	html += '<td><input type="text" data-type="productName" name="itemName[]" id="itemName_'+i+'" class="form-control autocomplete_txt" autocomplete="off"></td>';
	html += '<td><input type="text" name="price[]" id="price_'+i+'" readonly="readonly" class="form-control changesNo1" autocomplete="off" onkeypress="return IsNumeric(event);" ondrop="return false;" onpaste="return false;"></td>';
	html += '<td><input type="text" name="quantity[]" id="quantity_'+i+'" class="form-control changesNo" autocomplete="off" onkeypress="return IsNumeric(event);" ondrop="return false;" onpaste="return false;"></td>';
	html += '<td><input type="text" name="total[]" id="total_'+i+'" class="form-control totalLinePrice" readonly="readonly" autocomplete="off" onkeypress="return IsNumeric(event);" ondrop="return false;" onpaste="return false;"></td>';
	html += '</tr>';
	$('table').append(html);
	i++;
	var cCheck=$('#customcheck').val();
	if(cCheck==1)
	$('.changesNo1').removeAttr("readonly");

});
$(".addmore2").on('click',function(){
	var i=$('#productTable  tr').length;
	//alert(i);
	html = '<tr>';
	html += '<td><input class="case" type="checkbox"/></td>';
	html += '<input type="hidden"  name="pid[]" id="pid_'+i+'" value=""  autocomplete="off">';
	html += '<td><input type="text" data-type="productName" name="itemName[]" id="itemName_'+i+'" class="form-control autocomplete_txt2" autocomplete="off"></td>';
	html += '<td><input type="text" name="qty[]" id="qty_'+i+'"  class="form-control changesNo" autocomplete="off"></td>';
	html += '<td><input type="text" name="notes[]" id="notes_'+i+'"  class="form-control changesNo" autocomplete="off"></td>';
	html += '</tr>';
	$('table').append(html);
	i++;
});
$(".addmore3").on('click',function(){
	var i=$('table tr').length;

	html = '<tr>';
	html += '<td><input class="case" type="checkbox"/></td>';
	html += '<input type="hidden"  name="pid[]" id="pid_'+i+'" value=""  autocomplete="off">';
	html += '<td><input type="text" data-type="productName" name="itemName[]" id="itemName_'+i+'" class="form-control autocomplete_txt2" autocomplete="off"></td>';
	html += '<td><input type="text" name="qty[]" id="qty_'+i+'"  class="form-control changesNo" autocomplete="off"></td>';
	html += '</tr>';
	$('table').append(html);
	i++;
});

//to check all checkboxes
$(document).on('change','#check_all',function(){
	$('input[class=case]:checkbox').prop("checked", $(this).is(':checked'));
});

//deletes the selected table rows
$(".delete").on('click', function() {
	$('.case:checkbox:checked').parents("tr").remove();
	$('#check_all').prop("checked", false); 
	calculateTotal();
	
});
$(document).on('focus','.autocomplete_txt3',function(){
	type = $(this).data('type');
	  
	//alert(customer);
	if(type =='productCode' )autoTypeNo=0;
	if(type =='productName' )autoTypeNo=1; 	
		var shop_id=$('#shop').val(); 
	$(this).autocomplete({
		source: function( request, response ) {
			$.ajax({
				url :ajaxVal,
				dataType: "json",
				method: 'post',
				data: {
					
					action:'wastage',
					shop_id:shop_id,
				    name_startsWith: request.term,
				   type: type
				},
				 success: function( data ) {
					 response( $.map( data, function( item ) {
					 	var code = item.split("|");
						
						return {
							label: code[autoTypeNo],
							value: code[autoTypeNo],
							data : item
						}
					}));
				}
			});
		},
		autoFocus: true,	      	
		minLength: 0,
		select: function( event, ui ) {
			var names = ui.item.data.split("|");						
			id_arr = $(this).attr('id');
	  		id = id_arr.split("_");
			$('#pid_'+id[1]).val(names[0]);
			$('#itemName_'+id[1]).val(names[1]);

		}		
			      	
	});
});
$(document).on('focus','.autocomplete_txt2',function(){
	type = $(this).data('type');
	  
	//alert(customer);
	if(type =='productCode' )autoTypeNo=0;
	if(type =='productName' )autoTypeNo=1; 	
		var shop_id=$('#shop').val(); 
	$(this).autocomplete({
		source: function( request, response ) {
			$.ajax({
				url :ajaxVal,
				dataType: "json",
				method: 'post',
				data: {
					
					action:'wastage',
					shop_id:shop_id,
				    name_startsWith: request.term,
				   type: type
				},
				 success: function( data ) {
					 response( $.map( data, function( item ) {
					 	var code = item.split("|");
						
						return {
							label: code[autoTypeNo],
							value: code[autoTypeNo],
							data : item
						}
					}));
				}
			});
		},
		autoFocus: true,	      	
		minLength: 0,
		select: function( event, ui ) {
			var names = ui.item.data.split("|");						
			id_arr = $(this).attr('id');
	  		id = id_arr.split("_");
	  		$('#productTable').find('#pid_'+id[1]).val(names[0]);
	  		//$('#pid_'+id[1]).val(names[0]);
			$('#itemName_'+id[1]).val(names[1]);
			//$('#notes_'+id[1]).val(names[2]);
		}		
			      	
	});
});

//autocomplete script
$(document).on('focus','.autocomplete_txt',function(){

	type = $(this).data('type');
	var customer=$('#customer').val(); 
	var shop_id=$('#shop').val();  	
	//alert(customer);
	if(type =='productCode' )autoTypeNo=0;
	if(type =='productName' )autoTypeNo=1; 	
		
	$(this).autocomplete({
		source: function( request, response ) {
			$.ajax({
				url :ajaxVal,
				dataType: "json",
				method: 'post',
				data: {
					action:'invoice',
					getCustomerId:customer,
					shop_id:shop_id,
				   name_startsWith: request.term,
				   type: type
				},
				 success: function( data ) {
					 response( $.map( data, function( item ) {
					 	var code = item.split("|");
						return {
							label: code[autoTypeNo],
							value: code[autoTypeNo],
							data : item
						}
					}));
				}
			});
		},
		autoFocus: true,	      	
		minLength: 0,
		select: function( event, ui ) {
			var names = ui.item.data.split("|");						
			id_arr = $(this).attr('id');
	  		id = id_arr.split("_");

			$('#pid_'+id[1]).val(names[0]);
			$('#itemNo_'+id[1]).val(names[0]);
			$('#itemName_'+id[1]).val(names[1]);
			$('#quantity_'+id[1]).val(1);
			$('#price_'+id[1]).val(names[2]);
			$('#total_'+id[1]).val( 1*names[2] );
			calculateTotal();
		}		      	
	});
});

//price change
$(document).on('change keyup blur','.changesNo',function(){
	id_arr = $(this).attr('id');
	id = id_arr.split("_");
	quantity = $('#quantity_'+id[1]).val();
	price = $('#price_'+id[1]).val();
	if( quantity!='' && price !='' ) $('#total_'+id[1]).val( (parseFloat(price)*parseFloat(quantity)).toFixed(2) );	
	calculateTotal();
});

$(document).on('change keyup blur','.changesNo1',function(){
	id_arr = $(this).attr('id');
	id = id_arr.split("_");
	quantity = $('#quantity_'+id[1]).val();
	price = $('#price_'+id[1]).val();
	if( quantity!='' && price !='' ) $('#total_'+id[1]).val( (parseFloat(price)*parseFloat(quantity)).toFixed(2) );	
	calculateTotal();
});

$(document).on('change keyup blur','#tax',function(){
	calculateTotal();
});

//total price calculation 
function calculateTotal(){
	subTotal = 0 ; total = 0; 
	$('.totalLinePrice').each(function(){
		if($(this).val() != '' )subTotal += parseFloat( $(this).val() );
	
	});
	$('#subTotal').val( subTotal.toFixed(2) );
	tax = $('#tax').val();
	if(tax != '' && typeof(tax) != "undefined" ){
		taxAmount = subTotal * ( parseFloat(tax) /100 );
		$('#taxAmount').val(taxAmount.toFixed(2));
		total = subTotal + taxAmount;
	}else{
		$('#taxAmount').val(0);
		total = subTotal;
		
	}
	$('#totalAftertax').val( total.toFixed(2) );
	calculateAmountDue();
}

$(document).on('change keyup blur','#amountPaid',function(){
	calculateAmountDue();
});

//due amount calculation
function calculateAmountDue(){
	amountPaid = $('#amountPaid').val();
	total = $('#totalAftertax').val();
	if(amountPaid != '' && typeof(amountPaid) != "undefined" ){
		amountDue = parseFloat(total) - parseFloat( amountPaid );
		$('.amountDue').val( amountDue.toFixed(2) );
	}else{
		total = parseFloat(total).toFixed(2);
		$('.amountDue').val( total );
	}
}


//It restrict the non-numbers
var specialKeys = new Array();
specialKeys.push(8,46); //Backspace
function IsNumeric(e) {
    var keyCode = e.which ? e.which : e.keyCode;
    console.log( keyCode );
    var ret = ((keyCode >= 48 && keyCode <= 57) || specialKeys.indexOf(keyCode) != -1);
    return ret;
}

//datepicker
$(function () {
    $('#invoiceDate').datepicker({});
});