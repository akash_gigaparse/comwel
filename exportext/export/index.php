<?php
$file = fopen("Z048T01_150218154929.txt","r");
$data = array();
$writeFlag = false;
$j = 0;
$tmpRowData = "";
$writeRowCount = 0;
$copyReceiptcount = false;
$productCount = 0;
while(! feof($file)) {
	$line = fgetcsv($file);
	if(isset($line[0]) && !empty($line[0])){ 
		if((strpos($line[0],"COPY")!==false) &&  (strpos($line[0],"RECEIPT")!==false)){
			$copyReceiptcount = true;
			$writeFlag = false;
		}
		if($writeFlag){ 			
			if(((strpos($line[0],"TOTAL")===false) && (strpos($line[0],"PAYABLE")===false)) && (strpos($line[0],"CASH")===false) && (strpos($line[0],"CHANGE")===false) && (strpos($line[0],"GST")===false) && (strpos($line[0],"INCLUDED")===false) && (strpos($line[0],"CHANGE")===false) ){
				$tmpRow = explode("  ",preg_replace('/\s{3,}/','  ',$line[0]));				
				$rowCount = 0; 
				foreach ($tmpRow as $key => $value) { 
					if(isset($value) && !empty($value)){
						if(preg_match('/^[0-9]*$/', $value)){
							$data[$j]['Products'][$productCount]['Qty'] = $value;
						}
						elseif($rowCount==1){
							$data[$j]['Products'][$productCount]['Item'] = $value;
						}
						elseif(strpos($value,"$")!==false){
							$data[$j]['Products'][$productCount]['Price'] = $value;
						}
						elseif(strpos($value,"*")!==false){
							$data[$j]['Products'][$productCount]['Gst'] = 1;
						}
						$rowCount++;
					}
				}
				$productCount++;								
			}
			elseif((strpos($line[0],"TOTAL")!==false) &&  (strpos($line[0],"PAYABLE")!==false)){
				$tmpRow = explode("  ",preg_replace('/\s{3,}/','  ',$line[0]));	
				foreach ($tmpRow as $key => $value) {
					if(isset($value) && !empty($value) && (strpos($value,"$")!==false)){
						$data[$j]['Total'] = preg_replace('/\s+/', '',$value);
					}
				}				
			}
			elseif((strpos($line[0],"CASH")!==false)){
				$tmpRow = explode("  ",preg_replace('/\s{3,}/','  ',$line[0]));	
				foreach ($tmpRow as $key => $value) {
					if(isset($value) && !empty($value) && (strpos($value,"$")!==false)){
						$data[$j]['Cash'] = preg_replace('/\s+/', '',$value);
					}
				}
			}
			elseif((strpos($line[0],"CHANGE")!==false)){
				$tmpRow = explode("  ",preg_replace('/\s{3,}/','  ',$line[0]));	
				foreach ($tmpRow as $key => $value) {
					if(isset($value) && !empty($value) && (strpos($value,"$")!==false)){
						$data[$j]['Change'] = preg_replace('/\s+/', '',$value);
					}
				}
			}
			elseif((strpos($line[0],"EFTPOS")!==false)){
				$tmpRow = explode("  ",preg_replace('/\s{3,}/','  ',$line[0]));	
				foreach ($tmpRow as $key => $value) {
					if(isset($value) && !empty($value) && (strpos($value,"$")!==false)){
						$data[$j]['Eftpos'] = preg_replace('/\s+/', '',$value);
					}
				}
			}
			elseif((strpos($line[0],"GST")!==false) &&  (strpos($line[0],"INCLUDED")!==false)){
				$tmpRow = explode("  ",preg_replace('/\s{3,}/','  ',$line[0]));	
				foreach ($tmpRow as $key => $value) {
					if(isset($value) && !empty($value) && (strpos($value,"$")!==false)){
						$data[$j]['Tax'] = preg_replace('/\s+/', '',$value);
					}
				}				
			}
			$writeRowCount++;			
		}		
		if(strpos($line[0],"REG")!==false && (!$copyReceiptcount)){ 
			$j++;			
			$writeFlag = true;
			$tmpRow = explode(" ",preg_replace('/\s+/', ' ',$line[0]));
			$data[$j]['TillID'] = $tmpRowData;
			$writeRowCount = 0;
			$productCount = 0;
			foreach ($tmpRow as $key => $value) {
				if(isset($value) && (!empty($value)) && $value!="REG"){
					if(validateDate($value,"d-m-Y")){
						$data[$j]['Date'] = $value;
					}
					elseif(validateDate($value,"H:i")){
						$data[$j]['Time'] = $value;
					}
					elseif(preg_match('/^[0-9]*$/', $value)){
						$data[$j]['TransactionID'] = $value;
					}
					else {
						$data[$j]['SalesPersonName'] = $value;
					}
				}
			}
		}
		elseif(strpos($line[0],"REG")!==false && $copyReceiptcount){ 
			$copyReceiptcount = false;
		}		
	} 	
	$tmpRowData = preg_replace('/\s+/', ' ',$line[0]);  	
  }

	//Validate Date and Time
	function validateDate($date, $format = 'Y-m-d H:i:s') {
	    $d = DateTime::createFromFormat($format, $date);
	    return $d && $d->format($format) == $date;
	}

//print_r($data);
fclose($file);
require_once('connection.php');
if(isset($data) && !empty($data)){
	foreach ($data as $key => $value) {
		$value['TillID'] 			= isset($value['TillID'])?$value['TillID']:'';
		$value['SalesPersonName'] 	= isset($value['SalesPersonName'])?$value['SalesPersonName']:'';
		$value['Date'] 				= isset($value['Date'])?$value['Date']:'';
		$value['Time'] 				= isset($value['Time'])?$value['Time']:'';
		$value['TransactionID'] 	= isset($value['TransactionID'])?$value['TransactionID']:'';
		$value['Total'] 			= isset($value['Total'])?$value['Total']:'';
		$value['Cash'] 				= isset($value['Cash'])?$value['Cash']:'';
		$value['Change']			= isset($value['Change'])?$value['Change']:'';
		$sql = "insert into transaction set 
			tillid = '".$value['TillID']."',
			sales_person_name 	= '".$value['SalesPersonName']."',
			transaction_date 	= '".$value['Date']."',
			time 				= '".$value['Time']."',
			transactionid 		= '".$value['TransactionID']."',
			total 				= '".$value['Total']."',
			cash 				= '".$value['Cash']."',
			change_price 		= '".$value['Change']."'
		";
		//mysqli_query($conn,$sql) or die(mysqli_error($conn));
		if(isset($value['Products']) && !empty($value['Products'])){
			foreach ($value['Products'] as $key => $value1) {
				$value1['Qty']   = isset($value1['Qty'])?$value1['Qty']:'';
				$value1['Item']  = isset($value1['Item'])?$value1['Item']:'';
				$value1['Price'] = isset($value1['Price'])?$value1['Price']:'';
				$value1['Gst']   = isset($value1['Gst'])?$value1['Gst']:'';

				$sql1 = "insert into products set 
					qty 		= '".$value1['Qty']."',
					item 		= '".$value1['Item']."',
					price 		= '".$value1['Price']."',
					gst 		= '".$value1['Gst']."',
					transactionid = '".$value['TransactionID']."'
				";
				mysqli_query($conn,$sql1) or die(mysqli_error($conn));
			}
		}
		//echo $sql;die;
	}
}

?>