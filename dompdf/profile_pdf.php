<?php
    require_once("dompdf/dompdf_config.inc.php");
    ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta name="viewport" content="width=device-width; initial-scale=1.0; maximum-scale=1.0; user-scalable=0;" />
        <title>::WASTE MATERIAL PROFILE::</title>
    </head>
    <body style='border-style: solid; border-width: 1px;'>
        <div style="margin-top: 5px;">
        <center><h4>WASTE MATERIAL PROFILE</h4></center>
        <div style='border-style: solid; border-width: 1px;'>
                <table style='font-size:12px;' width='100%' >
                    <tr>
                        <td width='10%'>Profile :</td>
                        <td width='20%'><?php echo $summrydata['profile_no'];?></td>
                        <td width='15%'>Name Of Material :</td>
                        <td width=''><?php echo $var?></td>
                   </tr>
                </table>
            </div>
            <!--START GENERATOR INFORMATION -->
            <div style='border-style: solid; border-width: 1px;'>
                <font size='2'><b> A.GENERATOR INFORMATION</b></font>
                <table style='font-size:12px;' width='100%'border='0'>
                    <tr>
                        <td width='15%' align='center'>Customer : </td>
                        <td width='25%'><?php echo $summrydata['account_no']?></td>
                        <td width='30%' align='center'>Customer Name : </td>
                        <td width='25%' ><?php echo $summrydata['firstname'].$summrydata['lastname']?></td>
                        <td width='10%' align='center'>EPI ID : </td>
                        <td width='25%'><?php echo $summrydata['epa_id']?></td>
                    </tr>
                    <tr>
                        <td width='10%' align='center'>City/State/Zip</td>
                        <td width='40%'><?php echo $summrydata['city']."&nbsp;/&nbsp;".$summrydata['state']."&nbsp;/&nbsp;".$summrydata['zipcode'];?></td>
                    </tr>
                    <tr>
                        <td width='10%' align='center'>Contact</td>
                        <td width='40%'><?php echo $summrydata['contact']?></td>
                    </tr>
                </table>
            </div>
            <!--END GENERATOR INFORMATION -->
            <!--START WEST INFORMATION Page -->
            <div style='border-style: solid; border-width: 1px;'>
                <font size='2'><b> B .WEST INFORMATION</b></font>
                <br>
                <?php //array create for  genral wast page
                $col=array('waste_name','process_generating','profile_no','source','sample_available');
                foreach($generator_wast as $key){
                    if(in_array($key['mate_label'],$col)){
                    if(empty($key['mate_value']))
                    break;
                    $val=explode("_",$key['mate_label']);
                    $res=ucfirst(implode(" ",$val));
                    ?>
                    <table style='font-size:12px;' width='100%'border='0'>
                        <tr>
                            <td width='4%' align='right'><?php echo $res ?> :</td>
                            <td width='15%'><?php echo $key['mate_value'] ?> </td>
                        </tr>
                    </table>
                    <?php 
                    }
                }
                ?>
            </div>
            <!--END WEST INFORMATION Page -->
            <?php
            // CHECK PROFILE PAGE Avalable OR NOT
            if(in_array('state_of_texas',$checkValue)){
            ?>
            <!--START Is the site located within the state of Texas-->        
            <div style='border-style: solid; border-width: 1px;'>
                <font size='2'><b> C . Is the site located within the state of Texas ?</b></font>
                <br>
                <?php //array create for  state of Texas
                $col=array('state_of_texas');
                foreach($generator_wast as $key){
                    if(in_array($key['mate_label'],$col)){
                    if(empty($key['mate_value']))
                    break;
                    $val=explode("_",$key['mate_label']);
                    $res=ucfirst(implode(" ",$val));
                    ?>
                    <table style='font-size:12px;' width='100%'border='0'>
                        <tr>
                            <td width='4%' align='right'><?php echo $res ?> :</td>
                            <td width='15%'><?php echo $key['mate_value'] ?> </td>
                        </tr>
                    </table>
                    <?php 
                    }
                } ?>
            </div>
            <!--END Is the site located within the state of Texas-->
                <?php
                // CHECK PROFILE PAGE Avalable OR NOT
                }if(in_array('waste_stream_name',$checkValue)){
                ?>
            <!--GENERAL WASTE INFORMATION Page DIV-->
            <div style='border-style: solid; border-width: 1px;'>
                <font size='2'><b> D . GENERAL WASTE INFORMATION </b></font>
                <br>
                <font size='1'>    
                <?php 
                $col=array('waste_stream_name','process_generating_waste','waste_determination','unused','disposal_restriction','rcra_regulations','specific_hazards');
                $waste_determination=0;$disposal_restriction=0;$specific_hazards=0; $j=0;
                foreach($generator_wast as $key){
                    if(in_array($key['mate_label'],$col))
                    {
                        if(empty($key['mate_value']))
                        break;
                        $val=explode("_",$key['mate_label']);
                        $res=ucfirst(implode(" ",$val));
                        if(('waste_determination'==$key['mate_label']))
                        {
                            if($waste_determination==0 && $j==0)
                            {
                                echo "<br>";
                                echo $res .": &nbsp;&nbsp;&nbsp;&nbsp;";
                                echo $key['mate_value'];
                                $waste_determination++;
                            }else{
                                echo ",&nbsp;".$key['mate_value'];
                            }
                        }elseif(('disposal_restriction'==$key['mate_label']))
                        {
                            if($disposal_restriction==0 && $j==0)
                            {
                                echo "<br>";
                                echo $res .": &nbsp;&nbsp;&nbsp;&nbsp;";
                                echo $key['mate_value'];
                                $disposal_restriction++;
                            }else{
                                echo ",&nbsp;".$key['mate_value'];
                            }
                        }elseif(('specific_hazards'==$key['mate_label']))
                        {
                            if($specific_hazards==0 && $j==0)
                            {
                                echo "<br>";
                                echo $res .": &nbsp;&nbsp;&nbsp;&nbsp;";
                                echo $key['mate_value'];
                                $specific_hazards++;
                            }else{
                                echo ",&nbsp;".$key['mate_value'];
                            }
                        }elseif(('rcra_regulations'==$key['mate_label']) OR ('unused'==$key['mate_label'])){
                            echo '<br>';
                            echo $res;
                            echo " : &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;".$key['mate_value'];
                        }
                        else{
                            echo $res;
                            echo " : &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;".$key['mate_value']."<br>";
                        }
    
                    }
                } ?>
                </font>
            </div>
            <!--END GENERAL WASTE INFORMATION-->
            <?php
            // CHECK PROFILE PAGE Avalable OR NOT
            }if(in_array('component',$checkValue)){
            ?>
            <!--GENERAL WASTE COMPONENT Page DIV-->
            <div style='border-style: solid; border-width: 1px;'>
                 <font size='2'><b> E . GENERAL WASTE COMPONENT </b></font>
                <br>
                <font size='1'>    
                <?php //array create for  GENERAL WASTE COMPONENT
                $col=array('component','cas','minimum','min_unit','maximum','max_unit','metals');
                    $component=0;$cas=0;$minimum=0;$min_unit=0;$maximum=0;$max_unit=0;$metals=0; $j=0;
                foreach($generator_wast as $key){
                    if(in_array($key['mate_label'],$col))
                    {
                        if(empty($key['mate_value']))
                        break;
                        $val=explode("_",$key['mate_label']);
                        $res=ucfirst(implode(" ",$val));
                        if(('metals'==$key['mate_label']))
                        {
                            if($metals==0 && $j==0)
                            {
                                echo "<br>";
                                echo $res .": &nbsp;&nbsp;&nbsp;&nbsp;";
                                echo $key['mate_value'];
                                $metals++;
                            }else{
                                echo ",&nbsp;".$key['mate_value'];
                            }
                        }else if(('component'==$key['mate_label']))
                        {
                            if($component==0 && $j==0)
                            {
                                echo $res .": &nbsp;&nbsp;&nbsp;&nbsp;";
                                echo $key['mate_value'];
                                $component++;
                            }else{
                                echo ",&nbsp;".$key['mate_value'];
                            }
                        }else if(('cas'==$key['mate_label']))
                        {
                            if($cas==0 && $j==0)
                            {
                                echo "<br>";
                                echo $res .": &nbsp;&nbsp;&nbsp;&nbsp;";
                                echo $key['mate_value'];
                                $cas++;
                            }else{
                                echo ",&nbsp;".$key['mate_value'];
                            }
                        }else if(('minimum'==$key['mate_label']))
                        {
                            if($minimum==0 && $j==0)
                            {
                                echo "<br>";
                                echo $res .": &nbsp;&nbsp;&nbsp;&nbsp;";
                                echo $key['mate_value'];
                                $minimum++;
                            }else{
                                echo ",&nbsp;".$key['mate_value'];
                            }
                        }else if(('min_unit'==$key['mate_label']))
                        {
                            if($min_unit==0 && $j==0)
                            {
                                echo "<br>";
                                echo $res .": &nbsp;&nbsp;&nbsp;&nbsp;";
                                echo $key['mate_value'];
                                $min_unit++;
                            }else{
                                echo ",&nbsp;".$key['mate_value'];
                            }
                        }else if(('maximum'==$key['mate_label']))
                        {
                            if($maximum==0 && $j==0)
                            {
                                echo "<br>";
                                echo $res .": &nbsp;&nbsp;&nbsp;&nbsp;";
                                echo $key['mate_value'];
                                $maximum++;
                            }else{
                                echo ",&nbsp;".$key['mate_value'];
                            }
                        }else if(('max_unit'==$key['mate_label']))
                        {
                            if($max_unit==0 && $j==0)
                            {
                                echo "<br>";
                                echo $res .": &nbsp;&nbsp;&nbsp;&nbsp;";
                                echo $key['mate_value'];
                                $max_unit++;
                            }else{
                                echo ",&nbsp;".$key['mate_value'];
                            }
                        }
                        else{
                            echo $res;
                            echo $key['mate_value']."<br>";
                        }
                    }
                } ?>
                </font>
            </div>
            <!--END GENERAL WASTE COMPONENT-->
            <?php
            // CHECK PROFILE PAGE Avalable OR NOT
            }if(in_array('color',$checkValue)){
            ?>
            <!--sTART Characterstics Page DIV-->
            <div style='border-style: solid; border-width: 1px;'>
                <font size='2'><b> F . CHARACTERISTICS </b></font>
                <br>
                <font size='1'>    
                <?php 
                $col=array('color','odor','viscosity','layers','specific_gravity','halogens','btus','btus','flashpoint_with_ranges','pH_value_with_ranges');
                $layers=0;$j=0;
                foreach($generator_wast as $key){
                    if(in_array($key['mate_label'],$col))
                    {
                        if(empty($key['mate_value']))
                        break;
                        $val=explode("_",$key['mate_label']);
                        $res=ucfirst(implode(" ",$val));
                        if(('layers'==$key['mate_label']))
                        {
                            if($layers==0 && $j==0)
                            {
                                echo "<br>";
                                echo $res .": &nbsp;&nbsp;&nbsp;&nbsp;";
                                echo $key['mate_value'];
                                $layers++;
                            }else{
                                echo ",&nbsp;".$key['mate_value'];
                            }
                        }
                        else{
                            echo "<br>";
                            echo $res;
                            echo " : &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;".$key['mate_value'];
                        }
    
                    }
                } ?>
                </font>
            </div>
            <!--sTART SHIPPING VOLUME & FREQUENCY -->
            <?php
            // CHECK PROFILE PAGE Avalable OR NOT
            }if(in_array('bulk_liquid',$checkValue)){
            ?>
            <div style='border-style: solid; border-width: 1px;'>
                <font size='2'><b> G . SHIPPING VOLUME & FREQUENCY </b></font>
                <br>
                <?php //array create for  genral wast page
                $col=array('bulk_liquid','bulk_solids','drum_size','drum_type','totes','totes_types','return_of_tote','skids','frequency','quantity_to_ship');
                foreach($generator_wast as $key){
                    if(in_array($key['mate_label'],$col)){
                    if(empty($key['mate_value']))
                    break;
                    $val=explode("_",$key['mate_label']);
                    $res=ucfirst(implode(" ",$val));
                    ?>
                    <table style='font-size:12px;' width='100%'border='0'>
                        <tr>
                            <td width='4%' align='right'><?php echo $res ?> :</td>
                            <td width='15%'><?php echo $key['mate_value'] ?> </td>
                        </tr>
                    </table>
                    <?php 
                    }
                } ?>
            </div>
            <!--End SHIPPING VOLUME & FREQUENCY -->
            <?php
            // CHECK PROFILE PAGE Avalable OR NOT
            }if(in_array('USEPA_hazardous',$checkValue)){
            ?>
            <!--sTART SHIPPING VOLUME & FREQUENCY -->
            <div style='border-style: solid; border-width: 1px;'>
                <font size='2'><b> H . RCRA CHARACTERIZATION </b></font>
                <br>
                <?php 
                $col=array('USEPA_hazardous','universal','EPA_waste_codes','sorce_code','form_code','management_method','texas_state_waste_code','texas_state_waste_code_text');
                foreach($generator_wast as $key){
                    if(in_array($key['mate_label'],$col)){
                    if(empty($key['mate_value']))
                    break;
                    $val=explode("_",$key['mate_label']);
                    $res=ucfirst(implode(" ",$val));
                    ?>
                    <table style='font-size:12px;' width='100%'border='0'>
                        <tr>
                            <td width='4%' align='right'><?php echo $res ?> :</td>
                            <td width='15%'><?php echo $key['mate_value'] ?> </td>
                        </tr>
                    </table>
                    <?php 
                    }
                } ?>
            </div>
            <!--End RCRA CHARACTERIZATION  -->
             <?php
            // CHECK PROFILE PAGE Avalable OR NOT
            }
            if(in_array('USDOT_hazardous_material',$checkValue)){
            ?>
            <!--sTART DOT SHIPPING INFORMATION -->
            <div style='border-style: solid; border-width: 1px;'>
                <font size='1'><b> I . DOT SHIPPING INFORMATION </b></font>
                <br>
                <?php //array create for  genral wast page
                $col=array('USDOT_hazardous_material','shipping_name','technical_descriptors','hazard_class','form_code','un/na_number','parking_grup','erg','rq','inhalation_hazard','zone');
                foreach($generator_wast as $key){
                    if(in_array($key['mate_label'],$col)){
                    if(empty($key['mate_value']))
                    break;
                    $val=explode("_",$key['mate_label']);
                    $res=ucfirst(implode(" ",$val));
                    ?>
                    <table style='font-size:12px;' width='100%'border='0'>
                        <tr>
                            <td width='4%' align='right'><?php echo $res ?> :</td>
                            <td width='15%'><?php echo $key['mate_value'] ?> </td>
                        </tr>
                    </table>
                    <?php 
                    }
                } ?>
            </div>
            <!--End DOT SHIPPING INFORMATION -->
            <?php
            }?>
            </div>
            <!--sTART DOT SHIPPING INFORMATION -->
            <div style='border-style: solid; border-width: 1px;'>
                <font size='3'><b> Certification: </b></font>
                <br>
                    <p style='margin-left: 3px'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; I certify under penalty of law that this document, all attachments and supporting materials were prepared and presented by me under my direct supervision and that the information provided is a true,

accurate and complete representation of the waste materials to be presented for evaluation and management by Shamrock Environmental. Furthermore, I agree that any waste materials managed under 

any approval issued by Shamrock Environmental pursuant to this request will be transported, delivered and tendered to be in conformance with the material characterization profile. Where Shamrock 

Environmental determines a material to be non-conforming, the generator shall be responsible for all costs associated with timely return of the material to generator or for other alternate and lawful 

management of the non-conforming material conducted on behalf of the generator.
                    </p>
                <br>
                <table style='font-size:12px;' width='100%'border='0'>
                    <tr>
                        <td width='15%' align='right'>Generator Signature:</td>
                        <td width='25%'> </td>
                        <td width='15%' align='right'>Date:</td>
                        <td width='25%'> </td>
                    </tr>
                    <tr>
                        <td width='25%' align='right'>Printed Name :</td>
                        <td width='25%'> </td>
                        <td width='25%' align='right'>Title:</td>
                        <td width='25%'> </td>
                    </tr>
                </table>    
            </div>
        </div>
    </body>
</html>
<?php 
$homepage = ob_get_contents();
ob_end_clean();

$html ='<html><head><title> This is title </title></head><body>This is test</body></html>';
$dompdf = new DOMPDF();
$dompdf->load_html($homepage);
$dompdf->render();
$dompdf->stream("sample.pdf");
} ?>



